<?php $__env->startSection('title', 'Edit Store' ); ?> 
<link rel="stylesheet" href="<?php echo e(URL::asset('admin/assets/css/custom.css')); ?>">
<style>
   .panel{padding: 20px;}
 .m-t-small .form-control{ border: 0px;
    box-shadow: none;
    width: 80%;}
</style>
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('header'); ?>
<?php echo $__env->make('admin.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldSection(); ?>

<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">

  <script>
ng-init="something='<?php echo 'aaa'; ?>'";
</script>


   <div id="wrapper" >
      <div id="layout-static">
         <!---------- Static Sidebar Starts------->			
         <?php $__env->startSection('sidebar'); ?>
         <?php echo $__env->make('admin.includes.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
         <?php echo $__env->yieldSection(); ?>
         <!---------- Static Sidebar Ends------->
         <div class="static-content-wrapper">
            <section id="main-header">
               <div class="container-fluid">
                  <div class="row">
                     <div class="col-sm-6"   ng-controller="searchController" ng-cloak > 
                        <?php echo $__env->make('admin.includes.realtime-search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                     </div>
                     <div class="col-sm-12">
                        <div class="text-right">
                        </div>
                        <div class="tab-content" >
                           <!--------------------------- Angular App Starts ---------------------------->
                           <textarea id="res" style="display: none;" ></textarea>
                           <div id="loading" class="loading" style="display:none ;">
                              <img src="<?php echo e(URL::asset('admin/assets/images/89.svg')); ?>" class="img-responsive center-block">				 
                              <p >Calling all data...</p>
                           </div>
                           <!--------------------------Angular App Starts ------------------------------------>
                           <div  ng-controller="editStoreController" ng-cloak>
                              <div class="container-fluid" >
                                 <div class="row">
                                    <div class="col-sm-12" >
                                       <h2 class="header">Edit Store</h2>
                                    </div>
                                 </div>
                                 <div class="row" >
                                    <div class="col-sm-12" >
                                       <div class="panel">
                                          <div class="row">
                                             <div class="col-sm-12 col-md-12">
                                                <div class="form-horizontal"  >
                                                   <label class="control-label">Search</label>
                                                   <div class="search-input">
                                                      <input type="text" class="form-control" id="us3-edit-address"  ng-model="store.address" placeholder="Type area, city or street"  />
                                                   </div>
                                                   <br>
                                                   <!--Map by element. Also it can be attribute-->
                                                   <locationpicker options="locationpickerOptions" style="width:98%;padding:20px 5px;" ></locationpicker>
                                                   <div class="clearfix" >&nbsp;</div>
                                                   <div class="clearfix" >&nbsp;</div>
												   <div class="row">
                                                   <div class="m-t-small form-inline col-sm-5 col-lg-4"> 
												   <h5><b>Coordinates : </b></h5>
                                                      <label>Latitude : </label> <input type="text" class="form-control" id="us3-edit-lat"   value="{{store.latitude}}" ><br>
                                                        <label>Longitude : </label> <input type="text" class="form-control"  id="us3-edit-lon"  value="{{store.longitude}}" />
                                                   </div>
												    <div class="col-sm-7 col-lg-8">
												   <md-input-container  class="md-block text-left" >
                                                   <label>Address</label>
                                                   <textarea id="address2" name="address2" rows="3" max-rows="3"  md-no-autogrow >{{store.address}} </textarea>
                                                </md-input-container>
												   </div>
												   </div>
                                                   <div class="clearfix"></div>
                                                </div>
												<br>
                                             </div>
                                               <div class="col-sm-5 col-lg-4">
											  
													 <div class="img-upload" style="background:#f5f5f5;">
												<div class="form-group text-center"   >
												
												  <input type="file" id="file" style="display:none"/>
												   <input type=" " id="file_name" style="display: none" ng-model="store.store_photo"/>
												    	 
												   <div style="text-align: center;position: relative" id="image">
													   <img  style="padding:60px 40px;"  class="img-responsive center-block" id="preview_image1"  ng-hide="store.store_photo" src="<?php echo e(asset('admin/assets/images/img-placeholder.png')); ?>"/>
														   <img    class="img-responsive center-block" id="preview_image" ng-hide="!store.store_photo"  src="<?php echo e(asset('images/stores')); ?>/{{store.store_photo}}" style="padding:0px "/>
													  <i id="loading1" class="fa fa-spinner fa-spin fa-3x fa-fw" style="position: absolute;left: 40%;top: 40%;display: none"></i>
												   </div>
												   </div>
											 </div>
												   <div class="text-right" style="font-size: " >
													  <a href="javascript:changeProfile()" title="edit" style="text-decoration: none;background:#e2e2e2;padding:8px;position:absolute;bottom: 60px; right:50px">
													  <i class="fa fa-edit"></i> 
													  </a>&nbsp;&nbsp;
												   </div>
												   
												<br>
										  </div>
				  
                                             <div class="col-sm-12 col-md-7 col-lg-8"> 
                                                <div class="row">
                                                   <div class="col-sm-12">
                                                      <md-input-container  class="md-block text-left" >
                                                         <label>Title</label>
                                                         <input type="text" id="title" name="title"  ng-model="store.store_title" />
                                                      </md-input-container>
                                                   </div>


                                                               <div class="col-sm-12">
                                                      <md-input-container  class="md-block text-left" >
                                                         <label>Store Contact</label>
                                                         <input type="text" id="store_phone" name="store.store_phone"   />
                                                      </md-input-container>
                                                   </div>




                                                   
												    <div class="col-sm-12">
                                                      <md-input-container  class="md-block text-left" >
                                                         <label>Comission</label>
                                                         <input type="text" id="comission" name="comission"  ng-model="store.commission" />
                                                      </md-input-container>
                                                   </div>


                                                          <div class="col-sm-12">
                                                      <md-input-container  class="md-block text-left" >
                                                         <label>Store Tax</label>
                                                         <input type="text" id="store_tax" name="store_tax"  ng-model="store.store_tax"  />
                                                      </md-input-container>
                                                   </div>



												   
												    <div class="col-sm-12">
												  <label>Featured</label> 
												   <select class="form-control" id="featured"   >
														<option value="0" ng-selected="store.featured == '0'">No</option>
														<option value="1"  ng-selected="store.featured == '1'">Yes</option> 
													  </select>
                                           <br>
												  </div>



                                            <?php $vendor_plugins = @\App\Setting::where('key_title','vendors')->first(['key_value'])->key_value;
                                            if($vendor_plugins == 1)
                                            { ?>
                                                   <div class="col-sm-12">
                                                      <h5>Select Vendor</h5>
                                                      <md-autocomplete ng-disabled="isDisabled" md-no-cache="noCache" md-selected-item="store.vendor_name" md-search-text-change="searchVendorChange(searchVendor)" md-search-text="searchVendor" md-selected-item-change="selectedVendorChange(values, field)" md-items="values in vendorSearch(searchVendor)" md-item-text="values.first_name" md-min-length="0" placeholder="Select Vendor " md-menu-class="autocomplete-custom-template"   >
                                                         <md-item-template>
                                                            <span class="item-title"> 
                                                            <span> {{values.first_name}} </span> 
                                                            </span> 
                                                         </md-item-template>
                                                      </md-autocomplete>
													   <input type="text" id="vendor_id" name="vendor_id" ng-model="vendor" value="{{vendor_id}}" style="display:none;"/>
                                                      <br>
                                                   </div>

                                                 </div><?php  }  ?>





                                                   <div class="col-sm-12">
                                                      <h5>Select Manager </h5>
                                                      <md-autocomplete ng-disabled="isDisabled" md-no-cache="noCache" md-selected-item="store.manager_name" md-search-text-change="searchManagerChange(searchManager)" md-search-text="searchManager" md-selected-item-change="selectedManagerChange(data, field)" md-items="data in managerSearch(searchManager)" md-item-text="data.first_name " md-min-length="0" placeholder="Select Manager " md-menu-class="autocomplete-custom-template"     >
                                                         <md-item-template>
                                                            <span class="item-title"> 
                                                            <span> {{data.first_name}} </span> 
                                                            </span> 
                                                         </md-item-template>
                                                      </md-autocomplete>
													   <input type="text" id="manager_id" name="manager_id" ng-model="manager" value="{{manager_id}}"  style="display:none;"/>
                                                      <br>
                                                      <br>
                                                   </div>
												   
												  
                                                </div>
                                             </div>
                                          </div>
                                       </div>
									   
									   
									    
									   <!------------------STORE META INFORMATION STARTS HERE------------------------------------>
									    <div class="panel"  ng-repeat="meta in storeMeta">
                                          <div class="row"> 
                                             <div class="col-sm-12 col-md-12 category-add">
											 
                                                <div class="row">
                                                   <div class="col-sm-12 col-lg-6" >
												      
														  <h3>{{meta.title}}</h3><br>
														  
                                                      <md-input-container  class="md-block text-left" ng-repeat="data in meta.fields" ng-show="data.type == 'text' || data.type == 'string'">
                                                        <label>{{data.title}}</label>
                                                         <input type="{{data.type}}" id="{{data.identifier}}" name="{{data.identifier}}"  ng-model="data.value" >
                                                      </md-input-container>
													  
													 
													   <md-input-container  class="md-block text-left" ng-repeat="data in meta.fields" ng-show="data.type == 'integer'">
                                                         <label>{{data.title}}</label>
                                                         <input type="{{data.type}}" id="{{data.identifier}}" name="{{data.identifier}}"  ng-model="data.value" >
                                                      </md-input-container>
													  
													  
                                                   </div>
                                                   
                                                </div>
                                             </div>
                                          </div>
                                       </div>
									   <!----------------------STORE META INFORMATION ENDS HERE-------------------------------------------->
									   
									   
                                        <div class="col-lg-12 col-sm-12 submit text-right">
                                                      <button ng-click="updateStoreData()" class="btn md-raised bg-color md-submit md-button md-ink-ripple">Save Changes</button>
                                        </div>
												   
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <!--------------------------Angular App Ends ------------------------------------>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
         </div>
      </div>
   </div>
   <!------>
</div>
<script type="text/javascript" src="<?php echo e(URL::asset('admin/angular-controllers/stores.js')); ?>"></script> 

<!-- JavaScripts --> 
<script src="https://use.fontawesome.com/2c7a93b259.js"></script>
<script>
 
 
    function changeProfile() {
        $('#file').click();
    }
    $('#file').change(function () {
        if ($(this).val() != '') {
            upload(this);

        }
    });
    function upload(img) {
        var form_data = new FormData();
        form_data.append('file', img.files[0]);
        form_data.append('_token', '<?php echo e(csrf_token()); ?>');
        $('#loading1').css('display', 'block');
        $.ajax({
            url: "<?php echo e(url('image-upload-stores')); ?>",
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.fail) {
                    $('#preview_image').attr('src', '<?php echo e(asset('images/stores/noimage.jpg')); ?>');
                    alert(data.errors['file']);
                }
                else {
					 var data = data.filename;
					 console.log(data);
                    $('#file_name').val(data);
                     $('#preview_image1').attr('src', '<?php echo e(asset('images/stores')); ?>/'+data);
                    $('#preview_image').attr('src', '<?php echo e(asset('images/stores')); ?>/'+data);

                }
                $('#loading1').css('display', 'none');
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
                $('#preview_image').attr('src', '<?php echo e(asset('images/stores/noimage.jpg')); ?>');
            }
        });
    }
    function removeFile() {
        if ($('#file_name').val() != '')
            if (confirm('Are you sure want to remove profile picture?')) {
                $('#loading1').css('display', 'block');
                var form_data = new FormData();
                form_data.append('_method', 'DELETE');
                form_data.append('_token', '<?php echo e(csrf_token()); ?>');
                $.ajax({
                    url: "ajax-remove-image-stores/" + $('#file_name').val(),
                    data: form_data,
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        $('#preview_image').attr('src', '<?php echo e(asset('images/noimage.jpg')); ?>');
                        $('#file_name').val('');
                        $('#loading1').css('display', 'none');
                    },
                    error: function (xhr, status, error) {
                        alert(xhr.responseText);
                    }
                });
            }
    }
 
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>