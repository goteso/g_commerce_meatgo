<?php $__env->startSection('title', 'Areas' ); ?> 
<link rel="stylesheet" href="<?php echo e(URL::asset('admin/assets/css/custom.css')); ?>">
 <style>
 .ui-autocomplete {
z-index: 999999 !important;} 
 </style>
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('header'); ?>
<?php echo $__env->make('admin.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldSection(); ?>
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
<div id="wrapper" >
<div id="layout-static">
   <!---------- Static Sidebar Starts------->			
   <?php $__env->startSection('sidebar'); ?>
   <?php echo $__env->make('admin.includes.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
   <?php echo $__env->yieldSection(); ?>
   <!---------- Static Sidebar Ends------->
   <div class="static-content-wrapper"  >
      <section id="main-header">
         <div class="container-fluid">
            <div class="row">
               <div class="col-sm-6"   ng-controller="searchController" ng-cloak > 
                  <?php echo $__env->make('admin.includes.realtime-search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
               </div>
               <div class="col-sm-12">
                  <div class="text-right">
                  </div>
                  <div class="tab-content"  ng-controller="areaController as ctrl" ng-cloak  >
                     <!--------------------------- Angular App Starts ---------------------------->
                     <textarea id="res" style="display:  none  ;" ></textarea>
                     <div id="loading" class="loading" style="display:none ;">
                        <img src="<?php echo e(URL::asset('admin/assets/images/89.svg')); ?>" class="img-responsive center-block">				 
                        <p >Calling all data...</p>
                     </div>
                     <div class="container-fluid" >
                        <div class="row">
                           <div class="col-sm-10" >
                              <h2 class="header">Areas</h2>
                           </div>
                           <div class="col-sm-2">
                              <div class="text-right">
                                 <button type="button" class="btn md-raised bg-color md-add md-button md-ink-ripple text-right"  data-toggle="modal" data-target="#add">Add New</button>  
                              </div>
                           </div>
                        </div>
                        <div class="row" >
                           <div class="col-sm-12" >
                              <div class="" >
                                 <div id="tableToExport" class="products-table table-responsive"  >
                                    <table class="table" class="table table-striped" id="exportthis" >
                                       <thead>
                                          <tr>
                                             <th>ID</th>
                                             <th>ZIPCODE</th>
                                             <th>LOCATION</th>
                                            
                                             <th>CREATED</th>
                                             <th id="action">ACTION</th>
                                          </tr>
                                       </thead>
                                       <tbody>
                                          <tr ng-repeat="values in ctrl.areas.data.data  ">
                                             <td>#{{values.area_id}}</td>
                                            <td>{{values.pincode}}</td>
                                             <td>Lat : {{values.latitude}}<br> Lon : {{values.longitude}}</td>
                                            
                                             <td>{{values.created_at_formatted}}</td>
                                             <td>
                                                <a class="btn btn-xs edit-product" ng-click="ctrl.editArea(values, $index)" ><i class="fa fa-edit"></i></a>
                                                <a class="btn btn-xs delete-product" ng-click="ctrl.deleteArea(values.area_id, $index)"><i class="fa fa-trash"></i></a>               
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </div>
								  <div class="pagination text-right" style="display:block" >
                                           <button class="btn"  ng-show="ctrl.areas.data.first_page_url != null"ng-click="ctrl.pagination(ctrl.areas.data.first_page_url);">First</button> 
                                           <button class="btn"  ng-show="ctrl.areas.data.prev_page_url != null" ng-click="ctrl.pagination(ctrl.areas.data.prev_page_url);">Previous</button> 
                                          <span>{{ctrl.areas.data.current_page}}</span>
                                          <button class="btn"  ng-show="ctrl.areas.data.next_page_url != null" ng-click="ctrl.pagination(ctrl.areas.data.next_page_url);">Next</button> 
                                          <button class="btn"  ng-show="ctrl.areas.data.last_page_url != null" ng-click="ctrl.pagination(ctrl.areas.data.last_page_url);">Last</button> 
                                       </div>
                                 
                              </div>
                           </div>
                        </div>
                     </div>
                     <!--------------------------- Angular App Ends ---------------------------->
                     <!-----------------------------------------------AREA ADD MODAL STARTS HERE-------------------------------------------------------------------->
                     <div id="add" class="modal fade" data-backdrop="false"  style="z-index:20;" role="dialog">
                        <div class="modal-dialog modal-lg address-add"  >
                           <!-- Modal content-->
                           <div class="modal-content "   >
                              <div class="modal-header  "  >
                                 <button type="button" class="close" data-dismiss="modal">&times;</button>
                                 <h4 class="modal-title text-uppercase"  >Add <b>Area</b></h4>
                              </div>
                              <div class="modal-body "  >
                                 <div class="row"   >
                                    <div class="col-sm-12 col-md-6">
                                       <div class="form-horizontal"  >
                                          <h5 class="">Search</h5>
                                          <div class="search-input">
                                             <input type="text" class="form-control" id="us3-address" placeholder="Type area, city or street"  />
                                          </div>
                                          <br>
                                          <!--Map by element. Also it can be attribute-->
                                          <locationpicker options="locationpickerOptions" style="width:98%;padding:20px 5px;" ></locationpicker>
                                          <div class="clearfix" >&nbsp;</div>
                                          <div class="m-t-small  "> 
                                            <h4><b>Coordinates</b></h4>
										   <label> Latitude :</label> <input type="text" class="form-control"   id="us3-lat"  readonly style="width:80%"> <br>
                                            <label> Longitude :</label><input type="text" class="form-control"   id="us3-lon" readonly style="width:80%"/>
                                          </div>
                                          <div class="clearfix"></div>
                                       </div>
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                       <div class="row">
                                          
										   <div class="col-sm-12">
                                             <h5>Select Location </h5>
                                             <md-autocomplete ng-disabled="isDisabled" md-no-cache="noCache" md-selected-item="selectedLocation" md-search-text-change="ctrl.searchLocationChange(searchLocation)" md-search-text="searchLocation" md-selected-item-change="ctrl.selectedLocationChange(values, field)" md-items="values in ctrl.locationSearch(searchLocation)" md-item-text="values.title" md-min-length="0" placeholder="Select Location " md-menu-class="autocomplete-custom-template"   >
                                                <md-item-template>
                                                   <span class="item-title"> 
                                                   <span> {{values.title}} </span> 
                                                   </span> 
                                                </md-item-template>
                                             </md-autocomplete>
											 <input type="text" id="parent_location_id" name="parent_location_id" ng-model="ctrl.parent_location_id" value="{{parent_location_id}}" style="display:none;"/>
                                          </div>
										  
                                          <div class="col-sm-12">
                                             <h5>Parent Area</h5>
                                             <md-autocomplete ng-disabled="isDisabled" md-no-cache="noCache" md-selected-item="selectedParent" md-search-text-change="ctrl.searchParentChange(searchParentArea)" md-search-text="searchParentArea" md-selected-item-change="ctrl.selectedParentChange(data1, field)" md-items="data1 in ctrl.parentSearch(searchParentArea)" md-item-text="data1.title" md-min-length="0" placeholder="Select Parent Area" md-menu-class="autocomplete-custom-template"    >
                                                <md-item-template>
                                                   <span class="item-title"> 
                                                   <span> {{data1.title}} </span> 
                                                   </span> 
                                                </md-item-template>
                                             </md-autocomplete>
											 <input type="text" id="parent_area_id" name="parent_area_id" ng-model="ctrl.parent_area_id" value="{{ctrl.parent_area_id}}"  style="display:none;"/>
                                             <br> 		    
                                          </div>
                                          <div class="col-sm-12" style="display: none">
                                             <md-input-container  class="md-block">
                                                <label>Title</label>
                                                <input type="text"  id="title" name="title"/>
                                             </md-input-container>
                                          </div>
                                          <div class="col-sm-12">
                                             <md-input-container  class="md-block">
                                                <label>ZipCode</label>
                                                <input type="text"  id="pincode" name="pincode"/>
                                             </md-input-container>
                                          </div>
                                          <div class="col-sm-12">
                                             <md-button ng-click="ctrl.storeArea()" class="md-raised bg-color md-submit">Add Area</md-button>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
						   </div>
                           </div>
                           <!-----------------------------------------------AREA ADD MODAL ENDS HERE-------------------------------------------------------------------->
                           <!-----------------------------------------------AREA EDIT MODAL STARTS HERE-------------------------------------------------------------------->
                           <div id="edit" class="modal fade" data-backdrop="false" style="z-index:20" role="dialog">
                              <div class="modal-dialog modal-lg address-add"  >
                                 <!-- Modal content-->
                                 <div class="modal-content "   >
                                    <div class="modal-header  "  >
                                       <button type="button" class="close" data-dismiss="modal">&times;</button>
                                       <h4 class="modal-title text-uppercase"  >Edit <b>Area</b></h4>
                                    </div>
                                    <div class="modal-body "  >
                                       <div class="row"   >
                                          <div class="col-sm-12 col-md-6">
                                       <div class="form-horizontal"  >
                                          <label class="control-label">Search</label>
                                          <div class="search-input">
                                             <input type="text" class="form-control" id="us3-address_edit" placeholder="Type area, city or street"  />
                                          </div>
                                          <br>
                                          <!--Map by element. Also it can be attribute-->
                                          <locationpicker options="ctrl.locationpickerOptions" style="width:98%;padding:20px 5px;" ></locationpicker>
                                          <div class="clearfix" >&nbsp;</div>
                                          <div class="m-t-small  "> 
										  <h4><b>Coordinates</b></h4>
										  
                                             <label>Latitude :</label> <input type="text" class="form-control"   id="us3-lat_edit"  ng-model="ctrl.latitude" style="width:80%" readonly ><br>
                                            <label> Longitude :</label> <input type="text" class="form-control"   id="us3-lon_edit" ng-model="ctrl.longitude" readonly style="width:80%"/>
                                          </div>
                                          <div class="clearfix"></div>
                                       </div>
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                       <div class="row">
                                          
										   <div class="col-sm-12">
                                             <h5>Select Location </h5>
                                             <md-autocomplete ng-disabled="isDisabled" md-no-cache="noCache" md-selected-item="ctrl.parent_location_title" md-search-text-change="ctrl.searchEditLocationChange(searchArea)" md-search-text="searchArea" md-selected-item-change="ctrl.selectedEditLocationChange(values, field)" md-items="values in ctrl.editLocationSearch(searchArea)" md-item-text="values.title" md-min-length="0" placeholder="Select Location " md-menu-class="autocomplete-custom-template"   >
                                                <md-item-template>
                                                   <span class="item-title"> 
                                                   <span> {{values.title}} </span> 
                                                   </span> 
                                                </md-item-template>
                                             </md-autocomplete>
											 <input type="hidden" id="location_edit_id" name="location_edit_id"  ng-model="ctrl.location_id" value="{{ctrl.location_id}}" />
                                          </div>
										  
                                          <div class="col-sm-12">
                                             <h5>Parent Area</h5>
                                             <md-autocomplete ng-disabled="isDisabled" md-no-cache="noCache" md-selected-item="ctrl.parent_area_title" md-search-text-change="ctrl.searchEditAreaChange(searchParentArea)" md-search-text="searchParentArea" md-selected-item-change="ctrl.selectedEditAreaChange(data, field)" md-items="data in ctrl.editAreaSearch(searchParentArea)" md-item-text="data.title" md-min-length="0" placeholder="Select Parent Area" md-menu-class="autocomplete-custom-template"  >
                                                <md-item-template>
                                                   <span class="item-title"> 
                                                   <span> {{data.title}} </span> 
                                                   </span> 
                                                </md-item-template>
                                             </md-autocomplete>
											 <input type="hidden" id="parent_edit_id" name="parent_edit_id"  ng-model="ctrl.parent_id" value="{{ctrl.parent_id}}" />
                                             <br> 		    
                                          </div>
                                          <div class="col-sm-12">
                                             <md-input-container  class="md-block" style="display: none">
                                                <label>Title</label>
                                                <input type="text"  ng-model="ctrl.area_title" id="title_edit" name="title_edit"/>
                                             </md-input-container>
                                          </div>
                                          <div class="col-sm-12">
                                             <md-input-container  class="md-block">
                                                <label>ZipCode</label>
                                                <input type="text"  ng-model="ctrl.pincode" id="pincode_edit" name="pincode_edit"/>
                                             </md-input-container>
                                          </div>
                                          <div class="col-sm-12"> 
                                             <md-button ng-click="ctrl.updateArea(ctrl.area_id)" class="md-raised bg-color md-submit">Update Area</md-button>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
						   </div>
                     </div>
                           <!-----------------------------------------------AREA EDIT MODAL ENDS HERE-------------------------------------------------------------------->
                        </div>
                     </div>
                  </div>
               </div>
      </section>
      </div>
      </div>
   </div>
</div>
<script type="text/javascript" src="<?php echo e(URL::asset('admin/angular-controllers/areas.js')); ?>"></script> 
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>