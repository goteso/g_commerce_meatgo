<?php $__env->startSection('title', 'Store Management' ); ?> 
<link rel="stylesheet" href="<?php echo e(URL::asset('admin/assets/css/app.css')); ?>">
<div ng-app="mainApp">
   <?php $__env->startSection('content'); ?>
   <?php $__env->startSection('header'); ?>
   <?php echo $__env->make('admin.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
   <?php echo $__env->yieldSection(); ?>
   <div id="wrapper" >
      <div id="layout-static">
         <!---------- Static Sidebar Starts------->			
         <?php $__env->startSection('sidebar'); ?>
         <?php echo $__env->make('admin.includes.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
         <?php echo $__env->yieldSection(); ?>
         <!---------- Static Sidebar Ends------->
         <div class="static-content-wrapper"  >
            <section id="main-header">
               <div class="container-fluid">
                  <div class="row">
                     <div class="col-sm-6" ng-controller="searchController" ng-cloak > 
                        <?php echo $__env->make('admin.includes.realtime-search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                     </div>
                     <div class="col-sm-12">
                        <textarea id="res" style="display:none;" ></textarea>
                        <!-------------Loader Starts here------------->
                        <div id="loading" class="loading" style="display:none ;">
                           <img src="<?php echo e(URL::asset('admin/assets/images/89.svg')); ?>" class="img-responsive center-block">				 
                           <p >Calling all data...</p>
                        </div>
                        <!-------------Loader Ends here------------->
                        <!--------------------------- Angular App Starts ---------------------------->
                        <div class="tab-content"  ng-controller="sortStoreController" >
                           <div class="container-fluid" >
                              <div class="row">
                                 <div class="col-sm-5" >
                                    <h2 class="header">Stores</h2>
                                 </div>
                                 <div class="col-sm-7 text-right"  > 
								 
								  <div style="display:inline-flex;" >
                                                      <h5 style="padding-top:5px;padding-right:7px;">Search Store : </h5>
                                                      <md-autocomplete ng-disabled="isDisabled" md-no-cache="noCache" md-selected-item="selectedStore" md-search-text-change="searchStoreChange(searchStore,data)" md-search-text="searchStore" md-selected-item-change="selectedStoreChange(values, data)" md-items="values in storeSearch(searchStore)" md-item-text="values.store_title" md-min-length="0" placeholder="Select Store " md-menu-class="autocomplete-custom-template">
                                                         <md-item-template>
                                                            <span class="item-title"> 
                                                            <span> {{values.store_title}} </span> 
                                                            </span> 
                                                         </md-item-template>
                                                      </md-autocomplete>
													   <input type="hidden" id="store_id" name="store_id" ng-model="store_id"  value="{{store_id}}"/>
                                                      
                                                      <br>
                                                   </div>
												   
                                    <button type="button"ng-click="addStore();" class="btn md-raised bg-color md-add md-button md-ink-ripple text-right"style="margin-top:2px;" >Add Store</button> 
                                 </div>
                              </div>
                              <div class="row" >
                                 <div class="col-sm-12"  >
                                    <div class="panel">
                                       <div class="panel-body user-info">
                                          <div id="tableToExport" class="products-table table-responsive"  >
										  
                                             <table   class="  table table-striped" id="exportthis" >
                                                <thead>
                                                   <tr>
                                                      <th>ID</th>
                                                      <th>TITLE</th>
                                                      <th>PHOTO</th>
                                                      <th>STORE MANAGER</th>
                                                      <th>BUSY STATUS</th>
                                                      <th>CREATED</th>
                                                      <th>ACTIONS</th>
                                                   </tr>
                                                </thead>
                                                <tbody dnd-list="storesData1.d.d1">
                                                   <tr ng-repeat="values in storesData1.d.d1 track by $index"  dnd-draggable="values"  dnd-moved="storesData1.d.d1.splice($index, 1)" ng-click="storesDataDrag(values)" dnd-effect-allowed="move"  dnd-selected="storesData1.selected = values"  ng-class="{'selected': storesData1.selected === values}">
                                                      <td><a href="{{('store')}}/{{values.store_id}}" ><b>#{{values.store_id}}</b></a> </td>
                                                      <td>{{values.store_title}}</td>
                                                      <td> <img src="<?php echo e(URL::asset('/images/stores')); ?>/{{values.store_photo}}" ng-show="values.store_photo" style="width:40px;height:40px;"> <img src="../admin/assets/images/placeholder.jpg" ng-show="!values.store_photo" style="width:40px;height:40px;"> </td>
                                                      <td > {{values.manager_name}}</td>
                                                      <td>{{values.busy_status_title}}</td>
                                                      <td>{{values.created_at_formatted}}</td>
                                                      <td class="actions"> 
                                                         <a class="btn btn-xs delete-product" ng-click="deleteStore(values.store_id, $index)"><i class="fa fa-trash"></i></a> 
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div> 
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!--------------------------- Angular App Ends ---------------------------->
                     </div>
                  </div>
               </div>
            </section>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript" src="<?php echo e(URL::asset('admin/angular-controllers/sortStores.js')); ?>"></script> 
<!------>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>