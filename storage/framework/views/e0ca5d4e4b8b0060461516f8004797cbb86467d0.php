<?php $__env->startSection('title', 'Upload Logo' ); ?>
 <link rel="stylesheet" href="<?php echo e(URL::asset('admin/assets/css/app.css')); ?>">
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('header'); ?>
<?php echo $__env->make('admin.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldSection(); ?>
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
<div id="wrapper"  >
   <div id="layout-static">
      <!---------- Static Sidebar Starts------->			
      <?php $__env->startSection('sidebar'); ?>
      <?php echo $__env->make('admin.includes.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php echo $__env->yieldSection(); ?>
      <!---------- Static Sidebar Ends------->
	  
	  
	    <div class="static-content-wrapper"  >
            <section id="main-header">
               <div class="container-fluid">
                  <div class="row">
                     <div class="col-sm-6"  ng-controller="searchController" ng-cloak > 
                        <?php echo $__env->make('admin.includes.realtime-search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                     </div>
                     <div class="col-sm-12">
                        <div class="text-right">
                        </div>
                        <div class="tab-content"   >
                           <textarea id="res" style="display:  none  ;" ></textarea>
                           <div id="loading" class="loading" style="display:none ;">
                              <img src="<?php echo e(URL::asset('admin/assets/images/89.svg')); ?>" class="img-responsive center-block">				 
                              <p >Calling all data...</p>
                           </div>
						   <input type="file" id="file" style="display:none"/> 
                           <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                      <!--------------------------- Angular App Starts ---------------------------->
                           <div ng-controller="logoController" ng-cloak> 
                          
						  <!---------CONTENT SECTION STARTS HERE------>
	                      <div class="container-fluid" >
                             <div class="row mt-5 mb-4">
	                            <div class="col-sm-12">
	    
		                         <div class="row">
                                 <div class="col-sm-12" >
                                    <h2 class="header">Upload Logo</h2>
								  
                                 </div>
                              </div>
	    
			   
            
			 
								
            <div class="row" >
               <div class="col-sm-12">
          
			   
 
			 
                     <div class="panel"   >
                        <div class="panel-body">
                        <div class="row category-add"  >
                           <div class="col-sm-5"  >
						    <label  >Business Logo</label> 
							
                                                     
							
							
													 <div class="img-upload" style="background:#f5f5f5;max-width:350px;">
												<div class="form-group text-center"   >
												 
												    <input type="text"   value=" " id="item_photo" style="display:none;"/>
								 
												   <div style="text-align: center;position: relative" id="image">
													  <img width="100%"  id="preview_image1" src="<?php echo e(asset('admin/assets/images/img-placeholder.png')); ?>" ng-hide="business_logo_value" style="padding:20px;"/>
													  <img width="100%"  id="preview_image" src="<?php echo env('APP_URL')."/images/logo/";?>{{business_logo_value}}"  ng-hide="!business_logo_value" style="padding:20px 20px 30px;"/>
													  <i id="loading1" class="fa fa-spinner fa-spin fa-3x fa-fw" style="position: absolute;left: 40%;top: 40%;display: none"></i>
												   </div>
												   </div> 
												    </div>
												   <div class="text-right" style="font-size:;max-width:350px; " >
													  <a href="javascript:changeProfile()" title="edit" style="text-decoration: none;background:#e2e2e2;padding:8px;position:relative;bottom: 50px; right:8px">
													  <i class="fa fa-edit"></i> 
													  </a>&nbsp;&nbsp;
													  <!--<a href="javascript:removeFile()" title="delete" style="color: red;text-decoration: none;background:#e2e2e2;padding:8px; position:relative;bottom: 50px;right:10px ">
													  <i class="fa fa-trash-o"></i>
													  </a>-->
												   </div>
												   
												   
						  
											 
            
							  
							  </div>
							  
							   
                        <div class="col-sm-7">
						   
						     <md-button ng-click="upload_logo()" class="md-raised bg-color md-submit" style="top:200px">Update</md-button> 
                        </div>
                   
					    </div>
					 
				     </div>
               </div>
            </div>
         
				  </div>
				  
				 
                <!--</form>-->
              </div>
            
			
			 
			</div>
			
			
			 </div>
 <!--------message box starts-------------->
 
    <div class="col-sm-12 text-center" style="padding:2%;">

	
	</div>
 
 
 
 
 <!--------message box ends-------------->
 
          </div>
	    </div>
			
     
       
   </div> 
  <!--/Section: Contact v.1-->
	</div>	
		
		
	 <!-------------CONTENT SECTION ENDS HERE-------------->
		 
         </section>
      </div>
   </div>
  </div>
     </div>
		
       </div>
		
       
     
<!-- SCRIPTS -->
	<script type="text/javascript" src="<?php echo e(URL::asset('admin/angular-controllers/logo_upload.js')); ?>"></script>  
<!-- JavaScripts --> 
<script src="https://code.jquery.com/jquery-3.1.1.min.js"
   integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
<script src="https://use.fontawesome.com/2c7a93b259.js"></script>
<script>
   function changeProfile() {
       $('#file').click();
   }
   $('#file').change(function () {
       if ($(this).val() != '') {
           upload(this);
   
       }
   });
    function upload(img) {
        var form_data = new FormData();
        form_data.append('file', img.files[0]);
        form_data.append('_token', '<?php echo e(csrf_token()); ?>');
        $('#loading1').css('display', 'block');
        $.ajax({
            url: "<?php echo e(url('image-upload-logo')); ?>",
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.fail) {
                    $('#preview_image').attr('src', '<?php echo e(asset('images/logo/noimage.jpg')); ?>');
                    alert(data.errors['file']);
                }
                else {
                       var data = data.filename;
					 console.log(data);
                 $('#item_photo').val(data).trigger("change");
   
   //$("#photo").dispatchEvent(new Event("input", { bubbles: true }));
    
   
               
               //document.getElementById('photo').value=data;
					$('#preview_image1').attr('src', '<?php echo e(asset('images/logo')); ?>/' + data);
                    $('#preview_image').attr('src', '<?php echo e(asset('images/logo')); ?>/' + data);
                }
                $('#loading1').css('display', 'none');
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
                $('#preview_image').attr('src', '<?php echo e(asset('images/logo/noimage.jpg')); ?>');
            }
        });
    }
   
   
   
   
   function removeFile() {
       if ($('#item_photo').val() != '')
           if (confirm('Are you sure want to remove profile picture?')) {
               $('#loading1').css('display', 'block');
               var form_data = new FormData();
               form_data.append('_method', 'DELETE');
               form_data.append('_token', '<?php echo e(csrf_token()); ?>');
               $.ajax({
                   url: "ajax-remove-image-logo/" + $('#item_photo').val(),
                   data: form_data,
                   type: 'POST',
                   contentType: false,
                   processData: false,
                   success: function (data) {
                       $('#preview_image').attr('src', '<?php echo e(asset('images/noimage.jpg')); ?>');
                       $('#item_photo').val('');
                       $('#loading1').css('display', 'none');
                   },
                   error: function (xhr, status, error) {
                       alert(xhr.responseText);
                   }
               });
           }
   }
   
   
   
   
</script>
	
	<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>