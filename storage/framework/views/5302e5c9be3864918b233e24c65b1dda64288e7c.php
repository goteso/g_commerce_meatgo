<?php $__env->startSection('title', 'Reviews' ); ?> 
<link rel="stylesheet" href="<?php echo e(URL::asset('admin/assets/css/custom.css')); ?>">
 <style>
 .md-autocomplete-suggestions-container{  
   z-index:100000 !important; /* any number of choice > 1050*/
  }
 </style>
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('header'); ?>
<?php echo $__env->make('admin.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldSection(); ?>
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
   <div id="wrapper" >
      <div id="layout-static">
         <!---------- Static Sidebar Starts------->			
         <?php $__env->startSection('sidebar'); ?>
         <?php echo $__env->make('admin.includes.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
         <?php echo $__env->yieldSection(); ?>
         <!---------- Static Sidebar Ends------->
         <div class="static-content-wrapper"  >
		 
<section id="main-header">
   <div class="container-fluid">
      <div class="row">
         <div class="col-sm-6"   ng-controller="searchController" ng-cloak > 
            <?php echo $__env->make('admin.includes.realtime-search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
         </div>
         <div class="col-sm-12">
            <div class="text-right">
            </div>
			
            <div class="tab-content"  ng-controller="reviewsController" ng-cloak  >
               <!--------------------------- Angular App Starts ---------------------------->
               <textarea id="res" style="display: none  ;" ></textarea>
                  <div id="loading" class="loading" style="display:none ;">
                  <img src="<?php echo e(URL::asset('admin/assets/images/89.svg')); ?>" class="img-responsive center-block">				 
                  <p >Calling all data...</p>
                  </div>   
               <div class="container-fluid" >
                  <div class="row">
                     <div class="col-sm-10" >
                        <h2 class="header">Reviews</h2>
                     </div>
                     <div class="col-sm-2">
                        <div class="text-right">
                           
                        </div>
                     </div>
                  </div>
                  <div class="row" >
                     <div class="col-sm-12" >
                        <div class="" >
                           <div id="tableToExport" class="products-table table-responsive"  >
                              <table class="table" class="table table-striped" id="exportthis" >
                                 <thead>
                                    <tr> 
										<th >REVIEW</th>
                                       <th>ORDER ID</th>
                                       <th>STORE TITLE</th>
									   <th>CUSTOMER</th>
									    <th>RATING</th>  
										<th>CREATED AT</th>
										<th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    
                                    <tr ng-repeat="values in reviews.data.data  "> 
										 <td>{{values.review}}</td>
                                       <td><a href="<?php echo e(URL::to('v1/order_detail')); ?>/{{values.order_id}}">#{{values.order_id}}</a></td>
                                       <td>{{values.store_details[0].store_title}}</td>
									   <!--<td><img src="<?php echo e(URL::asset('images/stores')); ?>/{{values.store_details[0].store_photo}}" ng-hide="!values.store_details[0].store_photo">
									   <img src="<?php echo e(URL::asset('admin/assets/images/placeholder.jpg')); ?>" ng-hide="values.store_details[0].store_photo"></td>-->
									   <td> {{values.user_details[0].first_name}} {{values.user_details[0].last_name}} </td>
									    <td>{{values.rating}}</td>
                                         <td>{{values.created_at_formatted}}</td>
										 <td><a ng-click="deleteReview(values.id, $index)"><i class="fa fa-trash"></i></a></td>
                                    </tr>
                                    
                                 </tbody>
                              </table>
                           </div>
						   <div class="pagination text-right" style="display:block" >
                                           <button class="btn" ng-click="pagination(reviews.data.first_page_url);">First</button> 
                                           <button class="btn" ng-click="pagination(reviews.data.prev_page_url);">Previous</button> 
                                          <span>{{reviews.data.current_page}}</span>
                                          <button class="btn" ng-click=".pagination(reviews.data.next_page_url);">Next</button> 
                                          <button class="btn" ng-click="pagination(reviews.data.last_page_url);">Last</button> 
                                       </div>
									   
                           <!--<div class="pagination">
						                  <a href="{{reviews.data.first_page_url}}"><button class="btn">First</button></a>
                                          <a href="{{reviews.data.prev_page_url}}"><button class="btn">Previous</button></a>
                                          <span>{{reviews.data.current_page}}</span>
                                          <a href="{{reviews.data.next_page_url}}"><button class="btn" >Next</button></a>
                                          <a href="{{reviews.data.last_page_url}}"><button class="btn" >Last</button></a>
                                 
                           </div>-->
                        </div>
                     </div>
                     
                  </div>
               </div>
           
            <!--------------------------- Angular App Ends ---------------------------->
         
   
   
   
  
	 </div>
	</div>
      </div>
   </div>
	
</section>

 </div>
   </div>
   </div>
</div>
<script type="text/javascript" src="<?php echo e(URL::asset('admin/angular-controllers/reviews.js')); ?>"></script> 
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>