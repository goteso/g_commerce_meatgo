 <?php $auth_user_type = @\Auth::user()->user_type;   
$auth_user_id =   @\Auth::id();
App::setLocale('settings');   

$business_logo = @\App\Setting::where( 'key_title' , 'business_logo' )->first(['key_value'])->key_value;
  

?>

<header id="topnav" class="navbar navbar-dark navbar-fixed-top clearfix" role="banner" style="z-index:10;">
   <span id="trigger-sidebar" class="toolbar-trigger" >
      <div class="navbar-header" style="height: 100%;" >
         <a class="navbar-brand" href="#" style="height: 50px;" >
         <img src="<?php echo env('APP_URL')."/images/logo/".@$business_logo;?>" class="img-responsive" style="width:auto;height:35px;"></a>
      </div>
      <a data-toggle="tooltips" data-placement="right" title="Toggle Sidebar"><span class="icon-bg"><i class="fa fa-fw fa-bars"></i></span></a>
   </span>
   <ul class="nav navbar-nav toolbar pull-right">
      <!-- <li class="dropdown">
         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                          <span><i class="fa fa-cog"></i></span>
                       </a>
                       <ul class="dropdown-menu" role="menu"> 									   
                       </ul>
                   </li> -->
      <li class="dropdown">
         <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Add" role="button" aria-expanded="false">
         <span><i class="fa fa-plus-circle"></i></span>
         </a>
         <ul class="dropdown-menu entities" role="menu">
            <!-- <li><a href="<?php echo e(URL::to('product_get_form_basic')); ?>"><i class="fa fa-list"></i><span> Products Add Form</span></a></li>
               <li><a href="<?php echo e(URL::to('user_forms')); ?>"><i class="fa fa-user"></i><span> Add User</span></a></li>  
               <li><a href="<?php echo e(URL::to('order_add')); ?>"><i class="fa fa-truck"></i><span> Add order</span></a></li> ---> 
            <li>
               <div class=" border" id="main"  >
                  <div class=" title">
                     <h5>PRODUCTS</h5>
                  </div>
                  <div class="row">
                     <div class="col-lg-12">
                        <div class="row">

                          <?php if(trans('permission'.$auth_user_type.'.add_items') == '1'): ?>
                           <div class="col-lg-4 col-sm-4">
                               <a href="<?php echo e(URL::to('v1/item')); ?>">
                                 <img src="<?php echo e(URL::to('admin/assets/images/product.png')); ?>" class="img-responsive center-block">
                                 <p class="text-center ">Add Items</p>
                              </a>
                          </div>
                          <?php endif; ?>


                           <div class="col-lg-4 col-sm-4">
 
                              <a href="<?php echo e(URL::to('v1/categories')); ?>">
 
                                <a href="<?php echo e(URL::to('v1/categories')); ?>">
 
                                 <img src="<?php echo e(URL::to('admin/assets/images/categories.png')); ?>" class="img-responsive center-block">
                                 <p class="text-center">Add Categories</p>
                              </a>
                           </div>

                           <div class="col-lg-4 col-sm-4">
                              <a href=" <?php echo e(URL::to('v1/tags')); ?>">
                                 <img src="<?php echo e(URL::to('admin/assets/images/brand.png')); ?>" class="img-responsive center-block">
                                 <p class="text-center">Add Tags</p>
                              </a>
                           </div>


                        </div>
                     </div>
                  </div>



<?php if(trans('permission'.$auth_user_type.'.add_customers') == '1' || trans('permission'.$auth_user_type.'.add_vendors') == '1' || trans('permission'.$auth_user_type.'.add_store_manager') == '1' ) { ?>
                  <div class=" title">
                     <h5>USERS</h5>
                  </div>
                  <div class="row">
                     <div class="col-lg-12">
                        <div class="row">


                           <?php if(trans('permission'.$auth_user_type.'.add_customers') == '1'): ?>
                           <div class="col-lg-4 col-sm-4">
                                 <a href="<?php echo e(URL::to('v1/customer')); ?>">
                                 <img src="<?php echo e(URL::to('admin/assets/images/user.png')); ?>" class="img-responsive center-block">
                                 <p class="text-center">Add Customers</p>
                              </a>
                           </div>
                           <?php endif; ?>


                           <?php if(trans('permission'.$auth_user_type.'.add_vendors') == '1'): ?>
						         <div class="col-lg-4 col-sm-4">
                                 <a href="<?php echo e(URL::to('v1/vendor')); ?>">
                                 <img src="<?php echo e(URL::to('admin/assets/images/user.png')); ?>" class="img-responsive center-block">
                                 <p class="text-center">Add Vendor</p>
                              </a>
                           </div>
                           <?php endif; ?>


                           <?php if(trans('permission'.$auth_user_type.'.add_store_manager') == '1'): ?>
						         <div class="col-lg-4 col-sm-4">
                              <a href="<?php echo e(URL::to('v1/manager')); ?>">
                                 <img src="<?php echo e(URL::to('admin/assets/images/user.png')); ?>" class="img-responsive center-block">
                                 <p class="text-center">Add Store Manager</p>
                              </a>
                           </div>
                           <?php endif; ?>

                        </div>
                     </div>
                  </div>
<?php } ?>



                  <?php if(trans('permission'.$auth_user_type.'.add_orders') == '1'): ?>
                  <div class=" title">
                     <h5>ORDERS</h5>
                  </div>
                  <div class="row">
                     <div class="col-lg-12">
                        <div class="row">
                           <div class="col-lg-4 col-sm-4">
                              <a href="<?php echo e(URL::to('v1/order')); ?>">
                                 <img src="<?php echo e(URL::to('admin/assets/images/order.png')); ?>" class="img-responsive center-block">
                                 <p class="text-center">Add Orders</p>
                              </a>
                           </div>
                        </div>
                     </div>
                  </div>
                  <?php endif; ?>



               </div>
            </li>
         </ul>
      </li>
      <li class="dropdown">
         <a href="#" class="dropdown-toggle name" data-toggle="dropdown" title="Sign Out" role="button" aria-expanded="false">
            <span class="user-name"><i class="fas fa-sign-out-alt"></i></span><!--<span class="caret head"></span>-->
         </a>
         <ul class="dropdown-menu entities" role="menu">
            <li>
               <a id="logout" href="<?php echo e(url('/logouts')); ?>" >
               Logout
               </a>
               <form id="logout-form" action="<?php echo e(url('/logouts')); ?>" method="GET" style="display: none;">
               </form>
            </li>
         </ul>
      </li>
	  
	  <!-- <li class="dropdown"  >
         <a href="#" class="dropdown-toggle name" data-toggle="dropdown" title="profile" role="button" aria-expanded="false">
            <span class="user-name"><i class="fa fa-user"></i></span><!--<span class="caret head"></span>--
         </a>
         <ul class="dropdown-menu entities" role="menu"  >
            <li ng-controller="loginController" ng-cloak>
                {{profileData.data[0].first_name}}
            </li>
         </ul>
      </li>-->
   </ul>
</header>