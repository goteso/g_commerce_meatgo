<?php $__env->startSection('title', 'Store Requests' ); ?> 
<link rel="stylesheet" href="<?php echo e(URL::asset('admin/assets/css/app.css')); ?>"> 
<style>
table>tbody>tr>td,table>thead>tr>th{text-align:center;}
#showRequestModal h4.header{font-family:'Open Sans Semibold',sans-serif;font-size:21px;}
#showRequestModal p{font-size:15px;}
</style>
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('header'); ?>
<?php echo $__env->make('admin.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldSection(); ?>
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
   <div id="wrapper" >
      <div id="layout-static">
         <!---------- Static Sidebar Starts------->			
         <?php $__env->startSection('sidebar'); ?>
         <?php echo $__env->make('admin.includes.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
         <?php echo $__env->yieldSection(); ?>
         <!---------- Static Sidebar Ends------->
         <div class="static-content-wrapper"  >
            <section id="main-header">
               <div class="container-fluid">
                  <div class="row">
				  <div class="col-sm-5" ng-controller="searchController" ng-cloak > 
                                    <?php echo $__env->make('admin.includes.realtime-search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                 </div>
                     <div class="col-sm-12">
                        <textarea id="res" style="display:none;" ></textarea>
						
						<!-------------Loader Starts here------------->
                        <div id="loading" class="loading" style="display:none ;">
                           <img src="<?php echo e(URL::asset('admin/assets/images/89.svg')); ?>" class="img-responsive center-block">				 
                           <p >Calling all data...</p>
                        </div>
						<!-------------Loader Ends here------------->
						 
                        <!--------------------------- Angular App Starts ---------------------------->
                        <div class="tab-content"   >
                           <div class="container-fluid" >
						   
						   <div class="row">
                               <div class="col-sm-7" >
                                    <h2 class="header">  Store Requests</h2>
                                 </div>
								 <div class="col-sm-5 text-right">
                                    <!-- <a href="<?php echo e(URL::to('v1/item')); ?>"><button type="button" class="btn md-raised bg-color md-add md-button md-ink-ripple text-right" >Add New</button></a>-->
                                 </div> 
								 </div>
                             <div ng-view></div>
                           </div>
						   
						   
						   
						   
						
                        </div>
                        <!--------------------------- Angular App Ends ---------------------------->
                     </div>
                  </div>
               </div>
         </div>
         </section>
      </div>
   </div>
</div>
<script type="text/javascript" src="<?php echo e(URL::asset('admin/angular-controllers/requests.js')); ?>"></script> 

<!------>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>