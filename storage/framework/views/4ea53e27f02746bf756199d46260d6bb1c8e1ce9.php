<?php $__env->startSection('title', 'Account' ); ?> 
<link rel="stylesheet" href="<?php echo e(URL::asset('admin/assets/css/app.css')); ?>">
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('header'); ?>
<?php echo $__env->make('admin.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldSection(); ?>
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
<div id="wrapper" >
   <div id="layout-static">
      <!---------- Static Sidebar Starts------->			
      <?php $__env->startSection('sidebar'); ?>
      <?php echo $__env->make('admin.includes.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php echo $__env->yieldSection(); ?>
      <!---------- Static Sidebar Ends------->
      <div class="static-content-wrapper"  >
         <section id="main-header">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-sm-6"  ng-controller="searchController" ng-cloak > 
                     <?php echo $__env->make('admin.includes.realtime-search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                  </div>
                  <div class="col-sm-12">
                     <div class="text-right">
                     </div>
                     <div class="tab-content"   >
					  <input type="file" id="file" style="display: none "/>
                           <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                        <!--------------------------- Angular App Starts ---------------------------->
                        <textarea id="res" style="display:  none  ;" ></textarea>
                        <div id="loading" class="loading" style="display:none ;">
                           <img src="<?php echo e(URL::asset('admin/assets/images/89.svg')); ?>" class="img-responsive center-block">				 
                           <p >Calling all data...</p>
                        </div>
                        <div  >
                           <div class="container-fluid" >
                              <div class="row">
                                 <div class="col-sm-10" >
                                    <h2 class="header"> </h2>
                                 </div>
                                 <div class="col-sm-2">
                                    <div class="text-right">  
                                    </div>
                                 </div>
                              </div>
                           </div>
                           
                           <div class="container-fluid" ng-cloak>
                              <div class="row">
                                 <div class="col-sm-12" > 
									<!------------------------NG VIEW STARTS HERE------------------------->
								     <div ng-view></div> 
									<!------------------------NG VIEW ENDS HERE--------------------------->
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
         </section>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript" src="<?php echo e(URL::asset('admin/angular-controllers/account.js')); ?>"></script> 
<script src="https://use.fontawesome.com/2c7a93b259.js"></script>
<script>
   function changeProfile() {
       $('#file').click();
   }
   $('#file').change(function () {
       if ($(this).val() != '') {
           upload(this);
   
       }
   });
    function upload(img) {
        var form_data = new FormData();
        form_data.append('file', img.files[0]);
        form_data.append('_token', '<?php echo e(csrf_token()); ?>');
        $('#loading1').css('display', 'block');
        $.ajax({
            url: "<?php echo e(url('image-upload-users')); ?>",
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.fail) {
                    $('#preview_image').attr('src', '<?php echo e(asset('images/users/noimage.jpg')); ?>');
                    alert(data.errors['file']);
                }
                else {
                    var data = data.filename; 
                 $('#item_photo').val(data).trigger("change");
   
   //$("#photo").dispatchEvent(new Event("input", { bubbles: true }));
    
   
               
               //document.getElementById('photo').value=data;
                    $('#preview_image').attr('src', '<?php echo e(asset('images/users')); ?>/' + data);
					$('#preview_image1').attr('src', '<?php echo e(asset('images/users')); ?>/' + data);
                }
                $('#loading1').css('display', 'none');
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
                $('#preview_image').attr('src', '<?php echo e(asset('images/users/noimage.jpg')); ?>');
            }
        });
    }
   
   
   
   
   function removeFile() {
       if ($('#item_photo').val() != '')
		   alert("<?php echo e(url('ajax-remove-image-users')); ?>/" + $('#item_photo').val());
           if (confirm('Are you sure want to remove profile picture?')) {
               $('#loading1').css('display', 'block');
               var form_data = new FormData();
               form_data.append('_method', 'DELETE');
               form_data.append('_token', '<?php echo e(csrf_token()); ?>');
               $.ajax({
                   url: "<?php echo e(url('ajax-remove-image-users')); ?>/" + $('#item_photo').val(),
                   data: '',
                   type: 'DELETE',
                   contentType: false,
                   processData: false,
                   success: function (data) {
					   alert('s');
                       $('#preview_image').attr('src', '<?php echo e(asset('assets/images/placeholder.jpg')); ?>');
                       $('#item_photo').val('');
                       $('#loading1').css('display', 'none');
                   },
                   error: function (xhr, status, error) {
                       alert(xhr.responseText);
                   }
               });
           }
   }
   
   
   
   
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>