<?php $__env->startSection('title', 'Edit Customer' ); ?>
<link rel="stylesheet" href="<?php echo e(URL::asset('admin/css/app.css')); ?>">
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('header'); ?>
<?php echo $__env->make('admin.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldSection(); ?>
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
   <div id="wrapper"  >
      <div id="layout-static">
         <!---------- Static Sidebar Starts------->			
         <?php $__env->startSection('sidebar'); ?>
         <?php echo $__env->make('admin.includes.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
         <?php echo $__env->yieldSection(); ?>
         <!---------- Static Sidebar Ends------->
         <div class="static-content-wrapper">
            <section id="main-header">
               <div class="container-fluid">
                  <div class="row">
                     <div class="col-sm-6"   ng-controller="searchController" ng-cloak > 
                        <?php echo $__env->make('admin.includes.realtime-search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                     </div>
                     <div class="col-sm-12">
                        <textarea id="res" style="display: none ;" ></textarea>
						  <?php    $auth_user_type = Auth::user()->user_type;   
                             $auth_user_id =   Auth::id();  
                          ?>
                        <!-------------Loader Starts here------------->
                        <div id="loading" class="loading" style="display:none ;">
                           <img src="<?php echo e(URL::asset('admin/assets/images/89.svg')); ?>" class="img-responsive center-block">				 
                           <p >Calling all data...</p>
                        </div>
                        <!-------------Loader Ends here------------->
                        <div class="text-right">
                        </div>
                        <div class="tab-content" >
                           <input type="file" id="file" style="display: none "/>
                           <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                           <!--------------------------Angular App Starts ------------------------------------>
                           <div  ng-controller="userEditController" ng-cloak>
                              <div class="container-fluid" >
                                 <div class="row">
                                    <div class="col-sm-6" >
                                       <h2 class="header">Edit Customer</h2>
                                    </div>
									
									<div class="col-sm-6 text-right" >
                                       <button ng-click="resetPassword()" class="btn md-raised bg-color md-add md-button md-ink-ripple" data-toggle="modal" data-target="#reset">Reset Password</button>
                                    </div>
                                 </div>
                                 <div class="row" >
                                    <div class="col-sm-12 " >
                                       <form   >
                                          <div class="panel" ng-repeat="users in editUser">
                                             <div class="panel-body">
                                                <h3>{{users.title}}</h3>
                                                <div class="row">
                                                   <div class="col-lg-5 col-sm-8">
                                                     
														  <div class="form-group" ng-repeat="data in users.fields" ng-show="data.type == 'file'" >
                                                      <label for="{{data.identifier}}">{{data.title}}</label> 
                                                      <input type=" " ng-model="data.value" value="{{data.value}}" id="item_photo" style="display:  none;"/>
                                                      <div style="width:150px;height: 180px; border: 1px solid whitesmoke ;text-align: center;position: relative" id="image">
                                                          <img width="100%" height="100%" id="preview_image1" src="<?php echo e(asset('admin/assets/images/placeholder.jpg')); ?>" ng-hide="data.value"/>
                                                         <img width="100%" height="100%" id="preview_image" src="<?php echo env('APP_URL')."/images/users/";?>{{data.value}}"  ng-hide="!data.value"/>
                                                         <i id="loading1" class="fa fa-spinner fa-spin fa-3x fa-fw" style="position: absolute;left: 40%;top: 40%;display: none"></i>
                                                      </div>
                                                      <a href="javascript:changeProfile()" style="text-decoration: none;">
                                                      <i class="fa fa-edit"></i> Change
                                                      </a>&nbsp;&nbsp;
                                                      <a href="javascript:removeFile()" style="color: red;text-decoration: none;">
                                                      <i class="fa fa-trash-o"></i>
                                                      Remove
                                                      </a>
 
                                                      </p>
                                                   </div>
												    
												   <br>
												   
												   <md-input-container  class="md-block text-left" ng-repeat="data in users.fields" ng-show="data.type == 'text' || data.type == 'string'" >
                                                         <label>{{data.title}}</label>
                                                         <input type="{{data.type}}" id="{{data.identifier}}" name="{{data.identifier}}"  ng-model="data.value" value="" />
                                                      </md-input-container>
													  
													  
													   <md-input-container  class="md-block text-left" ng-repeat="data in users.fields" ng-show="data.type == 'email' "  >
                                                         <label>{{data.title}}</label>
                                                         <input type="{{data.type}}" id="{{data.identifier}}" name="{{data.identifier}}"  ng-model="data.value" value="" />
                                                      </md-input-container>
													  
													  
													  <md-input-container  class="md-block text-left" ng-repeat="data in users.fields" ng-show="data.type == 'password' "  >
                                                         <label>{{data.title}}</label>
                                                         <input type="{{data.type}}" id="{{data.identifier}}" name="{{data.identifier}}"  ng-model="data.value" value="" />
                                                      </md-input-container>
													  
                                                  
													   <div class="form-group" ng-repeat="data in users.fields" ng-show="data.type == 'hidden'">
                                                      <label for="{{data.identifier}}"> </label>
                                                      <input type="{{data.type}}" ng-model="data.value" class="form-control"   id="{{data.identifier}}"  placeholder="Enter {{data.title}}" >
                                                   </div>
                                                      <div class="form-group" ng-repeat="data in users.fields" ng-show="data.type == 'tag'">
                                                         <label for="{{data.identifier}}">{{data.title}}</label> 
                                                         <tags-input ng-model="data.value" add-from-autocomplete-only="true" allow-leftover-text="false" display-property="label" key-property="id" text="text" ng-blur="text=''">
                                                            <auto-complete min-length="1" highlight-matched-text="true" source="searchData($query,data)"></auto-complete>
                                                         </tags-input>
                                                      </div>
                                                      <div class="form-group" ng-repeat="data in users.fields" ng-show="data.symbol">
                                                         <label for="{{data.identifier}}">{{data.title}}</label>
                                                         <div class="input-group">
                                                            <span class="input-group-addon">{{data.symbol}}</span>
                                                            <input type="{{data.type}}" class="form-control"   ng-model="data.value" id="{{data.identifier}}" placeholder="Enter {{data.title}}" >
                                                         </div>
                                                      </div>
													   <!----- For Radio Button Type Input Field----------->
                                                    <div class="form-group" ng-repeat="data in users.fields" ng-show="data.type == 'radio'">
                                                        <label for="{{data.identifier}}">{{data.title}}</label> <br> 
                                                        <label  class="radio-inline" ng-repeat="options in data.field_options">
								                           <input type="{{data.type}}" ng-model="data.value" value="{{options.value}}" name="{{data.identifier}}" >{{options.title}}
							 	                        </label> 
                                                    </div>
							  
                                                      <!----- For Checkbox Type Input Field----------->
                                                      <div class="form-group" ng-repeat="data in users.fields" ng-show="data.type == 'checkbox'">
                                                         <label for="{{data.identifier}}">{{data.title}} </label> <br>
                                                         <div class="checkbox" ng-repeat="options in data.field_options">
                                                            <label><input type="{{data.type}}"  ng-model="data.value[options.value]" id="{{options.value}}"  >{{options.title}}</label>
                                                         </div>
                                                         <label class="checkbox-inline" ng-repeat="options in data.field_options">
                                                         <input type="{{data.type}}" ng-model="data.value" value="{{options.value}}" >{{options.title}}
                                                         </label>
                                                      </div>
                                                      <!----- For Select Box Type Input Field----------->
                                                      <div class="form-group" ng-repeat="data in users.fields" ng-show="data.type == 'Selectbox'">
                                                         <label for="{{data.identifier}}">{{data.title}}</label> <br> 
                                                         <select ng-model="data.value" class="form-control"  ng-required="data.required_or_not">
                                                            <option value="" selected>Select User Type</option>
                                                            <option  ng-repeat="options in data.field_options" value="{{options.value}} ">{{options.title}}</option>
                                                         </select>
                                                      </div>
                                                      <div class="form-group" ng-repeat="data in users.fields" ng-show="data.message">
                                                         <label for="{{data.identifier}}">{{data.title}}</label>
                                                         <input type="{{data.type}}" ng-model="data.value" class="form-control"   id="{{data.identifier}}"  placeholder="Enter {{data.title}}"  >
                                                      </div>
													  <br>
                                                       <md-input-container  class="md-block text-left" ng-repeat="data in users.fields" ng-show="data.type == 'datePicker'" >
                                                         <label>{{data.title}}</label>
                                                          <datepicker
                                                         date-format="yyyy-MM-dd"  
                                                         button-prev='<i class="fa fa-arrow-circle-left"></i>'
                                                         button-next='<i class="fa fa-arrow-circle-right"></i>'>
                                                         <input ng-model="data.value" type="text" class="  font-fontawesome font-light radius3" placeholder="Enter {{data.title}}" />
                                                      </datepicker>
                                                      </md-input-container>
													  
                                                      <div class="form-group" ng-repeat="data in users.fields" ng-show="data.type == 'timePicker'">
                                                         <label for="{{data.identifier}}">{{data.title}}</label>
                                                         <div class="input-group clockpicker" 
                                                            clock-picker 
                                                            data-autoclose="true"   > 
                                                            <input ng-model="ctrl.time" data-format="hh:mm:ss" type="text" class="form-control" placeholder="Enter {{data.title}}">
                                                            <span class="input-group-addon">
                                                            <span class="fa fa-clock-o"></span>
                                                            </span>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-lg-12 col-sm-12 submit text-right">
                                             <button   ng-click="updateUser()" class="btn md-raised bg-color md-submit md-button md-ink-ripple">Save Changes</button>
                                          </div>
                                       </form>
                                    </div>
                                 </div>
                              </div>
							  
							  
							  
							  
							  
						  <!-----------------------------------------------RESET PASSWORD MODAL STARTS HERE-------------------------------------------------------------------->
                     <div id="reset" class="modal fade" data-backdrop="false"  style="z-index:20;" role="dialog">
                        <div class="modal-dialog modal-lg category-add"  >
                           <!-- Modal content-->
                           <div class="modal-content "   >
                              <div class="modal-header  "  >
                                 <button type="button" class="close" data-dismiss="modal">&times;</button>
                                 <h4 class="modal-title text-uppercase"  >Reset <b>Password</b></h4>
                              </div>
                              <div class="modal-body "  >
                                 <div class="row"   >
                                    
                                    <div class="col-sm-12 col-md-6">
                                       
                                             <md-input-container  class="md-block">
                                                <label>New Password</label>
                                                <input type="password"  id="password" name="password"/>
                                             </md-input-container>
                                          
                                           
                                             <md-input-container  class="md-block">
                                                <label>Confirm New Password</label>
                                                <input type="password"  id="confirm_password" name="confirm_password"/>
                                             </md-input-container>
                                        </div>
                                        <div class="col-sm-12 col-md-6">  
                                             <md-button ng-click="resetPass()" class="md-raised bg-color md-submit">RESET</md-button>
                                          
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
						   </div>
                           </div>
                           <!-----------------------------------------------RESET PASSWORD MODAL ENDS HERE-------------------------------------------------------------------->
						   
                           </div>
                           <!--------------------------Angular App Ends ------------------------------------>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
         </div>
      </div>
   </div>
   <!------>
</div>
<script type="text/javascript" src="<?php echo e(URL::asset('admin/angular-controllers/customers.js')); ?>"></script> 


<script src="https://code.jquery.com/jquery-3.1.1.min.js"
   integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
<script src="https://use.fontawesome.com/2c7a93b259.js"></script>
<script>
   function changeProfile() {
       $('#file').click();
   }
   $('#file').change(function () {
       if ($(this).val() != '') {
           upload(this);
   
       }
   });
    function upload(img) {
        var form_data = new FormData();
        form_data.append('file', img.files[0]);
        form_data.append('_token', '<?php echo e(csrf_token()); ?>');
        $('#loading1').css('display', 'block');
        $.ajax({
            url: "<?php echo e(url('image-upload-users')); ?>",
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.fail) {
                    $('#preview_image').attr('src', '<?php echo e(asset('images/users/noimage.jpg')); ?>');
                    alert(data.errors['file']);
                }
                else {
                    var data = data.filename;
					 console.log(data);   
                 $('#item_photo').val(data).trigger("change");
   
   //$("#photo").dispatchEvent(new Event("input", { bubbles: true }));
    
   
               
               //document.getElementById('photo').value=data;
                    $('#preview_image').attr('src', '<?php echo e(asset('images/users')); ?>/' + data);
					$('#preview_image1').attr('src', '<?php echo e(asset('images/users')); ?>/' + data);
                }
                $('#loading1').css('display', 'none');
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
                $('#preview_image').attr('src', '<?php echo e(asset('images/users/noimage.jpg')); ?>');
            }
        });
    }
   
   
   
   
   function removeFile() {
       if ($('#photo').val() != '')
           if (confirm('Are you sure want to remove profile picture?')) {
               $('#loading1').css('display', 'block');
               var form_data = new FormData();
               form_data.append('_method', 'DELETE');
               form_data.append('_token', '<?php echo e(csrf_token()); ?>');
               $.ajax({
                   url: "ajax-remove-image-users/" + $('#photo').val(),
                   data: form_data,
                   type: 'POST',
                   contentType: false,
                   processData: false,
                   success: function (data) {
                       $('#preview_image').attr('src', '<?php echo e(asset('images/noimage.jpg')); ?>');
                       $('#photo').val('');
                       $('#loading').css('display', 'none');
                   },
                   error: function (xhr, status, error) {
                       alert(xhr.responseText);
                   }
               });
           }
   }
   
   
   
   
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>