<?php $__env->startSection('title', 'Edit Item' ); ?>
<link rel="stylesheet" href="<?php echo e(URL::asset('admin/assets/css/app.css')); ?>">
<style>.md-chip-input-container{margin:0px;width:100%;}</style>
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('header'); ?>
<?php echo $__env->make('admin.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldSection(); ?>
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
   <div id="wrapper"  >
      <div id="layout-static">
         <!---------- Static Sidebar Starts------->			
         <?php $__env->startSection('sidebar'); ?>
         <?php echo $__env->make('admin.includes.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
         <?php echo $__env->yieldSection(); ?>
         <!---------- Static Sidebar Ends------->
         <div class="static-content-wrapper">
            <section id="main-header">
               <div class="container-fluid">
                  <div class="row">
                     <div class="col-sm-6"   ng-controller="searchController" ng-cloak > 
                        <?php echo $__env->make('admin.includes.realtime-search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                     </div>
                     <div class="col-sm-12">
                        <textarea id="res" style="display: none;" ></textarea>
                        <!-------------Loader Starts here------------->
                        <div id="loading" class="loading" style="display:none ;">
                           <img src="<?php echo e(URL::asset('admin/assets/images/89.svg')); ?>" class="img-responsive center-block">				 
                           <p >Calling all data...</p>
                        </div>
                        <!-------------Loader Ends here------------->
                        <div class="text-right">
                        </div>
                        <div class="tab-content" >
                           <input type="file" id="file" style="display:none"/>
                           <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                           <!--------------------------Angular App Starts ------------------------------------>
                           <div  ng-controller="editItemsController" ng-cloak>
                              <div class="container-fluid" >
                                 <div class="row">
                                    <div class="col-sm-6" >
                                       <h2 class="header">Edit Item</h2>
                                    </div>
									 <div class="col-lg-6 col-sm-6  submit text-right">
                                                   <button   ng-click="updateItem()" class="btn md-raised bg-color md-submit md-button md-ink-ripple">Save Changes</button>
                                      </div>
                                    <input type="file" id="file" style="display:none "/>
                                    <input type="hidden" id="file_name" style="display: "/>
                                 </div>
                                 <div class="row" >
                                    <div class="col-sm-12 " >
                                       <div class="panel" ng-repeat="items in editItem" ng-hide="items.type=='items_images'" >
                                          <div class="panel-body">
                                             <div class="row"  ng-hide="items.type=='item_variants'">
                                                <h3>{{items.title}}</h3>
                                                <div class="col-lg-5 col-sm-8">
                                                   <div class="form-group" ng-repeat="data in items.fields" ng-show="data.type == 'file'" >
                                                      <label for="{{data.identifier}}">{{data.title}}</label> 
                                                      <input type=" " ng-model="data.value" value="{{data.value}}" id="item_photo" style="display: none;"/>
                                                      <div style="width:150px;height: 180px; border: 1px solid whitesmoke ;text-align: center;position: relative" id="image">
                                                         <img width="100%" height="100%" id="preview_image" src="<?php echo env('APP_URL')."/images/items/";?>{{data.value}}"/>
                                                         <i id="loading1" class="fa fa-spinner fa-spin fa-3x fa-fw" style="position: absolute;left: 40%;top: 40%;display: none"></i>
                                                      </div>
                                                      <a href="javascript:changeProfile()" style="text-decoration: none;">
                                                      <i class="fa fa-edit"></i> Change
                                                      </a>&nbsp;&nbsp;
                                                      <a href="javascript:removeFile()" style="color: red;text-decoration: none;">
                                                      <i class="fa fa-trash-o"></i>
                                                      Remove
                                                      </a>
 
                                                      </p>
                                                   </div>
                                                    <br>
												   <md-input-container  class="md-block text-left" ng-repeat="data in items.fields" ng-show="data.type == 'text' || data.type == 'string'" >
                                                         <label>{{data.title}}</label>
                                                         <input type="{{data.type}}" id="{{data.identifier}}" name="{{data.identifier}}"  ng-model="data.value" value="" />
                                                      </md-input-container>
													  
													  
                                                   <div class="form-group" ng-repeat="data in items.fields" ng-show="data.type == 'tag'">
                                                      <label for="{{data.identifier}}">{{data.title}}</label> 
                                                      
													  
													  <tags-input ng-model="data.value" add-from-autocomplete-only="true" allow-leftover-text="false" display-property="label" key-property="value" text="text"   ng-blur="text=''" placeholder="Categories" >
															 <auto-complete min-length="1" highlight-matched-text="true" source="searchCatData($query)"  ></auto-complete>
																</tags-input>
																
													<!-- <md-chips ng-model="selectedCategory" md-autocomplete-snap
                                                         md-transform-chip="transformChip($chip)"
                                                         md-require-match="autocompleteDemoRequireMatch">
                                                         <md-autocomplete
                                                            md-selected-item="data.values"
                                                            md-search-text="searchText"
                                                            md-items="item in querySearch(searchText)"
                                                            md-item-text="item.category_title"
                                                            placeholder="Search for a Category">
                                                            <span md-highlight-text="searchText">{{item.category_title}}  </span>
                                                         </md-autocomplete>
                                                         <md-chip-template>
                                                            <span>
                                                            <strong>{{$chip.name}}</strong>
                                                            <em>({{$chip.type}})</em>
                                                            </span>
                                                         </md-chip-template>
                                                      </md-chips>-->
													   
													  
													
                                                   </div>
                                                   <!--- <div class="form-group" ng-repeat="data in items.fields" ng-show="data.type == 'tag'">
                                                      <label for="{{data.identifier}}">{{data.title}}</label> 
                                                      <tags-input ng-model="data.value" add-from-autocomplete-only="true" allow-leftover-text="false" display-property="label" key-property="id" text="text" ng-blur="text=''">
                                                         <auto-complete min-length="1" highlight-matched-text="true" source="searchData($query,data)"></auto-complete>
                                                      </tags-input>
                                                      </div>-->
													  
													  
                                                   <div class="form-group" ng-repeat="data in items.fields" ng-show="data.symbol">
                                                      <label for="{{data.identifier}}">{{data.title}}</label>
                                                      <div class="input-group">
                                                         <span class="input-group-addon">{{data.symbol}}</span>
                                                         <input type="{{data.type}}" class="form-control"   ng-model="data.value" id="{{data.identifier}}" placeholder="Enter {{data.title}}" >
                                                      </div>
                                                   </div>
                                                   <div class="form-group" ng-repeat="data in items.fields" ng-show="data.message">
                                                      <label for="{{data.identifier}}">{{data.title}}</label>
                                                      <input type="{{data.type}}" ng-model="data.value" class="form-control"   id="{{data.identifier}}"  placeholder="Enter {{data.title}}"  >
                                                   </div>
                                                   <div class="form-group" ng-repeat="data in items.fields" ng-show="data.type == 'datePicker'" placeholder="Enter {{data.title}}">
                                                      <label for="{{data.identifier}}">{{data.title}}</label>
                                                      <datepicker
                                                         date-format="yyyy-MM-dd"  
                                                         button-prev='<i class="fa fa-arrow-circle-left"></i>'
                                                         button-next='<i class="fa fa-arrow-circle-right"></i>'>
                                                         <input ng-model="data.value" type="text" class="form-control font-fontawesome font-light radius3" placeholder="Enter {{data.title}}" />
                                                      </datepicker>
                                                   </div>
                                                   <div class="form-group" ng-repeat="data in items.fields" ng-show="data.type == 'timePicker'">
                                                      <label for="{{data.identifier}}">{{data.title}}</label>
                                                      <div class="input-group clockpicker" 
                                                         clock-picker 
                                                         data-autoclose="true"   > 
                                                         <input ng-model="ctrl.time" data-format="hh:mm:ss" type="text" class="form-control" placeholder="Enter {{data.title}}">
                                                         <span class="input-group-addon">
                                                         <span class="fa fa-clock-o"></span>
                                                         </span>
                                                      </div>
                                                   </div>
												   
												    <div class="form-group" ng-repeat="data in items.fields" ng-show="data.type == 'api'"> 
														<div   ng-show="data.identifier == 'vendor_id'">
                                                      <h5>Select {{data.title}}</h5>
                                                      <md-autocomplete ng-disabled="isDisabled" md-no-cache="noCache" md-selected-item="data.display_value" md-search-text-change="searchVendorChange(searchVendor,data)" md-search-text="searchVendor" md-selected-item-change="selectedVendorChange(values, data)" md-items="values in vendorSearch(searchVendor)" md-item-text="values.first_name" md-min-length="0" placeholder="Select Vendor " md-menu-class="autocomplete-custom-template"    >
                                                         <md-item-template>
                                                            <span class="item-title"> 
                                                            <span> {{values.first_name}} </span> 
                                                            </span> 
                                                         </md-item-template>
                                                      </md-autocomplete>
													  <input type="text" id="vendor_id" name="vendor_id"  ng-model="vendor" value="{{vendor_id}}" style="display:none;"/>
                                                      <br> 
                                                   </div>
                                                   <div ng-show="data.identifier == 'store_id'">
                                                      <h5>Select {{data.title}} </h5>
                                                      <md-autocomplete ng-disabled="isDisabled" md-no-cache="noCache" md-selected-item="data.display_value" md-search-text-change="searchStoreChange(searchStore,data)" md-search-text="searchStore" md-selected-item-change="selectedStoreChange(value, data)" md-items="value in storeSearch(searchStore)" md-item-text="value.store_title" md-min-length="0" placeholder="Select Store " md-menu-class="autocomplete-custom-template">
                                                         <md-item-template>
                                                            <span class="item-title"> 
                                                            <span> {{value.store_title}} </span> 
                                                            </span> 
                                                         </md-item-template>
                                                      </md-autocomplete>
													   <input type="text" id="store_id" name="store_id" ng-model="store"  value="{{store_id}}" style="display:none;"/>
                                                       
                                                   </div> 
                                                   </div>
												   
                                                </div>
                                                
                                             </div>
                                             <!------------------------------------------------DIV FOR VARIANTS SHOW STARTS HERE-------------------------------------------------->
                                             <div class="row" ng-show="items.type=='item_variants'">
                                                <h3>{{items.title}}</h3>
                                                <div class="col-lg-12" >
                                                   <section class="tabbable" >
                                                      <ul class="nav nav-tabs" >
                                                         <li ng-class="{active: $index == selected}" ng-repeat="tab in items.fields"><a href="#{{tab.title}}" data-toggle="tab"   >{{tab.title}}</a></li>
                                                      </ul>
                                                      <div class="tab-content">
                                                         <div id="{{tab.title}}" class="tab-pane fade in  "  ng-class="{active: $index == selected}"  ng-repeat="tab in items.fields"  >
                                                            <table class="table tableAdd  ">
                                                               <tbody >
                                                                  <tr ng-repeat="value in tab.values  " class="table-row" >
                                                                     <td ng-repeat="field in tab.fields" >
                                                                        <div class="form-group" ng-show="field.type == 'text' && field.identifier=='title'">
                                                                           <label for="{{field.identifier}}"> {{field.title}}</label>
                                                                           <input type="{{field.type}}" class="form-control" name="title{{ $parent.$index }}{{ tab.product_variant_type_id}}" id="title{{ $index }}" ng-model="value.item_variant_value_title"   style="width:80%" placeholder="Enter {{field.title}}"required> 
                                                                        </div>
                                                                        <div class="form-group" ng-show="field.symbol && field.type == 'float' && field.identifier=='price'  ">
                                                                           <label for="{{field.identifier}}"> {{field.title}}</label>
                                                                           <input type="twxt" class="form-control" name="price{{ $parent.$index }}{{ tab.product_variant_type_id}}" id="price{{ $index }}" ng-model="value.item_variant_price"   style="width:80%" placeholder="Enter {{field.title}}"required> 
                                                                        </div>
                                                                        <div class="form-group" ng-show="  field.type == 'float' && field.identifier=='stock_count'  ">
                                                                           <label for="{{field.identifier}}"> {{field.title}}</label>
                                                                           <input type="number" class="form-control" name="stock_count{{ $parent.$index }}{{ tab.product_variant_type_id}}" id="stock_count{{ $index }}" ng-model="value.stock_count"  placeholder="Enter {{field.title}}" style="width:80%"required> 
                                                                        </div>
                                                                        <div class="form-group " ng-show="field.symbol && field.type == 'float' && field.identifier=='price_difference'">
                                                                           <label for="{{field.identifier}}">{{field.title}}   </label>
                                                                           <div class="input-group ">
                                                                              <span class="input-group-addon">{{field.symbol}}</span>
                                                                              <input type="text"    class="form-control" name="price_difference{{ $parent.$index }}{{ tab.product_variant_type_id}}"  id="price_difference{{ $index }}"  ng-model="value.item_variant_price_difference"  placeholder="Enter {{field.title}}" style="width:80%"required> 
                                                                           </div>
                                                                           <!-------->
                                                                        </div>
                                                                        <input type="text"   style="display:none;" class="form-control" name="variant_id{{ $parent.$index }}{{ tab.product_variant_type_id}}"  id="variant_id{{ $index }}"  ng-model="value.item_variant_value_id"   /  > 
                                                                        <input type="text"   style="display:none;" class="form-control" name="product_variant_type_id{{ $parent.$index }}{{ tab.product_variant_type_id}}"  id="product_variant_type_id{{ $index }}"  ng-model="tab.product_variant_type_id"   style="width:80% ;"  > 
                                                                     </td>
                                                                     <td>
                                                                        <label></label><br> 
                                                                        <button ng-click="save_variant($index, tab.product_variant_type_id)"   class="btn md-raised bg-color md-submit md-button md-ink-ripple">Save</button>
                                                                        <button ng-click="delete_variant($index, tab.product_variant_type_id,$parent.$index)" class="btn md-raised md-submit md-button md-ink-ripple">Delete </button>
                                                                     </td>
                                                                  </tr>
                                                               </tbody>
                                                            </table>
                                                            <div class="text-right">
                                                               <button ng-click="add($index,field)" class="btn md-raised bg-color md-add md-button md-ink-ripple"> Add New</button> 
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </section>
                                                </div>
                                             </div>
                                             <!------------------------------------------------DIV FOR VARIANTS SHOW ENDS HERE-------------------------------------------------->
                                          </div>
                                       </div>
									   
									   
									     <!------------------------------------------------DIV FOR ADDITIONAL IMAGES STARTS HERE-------------------------------------------------->
									     <div class="panel">
										    <div class="panel-body">
											  <div class="row">
											     <div class="col-sm-12">
												 <h3>Additional Images</h3>
												      <div class="form-group"  >  
													   <input type="file" id="add_file" style="display:none"/>
                                                          <input type=" " ng-model="add_img" value=" " id="add_img" style="display:none;"/>
														  
														   <div class="col-sm-4 col-md-3 col-lg-2" ng-repeat="img in addtional_images" ng-show="img != ''" style="height: 200px;width:200px display:inline-block;padding:20px;">
														      <img src="<?php echo env('APP_URL')."/images/items/";?>{{img.photo}}" class="img-responsive center-block img-thumbnail" style="max-width: 100%;max-height: 100%; width: 200px;  height: 200px;  object-fit: cover;" >
														   </div>
                                                         <div class="col-sm-4 col-md-3  col-lg-2" style="height: 200px;text-align: center;display:flex;padding:20px;" id="image">
														   <a href="javascript:changeAddImage()" style="text-decoration: none;text-align:center; border: 1px solid whitesmoke ;width: 200px;max-width: 100%;max-height: 100%;   height: 200px;  object-fit: cover;">
                                                      <img src="<?php echo e(URL::asset('admin/assets/images/add.png')); ?>" style="padding-top:40px;">
                                                      </a>&nbsp;&nbsp;
                                                            <i id="loading1" class="fa fa-spinner fa-spin fa-3x fa-fw" style="position: absolute;left: 40%;top: 40%;display: none"></i>
                                                      </div>
                                                      
                                                      <!--<a href="javascript:removeFile()" style="color: red;text-decoration: none;">
                                                      <i class="fa fa-trash-o"></i>
                                                      Remove
                                                      </a>-->
                                                <button ng-click="save_file();" id="save_file" class="btn md-raised bg-color md-add md-button md-ink-ripple" style="display:none;"> Submit</button> 
                                                      </p>
                                                   </div>
												 </div>
											  <div>
										    </div>
										 </div>
									    
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <!--------------------------Angular App Ends ------------------------------------>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
         </div>
      </div>
   </div>
   <!------>
</div>
<script type="text/javascript" src="<?php echo e(URL::asset('admin/angular-controllers/items.js')); ?>"></script> 
<script src="https://code.jquery.com/jquery-3.1.1.min.js"
   integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
<script src="https://use.fontawesome.com/2c7a93b259.js"></script>
<script>
   function changeProfile() {
       $('#file').click();
   }
   $('#file').change(function () {
       if ($(this).val() != '') {
           upload(this);
   
       }
   });
    function upload(img) {
        var form_data = new FormData();
        form_data.append('file', img.files[0]);
        form_data.append('_token', '<?php echo e(csrf_token()); ?>');
        $('#loading1').css('display', 'block');
        $.ajax({
            url: "<?php echo e(url('image-upload-items')); ?>",
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.fail) {
                    $('#preview_image').attr('src', '<?php echo e(asset('images/items/noimage.jpg')); ?>');
                    alert(data.errors['file']);
                }
                else {
                      
					   var data = data.filename;
					 console.log(data);
					 
   $('#item_photo').val(data).trigger("change");
   
   //$("#photo").dispatchEvent(new Event("input", { bubbles: true }));
    
   
               
               //document.getElementById('photo').value=data;
                  $('#preview_image').attr('src', '<?php echo e(asset('images/items')); ?>/' + data);
                }
                $('#loading1').css('display', 'none');
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
                $('#preview_image').attr('src', '<?php echo e(asset('images/items/noimage.jpg')); ?>');
            }
        });
    }
   
   
   
   
   function removeFile() {
       if ($('#photo').val() != '')
           if (confirm('Are you sure want to remove profile picture?')) {
               $('#loading1').css('display', 'block');
               var form_data = new FormData();
               form_data.append('_method', 'DELETE');
               form_data.append('_token', '<?php echo e(csrf_token()); ?>');
               $.ajax({
                   url: "ajax-remove-image-items/" + $('#photo').val(),
                   data: form_data,
                   type: 'POST',
                   contentType: false,
                   processData: false,
                   success: function (data) {
                       $('#preview_image').attr('src', '<?php echo e(asset('images/noimage.jpg')); ?>');
                       $('#photo').val('');
                       $('#loading1').css('display', 'none');
                   },
                   error: function (xhr, status, error) {
                       alert(xhr.responseText);
                   }
               });
           }
   }
   
    
</script>








<script>
   function changeAddImage() {
       $('#add_file').click();
   }
   $('#add_file').change(function () {
       if ($(this).val() != '') {
           upload1(this);
   
       }
   });
    function upload1(img) {
        var form_data = new FormData();
        form_data.append('file', img.files[0]);
        form_data.append('_token', '<?php echo e(csrf_token()); ?>');
        $('#loading1').css('display', 'block');
        $.ajax({
            url: "<?php echo e(url('image-upload-items')); ?>",
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.fail) {
                    $('#add_preview_image').attr('src', '<?php echo e(asset('images/items/noimage.jpg')); ?>');
                    alert(data.errors['file']);
                }
                else {
                      
					   var data = data.filename;
					 console.log(data);
					 
   $('#add_img').val(data).trigger("change");
   
   //$("#photo").dispatchEvent(new Event("input", { bubbles: true }));
    
                   $('#save_file').click();
               
               //document.getElementById('photo').value=data;
                    //$('#add_preview_image').attr('src', '<?php echo e(asset('images/items')); ?>/' + data);
                }
                $('#loading1').css('display', 'none');
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
                $('#add_preview_image').attr('src', '<?php echo e(asset('images/items/noimage.jpg')); ?>');
            }
        });
    }
    
   
   function removeFile() {
       if ($('#photo').val() != '')
           if (confirm('Are you sure want to remove profile picture?')) {
               $('#loading1').css('display', 'block');
               var form_data = new FormData();
               form_data.append('_method', 'DELETE');
               form_data.append('_token', '<?php echo e(csrf_token()); ?>');
               $.ajax({
                   url: "ajax-remove-image-items/" + $('#photo').val(),
                   data: form_data,
                   type: 'POST',
                   contentType: false,
                   processData: false,
                   success: function (data) {
                       $('#add_preview_image').attr('src', '<?php echo e(asset('images/noimage.jpg')); ?>');
                       $('#photo').val('');
                       $('#loading1').css('display', 'none');
                   },
                   error: function (xhr, status, error) {
                       alert(xhr.responseText);
                   }
               });
           }
   }
    
   
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>