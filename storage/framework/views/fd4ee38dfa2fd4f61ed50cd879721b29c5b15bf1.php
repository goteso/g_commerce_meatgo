<?php $__env->startSection('title', 'Edit Coupon' ); ?> 
<link rel="stylesheet" href="<?php echo e(URL::asset('admin/assets/css/custom.css')); ?>">
<style>
   .panel{padding: 20px;}
</style>
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('header'); ?>
<?php echo $__env->make('admin.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldSection(); ?>
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
   <div id="wrapper" >
      <div id="layout-static">
         <!---------- Static Sidebar Starts------->			
         <?php $__env->startSection('sidebar'); ?>
         <?php echo $__env->make('admin.includes.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
         <?php echo $__env->yieldSection(); ?>
         <!---------- Static Sidebar Ends------->
         <div class="static-content-wrapper">
            <section id="main-header">
               <div class="container-fluid">
                  <div class="row">
                     <div class="col-sm-6"   ng-controller="searchController" ng-cloak > 
                        <?php echo $__env->make('admin.includes.realtime-search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                     </div>
					 <?php    $auth_user_type = Auth::user()->user_type;   
                             $auth_user_id =   Auth::id();  
                          ?>
                     <div class="col-sm-12">
                        <div class="text-right">
                        </div>
                        <div class="tab-content" >
                           <!--------------------------- Angular App Starts ---------------------------->
                           <textarea id="res" style="display:none;" ></textarea>
                           <div id="loading" class="loading" style="display:none ;">
                              <img src="<?php echo e(URL::asset('admin/assets/images/89.svg')); ?>" class="img-responsive center-block">				 
                              <p >Calling all data...</p>
                           </div>
                           <!--------------------------Angular App Starts ------------------------------------>
                           <div  ng-controller="editCouponController" ng-cloak>
                              <div class="container-fluid" >
                                 <div class="row">
                                    <div class="col-sm-12" >
                                       <h2 class="header">Edit Coupon</h2>
                                    </div>
                                 </div>
                                 <div class="panel">
                                    <div class="row" >
                                       <div class="col-sm-12" >
                                          <div class="row">
                                             <div class="col-sm-5 col-lg-4">
                                                <div class="img-upload" style="background:#f5f5f5;">
                                                   <div class="form-group text-center"   >
												   
												  <input type="file" id="file" style="display: none  "/>
												   <input type="" id="file_name" style="display:none" ng-model="coupon.coupon_image"/>
                                                      <br> 		 
                                                      <div style="text-align: center;position: relative" id="image">
                                                         <img    class="img-responsive center-block" id="preview_image1"  ng-hide="coupon.coupon_image" src="<?php echo e(asset('admin/assets/images/placeholder.jpg')); ?>"/>
														   <img    class="img-responsive center-block" id="preview_image" ng-hide="!coupon.coupon_image"  src="<?php echo e(asset('images/coupons')); ?>/{{coupon.coupon_image}}" style="padding:0px 20px;"/>
                                                         <i id="loading1" class="fa fa-spinner fa-spin fa-3x fa-fw" style="position: absolute;left: 40%;top: 30%;display: none"></i>
                                                      </div>
                                                        <br><br>
                                                         <p class="text-right">
                                                            <a href="javascript:changeProfile()" title="edit" style="text-decoration: none;background:#e2e2e2;padding:8px;">
                                                            <i class="fa fa-edit"></i> 
                                                            </a>&nbsp;&nbsp;
                                                            <a href="javascript:removeFile()" title="delete" style="color: red;text-decoration: none;background:#e2e2e2;padding:8px;">
                                                            <i class="fa fa-trash-o"></i>
                                                            </a>
                                                         </p> 
                                                      <br>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="col-sm-7 col-lg-8">
                                                <div class="row">
                                                   <div class="col-md-6 col-sm-12">
                                                      <md-input-container  class="md-block text-left" >
                                                         <label>Title</label>
                                                         <input type="text" id="title" name="title"  ng-model="coupon.coupon_title" />
                                                      </md-input-container>
                                                   </div>
                                                   <div class="col-md-6 col-sm-12">
                                                      <md-input-container  class="md-block text-left" >
                                                         <label>Coupon Code</label>
                                                         <input type="text" id="coupon_code" name="coupon_code  "  ng-model="coupon.coupon_code" required />
                                                      </md-input-container>
                                                   </div>
                                                   <div class="col-md-6 col-sm-12">
                                                      <md-input-container  class="md-block text-left" >
                                                         <label>Valid From</label>
                                                         <datepicker
                                                            date-format="yyyy-MM-dd"  
                                                            button-prev='<i class="fa fa-arrow-circle-left"></i>'
                                                            button-next='<i class="fa fa-arrow-circle-right"></i>'>
                                                            <input  type="text"  id="valid_from" name="valid_from"  ng-model="coupon.valid_from">
                                                         </datepicker>
                                                      </md-input-container>
                                                   </div>
                                                   <div class="col-md-6 col-sm-12">
                                                      <md-input-container  class="md-block text-left" >
                                                         <label>Valid Upto (optional)</label>
                                                         <datepicker
                                                            date-format="yyyy-MM-dd"  
                                                            button-prev='<i class="fa fa-arrow-circle-left"></i>'
                                                            button-next='<i class="fa fa-arrow-circle-right"></i>'>
                                                            <input  type="text"  id="expiry" name="expiry"  ng-model="coupon.expiry">
                                                         </datepicker>
                                                      </md-input-container>
                                                   </div>
                                                   <div class="col-md-6 col-sm-12">
                                                      <md-input-container  class="md-block text-left" >
                                                         <label>Discount (in %)</label>
                                                         <input type="number" id="discount" name="discount"  ng-model="coupon.discount" value="{{coupon.discount}}" required />
                                                      </md-input-container>
                                                   </div>
                                                   <div class="col-md-6 col-sm-12">
                                                      <md-input-container  class="md-block text-left" >
                                                         <label>Max. Discount - $ (optional)</label>
                                                         <input type="number" id="max_discount" name="max_discount"  ng-model="coupon.max_discount" value="{{coupon.max_discount}}"/>
                                                      </md-input-container>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="col-md-5 col-sm-12">
                                                <md-input-container  class="md-block text-left" >
                                                   <label>Coupon Desciption</label>
                                                   <textarea id="coupon_desc" name="coupon_desc" rows="5" max-rows="5" ng-model="coupon.coupon_desc" md-no-autogrow> </textarea>
                                                </md-input-container>
                                             </div>
                                             <div class="col-md-7 col-sm-12">
                                                <div class="row" style="margin-top:10px;">
                                                   <div class="col-lg-12 col-sm-12">
                                                      <md-input-container  class="md-block text-left" >
                                                         <label> Total Usage Limit (Leave empty if it can be used unlimited times)</label>
                                                         <input type="number" id="limit_total" name="limit_total"   ng-model="coupon.limit_total" value="{{coupon.limit_total}}" />
                                                      </md-input-container>
                                                   </div>
                                                   <div class="col-lg-12 col-sm-12">
                                                      <md-input-container  class="md-block text-left" >
                                                         <label>Limit per user(Leave empty if there is no user limit)</label>
                                                         <input type="number" id="limit_user" name="limit_user" ng-model="coupon.limit_user"  value="{{coupon.limit_user}}"/>
                                                      </md-input-container>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="col-lg-12 col-sm-12">
                                                <md-input-container  class="md-block text-left" >
                                                   <label>Minimum Order Amount (Leave empty if there is no validation for minimum order value)</label>
                                                   <input type="number" id="minimum_order_amount" name="minimum_order_amount"   ng-model="coupon.minimum_order_amount" value="{{coupon.minimum_order_amount}}"/>
                                                </md-input-container>
                                             </div>
                                             <div class="col-lg-12 col-sm-12">
                                                <md-input-container  class="md-block text-left" >
                                                   <label>Maximum Order Amount (Leave empty if there is no validation for maximum order value)</label>
                                                   <input type="number" id="maximum_order_amount" name="maximum_order_amount" ng-model="coupon.maximum_order_amount" value="{{coupon.maximum_order_amount}}" />
                                                </md-input-container>
                                             </div>
											 
											  <div class="col-lg-6 col-sm-6">
										  
										   <h5>Select Type</h5>
                                                <select class="form-control" id="coupon_type" >
												   <option value="">Select Coupon Type</option>
												   <option value="cash" ng-selected="coupon.type == 'cash'">Cash</option>
												   <option value="points" ng-selected="coupon.type == 'points'">Points</option>
												</select>
                                             </div>
											 
											        <?php
                                          if($auth_user_type == '1' || $auth_user_type == '3' || $auth_user_type == '4')
                                              {
                                                $value = '';
                                                $type='text';
                                               if($auth_user_type == '4')
                                                   {
                                                            $value = @\App\Store::where('manager_id',$auth_user_id)->first(['store_id'])->store_id;
                                                            $type = 'hidden'; 
                                                   }
                                        ?>
                                           
										   
											  <div class="col-sm-6" ng-hide="<?php echo e($auth_user_type); ?>=='4'">
                                                      <h5>Select Store</h5>
                                                      <md-autocomplete ng-disabled="isDisabled" md-no-cache="noCache" md-selected-item="coupon.store_title" md-search-text-change="searchStoreChange(searchStore,coupon)" md-search-text="searchStore" md-selected-item-change="selectedStoreChange(values)" md-items="values in storeSearch(searchStore)" md-item-text="values.store_title" md-min-length="0" placeholder="Select Store " md-menu-class="autocomplete-custom-template"    >
                                                         <md-item-template>
                                                            <span class="item-title"> 
                                                            <span> {{values.store_title}} </span> 
                                                            </span> 
                                                         </md-item-template>
                                                      </md-autocomplete>
													                 <input type="text" id="store_id" name="store_id"  value="<?php echo @$value;?>" style="display:none;"/>
                                                      <br> 
                                                   </div>
												   
                                       
 
                                          <?php                                 
                                            }
                                        ?>
										
                                             <div class="col-lg-12 col-sm-12 submit text-right">
                                                <button   ng-click="updateCoupon(coupon.coupon_id)" class="btn md-raised bg-color md-submit md-button md-ink-ripple">Save Changes</button>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <!--------------------------Angular App Ends ------------------------------------>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
         </div>
      </div>
   </div>
   <!------>
</div>
<script type="text/javascript" src="<?php echo e(URL::asset('admin/angular-controllers/coupons.js')); ?>"></script>
<script src="https://use.fontawesome.com/2c7a93b259.js"></script>
<script>
 
 
    function changeProfile() {
        $('#file').click();
    }
    $('#file').change(function () {
        if ($(this).val() != '') {
            upload(this);

        }
    });
    function upload(img) {
        var form_data = new FormData();
        form_data.append('file', img.files[0]);
        form_data.append('_token', '<?php echo e(csrf_token()); ?>');
        $('#loading1').css('display', 'block');
        $.ajax({
            url: "<?php echo e(url('image-upload-coupons')); ?>",
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.fail) {
                    $('#preview_image').attr('src', '<?php echo e(asset('images/coupons/noimage.jpg')); ?>');
                    alert(data.errors['file']);
                }
                else {
					 var data = data.filename;
					 console.log(data);
                    $('#file_name').val(data); 
                    $('#preview_image').attr('src', '<?php echo e(asset('images/coupons')); ?>/'+data);
					 $('#preview_image1').attr('src', '<?php echo e(asset('images/coupons')); ?>/'+data);

                }
                $('#loading1').css('display', 'none');
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
                $('#preview_image').attr('src', '<?php echo e(asset('images/coupons/noimage.jpg')); ?>');
            }
        });
    }
    function removeFile() {
        if ($('#file_name').val() != '')
            if (confirm('Are you sure want to remove profile picture?')) {
                $('#loading1').css('display', 'block');
                var form_data = new FormData();
                form_data.append('_method', 'DELETE');
                form_data.append('_token', '<?php echo e(csrf_token()); ?>');
                $.ajax({
                    url: "ajax-remove-image-coupons/" + $('#file_name').val(),
                    data: form_data,
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        $('#preview_image').attr('src', '<?php echo e(asset('images/noimage.jpg')); ?>');
                        $('#file_name').val('');
                        $('#loading').css('display', 'none');
                    },
                    error: function (xhr, status, error) {
                        alert(xhr.responseText);
                    }
                });
            }
    }
 
</script> 
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>