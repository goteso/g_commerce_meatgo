<?php $__env->startSection('title', 'Orders Details' ); ?> 
<link rel="stylesheet" href="<?php echo e(URL::asset('admin/assets/css/app.css')); ?>">
 <style>
 .md-autocomplete-suggestions-container{  
   z-index:100000 !important; /* any number of choice > 1050*/
  } 
  .modal-open {
   position
}
   @media(min-width:766px){
   .card{height:330px;  overflow-y: auto;}
   .panel{height:auto;  overflow-y: auto;} 
   } 
   .autocomplete-custom-template li {
   border-bottom: 1px solid #ccc;
   height: auto;
   padding-top: 8px;
   padding-bottom: 8px;
   white-space: normal;
   }
   .autocomplete-custom-template li:last-child {
   border-bottom-width: 0;
   }
   .autocomplete-custom-template .item-title,
   .autocomplete-custom-template .item-metadata {
   display: block;
   line-height: 1.5;
   }
   table>thead>tr>th, table>tbody>tr>td {
   width: 0;}
	
	.StepProgress {
  position: relative;
  padding-left: 30px;
  list-style: none;
}
  .StepProgress::before {
    display: inline-block;
    content: '';
    position: absolute;
    top: 2px;
    left: 0px;
    width: 10px; 
    border-left: 2px solid #CCC;
  }
  
  .StepProgress-item {
    position: relative;
    counter-increment: list;
  }
    .StepProgress-item:not(:last-child) {
      padding-bottom: 10px;
    }
    
    .StepProgress-item-first::before {
      display: inline-block;
      content: '';
      position: absolute;
      left: -30px;
      height: 100%;
      width: 10px;
	    top: 2px;
    }
    
    .StepProgress-item::after {
      content: '';
      display: inline-block;
      position: absolute;
      top: 2px;
      left: -39px;
      width: 20px;
      height: 20px;
      border: 2px solid #CCC;
      border-radius: 50%;
      background-color: #FFF;
    }
    
    
      .StepProgress-item.is-done::before {
        border-left: 2px solid #488de4;
      }
      .StepProgress-item.is-done::after {
        content: "P";
        font-size: 12px;
        color: #FFF;
        text-align: center;
        border: 2px solid #488de4;
        background-color: #488de4;
      }
	  .unassigned .StepProgress-item.StepProgress-item-first.is-done::after {
        /*content: counter(list);*/
		content: "P";
        padding-top: 0px;
        width: 20px;
        height: 20px;
        top: 2px;
        left: -40px;
        font-size: 12px;
        text-align: center;
        color: #488de4;
        border: 2px solid #488de4;
        background-color: white;
      }
    
     
      .StepProgress-item.current::before {
        border-left: 2px solid #488de4;
      }
      
      .StepProgress-item.current::after {
        /*content: counter(list);*/
		content: "D";
        padding-top: 0px;
        width: 20px;
        height: 20px;
        top: 2px;
        left: -40px;
        font-size: 12px;
        text-align: center;
        color: #488de4;
        border: 2px solid #488de4;
        background-color: white;
      }
	  
	  .completed .StepProgress-item.StepProgress-item-last.is-done::after { 
		content: "D"!important;
         font-size: 12px;
        color: #FFF;
        text-align: center;
        border: 2px solid #488de4;
        background-color: #488de4;
      }
    */
   
</style>
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('header'); ?>
<?php echo $__env->make('admin.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldSection(); ?>

<?php $currency_symbol = @\App\Setting::where( 'key_title' , 'currency_symbol' )->first(['key_value'])->key_value;  ?>
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
   <div id="wrapper"  >
      <div id="layout-static">
         <!---------- Static Sidebar Starts------->			
         <?php $__env->startSection('sidebar'); ?>
         <?php echo $__env->make('admin.includes.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
         <?php echo $__env->yieldSection(); ?>
         <!---------- Static Sidebar Ends------->
         <div class="static-content-wrapper"  >
            <section id="main-header">
               <div class="container-fluid">
                  <div class="row">
                     <div class="col-sm-6"  ng-controller="searchController" ng-cloak > 
                        <?php echo $__env->make('admin.includes.realtime-search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                     </div>
                     <div class="col-sm-12">
                        <div class="text-right">
                        </div>
                           <textarea id="res" style="display:  none  ;" ></textarea>
                           <div id="loading" class="loading" style="display:none ;">
                              <img src="<?php echo e(URL::asset('admin/assets/images/89.svg')); ?>" class="img-responsive center-block">				 
                              <p >Calling all data...</p>
                           </div>
						   <div class="tab-content"   ng-controller="orderDetailController" ng-cloak>
                        
                           <!--------------------------- Angular App Starts ---------------------------->
                           <div >
                              <div class="container-fluid" >
                                 <div class="row">
                                    <div class="col-sm-12" >
                                       <h2 class="header">#{{order_id}} Order Details  </h2>
                                    </div>
                                 </div>
                                 <div class="row" >
                                    <div class="col-sm-7 col-md-8 col-lg-9" >
                                       <div class="row">
									   
									      <!-----Block Starts For Items Detail----->
                                          <div class="col-lg-8 col-md-12"  ng-repeat="order in orders" ng-show="order.type=='items'">
                                             <div class="card" ng-show="order.type=='items'" >
                                                <h3 class="block-title" style="float:left;">{{order.title}}</h3> 
                                                <table class="table items-detail">
												   <thead>
												     <th>Item</th>
													 <th>Item Name</th>
													 <th class="text-center">Quantity*Price</th>
													 <th class="text-right">Total</th>
												   </thead>
                                                   <tbody>
                                                      <tr ng-repeat="items in order.data ">
                                                         <td class="item-image">
                                                            <img src="<?php echo e(URL::asset('admin/assets/images/placeholder.jpg')); ?>"> 
                                                         </td>
                                                                     <td class="item-title">
                                                            {{items.item_title}} 
                               <p ng-repeat="v in items.order_item_variant" style="font-size: 13px!important;">{{v.item_variant_title}} : {{v.order_item_variant_value}}</p> 
                                                            <br><span class="item-desc">{{items.unit}}</span>
                                                         </td>
                                                         <td class="item-units text-center">{{items.order_item_quantity}} * {{items.single_quantity_item_price}}</td>
                                                         <td class="item-total text-right"><?php echo @$currency_symbol;?>{{items.item_price }}</td>
                                                         <td></td>
                                                         <td></td>
                                                        
                                                      </tr>
                                                      <tr class="total">
                                                         <td class="" width="40%">Sub Total</td>
                                                         <td></td>
                                                         <td></td>
                                                         <td class="text-right"><?php echo @$currency_symbol;?>{{order.sub_total}} </td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </div>
                                          </div>
										    <!-----Block Ends For Items Detail----->
										 
                                             <!-----Block Starts For Customer Information----->
                                         <div class="col-sm-12 col-md-6 col-lg-4" ng-repeat="order in orders" ng-show="order.type=='customer_details'"> 
                                             <div class="card"  >
                                                <h3 class="block-title">{{order.title}}</h3>
                                                <div ng-repeat="items in order.data ">
                                                   <p class="name"><a href="#"></a>
                                                      {{items.first_name}} {{items.last_name}} 
                                                   </p>
                                                   <p>
                                                   <pre>{{items.email}} </pre>
                                                   </p> 
                                                    <p>
                                                   <pre>{{items.phone}} </pre>
                                                   </p> 
                                                </div>
                                             </div>
											  </div>
                                             <!-----Block Ends For Customer Information----->
											 
											   <!-----Block Starts For Delivery Address ------>
                                         <div class="col-sm-12 col-md-6 col-lg-4" ng-repeat="order in orders" ng-show="order.type=='delivery_address'"> 
                                             <div class="card"  >
                                                <h3 class="block-title">{{order.title}}</h3>
                                                <div ng-repeat="items in order.data ">
                                                   <p class="name"> 
                                                      {{items.address_line1}} 
                                                   </p>
                                                   <p> {{items.address_line2}} </p> 
												     <p> {{items.address_phone}} </p>
													   <p> {{items.city}}, {{items.state}} </p>
													     <p> {{items.country}} </p>
                                                    <p> {{items.pincode}} </p> 
                                                </div>
                                             </div>
											  </div>
                                             <!-----Block Ends For Delivery Address----->






                                      <!-----Block Starts For Loyalty Points Information----->
                                         <div class="col-sm-12 col-md-6 col-lg-4"  ng-repeat="order in orders"  ng-show="order.type=='order_cancel_reasons'" >
 

                                             <div class="card"  >
                                                <h3 class="block-title">{{order.title}}</h3>
                                                <div ng-repeat="d in order.data " style="border-bottom:1px solid #ccc;padding:7px 2px">
                                                   <p class="name"><a href="#"></a>
                                                      <span ><b>{{d.label}}</b></span> </br>
                                                      <span>{{d.value}}</span>
                                                   </p>
                                                </div>
                                             </div>
                                       </div>
                                             <!-----Block Ends For Loyalty Points ----->
											 
										 
											 <!-----Block Starts For Payment Summary----->
											   <div class="col-sm-12 col-md-6 col-lg-4" ng-repeat="order in orders"  ng-show="order.type=='payment_details'" >
											   <div class="card"  >
                                                <h3 class="block-title">{{order.title}}</h3>
                                                <table class="table paymentSummary" >
                                                   <tbody >
                                                      <tr  ng-repeat="items in order.data " style="border-bottom:1px solid #ccc">
                                                         <td class="" style="font-size:14px;text-transform: capitalize">{{items.title.replace('_', ' ')  }} </td>
                                                         <td class="text-right" style="font-size:14px">
                                                            <pre><?php echo @$currency_symbol;?> {{items.value}}</pre>
                                                         </td>
                                                      </tr> 
													  <tr>
													    <td><b>Total</b></td>
														<td  class="text-right"><b><?php echo @$currency_symbol;?> {{order.order_total}}</b></td>
													  </tr>
                                                   </tbody>
                                                </table>
                                               </div>
                                             </div>
											 <!-----Block Ends For Payment Summary----->
											      <!-----Block Starts For Store Information----->
                                         <div class="col-sm-12 col-md-6 col-lg-4" ng-repeat="order in orders" ng-show="order.type=='store_details'"> 
                                             <div class="card"  >
                                                <h3 class="block-title">{{order.title}}</h3>
                                                <div ng-repeat="items in order.data ">
                                                   <p class="name"><a href="#"></a>
                                                      Title : {{items.store_title}}  
                                                   </p>
                                                   <p>
                                                   <pre>Store Manager : {{items.manager_name}} </pre>
                                                   </p> 
                                                    <p>
                                                   <pre>Vendor : {{items.vendor_name}} </pre>
                                                   </p> 
                                                </div>
                                             </div>
											  </div>
                                             <!-----Block Ends For Store Information----->
											  <!-----Block Starts For Meta Summary----->
											 <div class="col-sm-12 col-md-6 col-lg-4" ng-repeat="order in orders"  ng-show="order.type=='order_meta'" >
											   <div class="card"  >
                                                 <h3 class="block-title">{{order.title}}</h3>
                                                 <div ng-repeat="items in order.data" class="ordersummary">
	                                                 <h4 class="order-title">{{items.title}}</h4>
		                                             <p class="order-desc"  >{{items.value}}</p> 
		                                         </div>
                                             </div>
                                          </div>
											 <!-----Block Ends For Meta Summary----->
											 
											   <!-----Block Starts For Task Detail----->
                                         <div class="col-sm-12 col-md-6 col-lg-4" ng-repeat="order in orders" ng-show="order.type=='tasks'"> 
                                             <div class="card"  >
                                                <h3 class="block-title">{{order.title}}</h3>
                                                <div ng-repeat="items in order.data ">
                                                   <table class="table" class="table table-striped" id="exportthis" >
												  
                        <tbody >
                           <tr  >
                              <td class="text-center" style="width:40%;padding-right:10px!important;">
							  <div ng-show="items.driver_details[0].first_name">
                                 <img class="" src="products/{{items.photo}}" style="height:40px;width:40px" ng-hide="!items.photo"> <img class="img-responsive center-block" style="height:40px;width:40px;border-radius:50px;" src="<?php echo e(URL::asset('/admin/assets/images/boy.png')); ?>" ng-show="!items.photo"> 
                                 <p >{{items.driver_details[0].first_name}} <br></p></div>
								 <p ng-show="!items.driver_details[0].first_name"><button class="btn bg-color" ng-click="getDriver(items)">Assign <br>Driver</button></p>
                              </td>
                              <td>
                                 <ul class="StepProgress unassigned">
                                    <li class="StepProgress-item-first StepProgress-item is-done">
                                       <strong>{{items.pickup_contact_title}}  <br>{{items.pickup_time}}</strong>
                                       <p>{{items.pickup_address}}</p>
                                    </li>
                                    <li class="StepProgress-item current">
                                       <strong>{{items.dropoff_contact_title}}  {{items.dropoff_time}}</strong>
                                       <p>{{items.dropoff_address_string}}</p>
                                    </li>
                                 </ul>
                              </td> 
                           </tr>
                        </tbody>
                     </table>


                                                                  </div>
                                             </div>
											  </div>
                                             <!-----Block Ends For Task Detail----->
                                       </div>
									   </div>
									   
 
									   
									     <div class="col-sm-7 col-md-8 col-lg-3" >
										   <div class="row">
										    <div class="col-sm-12"  ng-repeat="order in orders" ng-show="order.type=='admin_buttons'">
											   <div class="card"  >
                          <div style="display:flex">
                                                <h3 style="padding-right:2%" class="block-title">{{order.title}}</h3> 


 <?php $url = $_SERVER['REQUEST_URI'];  $id = substr($url, strrpos( $url , '/') + 1);
$order_status = @\App\Order::where('order_id',$id)->first(['order_status'])->order_status;
 
 ?>
<select class="form-control" id="order_status" name="order_status" style="margin-top:4%" ng-model='this.value' ng-change="updateStatus2(this.value)" >
  <option value="" >Select Status</option>
<?php
$setting_order_status =@\App\SettingOrderStatus::get();


foreach($setting_order_status as $status)
{

   if($order_status == $status['identifier'] ) { $selected = 'ng-selected';} else { $selected = '';}


   echo '<option value="'.$status['identifier'].'"    >'.$status['title'].'</option>';
}
?>
</select>
</div>


												 
												      <table class="table paymentSummary" >
                                                      <tbody >
                                                      <tr  ng-repeat="items in order.data " >
                                                         <td class="text-left" style="font-size:14px;text-transform: capitalize" ><button class="btn md-raised md-add md-button md-ink-ripple" style="background-color: {{items.status_details[0].label_colors}};color:#000;" ng-disabled="items.enabled == '0'" ng-click="updateStatus(items)"> {{items.title}}</button>   </td>
                                                         
                                                      </tr> 
                                                   </tbody>
                                                </table> 
										     </div>
										    </div>
											
											
											
											
											<div class="col-sm-12 " >
									   <div class="  card"> 
										  <h3 class="block-title">Invoice</h3> 
										  <h4>
										  <a href="<?php echo e(URL::asset('invoice')); ?>/{{order_id}}">Print Invoice (a4)</a></h4> 
										  
										  <br>
										 <!-- <table class="table">
										    <tr>
										     <td>  <h5>Update Status</h5></td>
											 <td class="text-right"> <button class="btn btn-raised btn-add bg-color" ng-click="statusChange();">Update</td>
											</tr> 
										  </table>
										  
										    <div >
												<div class="row" ng-show="IsVisible"  >
												<div class="col-sm-9">
									 <md-autocomplete  md-selected-item="selectedItem" md-search-text-change="searchTextChange(searchText)" md-search-text="searchText" md-selected-item-change="selectedItemChange(item, field)" md-items="item in querySearch(searchText)" md-item-text="item.first_name" md-min-length="0" placeholder="Select Driver" md-menu-class="autocomplete-custom-template"  md-autofocus=""  id="customer_id" style="min-width:100px;"> 
											<md-item-template>
											  <span class="item-title"> 
												<span> {{item.first_name}} </span>
												 <span > {{item.last_name}} </span>
											  </span>
											  <span class="item-metadata">
												<span> {{item.mobile}} </span>  
											  </span>
											</md-item-template>
										  </md-autocomplete>
										  </div>
										  <div class="col-sm-3">
									 <input type="text" ng-model="customer_id_value"  id="driver_id" style="display:none;">
										   
										  <button class="btn btn-info" ng-click="submitDriverId()">Save</button>	
										  </div>
										  </div>
										  </div>-->
										  </div>
									   </div>
									</div> 
											 
										   </div>
										 </div>
                                    </div> 
									
									
									
									
									</div>
									
									
									
									 <!-----------------------------------------------DRIVER ASSIGN MODAL STARTS HERE-------------------------------------------------------------------->
									  
   <div id="driverModal" class="modal fade" data-backdrop="false" style="z-index:9999" role="dialog">
      <div class="modal-dialog modal-lg category-add"  >
         <!-- Modal content-->
         <div class="modal-content "   >
            <div class="modal-header  "  >
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title text-uppercase"  >Assign <b> Driver</b></h4>
            </div>
            <div class="modal-body "  >
               <div class="row"   > 
                  <div class="col-sm-7">
                     <h5>Select Driver</h5>
					   <md-autocomplete ng-disabled="isDisabled" md-no-cache="noCache" md-selected-item="selectedItem" md-search-text-change="searchDriverChange(searchText)" md-search-text="searchText" md-selected-item-change="selectedDriverChange(data)" md-items="data in driverSearch(searchText)" md-item-text="data.first_name" md-min-length="0" placeholder="Select Driver"  md-menu-class="autocomplete-custom-template">
                        <md-item-template>
                            <span class="item-title"> 
                                   <span> {{data.first_name}} </span> 
                             </span> 
                        </md-item-template>
                     </md-autocomplete>
						<input type="hidden" id="driver_id" name="driver_id" ng-model="driver_id" value="{{driver_id}}" />
                     <br><br><br><br> 
                     </div>
                      <div class="col-sm-5">					 
                     <md-button ng-click="assignDriver()" class="md-raised bg-color md-submit">Assign</md-button>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
    <!-----------------------------------------------DRIVER ASSIGN MODAL ENDS HERE-------------------------------------------------------------------->
	
	
	
								 
								                       <!-----------------------------------------------DRIVER ASSIGN MODAL STARTS HERE-------------------------------------------------------------------->
                    
   <div id="cancel_reason_model" class="modal fade" data-backdrop="false" style="z-index:9999" role="dialog">
      <div class="modal-dialog modal-lg category-add"  >
         <!-- Modal content-->
         <div class="modal-content "   >
            <div class="modal-header  "  >
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title text-uppercase"  >Enter Reason <b> Below</b></h4>
            </div>
            <div class="modal-body "  >
               <div class="row"   > 
                  <div class="col-sm-7">
                     <h5>Enter Cancel Reason</h5>

                     <select id="order_cancel_reason" class="form-control">
                        <option value="">Select Cancel Reason</option>';
                     <?php $order_cancel_reasons =  @\App\OrderCancelReasons::get(); 

                      foreach($order_cancel_reasons  as $reason )
                      {
                        $title = $reason['title'];
                        echo '<option value="'.$title.'">'.$title.'</option>';
                      }
                     ?>
                   </select>
                   <br>
                     <input type="hidden" id="order_cancel_action" value="cancelled"> 
                     </div>
                      <div class="col-sm-5">           
                     <md-button ng-click="updateStatusCancelled()" class="md-raised bg-color md-submit" style="bottom :-20px">Submit</md-button>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
    <!-----------------------------------------------DRIVER ASSIGN MODAL ENDS HERE-------------------------------------------------------------------->
	
									
                                 </div>
                              </div>   
                           </div>
                              <!--------------------------- Angular App Ends ---------------------------->  
                        </div>
                     </div>
                  </div>
               </div> 
         </section>
		 
		   </div>
      </div>
   </div>
</div>
<script type="text/javascript" src="<?php echo e(URL::asset('admin/angular-controllers/ordersDetail.js')); ?>"></script>  
<!------>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>