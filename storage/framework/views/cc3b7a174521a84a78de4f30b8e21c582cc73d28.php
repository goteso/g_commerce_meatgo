 
 <div class="static-sidebar-wrapper sidebar-default">
   <div class="static-sidebar">
      <div class="sidebar">
        <div class="widget stay-on-collapse" id="widget-sidebar">
            <nav role="navigation" class="widget-body">
               <ul class="acc-menu"> 
               
               <?php @$auth_user_type = @Auth::user()->user_type;   
                     @$auth_user_id =   @Auth::id();
                     App::setLocale('settings');  
                     if($auth_user_id =='' || $auth_user_id == null)
                     {
                           Auth::logout();
                           echo '<script>document.getElementById("logout").click();</script>';
                     }

                     if($auth_user_type == '4')
                     {
                     	$auth_store_id = @\App\Store::where('manager_id',$auth_user_id)->first(['store_id'])->store_id;
                     }
                     else
                     {
                     	$auth_store_id = '';
                     }
                ?>
                  <input type="hidden" id="auth_user_type" value="<?php echo $auth_user_type;?>">
                  <input type="hidden" id="auth_user_id" value="<?php echo $auth_user_id;?>">
                  <input type="hidden" id="auth_store_id" value="<?php echo $auth_store_id;?>">

   
				    <li><a href="<?php echo e(URL::to('/v1/dashboard_data')); ?>"><i class="fas fa-tachometer-alt"></i><span>Dashboard  </span></a></li>
 	         

  
                    <?php if(trans('permission'.$auth_user_type.'.view_orders') == '1'): ?>
				    <li><a href="<?php echo e(URL::to('v1/orders')); ?>"><i class="fa fa-shopping-cart"></i><span>Orders</span></a></li>
				   <?php endif; ?>


				   <?php if(trans('permission'.$auth_user_type.'.view_logistics') == '1'): ?>
			       <li><a href="<?php echo e(URL::to('v1/logistics')); ?>"><i class="fa fa-truck"></i><span>Logistics</span></a></li>
		           <?php endif; ?>


				    <?php if(trans('permission'.$auth_user_type.'.view_items') == '1'): ?>
				    <li><a href="<?php echo e(URL::to('v1/items')); ?>"><i class="fa fa-list"></i><span>Items</span></a></li>
				    <?php endif; ?>


				    <?php if(trans('permission'.$auth_user_type.'.view_stores') == '1'): ?>
				    <li><a href="<?php echo e(URL::to('v1/stores')); ?>"><i class="fa fa-home"></i><span>Stores</span></a></li>
				    <?php endif; ?>
					
					
				    <!--<li><a href="#"><i class="fa fa-money"></i><span>Accounting</span></a></li>-->
					  

					<?php if(trans('permission'.$auth_user_type.'.view_coupons') == '1'): ?>
				    <li><a href="<?php echo e(URL::to('v1/coupons#/')); ?>"><i class="fas fa-ticket-alt"></i><span>Coupons</span></a></li>
					<?php endif; ?>


					<?php if(trans('permission'.$auth_user_type.'.view_customers') == '1'): ?>   
				    <li><a href="<?php echo e(URL::to('v1/customers')); ?>"><i class="fa fa-user"></i><span>Customers</span></a></li>
					<?php endif; ?>   


                <?php $vendor_plugins = @\App\Setting::where('key_title','vendors')->first(['key_value'])->key_value;
                 if($vendor_plugins == 1)
                   { ?>
					<?php if(trans('permission'.$auth_user_type.'.view_vendors') == '1'): ?>   
				    <li><a href="<?php echo e(URL::to('v1/vendors')); ?>"><i class="fa fa-user"></i><span>Vendors</span></a></li>
					<?php endif; ?>
				<?php } ?>
 
					
					  <?php if(trans('permission'.$auth_user_type.'.view_stores') == '1'): ?>
				    <li><a href="<?php echo e(URL::to('v1/reviews')); ?>"><i class="fa fa-home"></i><span>Reviews</span></a></li>
				    <?php endif; ?>


		    <?php if(trans('permission'.$auth_user_type.'.view_order_cancel_reasons') == '1'): ?>
				    <li><a href="<?php echo e(URL::to('v1/order-cancel-reasons')); ?>"><i class="fa fa-question-circle"></i><span>Order Cancel Reasons</span></a></li>
			    <?php endif; ?>


		  
                  <?php if(trans('permission'.$auth_user_type.'.send_mass_notifications') == '1'): ?>
				   <li style="padding-right:0;margin-left:0">
                     <a href="javascript:;"><i class="fa fa-bell"></i> <span>Send Notifications</span></a>	
		                     <ul class="acc-menu" style="padding-right:0;margin-left:0">
		                                 <li style="padding-right:0;margin-left:0"><a href="<?php echo e(URL::to('v1/emails-to-customers')); ?>"><i style="font-size:14px" class="fa fa-envelope"></i><span style="font-size:14px"> Emails to Customers</span></a></li>
						    			 <li style="padding-right:0;margin-left:0"><a href="<?php echo e(URL::to('v1/push-to-customers')); ?>"><i style="font-size:14px" class="fa fa-bell"></i><span style="font-size:14px"> Push Notifications</span></a></li>
						   				 <li style="padding-right:0;margin-left:0"><a href="<?php echo e(URL::to('v1/emails-to-stores')); ?>"><i style="font-size:14px" class="fa fa-envelope"></i><span style="font-size:14px"> Emails to Stores</span></a></li>
		                     </ul>
                  </li>
                  <?php endif; ?>







					<li><a href="<?php echo e(URL::to('v1/reports')); ?>"><i class="fa fa-file"></i><span>Reports</span></a></li>
						 
					<!--<li><a href="#"><i class="fa fa-tasks"></i><span>Roles</span></a></li>-->
			 
				    <li><a href="<?php echo e(URL::to('settings')); ?>"><i class="fa fa-wrench"></i><span>Settings</span></a></li>
					
					<?php if(trans('permission'.$auth_user_type.'.approve_edit_request') == '1'): ?>
					 <li><a href="<?php echo e(URL::to('v1/store-requests')); ?>"><i class="fa fa-wrench"></i><span>Approval Requests</span></a></li>
					<?php endif; ?>

               </ul>
            </nav>
         </div>
      </div>
   </div>
</div>