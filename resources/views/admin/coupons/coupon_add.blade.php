@extends('admin.layout.auth')
@section('title', 'Add Coupon' ) 
<link rel="stylesheet" href="{{ URL::asset('admin/assets/css/custom.css')}}">
<style>
   .panel{padding: 20px;}
</style>
@section('content')
@section('header')
@include('admin.includes.header')
@show

    
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
   <div id="wrapper" >
      <div id="layout-static">
         <!---------- Static Sidebar Starts------->			
         @section('sidebar')
         @include('admin.includes.sidebar')
         @show
         <!---------- Static Sidebar Ends------->
         <div class="static-content-wrapper">
            <section id="main-header">
               <div class="container-fluid">
                  <div class="row">
                     <div class="col-sm-6"   ng-controller="searchController" ng-cloak > 
                        @include('admin.includes.realtime-search')
                     </div>
					 
					   <?php    $auth_user_type = Auth::user()->user_type;   
                             $auth_user_id =   Auth::id();  
                          ?>
                     <div class="col-sm-12">
                        <div class="text-right">
                        </div>
                        <div class="tab-content" >
                           <!--------------------------- Angular App Starts ---------------------------->
                           <textarea id="res" style="display:  none  ;" ></textarea>
                           <div id="loading" class="loading" style="display:none ;">
                              <img src="{{URL::asset('admin/assets/images/89.svg')}}" class="img-responsive center-block">				 
                              <p >Calling all data...</p>
                           </div>
                           <!--------------------------Angular App Starts ------------------------------------>
                           <div  ng-controller="addCouponController" ng-cloak>
                              <div class="container-fluid" >
                                 <div class="row">
                                    <div class="col-sm-12" >
                                       <h2 class="header">Add Coupon</h2>
                                    </div>
                                 </div>
                                 <div class="panel">
                                    <div class="row" >
                                       <div class="col-sm-12" >
                                          <div class="row">
                                             <div class="col-sm-5 col-lg-4">
											 
                                               <div class="img-upload" style="background:#f5f5f5;max-width:350px;">
												<div class="form-group text-center"   >
												
												  <input type="file" id="file" style="display:none "/>
												   <input type="hidden" id="file_name" style="display:none "/>
												   		 
												   <div style="text-align: center;position: relative" id="image">
													  <img style="padding:0px;width:100%" class="img-responsive center-block"  id="preview_image" src="{{asset('admin/assets/images/placeholder.jpg')}}"/>
													  <i id="loading1" class="fa fa-spinner fa-spin fa-3x fa-fw" style="position: absolute;left: 40%;top: 40%;display: none"></i>
												   </div>
												    </div>
                                                  </div>
												   <div class="text-right" style="font-size: ;width:350px;" >
                                                      <a href="javascript:changeProfile()" title="edit" style="text-decoration: none;background:#e2e2e2;padding:8px; position: relative; bottom:45px; ">
                                                      <i class="fa fa-edit"></i> 
                                                      </a>&nbsp;&nbsp;
                                                      <!-- <a href="javascript:removeFile()" title="delete" style="color: red;text-decoration: none;background:#e2e2e2;padding:8px; position:absolute;bottom: 50px;right:20px ">
                                                         <i class="fa fa-trash-o"></i>
                                                         </a>  -->
                                                   </div>
												  
												</div>
											

 
                                             <div class="col-sm-7 col-lg-8">
                                                <div class="row">
                                                   <div class="col-md-6 col-sm-12">
                                                      <md-input-container  class="md-block text-left" >
                                                         <label>Title</label>
                                                         <input type="text" id="title" name="title"   />
                                                      </md-input-container>
                                                   </div>
                                                   <div class="col-md-6 col-sm-12">
                                                      <md-input-container  class="md-block text-left" >
                                                         <label>Coupon Code</label>
                                                         <input type="text" id="coupon_code" name="coupon_code  " required />
                                                      </md-input-container>
                                                   </div>
                                                   <div class="col-md-6 col-sm-12">
                                                      <md-input-container  class="md-block text-left" >
                                                         <label>Valid From</label>
                                                         <datepicker
                                                            date-format="yyyy-MM-dd"  
                                                            button-prev='<i class="fa fa-arrow-circle-left"></i>'
                                                            button-next='<i class="fa fa-arrow-circle-right"></i>'>
                                                            <input  type="text"  id="valid_from" name="valid_from">
                                                         </datepicker>
                                                      </md-input-container>
                                                   </div>
                                                   <div class="col-md-6 col-sm-12">
                                                      <md-input-container  class="md-block text-left" >
                                                         <label>Valid Upto(optional)</label>
                                                         <datepicker
                                                            date-format="yyyy-MM-dd" date-min-limit="@{{today}}"  
                                                            button-prev='<i class="fa fa-arrow-circle-left"></i>'
                                                            button-next='<i class="fa fa-arrow-circle-right"></i>'>
                                                            <input  type="text"  id="expiry" name="expiry"  >
                                                         </datepicker>
                                                      </md-input-container>
                                                   </div>
                                                   <div class="col-md-6 col-sm-12">
                                                      <md-input-container  class="md-block text-left" >
                                                         <label>Discount (in %)</label>
                                                         <input type="number" id="discount" name="discount"  required />
                                                      </md-input-container>
                                                   </div>
                                                   <div class="col-md-6 col-sm-12">
                                                      <md-input-container  class="md-block text-left" >
                                                         <label>Max. Discount - $ (optional)</label>
                                                         <input type="number" id="max_discount" name="max_discount" />
                                                      </md-input-container>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
										  
										  
                                          <div class="row">
                                             <div class="col-md-5 col-sm-12">
                                                <md-input-container  class="md-block text-left" >
                                                   <label>Coupon Desciption</label>
                                                   <textarea id="coupon_desc" name="coupon_desc" rows="5" max-rows="5"  md-no-autogrow > </textarea>
                                                </md-input-container>
												
											 
                                             </div> 
											 
											  <div class="col-md-7 col-sm-12">
											   
											  <div class="row" style="margin-top:10px;">
                                             <div class="col-lg-12 col-sm-12">
                                                <md-input-container  class="md-block text-left" >
                                                   <label> Total Usage Limit (Leave empty if it can be used unlimited times)</label>
                                                   <input type="number" id="limit_total" name="limit_total"   />
                                                </md-input-container>
                                             </div>
                                             <div class="col-lg-12 col-sm-12">
                                                <md-input-container  class="md-block text-left" >
                                                   <label>Limit per user(Leave empty if there is no user limit)</label>
                                                   <input type="number" id="limit_user" name="limit_user" />
                                                </md-input-container>
                                             </div>
											 </div>
											 </div>
											 </div>
											 
											 
											 <div class="row">
                                             <div class="col-lg-12 col-sm-12">
                                                <md-input-container  class="md-block text-left" >
                                                   <label>Minimum Order Amount (Leave empty if there is no validation for minimum order value)</label>
                                                   <input type="number" id="minimum_order_amount" name="minimum_order_amount"   />
                                                </md-input-container>
                                             </div>
                                             <div class="col-lg-12 col-sm-12">
                                                <md-input-container  class="md-block text-left" >
                                                   <label>Maximum Order Amount (Leave empty if there is no validation for maximum order value)</label>
                                                   <input type="number" id="maximum_order_amount" name="maximum_order_amount" />
                                                </md-input-container>
                                             </div>
                                          

                                           <div class="col-lg-6 col-sm-6">
										  
										   <h5>Select Type</h5>
                                                <select class="form-control" id="coupon_type">
											  <option value="">Select Coupon Type</option>
												   <option value="cash">Cash</option>
												   <option value="points">Points</option>
												</select>
                                             </div>



                                        <?php
                                          if($auth_user_type == '1' || $auth_user_type == '3' || $auth_user_type == '4')
                                              {
                                                $value = '';
                                                $type='text';
                                               if($auth_user_type == '4')
                                                   {
                                                            $value = @\App\Store::where('manager_id',$auth_user_id)->first(['store_id'])->store_id;
                                                            $type = 'hidden'; 
                                                   }
                                        ?>
                                           
										   
											  <div class="col-sm-6" ng-hide="{{$auth_user_type}}=='4'">
                                                      <h5>Select Store</h5>
                                                      <md-autocomplete ng-disabled="isDisabled" md-no-cache="noCache" md-selected-item="selectedStore" md-search-text-change="searchStoreChange(searchStore)" md-search-text="searchStore" md-selected-item-change="selectedStoreChange(values, field)" md-items="values in storeSearch(searchStore)" md-item-text="values.store_title" md-min-length="0" placeholder="Select Store " md-menu-class="autocomplete-custom-template"    >
                                                         <md-item-template>
                                                            <span class="item-title"> 
                                                            <span> @{{values.store_title}} </span> 
                                                            </span> 
                                                         </md-item-template>
                                                      </md-autocomplete>
													  <input type="text" id="store_id" name="store_id"    value="<?php echo @$value;?>" style="display:none;"/>
                                                      <br> 
                                                   </div>
												   
                                       
 
                                          <?php                                 
                                            }
                                        ?>




   <div class="col-lg-12 col-sm-12 submit text-right">
                                                <button   ng-click="storeCoupon()" class="btn md-raised bg-color md-submit md-button md-ink-ripple">Add Coupon</button>
                                             </div>
                                          </div>

                                          
                                          

                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!--------------------------Angular App Ends ------------------------------------>
                     </div>
                  </div>
               </div>
         </div>


         </section>
      </div>
   </div>
</div>
<!------>
</div>
<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers/coupons.js')}}"></script> 
<!-- JavaScripts --> 
<script src="https://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
<script src="https://use.fontawesome.com/2c7a93b259.js"></script>
<script>
 
 
    function changeProfile() {
        $('#file').click();
    }
    $('#file').change(function () {
        if ($(this).val() != '') {
            upload(this);

        }
    });
    function upload(img) {
        var form_data = new FormData();
        form_data.append('file', img.files[0]);
        form_data.append('_token', '{{csrf_token()}}');
        $('#loading1').css('display', 'block');
        $.ajax({
            url: "{{url('image-upload-coupons')}}",
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.fail) {
                    $('#preview_image').attr('src', '{{asset('images/coupons/noimage.jpg')}}');
                    alert(data.errors['file']);
                }
                else {
					 var data = data.filename;
					 console.log(data);
                    $('#file_name').val(data);
					 $('#item_photo').val(data); 
                    $('#preview_image').attr('src', '{{asset('images/coupons')}}/'+data);

                }
                $('#loading1').css('display', 'none');
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
                $('#preview_image').attr('src', '{{asset('images/coupons/noimage.jpg')}}');
            }
        });
    }
    function removeFile() {
        if ($('#file_name').val() != '')
            if (confirm('Are you sure want to remove profile picture?')) {
                $('#loading1').css('display', 'block');
                var form_data = new FormData();
                form_data.append('_method', 'DELETE');
                form_data.append('_token', '{{csrf_token()}}');
                $.ajax({
                    url: "ajax-remove-image/" + $('#file_name').val(),
                    data: form_data,
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        $('#preview_image').attr('src', '{{asset('images/noimage.jpg')}}');
                        $('#file_name').val('');
                        $('#loading1').css('display', 'none');
                    },
                    error: function (xhr, status, error) {
                        alert(xhr.responseText);
                    }
                });
            }
    }
 
</script>
@endsection