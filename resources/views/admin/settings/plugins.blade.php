@extends('admin.layout.auth')
@section('title', 'Plugins' ) 
<link rel="stylesheet" href="{{ URL::asset('admin/assets/css/custom.css')}}">
<style>
table>tbody>tr>td{border-bottom:1px solid #f5f5f5;}
</style>
@section('content')
@section('header')
@include('admin.includes.header')
@show
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
   <div id="wrapper" >
      <div id="layout-static">
         <!---------- Static Sidebar Starts------->			
         @section('sidebar')
         @include('admin.includes.sidebar')
         @show
         <!---------- Static Sidebar Ends------->
         <div class="static-content-wrapper"  >
		 
<section id="main-header">
   <div class="container-fluid">
      <div class="row">
         <div class="col-sm-6"  ng-controller="searchController" ng-cloak > 
            @include('admin.includes.realtime-search')
         </div>
         <div class="col-sm-12">
            <div class="text-right">
            </div>
            <div class="tab-content"  ng-controller="pluginController" ng-cloak  >
               <!--------------------------- Angular App Starts ---------------------------->
               <textarea id="res" style="display:  none  ;" ></textarea>
                  <div id="loading" class="loading" style="display:none ;">
                  <img src="{{URL::asset('admin/assets/images/89.svg')}}" class="img-responsive center-block">				 
                  <p >Calling all data...</p>
                  </div>   
               <div class="container-fluid" >
                  <div class="row">
                     <div class="col-sm-10" >
                        <h2 class="header">Plugins</h2>
                     </div>
                     <div class="col-sm-2">
                        <div class="text-right">  
                        </div>
                     </div>
                  </div>
                  <div class="row" >
                     <div class="col-sm-12" >
                        <div class="" >
                           <div id="tableToExport" class="products-table table-responsive"  >
                               <table class="table   tableAdd">	
					            <tbody >
  
						 <tr  class=" border-bottom:1px solid #f5f5f5;" >
							 <td style="border-top:0px;" > 
							 
							<img src="{{URL::asset('admin/assets/images/placeholder.jpg')}}" style="width:60px;height:60px;">
											 </td  >   
											 <td style="border-top:0px;text-align:left"  >
										     <h4>Order Update SMS</h4>
					                         <p>Send text sms updates to customer, whenever there is new activity on the order.</p>
											  </td>   
											 <td style="border-top:0px;" > 
											  <md-switch ng-model="switch" aria-label="Switch 1">
                                                 
                                              </md-switch>
											 </td>
						 </tr> 
					  </tbody>
				  </table>
                           </div> 
                        </div>
                     </div>
                     
                  </div>
               </div>
           
            <!--------------------------- Angular App Ends ---------------------------->
         
   
   
   
	 </div>
	</div>
      </div>
   </div>
	
</section>

 </div>
   </div>
   </div>
</div>
<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers/plugins.js')}}"></script> 
@endsection