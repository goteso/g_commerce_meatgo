@extends('admin.layout.auth')
@section('title', 'Tax' ) 
<link rel="stylesheet" href="{{ URL::asset('admin/assets/css/custom.css')}}">
@section('content')
@section('header')
@include('admin.includes.header')
@show
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
   <div id="wrapper" >
      <div id="layout-static">
         <!---------- Static Sidebar Starts------->			
         @section('sidebar')
         @include('admin.includes.sidebar')
         @show
         <!---------- Static Sidebar Ends------->
         <div class="static-content-wrapper"  >
		 
<section id="main-header">
   <div class="container-fluid">
      <div class="row">
         <div class="col-sm-6"  ng-controller="searchController" ng-cloak > 
            @include('admin.includes.realtime-search')
         </div>
         <div class="col-sm-12">
            <div class="text-right">
            </div>
            <div class="tab-content"  ng-controller="taxController as ctrl" ng-cloak  >
               <!--------------------------- Angular App Starts ---------------------------->
               <textarea id="res" style="display:  none  ;" ></textarea>
                  <div id="loading" class="loading" style="display:none ;">
                  <img src="{{URL::asset('admin/assets/images/89.svg')}}" class="img-responsive center-block">				 
                  <p >Calling all data...</p>
                  </div>   
               <div class="container-fluid" >
                  <div class="row">
                     <div class="col-sm-10" >
                        <h2 class="header">Tax</h2>
                     </div>
                     <div class="col-sm-2">
                        <div class="text-right">  
                        </div>
                     </div>
                  </div>
                  <div class="row" >
                     <div class="col-sm-12" >
                        <div class="panel" >
                           <div id="tableToExport" class="products-table table-responsive panel-body"  > 
                               <table class="table   tableAdd">	
					 <tbody >
  
						 <tr  class=" " ng-repeat="data in ctrl.tax.data"  >
							 <td style="border-top:0px;"> 
							  <input type="hidden" id="setting_tax_id" name="setting_tax_id@{{ $index }}@{{ data.setting_tax_id}}" style="width:80%" value="@{{data.setting_tax_id}}"   />
							 <md-input-container  class="md-block text-left"  >
                        <label>Title</label>
                        <input type="text" id="title" name="title@{{ $index }}@{{ data.setting_tax_id}}" style="width:80%" value="@{{data.title}}" required />
                     </md-input-container>
					 
											 </td  >   
											 <td style="border-top:0px;"   >
											  <md-input-container  class="md-block text-left" >
                        <label>value (%)</label>
                        <input type="text" id="percentage" name="percentage@{{ $index }}@{{ data.setting_tax_id}}" style="width:80%"  value="@{{data.percentage}}" required />
                     </md-input-container>
					 
											  </td>   
											 <td style="border-top:0px;" > 
											  <button ng-click="ctrl.updateTax($index,data.setting_tax_id)"   class="btn md-raised bg-color md-add md-button md-ink-ripple ">Save</button>
											 </td>
						 </tr>
						 
					 
					  </tbody>
				  </table>
                           </div> 
                        </div>
                     </div>
                     
                  </div>
               </div>
           
            <!--------------------------- Angular App Ends ---------------------------->
         
   
    
	 </div>
	</div>
      </div>
   </div>
	
</section>

 </div>
   </div>
   </div>
</div>
<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers/tax.js')}}"></script> 
@endsection