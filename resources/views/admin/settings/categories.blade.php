@extends('admin.layout.auth')
@section('title', 'Categories' ) 
<link rel="stylesheet" href="{{ URL::asset('admin/assets/css/custom.css')}}">
 <style>
 .md-autocomplete-suggestions-container{  
   z-index:100000 !important; /* any number of choice > 1050*/
  }
 </style>
@section('content')
@section('header')
@include('admin.includes.header')
@show
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
   <div id="wrapper" >
      <div id="layout-static">
         <!---------- Static Sidebar Starts------->     
         @section('sidebar')
         @include('admin.includes.sidebar')
         @show
         <!---------- Static Sidebar Ends------->
         <div class="static-content-wrapper"  >
     
<section id="main-header">
   <div class="container-fluid">
      <div class="row">
         <div class="col-sm-6"   ng-controller="searchController" ng-cloak > 
            @include('admin.includes.realtime-search')
         </div>
       <?php          $auth_user_type = Auth::user()->user_type;   
                     $auth_user_id =   Auth::id();  
 
                 ?>
         <div class="col-sm-12">
            <div class="text-right">
            </div>
       <input type="file" id="file" style="display:none "/>
            <div class="tab-content"  ng-controller="categoryController as ctrl" ng-cloak  >
               <!--------------------------- Angular App Starts ---------------------------->
               <textarea id="res" style="display: none  ;" ></textarea>
                  <div id="loading" class="loading" style="display:none ;">
                  <img src="{{URL::asset('admin/assets/images/89.svg')}}" class="img-responsive center-block">         
                  <p >Calling all data...</p>
                  </div>   
               <div class="container-fluid" >
                  <div class="row">
                     <div class="col-sm-7" >
                        <h2 class="header">Categories</h2>
                     </div>
                         <div class="col-sm-5">
 
                        <div class="text-right">
                              <md-autocomplete  md-selected-item="store_title" md-search-text-change="searchFilterStoreChange(searchText)" md-search-text="searchText" md-selected-item-change="selectedFilterStoreChange(store, field)" md-items="store in storeSearchFilter(searchText)" md-item-text="store.store_title" md-min-length="0" placeholder="Select Store" md-menu-class="autocomplete-custom-template"  md-autofocus=""  id="filter_store_id" style="min-width:100px;"> 
                      <md-item-template>
                        <span class="item-title"> 
                        <span> @{{store.store_title}} </span> 
                        </span>
                        <span class="item-metadata">
                        <span> @{{item.mobile}} </span>  
                        </span>
                      </md-item-template>
                      </md-autocomplete>
                        </div>
 

                     </div>
                    <br>
                  </div>
                  <div class="row " >
                    
                     <div class="col-sm-12 " >
                        <div class="panel" > 
                               <div class="row " >
                    
                     <div class="col-sm-12 " >
                        <div class="text-right"> 
                           <button type="button" class="btn md-raised bg-color md-add md-button md-ink-ripple text-right"  data-toggle="modal" data-target="#add">Add New</button> 
                        </div>

                   </div>
                    <div class="col-sm-12 " >
                  
                           <div id="tableToExport" class="products-table table-responsive"  >
                              <table class="table" class="table table-striped" id="exportthis" >
                                 <thead>
                                    <tr>
                                       <th>ID</th>
                                       <th>CATEGORY NAME</th>
                     <th>CATEGORY PHOTO</th>
                                       <th>CREATED</th>
                            <th id="action">ACTION</th>  
                                    </tr>
                                 </thead>
                                 <tbody>
                                    
                                    <tr ng-repeat="values in ctrl.categories.data.data  ">
                                       <td>#@{{values.category_id}}</td>
                                       <td>@{{values.category_title}}</td>
                     <td><img style="height:40px;width:40px" class="img-responsive center-block" ng-hide="!values.category_photo" src="{{URL::asset('/images/categories')}}/@{{values.category_photo}}" />
                       <img  style="height:40px;width:40px" class="img-responsive center-block" ng-show="!values.category_photo" src="{{URL::asset('/admin/assets/images/placeholder.jpg')}}" /></td>
                                       <td>@{{values.created_at_formatted}}</td>
                                      <td>
                                          <a class="btn btn-xs edit-product" ng-click="ctrl.editCategory(values, $index)" ><i class="fa fa-edit"></i></a>
                                          <a class="btn btn-xs delete-product" ng-click="ctrl.deleteCategoryId(values.category_id, $index)"><i class="fa fa-trash"></i></a>               
                                       </td>
                                 
                                    </tr>
                                    
                                 </tbody>
                              </table>
                           </div>
                           </div> 
                         </div>
                <div class="pagination text-right" style="display:block" >
                                           <button class="btn"  ng-show="ctrl.categories.data.first_page_url != null" ng-click="ctrl.pagination(ctrl.categories.data.first_page_url);">First</button> 
                                           <button class="btn"  ng-show="ctrl.categories.data.prev_page_url != null" ng-click="ctrl.pagination(ctrl.categories.data.prev_page_url);">Previous</button> 
                                          <span>@{{ctrl.categories.data.current_page}}</span>
                                          <button class="btn"  ng-show="ctrl.categories.data.next_page_url != null" ng-click="ctrl.pagination(ctrl.categories.data.next_page_url);">Next</button> 
                                          <button class="btn"  ng-show="ctrl.categories.data.last_page_url != null" ng-click="ctrl.pagination(ctrl.categories.data.last_page_url);">Last</button> 
                                       </div>
                     
                           
                        </div>
                     </div>
                     
                  </div>
               </div>
           
            <!--------------------------- Angular App Ends ---------------------------->
         
   
   
   
   <!-----------------------------------------------CATEGORY ADD MODAL STARTS HERE-------------------------------------------------------------------->
   <div id="add" class="modal fade" data-backdrop="false" style="z-index:9999" role="dialog">
      <div class="modal-dialog modal-lg category-add"  >
         <!-- Modal content-->
         <div class="modal-content "   >
            <div class="modal-header  "  >
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title text-uppercase"  >Add <b>Category</b></h4>
            </div>
            <div class="modal-body "  >
               <div class="row"   >
                  <div class="col-sm-6">
             

                     <div class="img-upload" style="background:#f5f5f5;">
                        <div class="form-group text-center"   >
            
              <input type="file" id="file" style="display:none "/>
                           <input type="" id="file_name" style="display:none "/>
                           <br><br>      
                           <div style="text-align: center;position: relative" id="image">
                              <img width="300px"  id="preview_image" src="{{asset('admin/assets/images/placeholder.jpg')}}"/>
                              <i id="loading1" class="fa fa-spinner fa-spin fa-3x fa-fw" style="position: absolute;left: 40%;top: 40%;display: none"></i>
                           </div>
                           <br><br>
                           <p class="text-right">
                              <a href="javascript:changeProfile()" title="edit" style="text-decoration: none;background:#e2e2e2;padding:8px;">
                              <i class="fa fa-edit"></i> 
                              </a>&nbsp;&nbsp; 
                           </p>
                           <br>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <h5>Parent Category</h5>
             <md-autocomplete ng-disabled="ctrl.isDisabled" md-no-cache="ctrl.noCache" md-selected-item="selectedItem" md-search-text-change="searchCategoryChange(searchText)" md-search-text="searchText" md-selected-item-change="ctrl.selectedCategoryChange(values)" md-items="values in ctrl.categorySearch(searchText)" md-item-text="values.category_title" md-min-length="0" placeholder="Select Parent Category"  md-menu-class="autocomplete-custom-template">
                        <md-item-template>
                            <span class="item-title"> 
                                   <span> @{{values.category_title}} </span> 
                             </span> 
                        </md-item-template>
                     </md-autocomplete>
            <input type="hidden" id="category_id" name="category_id" ng-model="category_id" value="@{{category_id}}" />
                     
                     <br>
                     <md-input-container  class="md-block">
                        <label>Title</label>
                        <input type="text"     id="category_title" name="category_title"/>
                     </md-input-container>
                     
               <?php
                                 if($auth_user_type == '1' || $auth_user_type == '3' || $auth_user_type == '4')
                                              {
                                                $value = '';
                                                $type='text';
                                               if($auth_user_type == '4')
                                                   {
                                                         echo   $value = @\App\Store::where('manager_id',$auth_user_id)->first(['store_id'])->store_id;
                                                            $type = 'hidden'; 
                                                   }
                                        ?>
                                           
                        <div class="row"   > 
                        <div class="col-sm-12" ng-hide="{{$auth_user_type}}=='4'">
                                                      <h5>Select Store</h5>
                                                      <md-autocomplete ng-disabled="isDisabled" md-no-cache="noCache" md-selected-item="selectedStore" md-search-text-change="searchStoreChange(searchStore)" md-search-text="searchStore" md-selected-item-change="selectedStoreChange(values, field)" md-items="values in storeSearch(searchStore)" md-item-text="values.store_title" md-min-length="0" placeholder="Select Store " md-menu-class="autocomplete-custom-template"    >
                                                         <md-item-template>
                                                            <span class="item-title"> 
                                                            <span> @{{values.store_title}} </span> 
                                                            </span> 
                                                         </md-item-template>
                                                      </md-autocomplete>
                            <input type="text"   id="store_id" name="store_id"  value="<?php echo @$value;?>" style="display: none"  />
                                                      <br> 
                                                   </div> 
                                       </div>
 
                                          <?php                                 
                                            }
                                        ?>
                     <md-button ng-click="ctrl.storeCategory()" class="md-raised bg-color md-submit" style="bottom:0px;">Add Category</md-button>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
    <!-----------------------------------------------CATEGORY ADD MODAL ENDS HERE-------------------------------------------------------------------->
  
  
  
  
  
  
  
  
   <!-----------------------------------------------CATEGORY ADD MODAL STARTS HERE-------------------------------------------------------------------->
   <div id="editCat" class="modal fade" data-backdrop="false" style="z-index:9999" role="dialog">
      <div class="modal-dialog modal-lg category-add"  >
         <!-- Modal content-->
         <div class="modal-content "   >
            <div class="modal-header  "  >
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title text-uppercase"  >Edit <b>Category</b></h4>
            </div>
            <div class="modal-body "  >
               <div class="row"   >
                  <div class="col-sm-6">
                      <div class="img-upload" style="background:#f5f5f5;">
                        <div class="form-group text-center"   >
            
              <input type="file" id="file" style="display:none "/>
                           <input type="" id="file_name1" style="display:none "/>
                           <br><br>      
                           <div style="text-align: center;position: relative" id="image">
                              <img    class="img-responsive center-block"  id="preview_image1" ng-show="!ctrl.category_photo" src="{{asset('admin/assets/images/placeholder.jpg')}}"/>
                  <img    class="img-responsive center-block" id="preview_image2" ng-show="ctrl.category_photo"  src="{{asset('images/categories')}}/@{{ctrl.category_photo}}" style="padding:0px 20px;"/> 
                              <i id="loading1" class="fa fa-spinner fa-spin fa-3x fa-fw" style="position: absolute;left: 40%;top: 40%;display: none"></i>
                           </div>
                           <br><br>
                           <p class="text-right">
                              <a href="javascript:changeProfile()" title="edit" style="text-decoration: none;background:#e2e2e2;padding:8px;">
                              <i class="fa fa-edit"></i> 
                              </a>&nbsp;&nbsp; 
                           </p>
                           <br>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <h5>Parent Category</h5>
           <md-autocomplete ng-disabled="ctrl.isDisabled" md-no-cache="ctrl.noCache" md-selected-item="ctrl.parent_category_title" md-search-text-change="ctrl.searchEditCategoryChange(searchText)" md-search-text="searchText" md-selected-item-change="ctrl.selectedEditCategoryChange(data)" md-items="data in ctrl.editCategorySearch(searchText)" md-item-text="data.category_title" md-min-length="0" placeholder="Select Parent Category"  md-menu-class="autocomplete-custom-template">
                        <md-item-template>
                            <span class="item-title"> 
                                   <span> @{{data.category_title}} </span> 
                             </span> 
                        </md-item-template>
                     </md-autocomplete>
            <input type="hidden" id="category_edit_id" name="category_edit_id"  ng-model="ctrl.parent_id" value="@{{ctrl.parent_id}}" />
         
                     <br>
                     <md-input-container  class="md-block">
                        <label>Title</label>
                        <input type="text"  ng-model="ctrl.category_edit_title"  value="" id="category_edit_title" name="category_edit_title"/>
                     </md-input-container>
           
            <?php
                                 if($auth_user_type == '1' || $auth_user_type == '3' || $auth_user_type == '4')
                                              {
                                                $value = '';
                                                $type='text';
                                               if($auth_user_type == '4')
                                                   {
                                                            $value = @\App\Store::where('manager_id',$auth_user_id)->first(['store_id'])->store_id;
                                                            $type = 'hidden'; 
                                                   }
                                        ?>
                                           
                        <div class="row"   >
                        <div class="col-sm-12" ng-hide="{{$auth_user_type}}=='4'">
                                                      <h5>Select Store</h5>
                                                      <md-autocomplete ng-disabled="isDisabled" md-no-cache="noCache" md-selected-item="ctrl.store_edit_title" md-search-text-change="ctrl.searchEditStoreChange(searchStore)" md-search-text="searchStore" md-selected-item-change="ctrl.selectedEditStoreChange(values, field)" md-items="values in ctrl.storeEditSearch(searchStore)" md-item-text="values.store_title" md-min-length="0" placeholder="Select Store " md-menu-class="autocomplete-custom-template"    >
                                                         <md-item-template>
                                                            <span class="item-title"> 
                                                            <span> @{{values.store_title}} </span> 
                                                            </span> 
                                                         </md-item-template>
                                                      </md-autocomplete>
                            <input type="" id="store_edit_id" name="store_edit_id" ng-model="ctrl.store_edit_id"  value="<?php echo @$value;?>" style="display:none;"/>
                                                      <br> 
                                                   </div> 
                                       </div>
 
                                          <?php                                 
                                            }
                                        ?>
                     <br>
                     <md-button ng-click="ctrl.updateCategory(ctrl.category_id)" class="md-raised bg-color md-submit" style="bottom:0;">Update Category</md-button>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
    <!-----------------------------------------------CATEGORY ADD MODAL ENDS HERE-------------------------------------------------------------------->
   </div>
  </div>
      </div>
   </div>
  
</section>

 </div>
   </div>
   </div>
</div>
<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers/category.js')}}"></script> 

<!-- JavaScripts --> 
<script src="https://use.fontawesome.com/2c7a93b259.js"></script>
<script>
 
 
    function changeProfile() {
        $('#file').click();
    }
    $('#file').change(function () {
        if ($(this).val() != '') {
            upload(this);
alert(this);
        }
    });
    function upload(img) {
        var form_data = new FormData();
        form_data.append('file', img.files[0]);
        form_data.append('_token', '{{csrf_token()}}');
        $('#loading1').css('display', 'block');
        $.ajax({
            url: "{{url('image-upload-categories')}}",
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.fail) {
                    $('#preview_image').attr('src', '{{asset('images/categories/noimage.jpg')}}');
                    alert(data.errors['file']);
                }
                else {
             
           var data = data.filename;
          
                     $('#file_name').val(data);          
                    $('#file_name1').val(data);
                    //$('#photo').val(data);
          $('#preview_image').attr('src', '{{asset('images/categories')}}/'+data);
                    $('#preview_image2').attr('src', '{{asset('images/categories')}}/'+data);
          $('#preview_image1').attr('src', '{{asset('images/categories')}}/'+data);

                }
                $('#loading1').css('display', 'none');
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
                $('#preview_image').attr('src', '{{asset('images/categories/noimage.jpg')}}');
            }
        });
    }
    function removeFile() {
        if ($('#file_name').val() != '')
            if (confirm('Are you sure want to remove profile picture?')) {
                $('#loading1').css('display', 'block');
                var form_data = new FormData();
                form_data.append('_method', 'DELETE');
                form_data.append('_token', '{{csrf_token()}}');
                $.ajax({
                    url: "ajax-remove-image-categories/" + $('#file_name').val(),
                    data: form_data,
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        $('#preview_image').attr('src', '{{asset('images/noimage.jpg')}}');
                        $('#file_name').val('');
                        $('#loading').css('display', 'none');
                    },
                    error: function (xhr, status, error) {
                        alert(xhr.responseText);
                    }
                });
            }
    }
 
</script>


@endsection