@extends('admin.layout.auth')
@section('title', 'Add Manager' )
<link rel="stylesheet" href="{{ URL::asset('admin/css/app.css')}}">
@section('content')
@section('header')
@include('admin.includes.header')
@show
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
   <div id="wrapper"  >
      <div id="layout-static">
         <!---------- Static Sidebar Starts------->			
         @section('sidebar')
         @include('admin.includes.sidebar')
         @show
         <!---------- Static Sidebar Ends------->
         <div class="static-content-wrapper">
            <section id="main-header">
               <div class="container-fluid">
                  <div class="row">
                     <div class="col-sm-6"   ng-controller="searchController" ng-cloak > 
                        @include('admin.includes.realtime-search')
                     </div>
                     <div class="col-sm-12">
                        <textarea id="res" style="display:none ;" ></textarea>
						  <?php    $auth_user_type = Auth::user()->user_type;   
                             $auth_user_id =   Auth::id();  
                          ?>
                        <!-------------Loader Starts here------------->
                        <div id="loading" class="loading" style="display:none ;">
                           <img src="{{URL::asset('admin/assets/images/89.svg')}}" class="img-responsive center-block">				 
                           <p >Calling all data...</p>
                        </div>
                        <!-------------Loader Ends here------------->
                        <div class="text-right">
                        </div>
                        <div class="tab-content" >
                           <input type="file" id="file" style="display:none "/>
                           <input type="hidden" id="file_name" style="display:none "/>
                           <!--------------------------Angular App Starts ------------------------------------>
                           <div  ng-controller="userAddController" ng-cloak>
                              <div class="container-fluid" >
                                 <div class="row">
                                    <div class="col-sm-12" >
                                       <h2 class="header">Add Manager</h2>
                                    </div>
                                 </div>
                                 <div class="row" >
                                    <div class="col-sm-12 " >
                                       <div class="panel" ng-repeat="users in addUser">
                                          <div class="panel-body">
                                             <h3>@{{users.title}}</h3>
                                             <div class="row">
                                                <div class="col-lg-5 col-sm-8">
												<br>
                                                     <!----- For IMAGE Type Input Field----------->
												   <div ng-repeat="data in users.fields" ng-show="data.type == 'file'">
                                                   <h5 for="@{{data.identifier}}">@{{data.title}}</h5>
                                                   <div class="img-upload" style="width:250px;background:#f5f5f5"  >
                                                      <div class="form-group"  >
                                                         <input type=" " ng-model="data.value" value="@{{data.value}}" id="photo" style="display:none;"/>
                                                         <div style="text-align: center;position: relative" id="image">
                                                            <img  style="padding:0px;"  class="img-responsive center-block" id="preview_image"   src="{{asset('admin/assets/images/img-placeholder.png')}}"/>
                                                            <i id="loading1" class="fa fa-spinner fa-spin fa-3x fa-fw" style="position: absolute;left: 40%;top: 40%;display: none"></i>
                                                         </div>
                                                         </p>
                                                      </div>
                                                   </div>
                                                   <div class="text-right" style="font-size: ;width:250px;" >
                                                      <a href="javascript:changeProfile()" title="edit" style="text-decoration: none;background:#e2e2e2;padding:8px; position: relative; bottom:45px; ">
                                                      <i class="fa fa-edit"></i> 
                                                      </a>&nbsp;&nbsp;
                                                      <!-- <a href="javascript:removeFile()" title="delete" style="color: red;text-decoration: none;background:#e2e2e2;padding:8px; position:absolute;bottom: 50px;right:20px ">
                                                         <i class="fa fa-trash-o"></i>
                                                         </a>  -->
                                                   </div>
												   </div> 
<br>
                                                  <md-input-container  class="md-block text-left" ng-repeat="data in users.fields" ng-show="data.type == 'text' || data.type == 'string'" >
                                                         <label>@{{data.title}}</label>
                                                         <input type="@{{data.type}}" id="@{{data.identifier}}" name="@{{data.identifier}}"  ng-model="data.value" value="" />
                                                      </md-input-container>
													  
													  
													   <md-input-container  class="md-block text-left" ng-repeat="data in users.fields" ng-show="data.type == 'email' "  >
                                                         <label>@{{data.title}}</label>
                                                         <input type="@{{data.type}}" id="@{{data.identifier}}" name="@{{data.identifier}}"  ng-model="data.value" value="" />
                                                      </md-input-container>
													  
													  
													  <md-input-container  class="md-block text-left" ng-repeat="data in users.fields" ng-show="data.type == 'password' "  >
                                                         <label>@{{data.title}}</label>
                                                         <input type="@{{data.type}}" id="@{{data.identifier}}" name="@{{data.identifier}}"  ng-model="data.value" value="" />
                                                      </md-input-container>
												    <div class="form-group" ng-repeat="data in users.fields" ng-show="data.type == 'hidden'">
                                                      <label for="@{{data.identifier}}"> </label>
                                                      <input type="@{{data.type}}" ng-model="data.value" class="form-control"   id="@{{data.identifier}}"  placeholder="Enter @{{data.title}}" >
                                                   </div>
                                                   <div class="form-group" ng-repeat="data in users.fields" ng-show="data.type == 'tag'">
                                                      <label for="@{{data.identifier}}">@{{data.title}}</label> 
                                                      <tags-input ng-model="data.value" add-from-autocomplete-only="true" allow-leftover-text="false" display-property="label" key-property="id" text="text" ng-blur="text=''">
                                                         <auto-complete min-length="1" highlight-matched-text="true" source="searchData($query,data)"></auto-complete>
                                                      </tags-input>
                                                   </div>
                                                   <div class="form-group" ng-repeat="data in users.fields" ng-show="data.symbol">
                                                      <label for="@{{data.identifier}}">@{{data.title}}</label>
                                                      <div class="input-group">
                                                         <span class="input-group-addon">@{{data.symbol}}</span>
                                                         <input type="@{{data.type}}" class="form-control"   ng-model="data.value" id="@{{data.identifier}}" placeholder="Enter @{{data.title}}" >
                                                      </div>
                                                   </div>
                                                   <div class="form-group" ng-repeat="data in users.fields" ng-show="data.message">
                                                      <label for="@{{data.identifier}}">@{{data.title}}</label>
                                                      <input type="@{{data.type}}" ng-model="data.value" class="form-control"   id="@{{data.identifier}}"  placeholder="Enter @{{data.title}}"  >
                                                   </div>
												    <!----- For Radio Button Type Input Field----------->
                                                    <div class="form-group" ng-repeat="data in users.fields" ng-show="data.type == 'radio'">
                                                        <label for="@{{data.identifier}}">@{{data.title}}</label> <br> 
                                                        <label  class="radio-inline" ng-repeat="options in data.field_options">
								                           <input type="@{{data.type}}" ng-model="data.value" value="@{{options.value}}" name="@{{data.identifier}}" >@{{options.title}}
							 	                        </label> 
                                                    </div>
							  
                                                   <!----- For Checkbox Type Input Field----------->
                                                   <div class="form-group" ng-repeat="data in users.fields" ng-show="data.type == 'checkbox'">
                                                      <label for="@{{data.identifier}}">@{{data.title}} </label> <br>
                                                      <div class="checkbox" ng-repeat="options in data.field_options">
                                                         <label><input type="@{{data.type}}"  ng-model="data.value[options.value]" id="@{{options.value}}"  >@{{options.title}}</label>
                                                      </div>
                                                      <label class="checkbox-inline" ng-repeat="options in data.field_options">
                                                      <input type="@{{data.type}}" ng-model="data.value" value="@{{options.value}}" >@{{options.title}}
                                                      </label>
                                                   </div>
                                                   <!----- For Select Box Type Input Field----------->
                                                   <div class="form-group" ng-repeat="data in users.fields" ng-show="data.type == 'Selectbox'">
                                                      <label for="@{{data.identifier}}">@{{data.title}}</label> <br> 
                                                      <select ng-model="data.value" class="form-control"  ng-required="data.required_or_not">
                                                         <option value="" selected>Select User Type</option>
                                                         <option  ng-repeat="options in data.field_options" value="@{{options.value}} ">@{{options.title}}</option>
                                                      </select>
                                                   </div>
                                                   <md-input-container  class="md-block text-left" ng-repeat="data in users.fields" ng-show="data.type == 'datePicker'" >
                                                         <label>@{{data.title}}</label>
                                                          <datepicker
                                                         date-format="yyyy-MM-dd"  
                                                         button-prev='<i class="fa fa-arrow-circle-left"></i>'
                                                         button-next='<i class="fa fa-arrow-circle-right"></i>'>
                                                         <input ng-model="data.value" type="text" class="  font-fontawesome font-light radius3" placeholder="Enter @{{data.title}}" />
                                                      </datepicker>
                                                      </md-input-container>
													  
                                                   <div class="form-group" ng-repeat="data in users.fields" ng-show="data.type == 'timePicker'">
                                                      <label for="@{{data.identifier}}">@{{data.title}}</label>
                                                      <div class="input-group clockpicker" 
                                                         clock-picker 
                                                         data-autoclose="true"   > 
                                                         <input ng-model="ctrl.time" data-format="hh:mm:ss" type="text" class="form-control" placeholder="Enter @{{data.title}}">
                                                         <span class="input-group-addon">
                                                         <span class="fa fa-clock-o"></span>
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col-lg-12 col-sm-12 submit text-right">
                                          <button   ng-click="storeUser()" class="btn md-raised bg-color md-submit md-button md-ink-ripple">Add </button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <!--------------------------Angular App Ends ------------------------------------>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
         </div>
      </div>
   </div>
   <!------>
</div>
<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers/managers.js')}}"></script> 
<script src="https://use.fontawesome.com/2c7a93b259.js"></script>
<script>
 
 
    function changeProfile() {
        $('#file').click();
    }
    $('#file').change(function () {
        if ($(this).val() != '') {
            upload(this);

        }
    });
    function upload(img) {
        var form_data = new FormData();
        form_data.append('file', img.files[0]);
        form_data.append('_token', '{{csrf_token()}}');
        $('#loading1').css('display', 'block');
        $.ajax({
            url: "{{url('image-upload-users')}}",
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.fail) {
                    $('#preview_image').attr('src', '{{asset('images/users/noimage.jpg')}}');
                    alert(data.errors['file']);
                }
                else {
					 var data = data.filename; 
                    $('#file_name').val(data);
                    $('#photo').val(data);
                    $('#preview_image').attr('src', '{{asset('images/users')}}/'+data);

                }
                $('#loading1').css('display', 'none');
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
                $('#preview_image').attr('src', '{{asset('images/users/noimage.jpg')}}');
            }
        });
    }
    function removeFile() {
        if ($('#file_name').val() != '')
            if (confirm('Are you sure want to remove profile picture?')) {
                $('#loading1').css('display', 'block');
                var form_data = new FormData();
                form_data.append('_method', 'DELETE');
                form_data.append('_token', '{{csrf_token()}}');
                $.ajax({
                    url: "ajax-remove-image-users/" + $('#file_name').val(),
                    data: form_data,
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        $('#preview_image').attr('src', '{{asset('images/noimage.jpg')}}');
                        $('#file_name').val('');
                        $('#loading1').css('display', 'none');
                    },
                    error: function (xhr, status, error) {
                        alert(xhr.responseText);
                    }
                });
            }
    }
 
</script>


@endsection