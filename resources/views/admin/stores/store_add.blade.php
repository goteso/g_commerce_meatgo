@extends('admin.layout.auth')
@section('title', 'Add Store' ) 
<link rel="stylesheet" href="{{ URL::asset('admin/assets/css/custom.css')}}">
<style>
   .panel{padding: 20px;} .m-t-small .form-control{ border: 0px;
    box-shadow: none;
    width: 80%;}
</style>
@section('content')
@section('header')
@include('admin.includes.header')
@show
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
   <div id="wrapper" >
      <div id="layout-static">
         <!---------- Static Sidebar Starts------->			
         @section('sidebar')
         @include('admin.includes.sidebar')
         @show
         <!---------- Static Sidebar Ends------->
         <div class="static-content-wrapper">
            <section id="main-header">
               <div class="container-fluid">
                  <div class="row">
                     <div class="col-sm-6"   ng-controller="searchController" ng-cloak > 
                        @include('admin.includes.realtime-search')
                     </div>
					  <?php          $auth_user_type = Auth::user()->user_type;   
                     $auth_user_id =   Auth::id();  
 
                 ?>
                     <div class="col-sm-12">
                        <div class="text-right">
                        </div>
                        <div class="tab-content" >
                           <!--------------------------- Angular App Starts ---------------------------->
                           <textarea id="res" style="display:  none  ;" ></textarea>
                           <div id="loading" class="loading" style="display:none ;">
                              <img src="{{URL::asset('admin/assets/images/89.svg')}}" class="img-responsive center-block">				 
                              <p >Calling all data...</p>
                           </div>
						    <input type="file" id="file" style="display:none "/>
                           <!--------------------------Angular App Starts ------------------------------------>
                           <div  ng-controller="addStoreController" ng-cloak>
                              <div class="container-fluid" >
                                 <div class="row">
                                    <div class="col-sm-12" >
                                       <h2 class="header">Add Store</h2>
                                    </div>
                                 </div>
                                 <div class="row" >
                                    <div class="col-sm-12" >
                                       <div class="panel">
                                          <div class="row">
                                              <div class="col-sm-12 col-md-12">
                                                <div class="form-horizontal"  >
                                                   <label class="control-label">Search</label>
                                                   <div class="search-input">
                                                      <input type="text" class="form-control" id="us3-address" placeholder="Type area, city or street"  />
                                                   </div>
                                                   <br>
                                                   <!--Map by element. Also it can be attribute-->
                                                   <locationpicker options="locationpickerOptions" style="width:98%;padding:20px 5px;" ></locationpicker>
                                                   <div class="clearfix" >&nbsp;</div>
                                                   <div class="clearfix" >&nbsp;</div>
                                                    <div class="row">
												    <div class="m-t-small form-inline col-sm-5 col-lg-4"> 
												   <h5><b>Coordinates : </b></h5>
                                                      <label>Latitude : </label> <input type="text" class="form-control" id="us3-lat" ><br>
                                                        <label>Longitude : </label> <input type="text" class="form-control"  id="us3-lon" />
                                                   </div>
												   <div class="col-sm-7 col-lg-8">
												   <md-input-container  class="md-block text-left" >
                                                   <label>Address</label>
                                                   <textarea id="address" name="address" rows="3" max-rows="3"  md-no-autogrow  >  </textarea>
                                                </md-input-container>
												   </div>
													  </div> 
                                                   <div class="clearfix"></div>
                                                </div>
												 
                                             </div>
											 
											 <div class="col-sm-5 col-lg-4 category-add">
											  
													 <div class="img-upload" style="background:#f5f5f5;max-width:350px;">
												<div class="form-group text-center"   >
												
												  <input type="file" id="file" style="display:none "/>
												   <input type="hidden" id="file_name" style="display:none "/>
												   		 
												   <div style="text-align: center;position: relative" id="image">
													  <img width="100%"  id="preview_image" src="{{asset('admin/assets/images/img-placeholder.png')}}"/>
													  <i id="loading1" class="fa fa-spinner fa-spin fa-3x fa-fw" style="position: absolute;left: 40%;top: 40%;display: none"></i>
												   </div>
												   </div> 
												    </div>
												   <div class="text-right" style="font-size:;max-width:350px; " >
													  <a href="javascript:changeProfile()" title="edit" style="text-decoration: none;background:#e2e2e2;padding:8px;position:relative;bottom: 50px; right:10px">
													  <i class="fa fa-edit"></i> 
													  </a>&nbsp;&nbsp;
													  <a href="javascript:removeFile()" title="delete" style="color: red;text-decoration: none;background:#e2e2e2;padding:8px; position:relative;bottom: 50px;right:10px ">
													  <i class="fa fa-trash-o"></i>
													  </a>
												   </div>
												  
												
											
											 
											 
										  </div>
				  
                                             <div class="col-sm-12 col-md-7 col-lg-8"> 
                                                <div class="row">
												<div class="col-sm-12">
                                                      <md-input-container  class="md-block text-left" >
                                                         <label>Title</label>
                                                         <input type="text" id="title" name="title"   />
                                                      </md-input-container>
                                                   </div>



                                                        <div class="col-sm-12">
                                                      <md-input-container  class="md-block text-left" >
                                                         <label>Store Contact</label>
                                                         <input type="text" id="store_phone" name="store_phone"   />
                                                      </md-input-container>
                                                   </div>





												    <div class="col-sm-12">
                                                      <md-input-container  class="md-block text-left" >
                                                         <label>Comission</label>
                                                         <input type="text" id="comission" name="comission"   />
                                                      </md-input-container>
                                                   </div>




                                                    <div class="col-sm-12">
                                                      <md-input-container  class="md-block text-left" >
                                                         <label>Store Tax</label>
                                                         <input type="text" id="store_tax" name="store_tax"   />
                                                      </md-input-container>
                                                   </div>



												   
                                                  <div class="col-sm-12">
												  <label>Featured</label>
												     <select class="form-control" id="featured">
														<option value="0" >No</option>
														<option value="1">Yes</option> 
													  </select>
													    <br>
												  </div>
												   
												    <?php $vendor_plugins = @\App\Setting::where('key_title','vendors')->first(['key_value'])->key_value;
                              if($vendor_plugins == 1)
                              {
                                          if($auth_user_type == '1' || $auth_user_type == '3' || $auth_user_type == '4')
                                              {
                                                $value = '';
                                                $type='text';
                                               if($auth_user_type == '4')
                                                   {
                                                            $value = @\App\Store::where('manager_id',$auth_user_id)->first(['store_id'])->store_id;
                                                            $type = 'hidden'; 
                                                   }
                                        ?>
												  
                                                   <div class="col-sm-12" ng-hide="{{$auth_user_type}}=='4'">
                                                      <h5>Select Vendor</h5>
                                                      <md-autocomplete ng-disabled="isDisabled" md-no-cache="noCache" md-selected-item="selectedVendor" md-search-text-change="searchVendorChange(searchVendor)" md-search-text="searchVendor" md-selected-item-change="selectedVendorChange(values, field)" md-items="values in vendorSearch(searchVendor)" md-item-text="values.first_name" md-min-length="0" placeholder="Select Vendor " md-menu-class="autocomplete-custom-template"    >
                                                         <md-item-template>
                                                            <span class="item-title"> 
                                                            <span> @{{values.first_name}} </span> 
                                                            </span> 
                                                         </md-item-template>
                                                      </md-autocomplete>
													  <input type="text" id="vendor_id" name="vendor_id" ng-model="vendor_id" value="<?php echo @$value;?>" style="display:none;"/>
                                                      <br> 
                                                   </div>
												   
												     <?php                                 
                                            }
                                          }
                                        ?>
										
                                                   <div class="col-sm-12">
                                                      <h5>Select Manager </h5>
                                                      <md-autocomplete ng-disabled="isDisabled" md-no-cache="noCache" md-selected-item="selectedManager" md-search-text-change="searchManagerChange(searchManager)" md-search-text="searchManager" md-selected-item-change="selectedManagerChange(values, field)" md-items="values in managerSearch(searchManager)" md-item-text="values.first_name" md-min-length="0" placeholder="Select Manager " md-menu-class="autocomplete-custom-template">
                                                         <md-item-template>
                                                            <span class="item-title"> 
                                                            <span> @{{values.first_name}} </span> 
                                                            </span> 
                                                         </md-item-template>
                                                      </md-autocomplete>
													   <input type="hidden" id="manager_id" name="manager_id" ng-model="manager_id"  value="@{{manager_id}}"/>
                                                      
                                                      <br>
                                                   </div>
												    
												   
											    <div class="col-lg-12 col-sm-12 submit text-right">
                                                      <button   ng-click="addStore()" class="btn md-raised bg-color md-submit md-button md-ink-ripple">Add Store</button>
                                                   </div>
                                             </div>
											 
											 
                                          </div>
										  
										   
				  
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <!--------------------------Angular App Ends ------------------------------------>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
         </div>
      </div>
   </div>
   <!------>
</div>
<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers/stores.js')}}"></script>  
<!-- JavaScripts --> 
<script src="https://use.fontawesome.com/2c7a93b259.js"></script>
<script>
 
 
    function changeProfile() {
        $('#file').click();
    }
    $('#file').change(function () {
        if ($(this).val() != '') {
            upload(this);

        }
    });
    function upload(img) {
        var form_data = new FormData();
        form_data.append('file', img.files[0]);
        form_data.append('_token', '{{csrf_token()}}');
        $('#loading1').css('display', 'block');
        $.ajax({
            url: "{{url('image-upload-stores')}}",
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.fail) {
                    $('#preview_image').attr('src', '{{asset('images/stores/noimage.jpg')}}');
                    alert(data.errors['file']);
                }
                else {
					 var data = data.filename;
					 console.log(data);
                    $('#file_name').val(data);
                    $('#photo').val(data);
                    $('#preview_image').attr('src', '{{asset('images/stores')}}/'+data);

                }
                $('#loading1').css('display', 'none');
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
                $('#preview_image').attr('src', '{{asset('images/stores/noimage.jpg')}}');
            }
        });
    }
    function removeFile() {
        if ($('#file_name').val() != '')
            if (confirm('Are you sure want to remove profile picture?')) {
                $('#loading').css('display', 'block');
                var form_data = new FormData();
                form_data.append('_method', 'DELETE');
                form_data.append('_token', '{{csrf_token()}}');
                $.ajax({
                    url: "ajax-remove-image-stores/" + $('#file_name').val(),
                    data: form_data,
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        $('#preview_image').attr('src', '{{asset('images/noimage.jpg')}}');
                        $('#file_name').val('');
                        $('#loading').css('display', 'none');
                    },
                    error: function (xhr, status, error) {
                        alert(xhr.responseText);
                    }
                });
            }
    }
 
</script>
@endsection