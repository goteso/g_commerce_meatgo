@extends('admin.layout.auth')
@section('title', 'Reports' ) 
<link rel="stylesheet" href="{{ URL::asset('admin/assets/css/app.css')}}">
@section('content')
@section('header')
@include('admin.includes.header')
@show

<?php $currency_symbol = @\App\Setting::where( 'key_title' , 'currency_symbol' )->first(['key_value'])->key_value;  ?>
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
   <div id="wrapper" >
      <div id="layout-static">
         <!---------- Static Sidebar Starts------->			
         @section('sidebar')
         @include('admin.includes.sidebar')
         @show
         <!---------- Static Sidebar Ends------->
         <div class="static-content-wrapper"  >
            <section id="main-header">
               <div class="container-fluid">
                  <div class="row">
                     <div class="col-sm-6" ng-controller="searchController" ng-cloak > 
                        @include('admin.includes.realtime-search')
                     </div>
                     <div class="col-sm-12">
                        <br>
                        <div class="text-right">
                        </div>
                      
                           <!--------------------------- Angular App Starts ---------------------------->
                           <textarea id="res" style="display: none  ;" ></textarea>
                           <div id="loading" class="loading" style="display:none ;">
                              <img src="{{URL::asset('admin/assets/images/89.svg')}}" class="img-responsive center-block">				 
                              <p >Calling all data...</p>
                           </div>
						     <div class="tab-content"   ng-controller="reportsViewController">
                           <div class="container-fluid" >
                              <div   >
                                 <div class="row">
                                    <div class="col-sm-12">
                                       <div class="panel report-panel">
                                          <div class="row">
                                             <div class="col-sm-3">
                                                <ul class="nav nav-pills nav-stacked nav-reprt">
                                                   <li class="active revenue"><a data-toggle="pill" href="#revenue"><?php echo @$currency_symbol;?>1200<br><span>Revenue (Last 30 Days)</span></a></li>
                                                   <li class="order"><a  data-toggle="pill"  href="#order">12<br> <span>Orders (Last 30 Days)</span></a></li>
                                                   <li class="revenue"><a data-toggle="pill"   href="#users">25<br><span>Users Joined (Last 30 Days)</span></a></li>
                                                </ul>
                                             </div>
                                             <div class="col-sm-9">
                                                <div class="tab-content">
                                                   <div id="revenue" class="tab-pane fade in  active" >
                                                      <hc-chart options="revenueReport">Placeholder for generic chart</hc-chart>
                                                   </div>
                                                   <div id="order" class="tab-pane fade " >
                                                      <hc-chart options="orderReport">Placeholder for generic chart</hc-chart>
                                                   </div>
                                                   <div id="users" class="tab-pane fade  " >
                                                      <hc-chart options="userReport">Placeholder for generic chart</hc-chart>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div  >
                              <div class="container-fluid">
                                 <div class="row">
                                    <div class="col-sm-12" >
                                       <ul class="nav nav-tabs report-tabs" >
                                          <li  class="active" ><a href="#recomended" data-toggle="tab"   >Recommended</a></li>
                                          <li  class="" ><a href="#report-order" data-toggle="tab"   >Orders</a></li> 
                                          <li  class="" ><a href="#customer" data-toggle="tab"   >Customers </a></li>
                                          <li  class="" ><a href="#driver" data-toggle="tab"   >Drivers </a></li> 
                                       </ul>
                                       <div class="tab-content tab-content-data">
                                          <div id="recomended" class="tab-pane fade in  active" >
                                             <div class="panel">
                                                <div class="panel-body ">
                                                   <div class="row">
                                                      <div class="col-sm-4"  ng-repeat="data in reports.data"  >
                                                         <h4 class="report-title">@{{data.display_title}}  <span> <a target="_blank" class="btn" href="{{URL::to('v1/reports-detail')}}/@{{data.api}}">Generate</a></span></h4>
                                                         <p class="report-desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is not simply random text. </p>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div id="report-order" class="tab-pane fade " >
                                             <div class="panel">
                                                <div class="panel-body ">
                                                   <div class="row">
                                                      <div class="col-sm-4" ng-repeat="data in reports.data" ng-show="data.type == 'orders'">
                                                         <h4 class="report-title">@{{data.display_title}}  <span> <a target="_blank" ng-click="getReport(data.api);">Generate</a></span></h4>
                                                         <p class="report-desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is not simply random text. </p>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                         
                                          <div id="customer" class="tab-pane fade " >
                                             <div class="panel">
                                                <div class="panel-body ">
                                                   <div class="row">
                                                      <div class="col-sm-4"  ng-repeat="data in reports.data" ng-show="data.type == 'customers'" >
                                                         <h4 class="report-title">@{{data.display_title}}  <span> <a target="_blank" href=" #">Generate</a></span></h4>
                                                         <p class="report-desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is not simply random text. </p>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div id="driver" class="tab-pane fade " >
                                             <div class="panel">
                                                <div class="panel-body ">
                                                   <div class="row">
                                                      <div class="col-sm-4"   >
                                                         <h4 class="report-title">Drivers  <span> <a target="_blank" href=" #">Generate</a></span></h4>
                                                         <p class="report-desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is not simply random text. </p>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div> 
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <!--------------------------Angular App Ends ------------------------------------>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
         </div>
      </div>
   </div>
   <!------>
</div>
<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers/reports.js')}}"></script> 
@endsection