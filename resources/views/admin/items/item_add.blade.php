@extends('admin.layout.auth')
@section('title', 'Add Item' )
<link rel="stylesheet" href="{{ URL::asset('admin/css/app.css')}}">
@section('content')
@section('header')
@include('admin.includes.header')
@show
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
   <div id="wrapper"  >
      <div id="layout-static">
         <!---------- Static Sidebar Starts------->			
         @section('sidebar')
         @include('admin.includes.sidebar')
         @show
         <!---------- Static Sidebar Ends-------> 
         <div class="static-content-wrapper">
            <section id="main-header">
               <div class="container-fluid">
                  <div class="row">
                     <div class="col-sm-6"   ng-controller="searchController" ng-cloak > 
                        @include('admin.includes.realtime-search')
                     </div>
                     <div class="col-sm-12">
                        <textarea id="res" style="display:none ;" ></textarea>
                        <!-------------Loader Starts here------------->
                        <div id="loading" class="loading" style="display:none ;">
                           <img src="{{URL::asset('admin/assets/images/89.svg')}}" class="img-responsive center-block">				 
                           <p >Calling all data...</p>
                        </div>
                        <!-------------Loader Ends here------------->
                        <div class="text-right">
                        </div>

 


                        <div class="tab-content" >
                           <input type="file" id="file" style="display:none "/>
                           <input type="hidden" id="file_name" style="display:none "/>
                           <!--------------------------Angular App Starts ------------------------------------>
                           <div  ng-controller="itemAddController" ng-cloak>
                              <div class="container-fluid" >
                                 <div class="row">
                                    <div class="col-sm-12" >
                                       <h2 class="header">Add Item</h2>
                                    </div>
                                 </div>
                                 <div class="row" >
                                    <div class="col-sm-12 " >
                                       <form   >
                                          <div class="panel" ng-repeat="items in addItem">
                                             <div class="panel-body">
                                                <h3>@{{items.title}}</h3>
                                                <div class="row">
                                                   <div class="col-lg-5 col-sm-8">
 
                                               <!----- For IMAGE Type Input Field----------->
												   <div ng-repeat="data in items.fields" ng-show="data.type == 'file'">
                                                   <h5 for="@{{data.identifier}}">@{{data.title}}</h5>
                                                   <div class="img-upload" style="width:250px;background:#f5f5f5"  >
                                                      <div class="form-group"  >
                                                         <input type=" " ng-model="data.value" value="@{{data.value}}" id="@{{data.identifier}}" style="display:none;"/>
                                                         <div style="text-align: center;position: relative" id="image">
                                                            <img  style="padding:0px;"  class="img-responsive center-block" id="preview_image"   src="{{asset('admin/assets/images/img-placeholder.png')}}"/>
                                                            <i id="loading1" class="fa fa-spinner fa-spin fa-3x fa-fw" style="position: absolute;left: 40%;top: 40%;display: none"></i>
                                                         </div>
                                                         </p>
                                                      </div>
                                                   </div>
                                                   <div class="text-right" style="font-size: ;width:250px;" >
                                                      <a href="javascript:changeProfile()" title="edit" style="text-decoration: none;background:#e2e2e2;padding:8px; position: relative; bottom:45px; ">
                                                      <i class="fa fa-edit"></i> 
                                                      </a>&nbsp;&nbsp;
                                                      <!-- <a href="javascript:removeFile()" title="delete" style="color: red;text-decoration: none;background:#e2e2e2;padding:8px; position:absolute;bottom: 50px;right:20px ">
                                                         <i class="fa fa-trash-o"></i>
                                                         </a>  -->
                                                   </div>
												   </div> 
												   
												     
														 
														 <br>
												                              <md-input-container  class="md-block text-left" ng-repeat="data in items.fields" ng-show="data.type == 'text' || data.type == 'string'" >
                                                         <label>@{{data.title}}</label>
                                                         <input type="@{{data.type}}" id="@{{data.identifier}}" name="@{{data.identifier}}"  ng-model="data.value" value="" />
                                                      </md-input-container>
													  
													  
 
                                                      
													    
												
                                                      <div class="form-group" ng-repeat="data in items.fields" ng-show="data.type == 'tag'">
                                                         <h5 for="@{{data.identifier}}">@{{data.title}}</h5> 
                                                         <!--<tags-input ng-model="data.value" add-from-autocomplete-only="true" allow-leftover-text="false" display-property="category_title" key-property="category_id" text="text" ng-blur="text=''">
                                                            <auto-complete min-length="1" highlight-matched-text="true" source="searchData($query,data)"></auto-complete>
                                                         </tags-input>-->
														  
														<tags-input ng-model="data.value" add-from-autocomplete-only="true" allow-leftover-text="false" display-property="label" key-property="value" text="text" ng-click="getCatValue(data.value);" ng-blur="text=''" placeholder="Categories" >
															 <auto-complete min-length="1" highlight-matched-text="true" source="searchData($query)" ></auto-complete>
																</tags-input>
															
															<!--  <br>
																											  
															   <md-chips ng-model="selectedCategory" md-autocomplete-snap
																	  md-transform-chip="transformChip($chip)"
																	  md-require-match="autocompleteDemoRequireMatch">
															  <md-autocomplete
																  md-selected-item="selectedItem"
																  md-search-text="searchText"
																  md-items="item in querySearch(searchText)"
																  md-item-text="item.category_title"
																  placeholder="Search for a Category">
																<span md-highlight-text="searchText">@{{item.category_title}}</span>
															  </md-autocomplete>
															  <md-chip-template>
																<span>
																  <strong>@{{$chip.name}}</strong>
																  <em>(@{{$chip.type}})</em>
																</span>
															  </md-chip-template>
															</md-chips>-->
                                                      </div>
                                                      <div class="form-group" ng-repeat="data in items.fields" ng-show="data.symbol">
                                                         <h5 for="@{{data.identifier}}">@{{data.title}}</h5>
                                                         <div class="input-group">
                                                            <span class="input-group-addon">@{{data.symbol}}</span>
                                                            <input type="@{{data.type}}" class="form-control"   ng-model="data.value" id="@{{data.identifier}}" placeholder="Enter @{{data.title}}" >
                                                         </div>
                                                      </div>
                                                      <div class="form-group" ng-repeat="data in items.fields" ng-show="data.message">
                                                         <label for="@{{data.identifier}}">@{{data.title}}</label>
                                                         <input type="@{{data.type}}" ng-model="data.value" class="form-control"   id="@{{data.identifier}}"  placeholder="Enter @{{data.title}}"  >
                                                      </div>
                                                      <div class="form-group" ng-repeat="data in items.fields" ng-show="data.type == 'datePicker'" placeholder="Enter @{{data.title}}">
                                                         <label for="@{{data.identifier}}">@{{data.title}}</label>
                                                         <datepicker
                                                            date-format="yyyy-MM-dd"  
                                                            button-prev='<i class="fa fa-arrow-circle-left"></i>'
                                                            button-next='<i class="fa fa-arrow-circle-right"></i>'>
                                                            <input ng-model="data.value" type="text" class="form-control font-fontawesome font-light radius3" placeholder="Enter @{{data.title}}" />
                                                         </datepicker>
                                                      </div>
                                                      <div class="form-group" ng-repeat="data in items.fields" ng-show="data.type == 'timePicker'">
                                                         <label for="@{{data.identifier}}">@{{data.title}}</label>
                                                         <div class="input-group clockpicker" 
                                                            clock-picker 
                                                            data-autoclose="true"   > 
                                                            <input ng-model="ctrl.time" data-format="hh:mm:ss" type="text" class="form-control" placeholder="Enter @{{data.title}}">
                                                            <span class="input-group-addon">
                                                            <span class="fa fa-clock-o"></span>
                                                            </span>
                                                         </div>
                                                      </div>
													  
													  
													    <div class="form-group" ng-repeat="data in items.fields" ng-show="data.type == 'api'">
														
														<div   ng-show="data.identifier == 'vendor_id'">
                                                      <h5>Select @{{data.title}}</h5>
                                                      <md-autocomplete ng-disabled="isDisabled" md-no-cache="noCache" md-selected-item="selectedVendor" md-search-text-change="searchVendorChange(searchVendor,data)" md-search-text="searchVendor" md-selected-item-change="selectedVendorChange(values, data)" md-items="values in vendorSearch(searchVendor)" md-item-text="values.first_name" md-min-length="0" placeholder="Select Vendor " md-menu-class="autocomplete-custom-template"    >
                                                         <md-item-template>
                                                            <span class="item-title"> 
                                                            <span> @{{values.first_name}} </span> 
                                                            </span> 
                                                         </md-item-template>
                                                      </md-autocomplete>
													  <input type="hidden" id="@{{data.identifier}}" name="vendor_id"  ng-model="vendor_id" value="@{{vendor_id}}" />
                                                      <br> 
                                                   </div>
                                                   <div   ng-show="data.identifier == 'store_id'">
                                                      <h5>Select @{{data.title}} </h5>
                                                      <md-autocomplete ng-disabled="isDisabled" md-no-cache="noCache" md-selected-item="selectedStore" md-search-text-change="searchStoreChange(searchStore,data)" md-search-text="searchStore" md-selected-item-change="selectedStoreChange(values, data)" md-items="values in storeSearch(searchStore)" md-item-text="values.store_title" md-min-length="0" placeholder="Select Store " md-menu-class="autocomplete-custom-template">
                                                         <md-item-template>
                                                            <span class="item-title"> 
                                                            <span> @{{values.store_title}} </span> 
                                                            </span> 
                                                         </md-item-template>
                                                      </md-autocomplete>
													   <input type="hidden" id="store_id" name="store_id" ng-model="store_id"  value="@{{store_id}}"/>
                                                      
                                                      <br>
                                                   </div>
												   
                                                   </div>
                                                </div>
                                             </div>
                                         
                                          <div class="col-lg-12 col-sm-12 submit text-right">
                                             <button   ng-click="storeItem()" class="btn md-raised bg-color md-submit md-button md-ink-ripple">Add Item</button>
                                          </div>
                                        
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <!--------------------------Angular App Ends ------------------------------------>
                        </div>
                     </div>
                  </div>
               </div> </div>
      </div>
            </section>
         </div>
      </div>
   </div>
   <!------>
</div>
<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers/items.js')}}"></script> 
 



 <!-- JavaScripts -->
<script src="https://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
<script src="https://use.fontawesome.com/2c7a93b259.js"></script>
<script>
 
 
    function changeProfile() {
        $('#file').click();
    }
    $('#file').change(function () {
        if ($(this).val() != '') {
            upload(this);

        }
    });
    function upload(img) {
        var form_data = new FormData();
        form_data.append('file', img.files[0]);
        form_data.append('_token', '{{csrf_token()}}');
        $('#loading1').css('display', 'block');
        $.ajax({
            url: "{{url('image-upload-items')}}",
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.fail) {
                    $('#preview_image').attr('src', '{{asset('images/items/noimage.jpg')}}');
                    alert(data.errors['file']);
                }
                else {
					 var data = data.filename;
					 console.log(data);
                    $('#file_name').val(data);
					 $('#item_photo').val(data);
                    $('#item_photo').val(data);
                    $('#preview_image').attr('src', '{{asset('images/items')}}/'+data);

                }
                $('#loading1').css('display', 'none');
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
                $('#preview_image').attr('src', '{{asset('images/items/noimage.jpg')}}');
            }
        });
    }
    function removeFile() {
        if ($('#file_name').val() != '')
            if (confirm('Are you sure want to remove profile picture?')) {
                $('#loading1').css('display', 'block');
                var form_data = new FormData();
                form_data.append('_method', 'DELETE');
                form_data.append('_token', '{{csrf_token()}}');
                $.ajax({
                    url: "ajax-remove-image/" + $('#file_name').val(),
                    data: form_data,
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        $('#preview_image').attr('src', '{{asset('images/noimage.jpg')}}');
                        $('#file_name').val('');
                        $('#loading1').css('display', 'none');
                    },
                    error: function (xhr, status, error) {
                        alert(xhr.responseText);
                    }
                });
            }
    }
 
</script>



@endsection