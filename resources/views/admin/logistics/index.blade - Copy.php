@extends('admin.layout.auth')
@section('title', 'Logistics' ) 
<link rel="stylesheet" href="{{ URL::asset('admin/assets/css/app.css')}}">
<style>
md-tabs, .nav-tabs.nav-tabs1>li>a{font-family:'Open Sans',sans-serif}
.nav-tabs.nav-tabs1>li>a>i{color:#fff;padding-top:5px;}
.nav-tabs.nav-tabs1>li.icon:hover>a, .nav-tabs.nav-tabs1>li.icon:active>a, .nav-tabs.nav-tabs1>li.icon.active>a, .nav-tabs.nav-tabs1>li.icon:focus>a {
    color: #fff!important; 
    border-bottom: 0px solid #fff!important;
}
.nav-tabs{background:#488de4;}
.nav-tabs>li:hover>a, .nav-tabs>li:active>a, .nav-tabs>li.active>a, .nav-tabs>li:focus>a {
    color: #fff!important; 
    border-bottom: 2px solid #fff!important;
}
md-tabs .md-tab.md-active{color:#000!important;}
.table>tbody>tr>td  {
    padding: 15px 5px 10px!important;
	font-size:15px !important;
}

 md-tabs md-tabs-wrapper{background:#e9ecf0}
  .nav-tabs.nav-tabs1>li.icon>a{color:#fff;opacity:1;}
  .nav-tabs>li>a{color:#fff;opacity:0.6;}
  .nav-tabs>li:hover>a,.nav-tabs>li.active:hover>a, .nav-tabs>li.active>a{opacity:1;}

  
   .md-autocomplete-suggestions-container{  
   z-index:100000 !important; /* any number of choice > 1050*/
  } 
.StepProgress {
  position: relative;
  padding-left: 20px;
  list-style: none;
}
  .StepProgress::before {
    display: inline-block;
    content: '';
    position: absolute;
    top: 2px;
    left: 0px;
    width: 10px;
    height: 50%;
    border-left: 1px solid #CCC;
  }
  
  .StepProgress-item {
    position: relative;
    counter-increment: list;
	      padding-left: 10px;
  }
    .StepProgress-item:not(:last-child) {
      padding-bottom: 10px;
    }
    
    .StepProgress-item-first::before {
      display: inline-block;
      content: '';
      position: absolute;
      left: -20px;
      height: 100%;
      width: 10px;
	    top: 2px;
    }
    
    .StepProgress-item::after {
      content: '';
      display: inline-block;
      position: absolute;
      top: 0px;
	   padding-top: 6px;
      left: -34px;
      width: 30px;
      height: 30px;
      border: 0px solid #CCC;
      border-radius: 50%;
      background-color: #D8E1ED;
    }
    
    
      .StepProgress-item.is-done::before {
        border-left: 2px dotted #D8E1ED;
      }
      .StepProgress-item.is-done::after {
        content: "P";
		 padding-top: 6px;
        font-size: 12px;
        color: #FFF;
        text-align: center;
        border: 0px solid #488de4;
        background-color: #D8E1ED;
      }
	  .unassigned .StepProgress-item.StepProgress-item-first.is-done::after {
        /*content: counter(list);*/
		content: "P";
        padding-top: 6px;
        width: 30px;
        height: 30px;
        top: 0px;
        left: -34px;
        font-size: 12px;
        text-align: center;
        color: #fff; 
        background-color: #D8E1ED; 
        border: 0px solid #488de4;
      }
    
     
      .StepProgress-item.current::before {
        border-left: 2px dotted #D8E1ED;
      }
      
      .StepProgress-item.current::after {
        /*content: counter(list);*/
		content: "D";
        padding-top: 6px;
        width: 30px;
        height: 30px;
        top: 2px;
        left: -35px;
        font-size: 12px;
        text-align: center;
        color: #fff;
        border: 0px solid #488de4;
        background-color: D8E1ED;
      }
	  
	  .completed .StepProgress-item.StepProgress-item-last.is-done::after { 
		content: "D"!important;
         font-size: 12px;
        color: #FFF;
        text-align: center;
        border: 0px solid #488de4;
        background-color: #D8E1ED;
      }
    
   
  
  strong {
    display: block;
  }
  
  md-tabs:not(.md-no-tab-content):not(.md-dynamic-height){min-height:650px!important;}

  
  .container-2{
  width: auto;
  vertical-align: middle;
  white-space: nowrap;
  position: absolute!important;
  right:0;
}

.container-2 input#search{
	margin-top:5px;
  width: 50px;
  height: 30px;
  background: #fff;
  border: none;
  font-size: 10pt;
  float: right;
  color: #262626;
  padding-right: 35px;
  -webkit-border-radius: 5px;
  -moz-border-radius: 5px;
  border-radius: 0px;
  color: #fff;
 opacity:0;
  -webkit-transition: width .55s ease;
  -moz-transition: width .55s ease;
  -ms-transition: width .55s ease;
  -o-transition: width .55s ease;
  transition: width .55s ease;
}

.container-2 input#search::-webkit-input-placeholder {
   color: #65737e;
}
 
.container-2 input#search:-moz-placeholder { /* Firefox 18- */
   color: #65737e;  
}
 
.container-2 input#search::-moz-placeholder {  /* Firefox 19+ */
   color: #65737e;  
}
 
.container-2 input#search:-ms-input-placeholder {  
   color: #65737e;  
}

.container-2 .icon{
  position: absolute;
  right:0;
  top: 10%;
  margin-left: 10px;
  margin-top: 10px;
  z-index: 1;
  color: #fff;
}

.container-2 input#search:focus, .container-2 input#search:active{
  outline:none;
  width: 150px;
   color: #65737e;  
}
 
.container-2:hover input#search{
width: 150px;
  opacity:1;
}
 
.container-2:hover .icon{
  color: #fff;
}
</style>
@section('content')
@section('header')
@include('admin.includes.header')
@show
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
   <div id="wrapper" >
      <div id="layout-static">
         <!---------- Static Sidebar Starts------->			
         @section('sidebar')
         @include('admin.includes.sidebar')
         @show
         <!---------- Static Sidebar Ends------->
         <div class="static-content-wrapper"  >
            <section id="main-header">
               <div class="container-fluid">
                  <div class="row">
				  <div class="col-sm-5" ng-controller="searchController" ng-cloak > 
                                    @include('admin.includes.realtime-search')
                                 </div>
                     <div class="col-sm-12">
                        <textarea id="res" style="display: none;" ></textarea>
						
						<!-------------Loader Starts here------------->
                        <div id="loading" class="loading" style="display:none ;">
                           <img src="{{URL::asset('admin/assets/images/89.svg')}}" class="img-responsive center-block">				 
                           <p >Calling all data...</p>
                        </div>
						<!-------------Loader Ends here------------->
						 
                        <!--------------------------- Angular App Starts ---------------------------->
                        <div class="tab-content" id="tab-content"  >
                           <div class="container-fluid" >
                              <div class="row">
                                 <div class="col-sm-7" >
                                    <h2 class="header"> </h2>
                                 </div>
                                 
                              </div>
                              <div class="row" ng-controller="logisticsController as ctrl" ng-cloak  style="display: flex; ">
                                 <div class="col-sm-6 col-lg-8" style="padding:0px">
                                      <div class="form-horizontal"  >
									  
                                          <locationpicker options="locationpickerOptions" style="width:100%;height:700px;" ></locationpicker>
										  
										  <div id="dvMap" style="width:100%; height: 400px"></div>
										  
                                          <label class="control-label"> </label>
                                          <div class="search-input" >
                                             <input type="hidden" class="form-control" id="us3-address" placeholder="Type area, city or street"  />
                                          </div>
                                          <!--Map by element. Also it can be attribute-->
                                          <div class="m-t-small form-inline "> 
                                             <input type="hidden" class="form-control" id="us3-lat"  >,
                                             <input type="hidden" class="form-control"  id="us3-lon"  />
                                          </div> 
                                       </div>
                                 </div>
								 <div class="col-sm-6 col-lg-4"style="padding:0px" >
                                    <div ng-view></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!--------------------------- Angular App Ends ---------------------------->
                     </div>
                  </div>
               </div>
         </div>
         </section>
      </div>
   </div>
</div>
<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers/logistics.js')}}"></script> 
<script>
 

$(function() {

$("#wrapper")
.width($(window).width()).height($(window).height());
});
</script>
<!------>
@endsection