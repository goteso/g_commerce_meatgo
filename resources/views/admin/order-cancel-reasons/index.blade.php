@extends('admin.layout.auth')
@section('title', 'Order Cancel Reasons' ) 
<link rel="stylesheet" href="{{ URL::asset('admin/assets/css/custom.css')}}">
 <style>
 .md-autocomplete-suggestions-container{  
   z-index:100000 !important; /* any number of choice > 1050*/
  }
 </style>
@section('content')
@section('header')
@include('admin.includes.header')
@show
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
   <div id="wrapper" >
      <div id="layout-static">
         <!---------- Static Sidebar Starts------->     
         @section('sidebar')
         @include('admin.includes.sidebar')
         @show
         <!---------- Static Sidebar Ends------->
         <div class="static-content-wrapper"  >
     
<section id="main-header">
   <div class="container-fluid">
      <div class="row">
         <div class="col-sm-6"   ng-controller="searchController" ng-cloak > 
            @include('admin.includes.realtime-search')
         </div>
       <?php          $auth_user_type = Auth::user()->user_type;   
                     $auth_user_id =   Auth::id();  
 
                 ?>
         <div class="col-sm-12">
            <div class="text-right">
            </div>
       <input type="file" id="file" style="display:none "/>
            <div class="tab-content"  ng-controller="OrderCancelReasonsController as ctrl" ng-cloak  >
               <!--------------------------- Angular App Starts ---------------------------->
               <textarea id="res" style="display: none  ;" ></textarea>
                  <div id="loading" class="loading" style="display:none ;">
                  <img src="{{URL::asset('admin/assets/images/89.svg')}}" class="img-responsive center-block">         
                  <p >Calling all data...</p>
                  </div>   
               <div class="container-fluid" >
                  <div class="row">
                     <div class="col-sm-10" >
                        <h2 class="header">Order Cancel Reasons</h2>
                     </div>
                     <div class="col-sm-2">
                        <div class="text-right">


                           @if(trans('permission'.$auth_user_type.'.add_order_cancel_reasons') == '1')
                           <button type="button" class="btn md-raised bg-color md-add md-button md-ink-ripple text-right"  data-toggle="modal" data-target="#add">Add New</button>
                           @endif  
                        </div>
                     </div>
                  </div>
                  <div class="row" >
                     <div class="col-sm-12" >

                     
                        <div class="" >
                           <div id="tableToExport" class="products-table table-responsive"  >
                              <table class="table" class="table table-striped" id="exportthis" >
                                 <thead>
                                    <tr>
                                       <th>ID</th>
                                       <th>TITLE</th>
                                       <th>CREATED</th>
                                       @if(trans('permission'.$auth_user_type.'.edit_order_cancel_reasons') == '1') <th id="action">ACTION</th> @endif 
                                    </tr>
                                 </thead>
                                 <tbody>
                                    
                                    <tr ng-repeat="values in ctrl.order_cancel_reasons.data.data  ">
                                       <td>#@{{values.id}}</td>
                                       <td>@{{values.title}}</td>
                      
                
                                       <td>@{{values.created_at_formatted}}</td>
                                       


   @if(trans('permission'.$auth_user_type.'.edit_order_cancel_reasons') == '1')
                                       <td>
                                          
                                        
                                            <a class="btn btn-xs edit-product" ng-click="ctrl.editOrderCancelReasons(values, $index)" ><i class="fa fa-edit"></i></a>
                                           
                                          
                                           <a class="btn btn-xs delete-product" ng-click="ctrl.deleteOrderCancelReasons(values.id, $index)"><i class="fa fa-trash"></i></a>               
                                       </td>
                                        @endif 
                                    </tr>
                                    
                                 </tbody>
                              </table>
                           </div>

                           
                <div class="pagination text-right" style="display:block" >
                                           <button class="btn"  ng-show="ctrl.order_cancel_reasons.data.first_page_url != null" ng-click="ctrl.pagination(ctrl.order_cancel_reasons.data.first_page_url);">First</button> 
                                           <button class="btn"  ng-show="ctrl.order_cancel_reasons.data.prev_page_url != null" ng-click="ctrl.pagination(ctrl.order_cancel_reasons.data.prev_page_url);">Previous</button> 
                                          <span>@{{ctrl.order_cancel_reasons.data.current_page}}</span>
                                          <button class="btn"  ng-show="ctrl.order_cancel_reasons.data.next_page_url != null" ng-click="ctrl.pagination(ctrl.order_cancel_reasons.data.next_page_url);">Next</button> 
                                          <button class="btn"  ng-show="ctrl.order_cancel_reasons.data.last_page_url != null" ng-click="ctrl.pagination(ctrl.order_cancel_reasons.data.last_page_url);">Last</button> 
                                       </div>
                     
                           
                        </div>
                     </div>
                     
                  </div>
               </div>
           
            <!--------------------------- Angular App Ends ---------------------------->
         
   
   
   
   <!-----------------------------------------------CATEGORY ADD MODAL STARTS HERE-------------------------------------------------------------------->
   <div id="add" class="modal fade" data-backdrop="false" style="z-index:9999" role="dialog">
      <div class="modal-dialog modal-lg category-add"  >
         <!-- Modal content-->
         <div class="modal-content "   >
            <div class="modal-header  "  >
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title text-uppercase"  >Add <b>Cancel Reason</b></h4>
            </div>
            <div class="modal-body "  >
               <div class="row"   >
 
                  <div class="col-sm-12">
                     <h5>Cancel Reason</h5>
                     <textarea id="title" rows="5" class="form-control"></textarea>
                    <md-button ng-click="ctrl.storeOrderCancelReasons()" class="md-raised bg-color md-submit" style="bottom:0px;">Submit</md-button>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
    <!-----------------------------------------------CATEGORY ADD MODAL ENDS HERE-------------------------------------------------------------------->
  
  
  
  
  
  
  
  
   <!-----------------------------------------------CATEGORY ADD MODAL STARTS HERE-------------------------------------------------------------------->
   <div id="editCat" class="modal fade" data-backdrop="false" style="z-index:9999" role="dialog">
      <div class="modal-dialog modal-lg category-add"  >
         <!-- Modal content-->
         <div class="modal-content "   >
            <div class="modal-header  "  >
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title text-uppercase"  >Edit <b>Reason</b></h4>
            </div>
            <div class="modal-body "  >
               <div class="row"   >
              
                  <div class="col-sm-12">
                      <h5>Cancel Reason</h5>
                      <textarea id="edit_title" rows="5" class="form-control"></textarea>
  
              
           
 
                     <br>
                     <md-button ng-click="ctrl.updateOrderCancelReasons(ctrl.id)" class="md-raised bg-color md-submit" style="bottom:0;">Update Reason</md-button>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
    <!-----------------------------------------------CATEGORY ADD MODAL ENDS HERE-------------------------------------------------------------------->
   </div>
  </div>
      </div>
   </div>
  
</section>

 </div>
   </div>
   </div>
</div>
<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers/order_cancel_reasons.js')}}"></script> 

<!-- JavaScripts --> 
<script src="https://use.fontawesome.com/2c7a93b259.js"></script>
<script>
 
 
    function changeProfile() {
        $('#file').click();
    }
    $('#file').change(function () {
        if ($(this).val() != '') {
            upload(this);
alert(this);
        }
    });
    function upload(img) {
        var form_data = new FormData();
        form_data.append('file', img.files[0]);
        form_data.append('_token', '{{csrf_token()}}');
        $('#loading1').css('display', 'block');
        $.ajax({
            url: "{{url('image-upload-categories')}}",
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.fail) {
                    $('#preview_image').attr('src', '{{asset('images/categories/noimage.jpg')}}');
                    alert(data.errors['file']);
                }
                else {
             
           var data = data.filename;
          
                     $('#file_name').val(data);          
                    $('#file_name1').val(data);
                    //$('#photo').val(data);
          $('#preview_image').attr('src', '{{asset('images/categories')}}/'+data);
                    $('#preview_image2').attr('src', '{{asset('images/categories')}}/'+data);
          $('#preview_image1').attr('src', '{{asset('images/categories')}}/'+data);

                }
                $('#loading1').css('display', 'none');
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
                $('#preview_image').attr('src', '{{asset('images/categories/noimage.jpg')}}');
            }
        });
    }
    function removeFile() {
        if ($('#file_name').val() != '')
            if (confirm('Are you sure want to remove profile picture?')) {
                $('#loading1').css('display', 'block');
                var form_data = new FormData();
                form_data.append('_method', 'DELETE');
                form_data.append('_token', '{{csrf_token()}}');
                $.ajax({
                    url: "ajax-remove-image-categories/" + $('#file_name').val(),
                    data: form_data,
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        $('#preview_image').attr('src', '{{asset('images/noimage.jpg')}}');
                        $('#file_name').val('');
                        $('#loading').css('display', 'none');
                    },
                    error: function (xhr, status, error) {
                        alert(xhr.responseText);
                    }
                });
            }
    }
 
</script>


@endsection