@extends('admin.layout.auth')
@section('title', 'Add Order' ) 
<link rel="stylesheet" href="{{ URL::asset('admin/assets/css/main.css')}}">
<style>
   .autocomplete-custom-template li {
   border-bottom: 1px solid #ccc;
   height: auto;
   padding-top: 8px;
   padding-bottom: 8px;
   white-space: normal;
   }
   .autocomplete-custom-template li:last-child {
   border-bottom-width: 0;
   }
   .autocomplete-custom-template .item-title,
   .autocomplete-custom-template .item-metadata {
   display: block;
   line-height: 1.5;
   }
   .green{background:#309c20} 
</style>
@section('content')
@section('header')
@include('admin.includes.header')
@show
<?php $currency_symbol = @\App\Setting::where( 'key_title' , 'currency_symbol' )->first(['key_value'])->key_value;  ?>
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
   <div id="wrapper" data-spy="scroll" data-target=".order-content" data-offset="50">
      <div id="layout-static">
         <!---------- Static Sidebar Starts------->			
         @section('sidebar')
         @include('admin.includes.sidebar')
         @show
         <!---------- Static Sidebar Ends------->
		 
 
   <div class="static-content-wrapper">
      <section id="main-header">
         <div class="container-fluid">
            <div class="row">
			<div class="col-sm-5" ng-controller="searchController" ng-cloak > 
                   @include('admin.includes.realtime-search')
                </div>
								 
               <div class="col-sm-12"> 
			   <br>
                  <div class="text-right">
                  </div>
                  <div class="tab-content" >
                     <textarea id="res"  style="display:none;"></textarea> 
 
                     <!--------------------------Angular App Starts ------------------------------------>
                     <div   ng-controller="orderController as main" ng-cloak>
					 
							 	 
                        <div class="container-fluid ">
                           <div class="row order-content" >
						  
                              <div class="col-sm-3 col-lg-2" >
                                 <div class="form-group"  >
                                    <div class="input-group">
                                       <input type="text" class="form-control"   ng-model="search" placeholder="Search by item" >
                                       <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                    </div>
                                 </div>
                                 <nav role="navigation" class="sidebar-body" >
                                    <h4 class="sidebar-title">Categories</h4>
                                    <ul class=" ">
                                       <li ng-repeat="categories in main.addOrder" class="active " >
                                          <a  class="main" href="#@{{categories.category_title}}"> <span>@{{categories.category_title}}</span> </a>
                                          <ul class="acc-menu">
                                             <li ng-repeat="subCategories in categories.sub_categories ">
                                                <a  class="sub" href="#@{{subCategories.category_title}}"><span>@{{subCategories.category_title}}</span></a>
                                                <ul class="">
                                                   <li ng-repeat="subCategories in subCategories.sub_categories " >
                                                      <a class="subsub" href="#@{{subCategories.category_title}}"><span>@{{subCategories.category_title}}</span></a>
                                                      <ul class="acc-menu">
                                                         <li ng-repeat="subCategories in subCategories.sub"><a href="#@{{subCategories.category_title}}"><span>@{{subCategories.category_title}}</span></a></li>
                                                      </ul>
                                                   </li>
                                                </ul>
                                             </li>
                                          </ul>
                                       </li>
                                    </ul>
                                 </nav>
                              </div>
							  

                              <div class="col-sm-5  col-lg-7 text-right" >
                                 <div class="tab-content">
                                    <div class="panel-group product-view" id="accordion">
                                       <div ng-repeat="categories in main.addOrder | filter:search" id ="@{{categories.category_title}}" class="tab-pane fade in active">
                                          <div ng-show="categories.items != '' ">
                                             <div class="panel ">
                                                <div class="panel-heading">
                                                   <h4 class="panel-title text-left">
                                                      @{{categories.category_title}} <span>(@{{categories.items.length}} items)</span> <a data-toggle="collapse" data-parent="#accordion" href="#1@{{categories.category_title}}">  <i class="fa fa-angle-down"></i></a>
                                                   </h4>
                                                </div>
                                                <div id="1@{{categories.category_title}}" class="panel-collapse collapse in">
                                                   <table class="table items-detail">
                                                      <tbody>
                                                         <tr ng-repeat="item in categories.items | filter:search"  >
                                                            <td class="item-image ">
															      <img src="admin/images/item-placeholder.png" ng-show="item.item_photo == null">
															      <img src="{{URL::asset('/products')}}/@{{item.item_photo}}" ng-show="item.item_photo != null">
															   </td>
                                                            <td class="item-title"> @{{item.item_title}} </td>
                                                            <td class="item-price"><span ng-show='item.item_price > 0'><?php echo @$currency_symbol;?>@{{item.item_price}} </span>
                                                            <span ng-show='item.item_price == 0'>Price on Selection</span>															</td>
                                                            <td class="item-createdTime  "><button class="btn-addCart" ng-click="main.addToCart(item)"><i class="fa fa-plus"></i></button></td>
                                                         </tr>
                                                      </tbody>
                                                   </table>
                                                </div>
                                             </div>
                                          </div>
                                          <div ng-repeat="subCategories in categories.sub_categories" id ="@{{subCategories.category_title}}" class="tab-pane fade in active">
                                             <div ng-show="subCategories.items != ''" >
                                                <div class="panel ">
                                                   <div class="panel-heading">
                                                      <h4 class="panel-title text-left">
                                                         @{{subCategories.category_title}} <span>(@{{subCategories.items.length}} items)</span> <a data-toggle="collapse" data-parent="#accordion" href="#2@{{subCategories.category_title}}"> <i class="fa fa-angle-down"></i></a>
                                                      </h4>
                                                   </div>
                                                   <div id="2@{{subCategories.category_title}}" class="panel-collapse collapse in">
                                                      <table  class="table items-detail">
                                                         <tbody>
                                                            <tr ng-repeat="item in subCategories.items | filter:search ">
                                                               <td class="item-image ">
															      <img src="admin/images/item-placeholder.png" ng-show="item.item_photo == null">
															      <img src="{{URL::asset('/products')}}/@{{item.item_photo}}" ng-show="item.item_photo != null">
															   </td>
                                                               <td class="item-title"> @{{item.item_title}} </td>
                                                               <td class="item-price"><span ng-show='item.item_price > 0'><?php echo @$currency_symbol;?>@{{item.item_price}} </span>
                                                            <span ng-show='item.item_price == 0'>Price on Selection</span>  </td>
                                                               <td class="item-createdTime text-right"><button class="btn-addCart" ng-click="main.addToCart(item)" ng-disable="item.showAddToCart && !item.addedToCart"><i class="fa fa-plus"></i></button></td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </div>
                                                </div>
                                             </div>
                                             <div ng-repeat="subCategories in subCategories.sub_categories" id ="@{{subCategories.category_title}}" class="tab-pane fade in active">
                                                <div ng-show="subCategories.items != ''">
                                                   <div class="panel ">
                                                      <div class="panel-heading">
                                                         <h4 class="panel-title text-left">
                                                            @{{subCategories.category_title}} <span>(@{{subCategories.items.length}} items)</span><a data-toggle="collapse" data-parent="#accordion" href="#3@{{subCategories.category_title}}"> <i class="fa fa-angle-down"></i></a>
                                                         </h4>
                                                      </div>
                                                      <div id="3@{{subCategories.category_title}}" class="panel-collapse collapse in">
                                                         <table  class="table items-detail">
                                                            <tbody>
                                                               <tr ng-repeat="item in subCategories.items | filter:search">
                                                                  <td class="item-image ">
															        <img src="admin/images/item-placeholder.png" ng-show="item.item_photo == null">
															        <img src="{{URL::asset('/products')}}/@{{item.item_photo}}" ng-show="item.item_photo != null">
															      </td>
                                                                  <td class="item-title"> @{{item.item_title}} </td>
                                                                  <td class="item-price"><span ng-show='item.item_price > 0'><?php echo @$currency_symbol;?>@{{item.item_price}} </span>
                                                            <span ng-show='item.item_price == 0'>Price on Selection</span>  </td>
                                                                  <td class="item-createdTime text-right"><button class="btn-addCart" ng-click="main.addToCart(item)"><i class="fa fa-plus"></i></button></td>
                                                               </tr>
                                                            </tbody>
                                                         </table>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div ng-repeat="subCategories in subCategories.sub" id ="@{{subCategories.category_title}}" class="tab-pane fade in active">
                                                   <div ng-show="subCategories.items != ''">
                                                      <div class="panel ">
                                                         <div class="panel-heading">
                                                            <h4 class="panel-title text-left">
                                                               @{{subCategories.category_title}} <span>(@{{subCategories.items.length}} items)</span><a data-toggle="collapse"  data-parent="#accordion" href="#4@{{subCategories.category_title}}"> <i class="fa fa-angle-down"></i></a></span> 
                                                            </h4>
                                                         </div>
                                                         <div id="4@{{subCategories.category_title}}" class="panel-collapse collapse in">
                                                            <table  class="table items-detail">
                                                               <tbody>
                                                                  <tr ng-repeat="item in subCategories.items | filter:search ">
                                                                     <td class="item-image ">
															          <img src="admin/images/item-placeholder.png" ng-show="item.item_photo == null">
															          <img src="{{URL::asset('/products')}}/@{{item.item_photo}}" ng-show="item.item_photo != null">
															         </td>
                                                                     <td class="item-title"> @{{item.item_title}}  </td>
                                                                     <td class="item-price"><span ng-show='item.item_price > 0'><?php echo @$currency_symbol;?>@{{item.item_price}} </span>
                                                            <span ng-show='item.item_price == 0'>Price on Selection</span>  </td>
                                                                     <td class="item-createdTime text-right"><button class="btn-addCart" ng-click="main.addToCart(item)"><i class="fa fa-plus"></i></button></td>
                                                                  </tr>
                                                               </tbody>
                                                            </table>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-sm-4 col-lg-3" >
                                 <div class="panel panel-right">
                                    <div class="panel-header">
                                       <h4>Order Info</h4>
                                    </div>
                                 </div>
                                 <div ng-controller="cartController as cart" >
                                    <div class="panel" ng-show="cart.cartStorage.items">
                                       <div class="panel-body">
                                          <h5>Cart Items</h5>
                                        
										
										<!--<table class="cart_table table " style="padding-bottom:10px;border-bottom:1px solid #ccc;">
									 
										  <tr ng-repeat="product in cart.cartStorage.products track by $index" >
										     <td>   @{{product.title}}
												  <span ng-show="product.variants"  ><a href="#" data-toggle="tooltip" title="@{{product.variants[0].title}} @{{product.variants[1].title}} @{{product.variants[2].title}}">&#9432;</a></span> </td>
											<td>  <button ng-click="cart.increaseItemAmount(product)">
                                                   +
                                                   </button>
                                                   @{{product.quantity}} 
                                                   <button ng-click="cart.decreaseItemAmount(product)">
                                                   -
                                                   </button>
										   </td>
											<td><p><?php //echo env("CURRENCY_SYMBOL", "");?>@{{product.base_price}}</p></td>
											<td><button ng-click="cart.removeFromCart(product)"  >   <i class="fa fa-minus"></i> </button></td>
										  </tr>
										</table>-->
										
										
										    <div class="row cart_table" ng-repeat="item in cart.cartStorage.items track by $index" style="padding-bottom:10px;border-bottom:1px solid #ccc;" >
										   <div class="col-sm-12">
                                                <h4>@{{item.item_title}}
												  <span ng-show="item.variants != ''"  ><a href="#" data-toggle="tooltip" title="@{{variantsSelectedTitleData}} ">&#9432;</a></span> 
												   <span ng-show="item.variants" ng-repeat="variant in item.variants">@{{variant.title}} </span> 
												</h4>
												</div>
												 <div class="col-sm-6 text-center"><p>
                                                   <button ng-click="cart.increaseItemAmount(item)">
                                                   +
                                                   </button>
                                                   @{{item.quantity}} 
                                                   <button ng-click="cart.decreaseItemAmount(item)">
                                                   -
                                                   </button>
												   <p>
                                                </div>
                                                <div class="col-sm-2"><p><?php echo env("CURRENCY_SYMBOL", "");?>@{{item.discounted_price}}</p></div>
                                               
                                                <div class="col-sm-2">
                                                   <button ng-click="cart.removeFromCart(item)"  >
                                                   <i class="fa fa-minus"></i>
                                                   </button>
                                                </div>
                                             </div>
											 
											 
                                          <!--<span ng-bind="total"></span>-->
                                       </div>
                                    </div>
									
									  
								 <div class="panel cart" ng-show="cart.paymentData[0].type == 'payment_details'"> 
                                    <div class="panel-body">
                                        <div class="row  "    >
										    <div class="col-sm-12">
										       <h5>@{{cart.paymentData[0].data.title}}</h5> 
											</div>
											
											<table class="table paymentSummary" > 
		                                        <tbody >
		                                            <tr  ng-repeat="values in cart.paymentData[0].data.data ">
		                                               <td class="" style="font-size:14px;text-transform: capitalize">@{{values.title.replace('_', ' ')  }} </td>
		                                               <td class="text-right" style="font-size:14px"> <?php echo @$currency_symbol;?>@{{values.value}} </td>
		                                            </tr>
 
													<tr>
		                                               <td class="" style="font-size:14px;text-transform: capitalize">Total </td>
		                                               <td class="text-right" style="font-size:14px"> <?php echo @$currency_symbol;?>@{{cart.paymentData[0].order_total}} </td>
		                                            </tr>
 
 
                                                </tbody > 
								            </table>     
								        </div> 
                                       </div>
                                    </div>
									</div>
									 
								  
								 <div class="panel main-cart" ng-show="main.paymentData[0].type == 'payment_details'">
								 
                                    <div class="panel-body">
                                        <div class="row  "    >
										    <div class="col-sm-12">
										       <h5>@{{main.paymentData[0].data.title}}</h5> 
											</div>
											
											<table class="table paymentSummary" > 
		                                        <tbody >
		                                            <tr  ng-repeat="values in main.paymentData[0].data.data ">
		                                               <td class="" style="font-size:14px;text-transform: capitalize">@{{values.title.replace('_', ' ')  }} </td>
		                                               <td class="text-right" style="font-size:14px"> <?php echo @$currency_symbol;?>@{{values.value}} </td>
		                                            </tr>
 
													<tr>
		                                               <td class="" style="font-size:14px;text-transform: capitalize">Total </td>
		                                               <td class="text-right" style="font-size:14px"> <?php echo @$currency_symbol;?>@{{main.paymentData[0].order_total}} </td>
		                                            </tr>
 
                                                </tbody > 
								            </table>     
								        </div> 
                                       </div>
                                    </div>
									 
									
									
                                 <div class="panel" ng-repeat="fields in main.addOrderMeta.fields"  >
								  
                                    <div class="panel-body">
                                       <div ng-repeat="field in fields">
									   				
                                          <!--------------------div show if input type is api---------------------->													
                                          <div class="" ng-show="field.input_type == 'api'"> 
										  <div ng-show="field.identifier =='customer_id'"
											  <h5>@{{field.setting_order_meta_type_title}}</h5>
                                             <md-autocomplete ng-disabled="main.isDisabled" md-no-cache="main.noCache" md-selected-item="main.selectedValue" md-search-text-change="main.searchValueChange(main.searchText)" md-search-text="main.searchText" md-selected-item-change="main.selectedValueChange(data, field)" md-items="data in main.querySearch(main.searchText,field)" md-item-text="data.label" md-min-length="0" placeholder="Select a @{{field.setting_order_meta_type_title}}" md-menu-class="autocomplete-custom-template"  md-input-id="field.identifier" ng-required="field.required_or_not"  ng-model="field.value" > 
                                                <md-item-template>
                                                   <span class="item-title"> 
                                                   <span> @{{data.label}} </span> 
                                                   </span>
                                                   <span class="item-metadata">
                                                   <span> @{{item.mobile}} </span>  
                                                   </span>
                                                </md-item-template>
                                             </md-autocomplete>
                                             <input type="text" ng-model="field.value" style="display: none;"> 
                                          </div> 
										  </div>
									 
									    <!--------------------div for textaraea---------------------->	
										  <div class="form-group"   ng-show="field.input_type == 'text'">
                                                      <label for="@{{field.identifier}}">@{{field.setting_order_meta_type_title}}</label>
                                                      <input type="@{{field.type}}" ng-model="field.value" class="form-control"   id="@{{field.identifier}}"  placeholder="Enter @{{field.setting_order_meta_type_title}}" >
                                                   </div> 
										  </div>
                                       
                                          </div>
                                       </div>
                                     
                                 <!--   <md-button class="md-raised md-primary" flex="50" ng-click="addOrder()">CREATE ORDER</md-button>-->
                                 <button class="btn btn-success btn-block" ng-click="addOrder();">CREATE ORDER</button>
                              </div>
                              
							  
		 				  
                              <!-------------------------------Modal For Product Variants Starts Here---------------------------------->
                              <div id="variantsModal" class="modal fade" role="dialog" data-backdrop="false" style="z-index:9999999;">
                                 <div class="modal-dialog modal-lg" style="z-index:9999999;width:80%;" data-backdrop="false">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                       <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                          <h4 class="modal-title">Product Variants</h4>
                                       </div>
                                       <div class="modal-body">
                                          <div ng-repeat="data in main.variantsdata" ng-show="data.item_variant_values != ''">
                                             @{{data.item_variant_type_title}}  
                                             <div class="form-group"    >
                                                <label class="checkbox-inline" ng-repeat="variant in data.item_variant_values" >
                                                <input type="checkbox" id="@{{variant.item_variant_value_id}}" ng-model="variant.value" value="@{{variant.item_variant_value_title}}" ng-click="getVariantValue(variant);" >
                                                @{{variant.item_variant_value_title}}<span ng-show="@{{variant.item_variant_price_difference}}"> (+@{{variant.item_variant_price_difference}})</span> <span ng-show="@{{variant.price}}">(@{{variant.price}})</span>
                                                </label>
                                             </div>
											 
											 
											<!--- <div class="form-group">
                                                <label class="radio-inline" ng-repeat="variant in data.variants" ng-show="data.maximum_selection_needed =='0' || data.maximum_selection_needed =='1'">  
                                                <input type="radio" name="@{{data.title}}" id="@{{variant.id}}" ng-model="selectedVariant" value="@{{variant.title}}" ng-click="getVariantValue2(variant);" ng-required="data.minimum_selection_needed >= '1'">
                                                @{{variant.title}}<span ng-show="@{{variant.price_difference}}"> (+@{{variant.price_difference}})</span> <span ng-show="@{{variant.price}}">(@{{variant.price}})</span>
                                                </label>
                                             </div>-->
											 
                                          </div>
                                          <button class="btn btn-info" ng-click="main.addVariant();">Add Variant</button>
                                       </div>
                                       <div class="modal-footer">
                                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <!-------------------------------Modal For Product Variants Ends Here---------------------------------->
							  
							  
                             
                                  
                              
                              <!--------------------------Angular App Ends ------------------------------------>
                           </div>
						   
 		 
				
                        </div>
                     </div>
                  </div>
				   </div>
			  </div>
      </div>					
      </section>
     </div>
      </div>
   </div>
</div>


<!------>
 
<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers/orderAdd.js')}}"></script> 



 <script>
$(document).ready(function(){
  // Add smooth scrolling to all links
  $("a").on('click', function(event) {
alert('1');
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});
</script>

@endsection