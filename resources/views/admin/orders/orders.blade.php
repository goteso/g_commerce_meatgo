@extends('admin.layout.auth')
@section('title', 'Orders' ) 
<link rel="stylesheet" href="{{ URL::asset('admin/assets/css/custom.css')}}">
 <style>
		@media(min-width:1200px) {.header-tab{width:19%!important;}}
		 </style>
@section('content')
@section('header')
@include('admin.includes.header')
@show

<?php $currency_symbol = @\App\Setting::where( 'key_title' , 'currency_symbol' )->first(['key_value'])->key_value;  ?>
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
   <div id="wrapper" >
      <div id="layout-static">
         <!---------- Static Sidebar Starts------->			
         @section('sidebar')
         @include('admin.includes.sidebar')
         @show
         <!---------- Static Sidebar Ends------->
         <div class="static-content-wrapper"  >
            <section id="main-header">
               <div class="container-fluid">
                  <div class="row">
				  <div class="col-sm-5" ng-controller="searchController" ng-cloak > 
                                    @include('admin.includes.realtime-search')
                                 </div>
                     <div class="col-sm-12">
                        <textarea id="res" style="display:  none ;" ></textarea>
						
						<!-------------Loader Starts here------------->
                        <div id="loading" class="loading" style="display:none ;">
                           <img src="{{URL::asset('admin/assets/images/89.svg')}}" class="img-responsive center-block">				 
                           <p >Calling all data...</p>
                        </div>
						<!-------------Loader Ends here------------->
						 
                        <!--------------------------- Angular App Starts ---------------------------->
                        <div class="tab-content"   >
                           <div class="container-fluid" >
                              <div class="row">
                                 <div class="col-sm-7" >
                                    <h2 class="header">Orders</h2>
                                 </div>
                                 <div class="col-sm-5 text-right">
                                 </div>
                              </div>
							  
                              <div class="row" >
                                 <div class="col-sm-12" ng-controller="ordersController as ctrl" ng-cloak >
								 
               <ul class="nav nav-tabs" >
				  <li ng-class="{active: $index == selected}"style="width:100%;" >
						 <div class="col-sm-4 col-md-3 col-lg-2 header-tab"   style="background:@{{tab.color}}" ng-repeat="tab in ctrl.orders.count_data"> 
						    <button  class="btn" ng-click="ctrl.getOrdersByStatus(tab.identifier)"   style="width:100%;outline:none;background:transparent;color:white;text-align: left;padding: 0px 12px 8px;"  >
							  <h3 ><?php echo $currency_symbol; ?>@{{tab.total_orders_amount}} </h3>
							  <p>@{{tab.count}} @{{tab.title}} Orders</p>
						   </button>
						   </div> 
						  
						 </li>
                    </ul> 
						 
                                    <div class="" >
                                       <br> 
                                       <div id="tableToExport" class="products-table table-responsive"  >
                                          <table class="table" class="table table-striped" id="exportthis" >
                                             <thead>
                                                <tr> 
                                                   <th>ID</th>
                                                   <th>CUSTOMER NAME</th>
												   <th>STORE NAME</th>
                                                   <th>TOTAL</th>
                                                   <th>ORDER STATUS</th>
                                                   <th>CREATED</th>
                                                  <!-- <th>ACTIONS</th>-->
                                                </tr>
                                             </thead>
                                             <tbody >
                                                <tr  ng-repeat="values in ctrl.orders.data.data  "> 
                                                   <td><a href="{{ URL::to('/v1/order_detail')}}/@{{values.order_id}}" ><b>#@{{values.order_id}}</b></a> </td>
                                                   <td>@{{values.customer_details[0].first_name}} @{{values.customer_details[0].last_name}}</td>
												    <td>@{{values.store_details[0].store_title}}  </td>
                                                   <td ><?php echo $currency_symbol;?>@{{values.total}}</td>
                                                   <td><span style="background-color:@{{values.status_details[0].label_colors}};padding:2px 10px;border-radius:20px;">@{{values.order_status}}</span></td>
                                                   <td>@{{values.created_at_formatted}}</td>
                                                   <!--<td class="actions">
                                                      <a class="btn btn-xs edit-product" href="{{ URL::to('v1/form/item')}}/@{{values.item_id}}" ><i class="fa fa-edit"></i></a>
                                                      <a class="btn btn-xs delete-product" ng-click="ctrl.removeChoice(values.item_id, $index)"><i class="fa fa-trash-o"></i></a>
                                                      <!--  <a class="btn btn-xs edit-product" href="{{ URL::to('product_edit')}}/@{{values.id}}" ><img src="{{url::to('admin/images/edit.png')}} " style="height:16px;width:16px"/></a>
                                                         <a class="btn btn-xs delete-product" ng-click="ctrl.removeChoice(values.id, $index)"><img src="{{url::to('admin/images/bin.png')}} " style="height:16px;width:13px"/></a>--
                                                   </td>-->
                                                </tr>
                                             </tbody>
                                          </table>
                                       </div>  
									   <div class="pagination text-right" style="display:block" >
                                           <button class="btn" ng-show="ctrl.orders.data.first_page_url != null" ng-click="ctrl.pagination(ctrl.orders.data.first_page_url);">First</button> 
                                           <button class="btn"  ng-show="ctrl.orders.data.prev_page_url != null" ng-click="ctrl.pagination(ctrl.orders.data.prev_page_url);">Previous</button> 
                                          <span>@{{ctrl.orders.data.current_page}}</span>
                                          <button class="btn"  ng-show="ctrl.orders.data.next_page_url != null" ng-click="ctrl.pagination(ctrl.orders.data.next_page_url);">Next</button> 
                                          <button class="btn"  ng-show="ctrl.orders.data.last_page_url != null" ng-click="ctrl.pagination(ctrl.orders.data.last_page_url);">Last</button> 
                                       </div>
									    
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!--------------------------- Angular App Ends ---------------------------->
                     </div>
                  </div>
               </div>
         </div>
         </section>
      </div>
   </div>
</div>
<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers/orders.js')}}"></script> 

<!------>
@endsection