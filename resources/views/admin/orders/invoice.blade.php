<!DOCTYPE html>
<html>
<head>
	<!-- Define Charset -->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	
	<!-- Responsive Meta Tag -->
	<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" />

	<title>Invoice</title>
	
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,300,400,700" />
	 <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
	 	<script> var APP_URL = '<?php echo env("APP_URL", "");?>';  </script>
</head>

<?php $currency_symbol = @\App\Setting::where( 'key_title' , 'currency_symbol' )->first(['key_value'])->key_value;  ?>
<body ng-app="mainApp">
<div  ng-controller="invoiceController" ng-cloak>
     <textarea id="res" style="display:none ;" ></textarea>
<table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="" >
  <tr>
     <td height="30" style="font-size: 50px; line-height: 30px;">&nbsp;</td>
	  
  </tr>
  <tr >
     <td >
	  <table border="0" align="center" width="100%" cellpadding="0" cellspacing="0"  border="0"   class="container590 bodybg_color" style="border:1px solid #f5f5f5;padding:10px;" id="exportthis">
			 <tr>
				 <td >
				
		 <table border="0" align="center" width="100%" cellpadding="0" cellspacing="0"  border="0" bgcolor="f7f7f7" class="container590 bodybg_color"   >
			 <tr>
				 <td>
				     <table border="0" align="center" width="100%" cellpadding="0" cellspacing="0" class="container590 "  ng-repeat="data in invoiceData" ng-show="data.type == 'order_basic_details'">
								
								<tr><td height="15" style="font-size: 15px; line-height: 15px;">&nbsp;</td></tr>
								<tr><td style="text-align:-webkit-center">  
								  <img width="156" ng-show="invoice.app_logo" border="0" style="display: block; width: 150px;" src="{{ URL::asset('logo')}}/@{{invoice.app_logo}}" alt="logo" /> 
								  </td></tr>
								<tr>
								 <td>
								 <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590" >
								 
								<tr>
								<td style="font-family:'Open Sans Semibold',sans-serif;font-size:20px"> INVOICE #@{{data.data[0].order_id}} </td>
								</tr>
								</table>
								
								 <table border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590"> 
								<tr>
								 <td style="font-family:'Open Sans',sans-serif;" align="right">
								   
								
								   <p style="line-height:25px;font-size:16px;">Date : @{{data.data[0].created_at}}
								  </p>
								</td>
								
								 
								</tr>
								</table>
								
								</td>
								</tr>
								
								
								<tr>
								 <td>
								 <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590"  >
								<tr>
								<td >
								  <p style="font-family:'Open Sans',sans-serif;margin:8px 0 0 0;">Customer Details:</p>
								  <p style="font-family:'Open Sans',sans-serif;line-height:25px;font-size:16px;margin:0px;">@{{data.data[0].customer_details[0].first_name}} @{{data.data[0].customer_details[0].last_name}}</p>
								  <p style="font-family:'Open Sans',sans-serif;line-height:25px;font-size:16px;margin:0px;">@{{data.data[0].customer_details[0].email}}</p>
								  <p style="font-family:'Open Sans',sans-serif;line-height:25px;font-size:16px;margin:0px;">@{{data.data[0].customer_details[0].phone}}</p> 
								</td>
								</tr>
								<tr >
								<!--<td  style="font-family:'Open Sans',sans-serif;" > 
								 <span ng-show="data.data[0].order_address_array[0].data[0].address_line1">@{{data.data[0].order_address_array[0].data[0].address_line1}} </span><br>
								   <span ng-show="data.data[0].order_address_array[0].data[0].address_line2">@{{data.data[0]order_address_array[0]..data[0].address_line2}}</span><br>
								   <span ng-show="data.data[0].order_address_array[0].data[0].city">@{{data.data[0]order_address_array[0]..data[0].city}}</span><br>
								   <span ng-show="data.data[0].order_address_array[0].data[0].state"> @{{data.data[0]order_address_array[0]..data[0].state}}</span><br>
									<span ng-show="data.data[0].order_address_array[0].data[0].country"> @{{data.data[0]order_address_array[0].data[0].country}}</span><br>
								</td>-->
								</tr>
								</table>
								
								 <table border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590"> 
								<tr>
								<td align="right"  > 
								 <pre style="font-family:'Open Sans',sans-serif;line-height:25px;font-size:16px;">@{{invoice.vendor_details}}</pre>
								    
								</td>
								</tr>
								 <tr>
								<td align="right"  > 
								 <!--<p style="font-family:'Open Sans',sans-serif;line-height:25px;font-size:16px;">GST NO. : <?php echo env('GST_NUMBER');?></p>-->
								    
								</td>
								</tr>
								 
								</table>
								
								</td>
								
								</tr>
					</table>
				 </td>
			 </tr>
		 </table>
		 
		 <table border="0" align="center" width="100%" cellpadding="0" cellspacing="0"  border="0" bgcolor="ffffff" class="container590 bodybg_color"   >
		  <tr>
		    <td>
			   <table border="0" align="center" width="100%" cellpadding="0" cellspacing="0" class="container590 "  >
								
					<tr><td height="5" style="font-size: 5px; line-height: 5px;">&nbsp;</td></tr>
								
					<tr>
						<td style="font-family:'Open Sans',sans-serif;">
						  <h4 style="margin:10px 0;"> @{{invoice.products.title}}</h4>
						</td>
					</tr>
					
					<tr>
					   <td>  
					    <table border="0" width="100%" align="0" cellpadding="0" cellspacing="0" bgcolor="#f7f7f7"  >
											<thead>
											<tr bgcolor="" > 
												<th  align="left" height="40" valign="middle" style="color: #000; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px;  ">
													Description
												</th>
												<th align="center" height="40" valign="middle" style="color: #000; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px;  ">
													Rate
												</th>
												<th align="center" height="40" valign="middle" style="color: #000; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px;  ">
													Quantity
												</th>
												<th align="right" height="40" valign="middle" style="color: #000; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px;  ">
													Total
												</th> 
											</tr>
											</thead>
											<tbody ng-repeat="data in invoiceData"  ng-show="data.type =='items'">
											<tr ng-repeat="value in data.data"  > 
												<td  align="left" height="40" valign="middle" style="color: #686b74; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px; border-top: solid 1px #f1f1f1;">
													@{{value.item_title}} 
													<pre ng-hide="!value.variants_desc" style="margin: 0px; font-size: 12px; line-height:18px;font-family: 'Open Sans', sans-serif;">@{{value.variants_desc  }}</pre>
												</td>
												<td align="center" height="40" valign="middle" style="color: #686b74; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px; border-top: solid 1px #f1f1f1;">
													@{{invoice.currency_symbol}}@{{value.item_price}}<span ng-show="value.unit">/@{{value.unit}}</span>
												</td>
												<td align="center" height="40" valign="middle" style="color: #686b74; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px; border-top: solid 1px #f1f1f1;">
													@{{value.order_item_quantity}}
												</td>
												<td align="right" height="40" valign="middle" style="color: #686b74; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px; border-top: solid 1px #f1f1f1;">
													<?php echo $currency_symbol;?> @{{value.item_price * value.order_item_quantity}}
												</td> 
											</tr> 
											</tbody>
										</table>
					</td>
					</tr>
					
					 <tr ><td height="20" bgcolor="#f7f7f7"  style="font-size: 20px; line-height: 20px;">&nbsp;</td></tr> 
								
								<tr>
									<td >
										<table border="0"  bgcolor="#f7f7f7" align="right" width="100%" cellpadding="0" cellspacing="0" ng-repeat="data in invoiceData"  ng-show="data.type =='payment_details'">
											<tr ng-repeat="value in data.data">
												<td align="left" height="" valign="middle" style="color: #686b74; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px;border-top:1px solid #c5c5c5">
													@{{value.title}}
												</td>
												<td align="right" height="" valign="middle" style="color: #686b74; font-size: 14px; font-family: 'Open Sans', sans-serif; mso-line-height-rule: exactly; line-height: 26px;border-top:1px solid #c5c5c5">
													<?php echo $currency_symbol;?> @{{value.value}}
												</td>
											</tr> 
											<tr  >
												<td align="left" height="" valign="middle" style="color: #686b74; font-size: 14px; font-family: 'Open Sans Semibold', sans-serif; mso-line-height-rule: exactly; line-height: 26px;border-top:1px solid #c5c5c5">
													Total
												</td>
												<td align="right" height="" valign="middle" style="color: #686b74; font-size: 14px; font-family: 'Open Sans Semibold', sans-serif; mso-line-height-rule: exactly; line-height: 26px;border-top:1px solid #c5c5c5">
													<?php echo $currency_symbol;?> @{{data.order_total}}
												</td>
											</tr> 
										</table>
									</td>
								</tr>
								
								
		 
					
					
								
				</table>
				 
				<!--<table border="0"  bgcolor="#ffffff" align="center" width="100%" cellpadding="0" cellspacing="0"  >
				<tr ><td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td></tr>
				<tr>
				  <td style="font-family:'Open Sans',sans-serif;font-size:14px;">
				  
				  <p style="text-align:center"> For Terms & Conditions, <span ><a style="text-decoration:none;color:;" href="<?php echo env('APP_URL','');?>" target="_blank">www.goteso.com</a></span></p></td>
				</tr> 
				</table>-->
				
			</td>
		  </tr> 
		 </table>
		  </td>
			 </tr>
		 </table>
		 
	  </td>
  </tr>
</table>

<div id="editor"></div>
<!--<button id="pdf-button" type="button" value="Download PDF" ng-click="export()">Download as PDF</button>-->

</div>


 
<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers/invoice.js')}}"></script> 


</body>
</html>