@extends('admin.layout.auth')
@section('title', 'Notifications' ) 

	<link href="{{URL::to('admin/assets/editor/bootstrap3-wysihtml5.min.css')}}">
<link rel="stylesheet" href="{{ URL::asset('admin/assets/css/custom.css')}}">

<style type="text/css" media="screen">
  .btn.jumbo {
    font-size: 20px;
    font-weight: normal;
    padding: 14px 24px;
    margin-right: 10px;
    -webkit-border-radius: 6px;
    -moz-border-radius: 6px;
    border-radius: 6px;
  }
  ul.wysihtml5-toolbar {
    margin: 0;
    padding: 0;
  display: block;}
  
  ul.wysihtml5-toolbar>li{
      float: left;
    display: list-item;
    list-style: none;
    margin: 0 5px 10px 0;
	}
</style>
@section('content')


<!------
<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=zl00639cxqfb9dlwusi00sjd71beh2jmk90t7quzey8o69t4"></script>
 
<script>tinymce.init({
  selector: "textarea",  // change this value according to your HTML
  
 
});</script>
----->

@section('header')
@include('admin.includes.header')
@show
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
   <div id="wrapper" >
      <div id="layout-static">
         <!---------- Static Sidebar Starts------->			
         @section('sidebar')
         @include('admin.includes.sidebar')
         @show
         <!---------- Static Sidebar Ends------->
         <div class="static-content-wrapper"  >
		 
<section id="main-header">
   <div class="container-fluid">
      <div class="row">
         <div class="col-sm-6"   ng-controller="searchController" ng-cloak > 
            @include('admin.includes.realtime-search')
         </div>
         <div class="col-sm-12">
            <div class="text-right">
            </div>
            <div class="tab-content"  ng-controller="notificationController" ng-cloak  >
               <!--------------------------- Angular App Starts ---------------------------->
               <textarea id="res" style="display: none  ;" ></textarea>
                  <div id="loading" class="loading" style="display:none ;">
                  <img src="{{URL::asset('admin/assets/images/89.svg')}}" class="img-responsive center-block">				 
                  <p >Calling all data...</p>
                  </div>   
               <div class="container-fluid" >
                  <div class="row">
                     <div class="col-sm-10" >
                        <h2 class="header">Mass Emails to Customers</h2>
                     </div>
                     <div class="col-sm-2">
                        <div class="text-right">
                           
                        </div>
                     </div>
                  </div>
                  <div class="row" >
				 
	
                     <div class="col-sm-12 panel" >
                        <div class="panel-body  category-add" >
						
						 <div class="row" >
						 

<div class="col-sm-8">
  <h5>Select Customer (Optional)</h5>
  <tags-input ng-model="user_id" id="customer_ids" add-from-autocomplete-only="true" allow-leftover-text="false" display-property="first_name" key-property="user_id" text="text"  ng-blur="text=''" placeholder="Select Customers" >
        <auto-complete min-length="1" highlight-matched-text="true" source="searchData($query)" ></auto-complete>
                                </tags-input>
<br></div>



						  <div class="col-lg-8 nopadding">
								 <textarea class="textarea" id="email_body" placeholder="Enter text ..." style="width: 100%; height: 200px; font-size: 14px; line-height: 18px;"></textarea>
							 
							 
							 <br><br>
                            <md-input-container  class="md-block">
                        <label>Subject</label>
                        <input type="text" id="subject" name="subject" />
                     </md-input-container>
                      
					 <!-- <md-input-container  class="md-block text-left">
                        <label>Email Content</label>
                        <textarea id="email_body1" name="email_body1" rows="5" max-rows="5"  md-no-autogrow></textarea>
                     </md-input-container>-->
					 </div>
					 <div class="col-sm-4" ><br><br><br>
                     <md-button ng-click="emails_to_customers()" class="md-raised bg-color md-submit" style="margin-top:120px;">Send</md-button>
                        </div>
                     </div>
                     
                  </div>
               </div>
           
            <!--------------------------- Angular App Ends ---------------------------->
         
   
   
   
   
	
	 
	 </div>
	</div>
      </div>
   </div>
	
</section>

 </div>
   </div>
   </div>
</div>
   <!-- Include external CSS. -->
    <link rel="stylesheet" href="http://bootstrap-wysiwyg.github.io/bootstrap3-wysiwyg/components/components-font-awesome/css/font-awesome.min.css">
  
	
    <script type="text/javascript" src="http://bootstrap-wysiwyg.github.io/bootstrap3-wysiwyg/components/wysihtml5x/dist/wysihtml5x-toolbar.min.js"></script>
    <script type="text/javascript" src="http://bootstrap-wysiwyg.github.io/bootstrap3-wysiwyg/components/handlebars/handlebars.runtime.min.js"></script>
 
    <!-- Include Editor JS files. -->
    <script type="text/javascript" src="http://bootstrap-wysiwyg.github.io/bootstrap3-wysiwyg/dist/bootstrap3-wysihtml5.min.js"></script>
 
<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers/notifications.js')}}"></script> 
<script type="text/javascript">
		  $('.textarea').wysihtml5({
    toolbar: {
      fa: true
    }
  });
  </script>
  
@endsection