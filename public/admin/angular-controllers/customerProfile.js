 app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : APP_URL+"/customer-basicInfo.html",
        controller : "profileController"
    })
    .when("/order", {
        templateUrl : APP_URL+"/customer-order.html",
        controller : "ordersController"
    }) 
	 .when("/address", {
        templateUrl : APP_URL+"/customer-address.html",
        controller : "addressController"
    }) ;
});
 
 
 
 app.controller('profileDataController', function($http,$scope,toastr) {
 
   $scope.url = window.location.href;
 
 var value = $scope.url;
 var parts = value.split('/');
 var index = parts.length - 2;
 var str = parts[index];
 $scope.customer_id = str.replace("#", ""); 

  
 
	   //$('#loading').css('display', 'none');  
	   
		 var request = $http({
                 method: "GET",
                 url: APP_URL+'/api/v1/users/'+$scope.customer_id+'?include_meta=true',
                 data:  '',
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {  
			  $scope.profileData = data;
			  $('#loading').css('display', 'none');
			 document.getElementById("res").value =JSON.stringify(data); 
         }).error(function (data, status, headers, config) { 
			  
			document.getElementById("res").value =JSON.stringify(data);
               
        });   
});



//====================================================== PROFILE CONTROLLER=================================================================================
//==============================================================================================================================================================
 
app.controller('profileController', function($http,$scope,toastr) {
 
   $scope.url = window.location.href;

 // Jugaadi code starts here ====================created by Deepakshi Singla
 var value = $scope.url;
 var parts = value.split('/');
 var index = parts.length - 2;
 var str = parts[index];
 $scope.customer_id = str.replace("#", "");
  // Jugaadi code starts here ====================created by deepakshi Singla ends

 
    
 
	   $('#loading').css('display', 'none');  
	   
		 var request = $http({
                 method: "GET",
                 url: APP_URL+'/api/v1/users/'+$scope.customer_id+'?include_meta=true',
                 data:  '',
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {  
			  $scope.profileData = data;
			  $('#loading').css('display', 'none');
			 document.getElementById("res").value =JSON.stringify(data); 
         }).error(function (data, status, headers, config) { 
			  
			document.getElementById("res").value =JSON.stringify(data);
               
        });   
});




 //====================================================== ORDER CONTROLLER=================================================================================
//==============================================================================================================================================================
 
app.controller('ordersController', function($http,$scope,toastr) {
 
   $scope.url = window.location.href;
 
 var value = $scope.url;
 var parts = value.split('/');
 var index = parts.length - 2;
 var str = parts[index];
 $scope.customer_id = str.replace("#", ""); 
 

console.log("Url = "+APP_URL+'/api/v1/orders?per_page=2&customer_id='+$scope.customer_id);
     
	   $('#loading').css('display', 'none');  
	   
		 var request = $http({
                 method: "GET",
                 url: APP_URL+'/api/v1/orders?per_page=10&customer_id='+$scope.customer_id,
                 data:  '',
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {  
			  $scope.ordersData = data;
			  $('#loading').css('display', 'none');
			  console.log("Success Response = "+JSON.stringify(data));
         }).error(function (data, status, headers, config) { 
			  
		console.log("Error Response = "+JSON.stringify(data));
               
        });   











		 // FUNCTION FOR PAGINATION  ============================================================== clean done
		$scope.pagination = function(api_url){ 
			 if(api_url == null || api_url== ''){
				 return false;
			 }
			 else{
			 $('#loading').css('display', 'block'); 
		
		var request = $http({
            method: "GET",
            url: api_url+'&per_page=10&customer_id='+$scope.customer_id,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            $scope.ordersData = data;
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
		} 
		}


});




 //====================================================== ADDRESS CONTROLLER=================================================================================
//==============================================================================================================================================================
 
 
app.controller('addressController', function($http,$scope,toastr) {
 
  	$scope.url = window.location.href;

 // Jugaadi code starts here ====================created by Deepakshi Singla
 var value = $scope.url;
 var parts = value.split('/');
 var index = parts.length - 2;
 var str = parts[index];
 $scope.customer_id = str.replace("#", "");
  // Jugaadi code starts here ====================created by deepakshi Singla ends
  
   
		//================================ FUNCTION FOR GET ADDRESS ================================================================================ 
			 var request = $http({
                 method: "GET",
                 url: APP_URL+'/api/v1/address?linked_id='+$scope.customer_id,
                 data:  '',
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request .success(function(data, status, headers, config) {
                $scope.addressData = data; 
				  $('#loading').css('display', 'none');
			   document.getElementById("res").value =JSON.stringify(data);
 
            })
            .error(function(data, status, headers, config) {
0
                document.getElementById('res').value = JSON.stringify(data);
            });


	

  $scope.locationpickerOptions = {
                        location: {
                            latitude: 30.65118399999999,
                            longitude: 76.81360100000006
                        },
                        inputBinding: {
                            latitudeInput: $('#us3-lat'),
                            longitudeInput: $('#us3-lon'),
                            radiusInput: $('#us3-radius'),
                            locationNameInput: $('#us3-address') 
                        },
						zoom: 15,

                        radius: 0,
                        enableAutocomplete: true,
						 styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"color":"#7c93a3"},{"lightness":"-10"}]},{"featureType":"administrative.country","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"administrative.country","elementType":"geometry.stroke","stylers":[{"color":"#a0a4a5"}]},{"featureType":"administrative.province","elementType":"geometry.stroke","stylers":[{"color":"#62838e"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"color":"#dde3e3"}]},{"featureType":"landscape.man_made","elementType":"geometry.stroke","stylers":[{"color":"#3f4a51"},{"weight":"0.30"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"poi.attraction","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.government","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.place_of_worship","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.school","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.sports_complex","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":"-100"},{"visibility":"on"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#bbcacf"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"lightness":"0"},{"color":"#bbcacf"},{"weight":"0.50"}]},{"featureType":"road.highway","elementType":"labels","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"labels.text","stylers":[{"visibility":"on"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.stroke","stylers":[{"color":"#a9b4b8"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"invert_lightness":true},{"saturation":"-7"},{"lightness":"3"},{"gamma":"1.80"},{"weight":"0.01"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#a3c7df"}]}],
						 
						markerIcon: APP_URL+"/admin/assets/images/map-marker.png",
                       markerDraggable: true,
                       markerVisible : true
                    };
					


					
			//================================ FUNCTION FOR STORE NEW ADDRESS ================================================================================
    
                $scope.add_address = function() {
				 
					 $scope.address_type = 'customer'  ;
					 $scope.address_title = $scope.address_title;
					 $scope.longitude = $('#us3-lon').val();
					 $scope.latitude = $('#us3-lat').val();
					 $scope.address_phone = $('#address_phone').val();
					 $scope.address_line1 = $('#address_line1').val();
					 $scope.address_line2 = $('#address_line2').val();
					 $scope.city = $('#city').val();
					 $scope.state = $('#state').val();
					 $scope.pincode = $('#pincode').val();
					 $scope.country = $('#country').val();

					 
					$scope.address_data = {"address_type": $scope.address_type, "linked_id": $scope.customer_id, "address_title": $scope.address_title, "address_phone": $scope.address_phone, "address_line1": $scope.address_line1,
					"address_line2": $scope.address_line2, "latitude": $scope.latitude, "longitude": $scope.longitude, "city": $scope.city, "state": $scope.state, "pincode": $scope.pincode, "country": $scope.country}
				  
				    console.log(JSON.stringify($scope.address_data));
				 
					/*get data from api**/
					  var request = $http({
						method: "POST",
						url: APP_URL+'/api/v1/address',
						data: $scope.address_data,
						headers: {
							'Accept': 'application/json'
						}
					});

					/* Check whether the HTTP Request is successful or not. */
					request.success(function(data) { 
						$scope.data = data;
					    if(data.status_code == 1){
						   toastr.success(data.message,'Success!');
						   $('#address_add').modal('hide');
						   location.reload();
				        }
					    else{
						   toastr.error(data.message,'Error!');
					    }  
						//$window.location.href = 'product_edit/'+$scope.product_id;
					}).error(function(data, status, headers, config) {
						  toastr.error('Error Occurs','Error!');
						 document.getElementById("res2").value = JSON.stringify(data);
					});
				}
	
	
 
 
 
        //================================ FUNCTION FOR GET PARTICULAR ADDRESS ================================================================================
    
           $scope.getEditValues = function(data) { 
$scope.address_id = data.address_id		   
                $scope.address_type_edit = data.address_type ;
                $scope.address_title_edit = data.address_title;
				$scope.longitude_edit = data.longitude;
				$scope.latitude_edit = data.latitude;
				$scope.address_phone_edit = data.address_phone;
				$scope.address_line1_edit = data.address_line1;
			    $scope.address_line2_edit = data.address_line2;
				$scope.city_edit = data.city;
				$scope.state_edit = data.state;
				$scope.pincode_edit = data.pincode;
				$scope.country_edit = data.country;
				
	            $('#myModal_edit').modal('show');
            }
	
	
	
		

  $scope.locationpickerOptionsEdit = {
                        location: {
                            latitude: 30.65118399999999,
                            longitude: 76.81360100000006
                        },
                        inputBinding: {
                            latitudeInput: $('#us3-lat-edit'),
                            longitudeInput: $('#us3-lon-edit'),
                            radiusInput: $('#us3-radius-edit'),
                            locationNameInput: $('#us3-address-edit') 
                        },
						zoom: 15,

                        radius: 0,
                        enableAutocomplete: true,
						 styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"color":"#7c93a3"},{"lightness":"-10"}]},{"featureType":"administrative.country","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"administrative.country","elementType":"geometry.stroke","stylers":[{"color":"#a0a4a5"}]},{"featureType":"administrative.province","elementType":"geometry.stroke","stylers":[{"color":"#62838e"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"color":"#dde3e3"}]},{"featureType":"landscape.man_made","elementType":"geometry.stroke","stylers":[{"color":"#3f4a51"},{"weight":"0.30"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"poi.attraction","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.government","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.place_of_worship","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.school","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.sports_complex","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":"-100"},{"visibility":"on"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#bbcacf"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"lightness":"0"},{"color":"#bbcacf"},{"weight":"0.50"}]},{"featureType":"road.highway","elementType":"labels","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"labels.text","stylers":[{"visibility":"on"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.stroke","stylers":[{"color":"#a9b4b8"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"invert_lightness":true},{"saturation":"-7"},{"lightness":"3"},{"gamma":"1.80"},{"weight":"0.01"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#a3c7df"}]}],
						 
						markerIcon: APP_URL+"/admin/assets/images/map-marker.png",
                       markerDraggable: true,
                       markerVisible : true
                    };
					
	
	     //================================ FUNCTION FOR UPDATE A PARTICULAR ADDRESS ================================================================================
    
	       $scope.user_address_update = function() {
  
		  $('#myModal_edit').modal('hide');
		  
		  
					 $scope.address_type_edit = $scope.address_type_edit;
					 $scope.address_title_edit = $('#address_title_edit').val(); 
					 $scope.longitude_edit = $('#us3-lon-edit').val();
					 $scope.latitude_edit = $('#us3-lat-edit').val();
					 $scope.address_phone_edit = $('#address_phone_edit').val();
					 $scope.address_line1_edit = $('#address_line1_edit').val();
					 $scope.address_line2_edit = $('#address_line2_edit').val();
					 $scope.city_edit = $('#city_edit').val();
					 $scope.state_edit = $('#state_edit').val();
					 $scope.pincode_edit = $('#pincode_edit').val();
					 $scope.country_edit = $('#country_edit').val();

					 
					$scope.address_updated_data = {"address_type": $scope.address_type_edit, "linked_id": $scope.customer_id, "address_title": $scope.address_title_edit, "address_phone": $scope.address_phone_edit, "address_line1": $scope.address_line1_edit,
					"address_line2": $scope.address_line2_edit, "latitude": $scope.latitude_edit, "longitude": $scope.longitude_edit, "city": $scope.city_edit, "state": $scope.state_edit, "pincode": $scope.pincode_edit, "country": $scope.country_edit}
				  
				    console.log(JSON.stringify($scope.address_updated_data));
		 
        /*get data from api**/
        var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/address/'+$scope.address_id,
            data: $scope.address_updated_data,
            headers: {
                'Accept': 'application/json'
            }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function(data) {
			 $scope.data1 = data;
					    if(data.status_code == 1){
						   toastr.success(data.message,'Success!');
						   $('#address_add').modal('hide');
				        }
					    else{
						   toastr.error(data.message,'Error!');
					    }  
        }).error(function(data, status, headers, config) {
			toastr.error(data.message,'Error!');
			console.log(JSON.stringify(data));
             document.getElementById("res2").value = JSON.stringify(data);
        });


    }
	
	
		$scope.user_address_delete = function(itemId, index) {
     
	$scope.user_address_id = {user_address_id:itemId};
	
	   if (confirm("Are you sure?")) {
                 
				 var request = $http({
                 method: "POST",
                 url: APP_URL+'/user_address_delete',
                 data: $scope.user_address_id,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {
				
				$scope.data = data;
			   toastr.success(data.message,'Success!');
			   document.getElementById("res").value = JSON.stringify(data);
			    
			 }).error(function (data, status, headers, config) { 
			    toastr.error('Unknown Error Occurred','Error!');
			    document.getElementById("res").value = JSON.stringify(data);
				
        }); 
	   }
		
    };
	
	
});





