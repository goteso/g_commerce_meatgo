
app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : APP_URL+"/coupon-active.html",
        controller : "couponController as ctrl"
    })
    .when("/expired", {
        templateUrl : APP_URL+"/coupon-expired.html",
        controller : "couponExpiredController as ctrl"
    })
    .otherwise({
            redirectTo: '/'
        });	;
});
		
	
//======================================================VIEW COUPONS ACTIVE LIST CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('couponController', function ($http,$scope,$window,toastr,$location) {
	
		var auth_user_id = document.getElementById('auth_user_id').value;
        var pro = this; 
        $('#loading').css('display', 'block'); 
		
		var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/coupons?auth_user_id='+auth_user_id+'&active_status=true',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.coupons = data;
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
	
	
	
	
			 // FUNCTION FOR DELETE  COUPON  ============================================================== clean done
	pro.deleteCoupon = function(couponId) {
		    
			pro.coupon_id = couponId;
			
			if (confirm("Are you sure?")) {
		        var request = $http({
                    method: "DELETE",
                    url: APP_URL+'/api/v1/coupons/'+pro.coupon_id,
                    data:  '',
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
				    pro.data = data;
			       

			           if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }


                       location.reload();					
			    })
				.error(function (data, status, headers, config) { 
			        document.getElementById("res").value = JSON.stringify(data); 
				    toastr.error('Error Occurs','Error!');
                }); 
		    }
		
        };
	
	
			
		 // FUNCTION FOR PAGINATION  ============================================================== clean done
		pro.pagination = function(api_url){ 
			 if(api_url == null || api_url== ''){
				 return false;
			 }
			 else{
			 $('#loading').css('display', 'block'); 
		
		var request = $http({
            method: "GET",
            url: api_url+'&auth_user_id='+auth_user_id+'&active_status=true',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.coupons = data;
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
		} 
		}
		
		
 	});
	
	
	
	//======================================================VIEW COUPONS EXPIRED LIST CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('couponExpiredController', function ($http,$scope,$window,toastr) {
		
		var auth_user_id = document.getElementById('auth_user_id').value;
        var pro = this; 
        $('#loading').css('display', 'block'); 
		
		var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/coupons?auth_user_id='+auth_user_id+'&include_expired=true',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.coupons = data;
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
	
	
	 // FUNCTION FOR DELETE  COUPON  ============================================================== clean done
	pro.deleteCoupon = function(couponId) {
		    
			pro.coupon_id = couponId;
			
			if (confirm("Are you sure?")) {
		        var request = $http({
                    method: "DELETE",
                    url: APP_URL+'/api/v1/coupons/'+pro.coupon_id,
                    data:  '',
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
				    pro.data = data;
			   

			       if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }

            
                       location.reload();					
			    })
				.error(function (data, status, headers, config) { 
			        document.getElementById("res").value = JSON.stringify(data); 
				    toastr.error('Error Occurs','Error!');
                }); 
		    }
		
        };
		
		// FUNCTION FOR PAGINATION  ============================================================== clean done
		pro.pagination = function(api_url){ 
			 if(api_url == null || api_url== ''){
				 return false;
			 }
			 else{
			 $('#loading').css('display', 'block'); 
		
		var request = $http({
            method: "GET",
            url: api_url+'&auth_user_id='+auth_user_id+'&include_expired=true',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.coupons = data;
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
		} 
		}
		
		
 	});
	
	
	
	
	
	
//======================================================ADD COUPON CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('addCouponController', function ($http,$scope,$window,toastr,$log,$q) {
		 
		  $scope.today = new Date();
			 
			 
		 	 //========= FUNCTIONS FOR GET STORE====================================================================================== 
		  $scope.storeSearch = function(query){
		        return $http.get(APP_URL+"/api/v1/stores?search="+query, {params: {q: query}})
                .then(function(response){  
                    return response.data.data.data;
                }) 
            };
		  
		    $scope.selectedStoreChange = function(values) { 
                $log.info('Item changed to ' + JSON.stringify(values));  
	            $scope.store_id =  values.store_id;  		
	            $('#store_id').val($scope.store_id);
            }
			
		    $scope.searchStoreChange = function(text){ 
				   $scope.store_id = ''; 
			}
			
         //$('#loading').css('display', 'block');  
		$scope.storeCoupon = function(){
			
            $scope.coupon_type = $('#coupon_type').val();
	 
			
			$scope.coupon_title = $('#title').val();			
			$scope.coupon_code = $('#coupon_code').val(); 
			$scope.expiry = $('#expiry').val(); 
			$scope.valid_from = $('#valid_from').val(); 
			$scope.discount = $('#discount').val(); 
			$scope.max_discount = $('#max_discount').val(); 
			$scope.coupon_desc = $('#coupon_desc').val(); 
			$scope.limit_total = $('#limit_total').val(); 
			$scope.limit_user = $('#limit_user').val(); 
			$scope.minimum_order_amount = $('#minimum_order_amount').val();
			$scope.maximum_order_amount = $('#maximum_order_amount').val();
            $scope.store_id = $('#store_id').val();	
            $scope.coupon_image = $('#file_name').val();	
			
			$scope.couponData = { "coupon_title" : $scope.coupon_title, "type" : $scope.coupon_type,"coupon_code" : $scope.coupon_code, "expiry" : $scope.expiry, "valid_from" : $scope.valid_from, "discount" : $scope.discount, "max_discount" : $scope.max_discount, "coupon_desc" : $scope.coupon_desc, "limit_total" : $scope.limit_total, "limit_user" : $scope.limit_user, "minimum_order_amount" : $scope.minimum_order_amount, "maximum_order_amount" : $scope.maximum_order_amount, "store_id" : $scope.store_id , "coupon_image" : $scope.coupon_image};
		console.log(JSON.stringify($scope.couponData));
	 
		    var request = $http({
                 method: "POST",
                 url: APP_URL+'/api/v1/coupons',
                 data:  $scope.couponData,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {
                $scope.coupons = data;  
				if(data.status_code == 1){
                  toastr.success(data.message, 'Success');
			      location.reload();
				}
				else{
					toastr.error(data.message, 'Error');
				}
             // $('#loading').css('display', 'none');  
			    document.getElementById("res").value = JSON.stringify(data);
            })
		    .error(function (data, status, headers, config) { 
		        document.getElementById("res").value = JSON.stringify(data);
            });       
	
		}
		
		
		
		
		
 	});
		
 
 
 
 
 
 	
//======================================================ADD COUPON CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('editCouponController', function ($http,$scope,$window,toastr,$location,$log) {
		 
         //$('#loading').css('display', 'block'); 

		 $scope.coupon_id = window.location.href.substr(window.location.href.lastIndexOf('/') + 1); 
		
         var request = $http({
                 method: "GET",
                 url: APP_URL+'/api/v1/coupons/'+$scope.coupon_id,
                 data: '',
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) { 
                $scope.coupon = data.data[0];  
				 $scope.store = $scope.coupon.store_id;

				 $('#store_id').val($scope.store);
             // $('#loading').css('display', 'none');  
			    document.getElementById("res").value = JSON.stringify($scope.coupon);
            })
		    .error(function (data, status, headers, config) { 
		        document.getElementById("res").value = JSON.stringify(data);
            });       

			
			
			
			 //========= FUNCTIONS FOR GET STORE====================================================================================== 
		  $scope.storeSearch = function(query){
		        return $http.get(APP_URL+"/api/v1/stores?search="+query, {params: {q: query}})
                .then(function(response){  
                    return response.data.data.data;
                }) 
            };
		  
		  
		   $scope.selectedStoreChange = function(values) { 
                $log.info('Item changed to ' + JSON.stringify(values));  
	            $scope.store_id =  values.store_id ; 				
			    if($scope.store_id != undefined || $scope.store_id != null){
				   $scope.store = $scope.store_id;

				   $('#store_id').val($scope.store);
			    }
            }
			
		   
			
		    $scope.searchStoreChange = function(text){ 
				   $scope.store = ''; 
			}
			
		 
		$scope.updateCoupon = function(){  
			$scope.coupon_title = $('#title').val(); 
			 $scope.coupon_type = $('#coupon_type').val();
		 
			$scope.coupon_code = $('#coupon_code').val(); 
			$scope.expiry = $('#expiry').val(); 
			$scope.valid_from = $('#valid_from').val(); 
			$scope.discount = $('#discount').val(); 
			$scope.max_discount = $('#max_discount').val(); 
			$scope.coupon_desc = $('#coupon_desc').val(); 
			$scope.limit_total = $('#limit_total').val(); 
			$scope.limit_user = $('#limit_user').val(); 
			$scope.minimum_order_amount = $('#minimum_order_amount').val();
			$scope.maximum_order_amount = $('#maximum_order_amount').val(); 
			$scope.coupon_image = $('#file_name').val();	
			 $scope.store_id = $('#store_id').val();	
			 
			$scope.couponData = { "coupon_title" : $scope.coupon_title, "type" : $scope.coupon_type, "coupon_code" : $scope.coupon_code, "expiry" : $scope.expiry, "valid_from" : $scope.valid_from, "discount" : $scope.discount, "max_discount" : $scope.max_discount, "coupon_desc" : $scope.coupon_desc, "limit_total" : $scope.limit_total, "limit_user" : $scope.limit_user, "minimum_order_amount" : $scope.minimum_order_amount, "maximum_order_amount" : $scope.maximum_order_amount,  "store_id" : $scope.store_id ,"coupon_image" : $scope.coupon_image};
		    
			console.log(JSON.stringify($scope.couponData));
			
		    var request = $http({
                 method: "PUT",
                 url: APP_URL+'/api/v1/coupons/'+$scope.coupon_id,
                 data:  $scope.couponData,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {
                $scope.coupons = data; 
				
				if(data.status_text == 'Success'){
             toastr.success(data.message, 'Success');
				}
				else{

					console.log(JSON.stringify(data));
					toastr.error(data.message, 'Error');
				}
			 //location.reload();
             // $('#loading').css('display', 'none');  
			    document.getElementById("res").value = JSON.stringify(data);
            })
		    .error(function (data, status, headers, config) { 
		        document.getElementById("res").value = JSON.stringify(data);
            });       
	
		}
 	});
		
 