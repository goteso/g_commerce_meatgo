
app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : APP_URL+"/item-active.html",
        controller : "itemsController as ctrl"
    })
    .when("/inactive", {
        templateUrl : APP_URL+"/item-inactive.html",
        controller : "itemsInactiveController as ctrl"
    })
    .otherwise({
            redirectTo: '/'
        });	;
});
		
		
//====================================================== ADD ITEM CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('itemAddController', function ($http, $scope, $window,toastr, $log, $q ) {
 
	     // get basic form to add product ============================================================== clean done
	     	var auth_user_id = document.getElementById('auth_user_id').value;
 
         $('#loading').css('display', 'block');
		  
		 var request = $http({
            method: "GET",
            url: APP_URL+'/v1/form/item?auth_user_id='+auth_user_id,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.addItem  = data;
			
			for(var i=0;i<$scope.addItem[0].fields.length;i++){
               if($scope.addItem[0].fields[i].identifier == 'item_categories'){ 
				 var categories =  $scope.addItem[0].fields[i].options ; 
				  $scope.categories1 = categories; 
			 } 
			}
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) {  
		     document.getElementById("res").value = JSON.stringify(data);
        });  


		
		var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/category?auth_user_id='+auth_user_id+'&request_type=input_field',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
           $scope.categories = data.data.data;   
		   console.log(JSON.stringify(data.data.data));
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
		
		
		 $scope.searchData = function (query) { 
		    var search_term = query.toUpperCase(); 
		    $scope.people = [];
		    angular.forEach($scope.categories, function (item) {
			    if (item.label.toUpperCase().indexOf(search_term) >= 0)
				    $scope.people.push(item); 
		        });
				console.log(JSON.stringify($scope.people));
           return $scope.people;
		   
	    };
		
	 
		
       /*$scope.searchData = function(query) {
		   // alert(JSON.stringify($scope.categories));
		   return $scope.categories;
		    
           /* return $http.get(APP_URL + "/api/v1/category?search_text=" + query , {
                params: {
                    q: query
                }
            })
			  .then(function(response) { 	 			  
                return response.data.data.data;
            }) ;*
			
			//tags.query().$promise;
       };*/
		
		
		 
	
		
	 
	 
	  //========= FUNCTIONS FOR GET VENDOR FOR STORE A AREA====================================================================================== 
		  $scope.vendorSearch = function(query){
		        return $http.get(APP_URL+"/api/v1/users?user_type=3&search_text="+query, {params: {q: query}})
                .then(function(response){  
                    return response.data.data.data;
                }) 
            };
		  
		    $scope.selectedVendorChange = function(values, data) { 
                $log.info('Item changed to ' + JSON.stringify(values));
				$scope.vendor_id = values.user_id;  
            }
			 $scope.searchVendorChange = function(text,data){
				 if(data.identifier == 'store_id' ){ 
				   $scope.store = '';
				} 
			}
			
			//========= FUNCTIONS FOR GET STORE====================================================================================== 
		  $scope.storeSearch = function(query){
		        return $http.get(APP_URL+"/api/v1/stores?search_text="+query, {params: {q: query}})
                .then(function(response){ 
				//alert(JSON.stringify(response.data.data.data));
                    return response.data.data.data;
                }) 
            };
		  
		    $scope.selectedStoreChange = function(values, data) {
                $log.info('Item changed to ' + JSON.stringify(values)); 
                $scope.store_id =  values.store_id  ;  
            
            }
			
	      $scope.searchStoreChange = function(text,data){
				 if(data.identifier == 'store_id' ){ 
				   $scope.store = '';
				} 
			}
// FUNCTION FOR ADD ITEM  ==============================================================  
	     
		
           $scope.storeItem = function(){ 
	 

	  var item_photo = $('#item_photo').val();
 
 for(var i=0;i<$scope.addItem[0].fields.length;i++){
 if($scope.addItem[0].fields[i].identifier == 'item_photo'){
	 $scope.addItem[0].fields[i].value = item_photo;
 } 
 
 if($scope.addItem[0].fields[i].type == 'api'){
 if($scope.addItem[0].fields[i].identifier == 'vendor_id'){
	 $scope.addItem[0].fields[i].value = $scope.vendor_id;
 } 
 if($scope.addItem[0].fields[i].identifier == 'store_id'){
	 $scope.addItem[0].fields[i].value = $scope.store_id;
 } 
 }
 
  
}
           	console.log(JSON.stringify($scope.addItem));
 

			    var request = $http({
            method: "POST",
            url: APP_URL+'/api/v1/form/item',
            data:  $scope.addItem,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {  
            $scope.data  = data;
			
			if(data.status_code == 1){
			toastr.success(data.message, 'Success');
            $('#loading').css('display', 'none');   
            	console.log(JSON.stringify(data)); 
			 
			$scope.item_id = data.data.id
			  $window.location.href = 'item/'+$scope.item_id;
			}
			else{toastr.error(data.message, 'Error');}
        })
		.error(function (data, status, headers, config) {  	
		
             toastr.error(data.message, 'Error');
		     document.getElementById("res").value = JSON.stringify(data);
        });  
 
		   };			   
		  
	});





	
	
	
	
//======================================================VIEW ITEMS LIST CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('itemsInactiveController', function ($http,$scope,$window,toastr,$location,$log) {
		var auth_user_id = document.getElementById('auth_user_id').value;
        var pro = this; 
        $('#loading').css('display', 'block'); 
		
		var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/items?auth_user_id='+auth_user_id+'&status=inactive',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.products = data;
			
			
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
	
	
	
	// FUNCTION FOR CHANGE STATUS OF ITEM  ============================================================== clean done
	pro.switchStatus = function(value, itemId) {
		
		pro.item_id = itemId;
		pro.status_value = value;
		pro.item_status_value = { "item_active_status":pro.status_value};
		var request = $http({
                    method: "PUT",
                    url: APP_URL+'/api/v1/items/item-active-status/update/'+pro.item_id,
                    data: pro.item_status_value,
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
				    pro.data = data;
					if(data.status_code == 1){
			        toastr.success(data.message,'Success!');
					}
					else{toastr.error(data.message,'Error!');}
                       //location.reload();					
			    })
				.error(function (data, status, headers, config) { 
			        document.getElementById("res").value = JSON.stringify(data); 
				    toastr.error('Error Occurs','Error!');
                }); 
		
	};
	
	// FUNCTION FOR DELETE ITEM  ============================================================== clean done
	pro.deleteItem = function(itemId) {
		    
			$scope.item_id = itemId;
			
			if (confirm("Are you sure?")) {
		        var request = $http({
                    method: "DELETE",
                    url: APP_URL+'/api/v1/items/'+$scope.item_id,
                    data:  '',
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
				    pro.data = data;
			        toastr.success(data.message,'Success!');
                       location.reload();					
			    })
				.error(function (data, status, headers, config) { 
			        document.getElementById("res").value = JSON.stringify(data); 
				    toastr.error('Error Occurs','Error!');
                }); 
		    }
		
        };
		
		
		 // FUNCTION FOR PAGINATION  ============================================================== clean done
		pro.pagination = function(api_url){ 
			 if(api_url == null || api_url== ''){
				 return false;
			 }
			 else{
			 $('#loading').css('display', 'block'); 
		
		var request = $http({
            method: "GET",
            url: api_url+'&auth_user_id='+auth_user_id+'&status=inactive',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.products = data;
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
		} 
		}



		//========= FUNCTIONS FOR GET STORE====================================================================================== 
		  $scope.storeSearchFilter = function(query){
		        return $http.get(APP_URL+"/api/v1/stores?auth_user_id="+auth_user_id+"&search="+query, {params: {q: query}})
                .then(function(response){ 
				console.log(JSON.stringify(response.data.data.data));
                    return response.data.data.data;
                }) 
            };
		  
		    $scope.selectedFilterStoreChange = function(values, data) {
                $log.info('Item changed to ' + JSON.stringify(values)); 
                 $scope.filter_store_id =  values.store_id ; 
                 $scope.filterStore();  
			    /*if($scope.store_id != undefined || $scope.store_id != null){
				   $scope.store = $scope.store_id;
			    }  
            */
            }
	        $scope.searchFilterStoreChange = function(text,data){ 
				   $scope.filter_store_id = '';
				     $scope.filterStore();  
				} 
				 
			 


		
		$scope.filterStore = function(){

		 var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/items?auth_user_id='+auth_user_id+'&status=inactive&store_id='+$scope.filter_store_id,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            pro.products  = data;
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) {  
		     document.getElementById("res").value = JSON.stringify(data);
        });  
}

		
	
 	});
	
	
	
	//======================================================VIEW ITEMS LIST CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('itemsController', function ($http,$scope,$window,toastr,$location,$log) {
		var auth_user_id = document.getElementById('auth_user_id').value;
        var pro = this; 
        $('#loading').css('display', 'block'); 
		
		var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/items?auth_user_id='+auth_user_id+'&status=active',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.products = data;
			
			
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
	
	
	
	// FUNCTION FOR CHANGE STATUS OF ITEM  ============================================================== clean done
	pro.switchStatus = function(value, itemId) {
		
		pro.item_id = itemId;
		pro.status_value = value;
		pro.item_status_value = { "item_active_status":pro.status_value};
		var request = $http({
                    method: "PUT",
                    url: APP_URL+'/api/v1/items/item-active-status/update/'+pro.item_id,
                    data: pro.item_status_value,
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
				    pro.data = data;
					if(data.status_code == 1){
			        toastr.success(data.message,'Success!');
					}
					else{toastr.error(data.message,'Error!');}
                       //location.reload();					
			    })
				.error(function (data, status, headers, config) { 
			        document.getElementById("res").value = JSON.stringify(data); 
				    toastr.error('Error Occurs','Error!');
                }); 
		
	};
	
	// FUNCTION FOR DELETE ITEM  ============================================================== clean done
	pro.deleteItem = function(itemId) {
		    
			$scope.item_id = itemId;
			
			if (confirm("Are you sure?")) {
		        var request = $http({
                    method: "DELETE",
                    url: APP_URL+'/api/v1/items/'+$scope.item_id,
                    data:  '',
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
				    pro.data = data;
			        toastr.success(data.message,'Success!');
                       location.reload();					
			    })
				.error(function (data, status, headers, config) { 
			        document.getElementById("res").value = JSON.stringify(data); 
				    toastr.error('Error Occurs','Error!');
                }); 
		    }
		
        };
		
		
		 // FUNCTION FOR PAGINATION  ============================================================== clean done
		pro.pagination = function(api_url){ 
			 if(api_url == null || api_url== ''){
				 return false;
			 }
			 else{
			 $('#loading').css('display', 'block'); 
		
		var request = $http({
            method: "GET",
            url: api_url+'&auth_user_id='+auth_user_id+'&status=active',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.products = data;
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
		} 
		}
  
	

		//========= FUNCTIONS FOR GET STORE====================================================================================== 
		  $scope.storeSearchFilter = function(query){
		        return $http.get(APP_URL+"/api/v1/stores?auth_user_id="+auth_user_id+"&search="+query, {params: {q: query}})
                .then(function(response){ 
				console.log(JSON.stringify(response.data.data.data));
                    return response.data.data.data;
                }) 
            };
		  
		    $scope.selectedFilterStoreChange = function(values, data) {
                $log.info('Item changed to ' + JSON.stringify(values)); 
                 $scope.filter_store_id =  values.store_id ; 
                 $scope.filterStore();  
			    /*if($scope.store_id != undefined || $scope.store_id != null){
				   $scope.store = $scope.store_id;
			    }  
            */
            }
	        $scope.searchFilterStoreChange = function(text,data){ 
				   $scope.filter_store_id = '';
				     $scope.filterStore();  
				} 
				 
			 


		
		$scope.filterStore = function(){

		 var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/items?auth_user_id='+auth_user_id+'&status=active&store_id='+$scope.filter_store_id,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            pro.products  = data;
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) {  
		     document.getElementById("res").value = JSON.stringify(data);
        });  
}


 	});
	
	
	
	
	
	

	
//=========================================================EDIT ITEM CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('editItemsController', function ($http, $scope, $window,toastr,$log, $q) {
 var auth_user_id = document.getElementById('auth_user_id').value;
        var item_id = window.location.href.substr(window.location.href.lastIndexOf('/') + 1); 

		$scope.item_id = $scope.$eval(item_id);

		if(angular.isNumber($scope.item_id))
		{ 
		 
		}
		else
		{
			 $scope.url = window.location.href; 
		     var value = $scope.url;
			 var parts = value.split('/');
			 var index = parts.length - 2;
			 var str = parts[index];
			 $scope.item_id = str.replace("#", ""); 
		}

  
   

	    $('#loading').css('display', 'block'); 
		 var request = $http({
            method: "GET",
            url: APP_URL+'/v1/form/item/'+$scope.item_id,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {  
            $scope.editItem  = data;
			console.log(JSON.stringify(data));
			 $('#loading').css('display', 'none'); 


			 

	 
	  for(var i=0;i<=$scope.editItem[0].fields.length;i++){  
	    if($scope.editItem[0].fields[i].identifier == 'vendor_id'){  
	        var vendor =  $scope.editItem[0].fields[i].value;
            $scope.vendor = vendor;	 
        } 
      
	   if($scope.editItem[0].fields[i].identifier == 'store_id'){ 
	       var store = $scope.editItem[0].fields[i].value  ;
	       $scope.store = store;	 
        }   
        
	     if($scope.editItem[i].type == 'items_images'){  
			 var add_images =  $scope.editItem[i].fields ; 
			 $scope.addtional_images = add_images;  
			 console.log(add_images);
		} 
	  }
			  
       for(var k=0;k<=$scope.editItem[0].fields.length;k++){  
	 if($scope.editItem[0].fields[k].identifier == 'item_photo'){  
			var item_data_image =  $scope.editItem[0].fields[k].value ; 			 
			document.getElementById('item_data').value = item_data_image; 
		} 
	 }
	 
			 
			 console.log(JSON.stringify(data));
        })
		.error(function (data, status, headers, config) { 

			console.log(JSON.stringify(data));
 
        });  


		 
		 
    var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/category?auth_user_id='+auth_user_id+'&request_type=input_field',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
           $scope.categoriesList = data.data.data;   
		   console.log(JSON.stringify(data.data.data));
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
		
			 $scope.searchCatData = function (query) {
		    var search_term = query.toUpperCase(); 
		    $scope.cat = [];
		    angular.forEach($scope.categoriesList, function (item) {
			    if (item.label.toUpperCase().indexOf(search_term) >= 0)
				    $scope.cat.push(item);
		        });
				console.log(JSON.stringify($scope.cat));
               return $scope.cat;
		   
	    };
		
		
		 
		
	  //========= FUNCTIONS FOR GET VENDOR FOR STORE A AREA====================================================================================== 
		  $scope.vendorSearch = function(query){ 
		        return $http.get(APP_URL+"/api/v1/users?user_type=3&search="+query, {params: {q: query}})
                .then(function(response){  
                    return response.data.data.data;
                }) 
            };
		  
		    $scope.selectedVendorChange = function(values, data) { 
                $log.info('Item changed to ' + JSON.stringify(values));
				 $scope.vendor_id =  values.user_id ;   
			    if($scope.vendor_id != undefined || $scope.vendor_id != null){
				   $scope.vendor = $scope.vendor_id;
			    }
            }
			
			$scope.searchVendorChange = function(text,data){  
				if(data.identifier == 'vendor_id' ){ 
				   $scope.vendor = '';
				}
			}
			 
  
			
			//========= FUNCTIONS FOR GET STORE====================================================================================== 
		  $scope.storeSearch = function(query){
		        return $http.get(APP_URL+"/api/v1/stores?search="+query, {params: {q: query}})
                .then(function(response){ 
				//alert(JSON.stringify(response.data.data.data));
                    return response.data.data.data;
                }) 
            };
		  
		    $scope.selectedStoreChange = function(values, data) {
                $log.info('Item changed to ' + JSON.stringify(values)); 
                 $scope.store_id =  values.store_id ;   
			    if($scope.store_id != undefined || $scope.store_id != null){
				   $scope.store = $scope.store_id;
			    }  
            
            }
	        $scope.searchStoreChange = function(text,data){
				 if(data.identifier == 'store_id' ){ 
				   $scope.store = '';
				} 
				 
			}
	
	//FUNCTION FOR UPDATE ITEM==============================================================
		   $scope.updateItem = function(){ 
		   		  var item_photo = $('#item_photo').val();
 
 for(var j=0;j<$scope.editItem[0].fields.length;j++){
  if($scope.editItem[0].fields[j].identifier == 'item_photo'){
	 $scope.editItem[0].fields[j].value = item_photo;
	  // document.getElementById('preview_image').src = APP_URL+'/images/items/'+item_photo;
	   //console.log("this is deepu path - "+APP_URL+'/images/items/' + $('#item_photo').val(data));
 }  
 
 if($scope.editItem[0].fields[j].type == 'api'){
 if($scope.editItem[0].fields[j].identifier == 'vendor_id'){
	 $scope.editItem[0].fields[j].value = $scope.vendor;
 } 
 if($scope.editItem[0].fields[j].identifier == 'store_id'){
	 $scope.editItem[0].fields[j].value = $scope.store;
 } 
 }
  
}
     
	 console.log(JSON.stringify($scope.editItem));
	 
			    var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/form/item/'+$scope.item_id,
            data:  $scope.editItem,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {  
            $scope.data  = data; 
             toastr.success(data.message, 'Success');
            $('#loading').css('display', 'none');    
			 document.getElementById("res").value = JSON.stringify(data); 
        })
		.error(function (data, status, headers, config) {  	
		
             toastr.error(data.message, 'Error');
		      document.getElementById("res").value = JSON.stringify(data);
        });  
 
		   };		

$scope.selected = 0;









	

 //  ADD UPDATE VARIANTS (for Add Product Basic Form Autocomplete ) =====================================================================
	$scope.add = function(index) {  
		var value = {};
		for(var k=0;k<=$scope.editItem.length;k++){
			if($scope.editItem[k].type == 'item_variants')
		$scope.editItem[k].fields[index].values.push(value); 
		}
	}
 
	$scope.save_variant = function (index, tab_id) {
		
        $scope.variant_id = document.getElementsByName('variant_id' + index + tab_id)[0].value;
		$scope.item_variant_price_difference = document.getElementsByName('price_difference' + index + tab_id)[0].value;
        $scope.item_variant_stock_count = document.getElementsByName('stock_count' + index + tab_id)[0].value;
        $scope.item_variant_price = document.getElementsByName('price' + index + tab_id)[0].value;
        $scope.item_variant_type_id = document.getElementsByName('product_variant_type_id' + index + tab_id)[0].value;
        $scope.item_id = $scope.item_id;
		$scope.item_variant_value_title = document.getElementsByName('title' + index + tab_id)[0].value;
        $scope.item_variant_photo = '';

		$scope.variant_data = {
			"item_variant_type_id": $scope.item_variant_type_id,
			"item_variant_value_title": $scope.item_variant_value_title,
			"item_id": $scope.item_id,
			"item_variant_price" : $scope.item_variant_price,
			"item_variant_price_difference": $scope.item_variant_price_difference,
			"item_variant_stock_count": $scope.item_variant_stock_count, 
			"item_variant_photo": $scope.item_variant_photo
		};
         console.log(JSON.stringify($scope.variant_data));
		
		
		if($scope.variant_id == ''){  
		var request = $http({
			method: "POST",
			url: APP_URL+'/api/v1/items-variants',
			data: $scope.variant_data,
			headers: {
				'Accept': 'application/json'
			}
		}); 
		request.success(function (data) {
			toastr.success(data.message,'Success!');		
	         $('input[name="' + 'variant_id' + index + tab_id + '"]').val(data.data.id) ;
	 
        }).error(function (data, status, headers, config) { 
		toastr.error('Error Occurs','Error!');
	   }); 
	   
		}
		
		else{
			var request = $http({
			method: "PUT",
			url: APP_URL+'/api/v1/items-variants/'+$scope.variant_id,
			data: $scope.variant_data,
			headers: {
				'Accept': 'application/json'
			}
		}); 
		request.success(function (data) {
			toastr.success(data.message,'Success!');		
	         $('input[name="' + 'id' + index + tab_id + '"]').val(data.id) ;
	 
        }).error(function (data, status, headers, config) { 
		toastr.error('Error Occurs','Error!');
	   }); 
	   
		}
		}
		
		
		// Delete Product Variant (for Add Product Basic Form Autocomplete ) ================================================================
	$scope.delete_variant = function (index, tab_id ,index1) { 
	
	  $scope.variant_id = document.getElementsByName('variant_id' + index + tab_id)[0].value;
	    
		
		
   if (confirm("Are you sure?")) {
		$http.get(APP_URL+'/api/v1/items-variants/'+$scope.variant_id)
			.success(function (data, status, headers, config) {
				toastr.success(data.message,'Success!');
			})
			var request = $http({
			method: "DELETE",
			url: APP_URL+'/api/v1/items-variants/'+$scope.variant_id,
			data: '',
			headers: {
				'Accept': 'application/json'
			}
		}); 
		request.success(function (data) {
			
			$scope.data = data;
			
			if(data.status_code == 1){
			toastr.success(data.message,'Success!');	
 for(var m=0;m<=$scope.editItem.length;m++){
		 if($scope.editItem[m].type == "item_variants")
		$scope.editItem[m].fields[index1].values.splice(index, 1); 
	//alert('2');	
			}			}
			else{
				 toastr.error(data.message,'Error!'); 
			}
	 
        }).error(function (data, status, headers, config) {
				 toastr.error('Error Occurs','Error!'); 
			});
		  }  
   };
   
   
   
   
   
   
   
   
   
   
   
   
   $scope.save_file = function(){
	   
	   $scope.img = $('#add_img').val();
	   $scope.add_img_data = {"item_id":$scope.item_id, "photo":$scope.img};
	   
	   var request = $http({
			method: "POST",
			url: APP_URL+'/api/v1/items-images',
			data: $scope.add_img_data,
			headers: {
				'Accept': 'application/json'
			}
		}); 
		request.success(function (data) {
			$scope.addtional_images = data.data;
			console.log(JSON.stringify(data));
			toastr.success(data.message,'Success!');		
	         
	 
        }).error(function (data, status, headers, config) { 
		toastr.error('Error Occurs','Error!');
	   }); 
	   
   }
 		   
	});



