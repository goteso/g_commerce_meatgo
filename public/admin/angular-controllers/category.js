  
 app.directive('demoFileModel', function ($parse) {
        return {
            //restrict: 'A', //the directive can be used as an attribute only
 
            /*
             link is a function that defines functionality of directive
             scope: scope associated with the element
             element: element on which this directive used
             attrs: key value pair of element attributes
             */
            link: function (scope, element, attrs) {
                var model = $parse(attrs.demoFileModel),
                    modelSetter = model.assign; //define a setter for demoFileModel
 
                //Bind change event on the element
                element.bind('change', function () {
                    //Call apply on scope, it checks for value changes and reflect them on UI
                    scope.$apply(function () {
                        //set the model value
                        modelSetter(scope, element[0].files[0]);
                    });
                });
            }
        };
    });
	
	app.service('fileUploadService', function ($http, $q) {
 
        this.uploadFileToUrl = function (file, uploadUrl) {
            //FormData, object of key/value pair for form fields and values
            var fileFormData = new FormData();
            fileFormData.append('file', file);
 
            var deffered = $q.defer();
            $http.post(uploadUrl, fileFormData, {
                transformRequest: angular.identity,
                headers: {'Content-Type':  ''}
 
            }).success(function (response) {
                deffered.resolve(response);
 
            }).error(function (response) {
                deffered.reject(response);
            });
 
            return deffered.promise;
        }
    });
	
	
	
//==========================================================ADD CATEGORY CONTROLLER====================================================================================================
//==================================================================================================================================================================================
    app.controller('addCategoryController', function($http, $scope , $window, $log, $q, $timeout, toastr, fileUploadService) {
 
        
     
	//========= FUNCTIONS FOR GET PARENT CATEGORY SEARCH LIST=============================================================================================== 
       /* $scope.querySearch1 = function(query) {
            return $http.get(APP_URL + "/users_search?search_text=" + query + "&user_type=customer", {
                params: {
                    q: query
                }
            })
            .then(function(response) {
                return response.data;
            })
        };

		
        $scope.selectedItemChange = function(item, field) {
            $log.info('Item changed to ' + JSON.stringify(item));
            if (field.identifier == 'customer_id') {
                 $scope.customer_id_value = field.value = item.id;
            }
            $scope.selected_values = '';
        }*/


	       
	
	 
		
    }); 
	
	
	
	
	
	
 
	
	
	 
	
//==========================================================CATEGORY CONTROLLER=======================================================================================================
//====================================================================================================================================================================================
   app.controller('categoryController', function($http, $scope , $window, $log, $q, $timeout, toastr, fileUploadService) {
 var auth_user_id = document.getElementById('auth_user_id').value;
        var pro = this; 
		 $('#loading').css('display', 'block'); 
		
		var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/category?auth_user_id='+auth_user_id,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.categories = data;
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
		
		
				
		 // FUNCTION FOR PAGINATION  ============================================================== clean done
		pro.pagination = function(api_url){ 
			 if(api_url == null || api_url== ''){
				 return false;
			 }
			 else{
			 $('#loading').css('display', 'block'); 
		
		var request = $http({
            method: "GET",
            url: api_url+'&auth_user_id='+auth_user_id,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.categories = data;
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
		} 
		}
		
		




		//========= FUNCTIONS FOR GET STORE====================================================================================== 
		  $scope.storeSearchFilter = function(query){
		        return $http.get(APP_URL+"/api/v1/stores?auth_user_id="+auth_user_id+"&search="+query, {params: {q: query}})
                .then(function(response){ 
				console.log(JSON.stringify(response.data.data.data));
                    return response.data.data.data;
                }) 
            };
		  
		    $scope.selectedFilterStoreChange = function(values, data) {
                $log.info('Item changed to ' + JSON.stringify(values)); 
                 $scope.filter_store_id =  values.store_id ; 
                 $scope.filterStore();  
			    /*if($scope.store_id != undefined || $scope.store_id != null){
				   $scope.store = $scope.store_id;
			    }  
            */
            }
	        $scope.searchFilterStoreChange = function(text,data){ 
				   $scope.filter_store_id = '';
				     $scope.filterStore();  
				} 
				 
			 


		
		$scope.filterStore = function(){

		 var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/category?auth_user_id='+auth_user_id+'&store_id='+$scope.filter_store_id,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            pro.categories  = data;
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) {  
		     document.getElementById("res").value = JSON.stringify(data);
        });  
}


		
		// FUNCTION FOR DELETE CATEGORY  ============================================================== clean done
	pro.deleteCategoryId = function(categoryId) {
		    
			$scope.category_id = categoryId;
			
			if (confirm("Are you sure?")) {
		        var request = $http({
                    method: "DELETE",
                    url: APP_URL+'/api/v1/category/'+$scope.category_id,
                    data:  '',
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
				    pro.data = data;
					if(data.status_code == 1){
			        toastr.success(data.message,'Success!');
                       location.reload();	
					}
				else{
					toastr.error(data.message,'Error!');
				}	
			    })
				.error(function (data, status, headers, config) { 
			        document.getElementById("res").value = JSON.stringify(data); 
				    toastr.error('Error Occurs','Error!');
                }); 
		    }
		
        };
		
		
		
		
		
			//====================FUNCTION FOR GET PARENT CATEGORY API==========================================================================
		pro.categorySearch = function(query) {  
            return $http.get(APP_URL + "/api/v1/category?auth_user_id="+auth_user_id+"&parents_only=true&search=" + query , {
                params: {
                    q: query
                }
            })
            .then(function(response) { 
                return response.data.data.data;
            })
        };
  
        pro.selectedCategoryChange = function(data) { 
            $log.info('Item changed to ' + JSON.stringify(data)); 
              $scope.category_id =   data.category_id;  
        } 
		
		
		$scope.searchCategoryChange = function(text){  
				   $scope.category_id = ''; 
			};
		
		
		
		 //========= FUNCTIONS FOR GET STORE====================================================================================== 
		  $scope.storeSearch = function(query){
		        return $http.get(APP_URL+"/api/v1/stores?search="+query, {params: {q: query}})
                .then(function(response){  
                    return response.data.data.data;
                }) 
            };
		  
		    $scope.selectedStoreChange = function(values) { 
                $log.info('Item changed to ' + JSON.stringify(values));  
	            $scope.store_id =  values.store_id; 

	            $('#store_id').val($scope.store_id); 		
            }
			
			$scope.searchStoreChange = function(text){  
				   $scope.store_id = ''; 
			};
			
			
		
		 $scope.uploadFile = function () {
			 alert(JSON.stringify($scope.myFile));
            var file = $scope.myFile;
            var uploadUrl = APP_URL + "/api/v1/users/upload-profile-image", //Url of webservice/api/server
                promise = fileUploadService.uploadFileToUrl(file, uploadUrl);
 
            promise.then(function (response) {
				 
                $scope.serverResponse = response;
            }, function () {
				 
                $scope.serverResponse = 'An error has occurred';
            })
        };
		
		
		 pro.editCategory = function(){ 
		 
		 }
		
		//========= FUNCTIONS FOR STORE A CATEGORY=============================================================================================== 
		 pro.storeCategory = function(){ 
           pro.category_photo = $('#file_name').val();
		   pro.category_title = $('#category_title').val();
		   pro.parent_id = $('#category_id').val();
           pro.store_id = $('#store_id').val();			
		   pro.category_data = {"category_title" : pro.category_title, "parent_id" : pro.parent_id, "store_id" : pro.store_id, "category_photo" : pro.category_photo };
		   var request = $http({
            method: "POST",
            url: APP_URL+'/api/v1/category',
            data:  pro.category_data,
            headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
           request.success(function (data) { 
            pro.data = data;
             if(data.status_code == 1){

                    if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }


			 
              location.reload();	
			}
			else{
             toastr.error(data.message, 'Error'); 
			}			
        })
		.error(function (data, status, headers, config) {  
             toastr.error(data.message, 'Error');
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		 }
		 
		 
		 
		 
		 
		 
		 
		
		
		//========= FUNCTIONS FOR EDIT  A CATEGORY FORM=============================================================================================== 
		 pro.editCategory = function(values){  
		 console.log(JSON.stringify(values));
		 pro.category_photo = values.category_photo;
           pro.category_id = values.category_id;
		   pro.category_edit_title = values.category_title;
           pro.parent_category_title = values.parent_category_title;		   
		    pro.parent_id = values.parent_id;
			pro.store_edit_id = values.store_id;
			pro.store_edit_title = values.store_title;
		     $('#editCat').modal('show');   
		
		 
		 
		 
		  pro.editCategorySearch = function(query) { 
            return $http.get(APP_URL + "/api/v1/category?auth_user_id="+auth_user_id+"&parents_only=true&exclude_id="+pro.category_id +"&search_text=" + query , {
                params: {
                    q: query
                }
            })
            .then(function(response) { 
                return response.data.data.data;
            })
        };
  
        pro.selectedEditCategoryChange = function(data) { 
            $log.info('Item changed to ' + JSON.stringify(data)); 
                 pro.parent_category_id =   data.category_id; 
               if(pro.parent_category_id != undefined || pro.parent_category_id != null){
				   pro.parent_id = pro.parent_category_id;
			    } 
		  };
		  
		  pro.searchEditCategoryChange = function(text){  
				   pro.parent_id = ''; 
			};
		  
		  
		  
		 }
		  //========= FUNCTIONS FOR GET STORE====================================================================================== 
		  pro.storeEditSearch = function(query){
		        return $http.get(APP_URL+"/api/v1/stores?search="+query, {params: {q: query}})
                .then(function(response){  
                    return response.data.data.data;
                }) 
            };
		  
		    pro.selectedEditStoreChange = function(values) {
              $log.info('Item changed to ' + JSON.stringify(values)); 
                 pro.store_id =   values.store_id; 
               if(pro.store_id != undefined || pro.store_id != null){
				   pro.store_edit_id = pro.store_id;
			    }				 
        }; 			
              
			
			pro.searchEditStoreChange = function(text){  
				   pro.store_edit_id = ''; 
			};
		 
		 
		 //========= FUNCTIONS FOR UPDATE A CATEGORY=============================================================================================== 
		 pro.updateCategory = function(id){ 
           pro.category_id = id; 
		   pro.category_photo = $('#file_name1').val();
		   pro.store_edit_id = $('#store_edit_id').val();	
		   pro.category_edit_title = $('#category_edit_title').val();
		   pro.parent_id = $('#category_edit_id').val();
		  
		   pro.category_data = {"category_title" : pro.category_edit_title, "parent_id" : pro.parent_id,"store_id" : pro.store_edit_id , "category_photo" : pro.category_photo};
		   var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/category/'+pro.category_id,
            data:  pro.category_data,
            headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
           request.success(function (data) { 
            pro.data = data;
            if(data.status_code == 1){

                    if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }


		 
              location.reload();	
			}
			else{
             toastr.error(data.message, 'Error'); 
			}			
        })
		.error(function (data, status, headers, config) {  
             toastr.error(data.message, 'Error');
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		 }
		
   });