
//==========================================================FAQ CONTROLLER=======================================================================================================
//====================================================================================================================================================================================
   app.controller('notificationController', function($http, $scope , $window, $log, $q, $timeout, toastr) {
 
          
		//========= FUNCTIONS FOR STORE A FAQ=============================================================================================== 
		$scope.sendNotification = function(){ 
            $scope.title = $('#title').val();
		    $scope.sub_title = $('#sub_title').val();
		    $scope.notification_data = {title : $scope.title , sub_title : $scope.sub_title};
		   var request = $http({
            method: "POST",
            url: APP_URL+'/api/v1/offer-push-notification/push-to-customers',
            data:  $scope.notification_data,
            headers: { 'Accept':'application/json' }
            });

           /* Check whether the HTTP Request is successful or not. */
           request.success(function (data) {  

           $scope.data = data;
            
           if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
           else { toastr.error(data.message, 'Error');  }
             	 
		   console.log(JSON.stringify(data));
        })
		.error(function (data, status, headers, config) {  
                document.getElementById('res').value = JSON.stringify(data);
                toastr.error(data.message, 'Error');
		    	console.log(JSON.stringify(data));
        });       
		 }




/*=====================customers==============================*/

var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/users?per_page=5000',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
           $scope.users = data.data.data;   
       console.log(JSON.stringify(data.data.data));
        })
    .error(function (data, status, headers, config) { 
        console.log(JSON.stringify(data));
        });       
    
    
    
     $scope.searchData = function (query) { 
        var search_term = query.toUpperCase(); 
        $scope.people = [];
        angular.forEach($scope.users, function (customer) {

          if (customer.first_name.toUpperCase().indexOf(search_term) >= 0)
            $scope.people.push(customer); 
            });
        console.log(JSON.stringify($scope.people));
        //alert(JSON.stringify($scope.people));
           return $scope.people;
       
      }; 










var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/stores?per_page=5000',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
           $scope.stores = data.data.data;   
       console.log(JSON.stringify(data.data.data));
        })
    .error(function (data, status, headers, config) { 
        console.log(JSON.stringify(data));
        });       
    
    
    
     $scope.searchData = function (query) { 
        var search_term = query.toUpperCase(); 
        $scope.people2 = [];
        angular.forEach($scope.stores, function (customer) {

          if (customer.store_title.toUpperCase().indexOf(search_term) >= 0)
            $scope.people2.push(customer); 
            });
        console.log(JSON.stringify($scope.people2));
        //alert(JSON.stringify($scope.people));
           return $scope.people2;
       
      }; 







		 		$scope.emails_to_stores = function(){ 
            $scope.subject = $('#subject').val();
		    $scope.email_body = $("#email_body").val(); 
		    $scope.notification_data = {"subject" : $scope.subject , "email_body" : $scope.email_body , "store_ids" : $scope.people2};
		   var request = $http({
            method: "POST",
            url: APP_URL+'/api/v1/offer-push-notification/email-to-stores',
            data:  $scope.notification_data,
            headers: { 'Accept':'application/json' }
            });

           /* Check whether the HTTP Request is successful or not. */
           request.success(function (data) {  

           $scope.data = data;
            
           if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
           else { toastr.error(data.message, 'Error');  }
             	 
		   console.log(JSON.stringify(data));
        })
		.error(function (data, status, headers, config) {  
                document.getElementById('res').value = JSON.stringify(data);
                toastr.error(data.message, 'Error');
		    	console.log(JSON.stringify(data));
        });       
		 }




		 	$scope.emails_to_customers = function(){ 
            $scope.subject1 = $('#subject').val();
		    $scope.email_body1 = $("#email_body").val(); 
		    $scope.notification_data1 = {"subject" : $scope.subject1 , "email_body" : $scope.email_body1 , "customer_ids" : $scope.people };
			 
 

		   var request = $http({
            method: "POST",
            url: APP_URL+'/api/v1/offer-push-notification/email-to-customers',
            data:  $scope.notification_data1,
            headers: { 'Accept':'application/json' }
            });

           /* Check whether the HTTP Request is successful or not. */
           request.success(function (data) {  

           $scope.data = data;
            
           if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); 
		   }
           else { toastr.error(data.message, 'Error');  }
             	 
		   console.log(JSON.stringify(data));
        })
		.error(function (data, status, headers, config) {  
                document.getElementById('res').value = JSON.stringify(data);
                toastr.error(data.message, 'Error');
		    	console.log(JSON.stringify(data));
        });       
		 }


		 
		 
		 
		 
	 
		
   });