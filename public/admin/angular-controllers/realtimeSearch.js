/*var app = angular.module("mainApp", ['ui.directives',  'ngRoute', 'ngAnimate', 'ngTagsInput', 'ngResource', '720kb.datepicker','angular-jquery-locationpicker', 'ui.filters', 'ngMaterial', 'ngMessages',  'toastr', 'ngMaterialDatePicker','ui.sortable'])*/

var app = angular.module("mainApp", ['ui.directives',  'ngRoute', 'ngAnimate', 'ngTagsInput', 'ngResource', '720kb.datepicker','angular-jquery-locationpicker', 'ui.filters', 'pusher-angular','ngMaterial', 'ngMessages',  'toastr', 'ngMaterialDatePicker', 'dndLists'])

//==========================================================ADD CATEGORY CONTROLLER====================================================================================================
//==================================================================================================================================================================================
    app.controller('searchController', function($http, $scope , $window, $log, $q, $timeout, toastr) {
  
  var auth_user_id = document.getElementById('auth_user_id').value;
  
	//========= FUNCTIONS FOR GET PARENT CATEGORY SEARCH LIST=============================================================================================== 
        $scope.querySearch = function(query) {
	 
            return $http.get(APP_URL+"/api/v1/multisearch?auth_user_id="+auth_user_id+"&search_text="+query , {
                params: {
                    q: query
                }
            })
            .then(function(response) {
			// alert(JSON.stringify(response.data.data));
                return response.data.data;
            })
        };

		
        $scope.selectedItemChange = function(item, field) {
            $log.info('Item changed to ' + JSON.stringify(item));
            if (field.identifier == 'customer_id') {
                 $scope.customer_id_value = field.value = item.id;
            }
            $scope.selected_values = '';
        }


	
	
	 
		
    }); 
	
	
	
	
	
	
	
	
	 
  //==========================================================PROFILE CONTROLLER====================================================================================================
//==================================================================================================================================================================================

    app.controller('loginController', function($http, $scope , $window) {
  
	 	var auth_user_id = document.getElementById('auth_user_id').value;
  
   $http.get(APP_URL+'/api/v1/users/'+auth_user_id)
            .success(function(data, status, headers, config) { 
		    $scope.profileData = data;  
	    }) 
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
		
  
    }); 
	
	