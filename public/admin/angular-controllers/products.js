  

app.factory('Excel',function($window){
        var uri='data:application/vnd.ms-excel;base64,',
            template='<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
            base64=function(s){return $window.btoa(unescape(encodeURIComponent(s)));},
            format=function(s,c){return s.replace(/{(\w+)}/g,function(m,p){return c[p];})};
        return {
            tableToExcel:function(tableId,worksheetName){
                var table=$(tableId),
                    ctx={worksheet:worksheetName,table:table.html()},
                    href=uri+base64(format(template,ctx));
                return href;
            }
        };
    })
	
 
 
 
//========================================================== PRODUCTS CONTROLLER============================================================================
//==============================================================================================================================================================

    app.controller('productController',  productController);
	
	function productController($http,$scope,Excel,$timeout,$location,toastr) {
		 
		var pro = this;
        $('#loading').css('display', 'block'); 
		
		
		
	    //ONLOAD GET PRODUCT LIST===============================================================================
		
	    var date_filter = $location.search().date_filter;
	    var search_text = $location.search().search_text;
	    var category = $location.search().category;
	    var brand = $location.search().brand;
	  
	    if(date_filter == 'undefined' || date_filter == undefined || date_filter == '') { date_filter = 'lastAdded';}
	    if(search_text == 'undefined' || search_text == undefined || search_text == '') { search_text = '';}
	    if(category == 'undefined' || category == undefined || category == '') { category = '';}
	    if(brand == 'undefined' || brand == undefined || brand == '') { brand = '';}
	    if(search_text == undefined || search_text == '') { search_text = '';}
	    if(brand == null || brand == '' | brand == 'undefined') { var brand = ''; }
		  
		var request = $http({
            method: "GET",
            url: APP_URL+'/get_product_list?date_filter='+date_filter+'&search_text='+search_text+'&category='+category+'&brand='+brand+'&limit=50&fields=id,photo,title,base_price,created_at',
            /// data:  $scope.arrayText,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.products = data;
            $('#loading').css('display', 'none');
		    // $scope.product_id = data["user_data"][0]["product_id"];
			//$window.location.href = 'product_edit/'+$scope.product_id;
        })
		.error(function (data, status, headers, config) { 
            document.getElementById("res").value = JSON.stringify(data);
        });       
	
	
	
	
	    //fUNCTION fOR GET FILTERED PRODUCT LIST ==================================================================
	
	    $scope.filter_products = function() {
		
            $scope.Model = {};
	        var date_filter = $('#date_filter').val();
	        var search_text = $('#search_text').val();
            var category = $('#category_filter').val();
	        var brand = $('#brand_filter').val();
            var price_filter = $('#price_filter').val();
	        var limit = '5';
	        var fields = 'id,photo,title,base_price,created_at';

            if(brand == null || brand == '' | brand == 'undefined') {
                var brand = '';
            }

            $location.url('?date_filter='+date_filter+'&price_filter='+price_filter+'&search_text='+search_text+'&category='+category+'&brand='+brand+ '&limit=&fields=id,photo,title,base_price,created_at');
    
            var request = $http({
                 method: "GET",
                 url: APP_URL+'/get_product_list?date_filter='+date_filter+'&price_filter='+price_filter+'&search_text='+search_text+'&category='+category+'&brand='+brand+'&limit=&fields=id,photo,title,base_price,created_at',
                 data:  $scope.arrayText,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {
				 pro.products = data;
				 document.getElementById("res").value = APP_URL+'/get_product_list?date_filter='+date_filter+'&price_filter='+price_filter+'&search_text='+search_text+'&category='+category+'&brand='+brand+'&limit=&fields=id,photo,title,base_price,created_at';
            })
			.error(function (data, status, headers, config) { 
                  document.getElementById("res").value = JSON.stringify(data);
            });   
		
	    }	
		
		
 

        //fUNCTION FOR DELETE SINGLE PRODUCT ======================================================================
		 
		pro.removeChoice = removeChoice; 
		function removeChoice(itemId, index) {
		    
			if (confirm("Are you sure?")) {
		        var request = $http({
                    method: "POST",
                    url: APP_URL+'/delete_products_multiple',
                    data:  "["+JSON.stringify(pro.products.data.data[index])+"]",
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) {
			        for (var i = 0; i < pro.products.data.data.length; i++) { 
	                     pro.products.data.data.splice(index, 1);
                         break;
		            }
			        toastr.success(data.message,'Success!');
				    $scope.chckedIndexs = [];
			    })
				.error(function (data, status, headers, config) { 
			        document.getElementById("res").value = JSON.stringify(data);
                    $scope.chckedIndexs = [];
				    toastr.error('Error Occurs','Error!');
                }); 
		    }
		
        };
	
	
	
	
 
 
       //fUNCTION FOR DELETE MULTIPLE PRODUCT VIA CHECKBOX============================================================
	    $scope.chckedIndexs=[];

        $scope.checkedIndex = function (values) {
            if ($scope.chckedIndexs.indexOf(values) === -1) {
                $scope.chckedIndexs.push(values);
            }
            else {
                $scope.chckedIndexs.splice($scope.chckedIndexs.indexOf(values), 1);
            }
        }
      
        $scope.remove=function(index){
		    angular.forEach($scope.chckedIndexs, function (value, index) { 
		        for (var i = 0; i < pro.products.data.data.length; i++) { 
	                 var index = pro.products.data.data.indexOf(value); 	
                     pro.products.data.data.splice(index, 1);
                     break;
		        }
            }); 
	   
	        document.getElementById("res").value = JSON.stringify($scope.chckedIndexs);
	   
	        if (confirm("Are you sure?")) {
	              var request = $http({
                      method: "POST",
                      url: APP_URL+'/delete_products_multiple',
                      data:  $scope.chckedIndexs,
                      headers: { 'Accept':'application/json' }
                  });

                  /* Check whether the HTTP Request is successful or not. */
                  request.success(function (data) {
				      $scope.chckedIndexs = [];
			      })
			      .error(function (data, status, headers, config) { 
			          toastr.error('Error Occurs','Error!');
                      $scope.chckedIndexs = [];
                  });   
		    }
			
		};
	 
	 
		


		$scope.selectedAll = {
    	    selectedLabelList : []
        }
	
	    $scope.isSelectAll = function(){
             $scope.selectedAll.selectedLabelList = [];
		     if($scope.selectedAll){
		        $scope.selectedAll = true;
                for(var i=0;i<pro.products.length;i++){
			         var index = pro.products[i].data.indexOf(value); 
					 $scope.selectedAll.selectedLabelList.push(index);		
				}
			 }
             else{
				$scope.selectedAll = false;
			 }
			 
             angular.forEach(pro.products[i].data, function (values) {
		         values.selected = $scope.selectedAll;
             });
        }

 	
 

        //fUNCTION FOR DOWNLOAD AS EXCEL SHEET==================================================================
        $scope.exportToExcel=function(tableId){ // ex: '#my-table'
            var exportHref=Excel.tableToExcel(tableId,'WireWorkbenchDataExport');
            $timeout(function(){location.href=exportHref;},100); // trigger download
        }
		
		
		
		//fUNCTION FOR DOWNLOAD AS PDF=========================================================================== 
        $scope.export = function(){
            html2canvas(document.getElementById('exportthis'), {
                onrendered: function (canvas) {
                    var data2 = canvas.toDataURL();
                    var docDefinition = {
                        content: [{
                             image: data2,
                             width: 500,
                        }]
                    };
                    pdfMake.createPdf(docDefinition).download("test.pdf");
                }
            });
        };
   
   
   
   
         /**pagination**/
         $scope.currentPage = 1;
         $scope.pageSize = 5;
   
         $scope.pageChangeHandler = function(num) {
             console.log('meals page changed to ' + num);
         };
  
  
  
        
		//fUNCTION FOR PRINT TABLE=================================================================================  
  
  
        $scope.printToCart = function(tableToExport) {
           var innerContents = document.getElementById('tableToExport').innerHTML;
           var popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
           popupWinindow.document.open();
           popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + innerContents + '</html>');
           popupWinindow.document.close();
        }
	  
	  
};







//========================================================== OTHER CONTROLLER IF PAGINATION APPLY===============================================================
//==============================================================================================================================================================
app.controller('OtherController',  OtherController);
function OtherController($scope) {
  $scope.pageChangeHandler = function(num) {
    console.log('going to page ' + num);
  };
}


