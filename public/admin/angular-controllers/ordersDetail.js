

//========================================================== ORDER DETAIL CONTROLLER================================================================================================
//==================================================================================================================================================================================
    app.controller('orderDetailController', function($http,$scope,$window,$log, toastr, $location,$q) {
	
        $('#loading').css('display', 'none');
		
		$scope.order_id = window.location.href.substr(window.location.href.lastIndexOf('/') + 1); 
		   
       



	   //========= ONLOAD GET ORDER DETAILS  VIA CALLING API================================================================== 
        var request = $http({
             method: "GET",
             url: APP_URL+'/api/v1/orders/'+$scope.order_id,
            data: '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
			$scope.orders =  data ; 
            $('#loading').css('display', 'none');
            document.getElementById('res').value = JSON.stringify(data); 
        })
		.error(function (data, status, header, config) {            
        	document.getElementById('res').value = JSON.stringify(data);        
	    }); 
			
		

        /*=============================FUNCTION FOR UPDATE ORDER STATUS=============================================================*/		

        	$scope.updateStatusCancelled = function(data){ 
   $scope.order_cancel_reason = $('#order_cancel_reason').val();
   $scope.order_cancel_action = $('#order_cancel_action').val();

 

   	     	$scope.order_status = {"order_status": $scope.order_cancel_action , "cancel_reason_admin": $scope.order_cancel_reason };

    					if (confirm("Are you sure?")) {
		     var request = $http({
				 method: "PUT",
				 url: APP_URL+'/api/v1/orders/'+$scope.order_id+'/status-update',
				 data: $scope.order_status,
				 headers: { 'Accept':'application/json' }
             });

			/* Check whether the HTTP Request is successful or not. */
			request.success(function (data) { 
				$scope.statusData =  data ; 
				if(data.status_code == 1){
				 toastr.success(data.message, 'Success');
				 location.reload();
				   }
				   else{
					   toastr.error(data.message, 'Error');
				   }
				document.getElementById('res').value = JSON.stringify(data); 
			})
			.error(function (data, status, header, config) {            
				document.getElementById('res').value = JSON.stringify(data);        
			}); 
			}



        	}




     	$scope.updateStatus = function(data){ 
		
	     	$scope.order_status = {"order_status": data.action};
		if(data.action == "cancelled"){

		$('#cancel_reason_model').modal('show'); 

 
		}
else{
var request = $http({
				 method: "PUT",
				 url: APP_URL+'/api/v1/orders/'+$scope.order_id+'/status-update',
				 data: $scope.order_status,
				 headers: { 'Accept':'application/json' }
             });

			/* Check whether the HTTP Request is successful or not. */
			request.success(function (data) { 
				$scope.statusData =  data ; 
				if(data.status_code == 1){
				 toastr.success(data.message, 'Success');
				 location.reload();
				   }
				   else{
					   toastr.error(data.message, 'Error');
				   }
				document.getElementById('res').value = JSON.stringify(data); 
			})
			.error(function (data, status, header, config) {            
				document.getElementById('res').value = JSON.stringify(data);        
			}); 
}	
		 }





		      	$scope.updateStatus2 = function(action){ 
		
	     	$scope.order_status = {"order_status": action};
		if(action == "cancelled"){

		$('#cancel_reason_model').modal('show'); 

 
		}
else{
var request = $http({
				 method: "PUT",
				 url: APP_URL+'/api/v1/orders/'+$scope.order_id+'/status-update',
				 data: $scope.order_status,
				 headers: { 'Accept':'application/json' }
             });

			/* Check whether the HTTP Request is successful or not. */
			request.success(function (data) { 
				$scope.statusData =  data ; 
				if(data.status_code == 1){
				 toastr.success(data.message, 'Success');
				 location.reload();
				   }
				   else{
					   toastr.error(data.message, 'Error');
				   }
				document.getElementById('res').value = JSON.stringify(data); 
			})
			.error(function (data, status, header, config) {            
				document.getElementById('res').value = JSON.stringify(data);        
			}); 
}	
		 }
		 
	


		 
		 /*=============================FUNCTION FOR POPUP FOR ASSIGN TASK TO DRIVER=============================================================*/		
     	$scope.getDriver = function(data){ 			
		    $scope.task_id = data.task_id;
			$('#driverModal').modal('show');
			
		}
		
		


		//============================= FUNCTIONS FOR GET DRIVERS LIST=================================================================
		
		
		  $scope.driverSearch = function(query) { 
            return $http.get(APP_URL + "/api/v1/users?user_type=5&search=" + query , {
                params: {
                    q: query
                }
            })
            .then(function(response) {   
            	console.log(JSON.stringify(response.data.data.data));
                return response.data.data.data;
            })
        };
  
        $scope.selectedDriverChange = function(data) { 
            $log.info('Item changed to ' + JSON.stringify(data)); 
                 $scope.driver_id =   data.user_id;  
        } 
		
		



		//============================= FUNCTIONS FOR ASSIGN DRIVER TO A TASK================================================================
		$scope.assignDriver = function(){ 


	     	$scope.driver = {"driver_id": $scope.driver_id}; 

	     	

		     var request = $http({
				 method: "PUT",
				 url: APP_URL+'/api/v1/task/assign-driver/'+$scope.task_id,
				 data: $scope.driver,
				 headers: { 'Accept':'application/json' }
             });

			/* Check whether the HTTP Request is successful or not. */
			    request.success(function (data) { 
				$scope.statusData =  data ;  
				$('#loading').css('display', 'none');
       
				if( data.status_text == 'Success'  )
				{
                   toastr.success(data.message, 'Success');
console.log(JSON.stringify(data));

                   location.reload();		
				}
				else
				{
					console.log(JSON.stringify(data));
					toastr.error(data.message, 'Errorsss');
				}
			 
				 
				document.getElementById('res').value = JSON.stringify(data); 
			})
			.error(function (data, status, header, config) {            
				console.log( JSON.stringify(data));        
			}); 
		
		 }

      
   
});





 