 
	
 //====================================================== ADD CUSTOMER CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('userAddController', function ($http, $scope, $window,toastr,$log) {
 
	     // get basic form to add product ============================================================== clean done
	     var auth_user_id = document.getElementById('auth_user_id').value;
 
         $('#loading').css('display', 'block');
		  
		 var request = $http({
            method: "GET",
            url: APP_URL+'/v1/form/user?user_type=5&auth_user_id='+auth_user_id,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.addUser  = data;
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) {  
		     document.getElementById("res").value = JSON.stringify(data);
        });  




		
		  
			//========= FUNCTIONS FOR GET STORE====================================================================================== 
		  $scope.storeSearch = function(query){
		        return $http.get(APP_URL+"/api/v1/stores?search_text="+query+"&auth_user_id="+auth_user_id, {params: {q: query}})
                .then(function(response){ 
				//alert(JSON.stringify(response.data.data.data));
                    return response.data.data.data;
                }) 
            };
		  
		    $scope.selectedStoreChange = function(values, data) {
                $log.info('Item changed to ' + JSON.stringify(values)); 
                $scope.store_id =  values.store_id  ;  
            
            }
			
	      $scope.searchStoreChange = function(text,data){
				 
				   $scope.store_id = ''; 
			}
		
		
		   // FUNCTION FOR STORE ITEM  ============================================================== clean done
	     
		
           $scope.storeUser = function(){ 
		   
		    for(var i=0;i<$scope.addUser[0].fields.length;i++){ 
			     if($scope.addUser[0].fields[i].identifier == 'user_type'){
				    $scope.addUser[0].fields[i].value = '5';
			     }
				  if($scope.addUser[0].fields[i].identifier == 'store_id'){
					 $scope.addUser[0].fields[i].value = $scope.store_id;
				 } 
              }
			  
			    var photo = $('#photo').val();
				 
				 for(var i=0;i<$scope.addUser[0].fields.length;i++){
				 if($scope.addUser[0].fields[i].identifier == 'photo'){
					 $scope.addUser[0].fields[i].value = photo; 
				 } 
				}
				 
			    var request = $http({
            method: "POST",
            url: APP_URL+'/api/v1/form/user?auth_user_id='+auth_user_id,
            data:  $scope.addUser,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {  
            $scope.data  = data;
			if(data.status_code == 1){
            $('#loading').css('display', 'none');
			     toastr.success(data.message,'Success!');    
			document.getElementById("res").value = JSON.stringify(data);
			$scope.customer_id = data.data.user_id
			  $window.location.href = 'driver/'+$scope.customer_id;
			}
			else{
				toastr.error(data.message,'Error!'); 
			}
        })
		.error(function (data, status, headers, config) {   
             toastr.error(data.message, 'Error');
		     document.getElementById("res").value = JSON.stringify(data);
        });  
 
		   };			   
		  
	});



 
	

	
//=========================================================EDIT CUSTOMER CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('userEditController', function ($http, $scope, $window,toastr,$log) {
 
 var auth_user_id = document.getElementById('auth_user_id').value;
  $scope.customer_id = window.location.href.substr(window.location.href.lastIndexOf('/') + 1); 
	 
	        $('#loading').css('display', 'block');
		  
		 var request = $http({
            method: "GET",
            url: APP_URL+'/v1/form/user/'+$scope.customer_id+"?user_type=5&auth_user_id="+auth_user_id,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.editUser  = data;
			console.log(JSON.stringify(data));
            $('#loading').css('display', 'none'); 
for(var i=0;i<=$scope.editUser[0].fields.length;i++){ 
		 if($scope.editUser[0].fields[i].identifier == 'store_id'){ 
	       var store = $scope.editUser[0].fields[i].value  ;
	       $scope.store = store; 	   
}   }
for(var k=0;k<=$scope.editUser[0].fields.length;k++){ 
		if($scope.editUser[0].fields[k].identifier == 'photo'){ 		   
			var item_data_image =  $scope.editUser[0].fields[k].value ;  
			document.getElementById('item_photo').value = item_data_image; 
		} 
		
	 }						
			
        })
		.error(function (data, status, headers, config) {
		     document.getElementById("res").value = JSON.stringify(data);
        });  


		 
		 
			//========= FUNCTIONS FOR GET STORE====================================================================================== 
		  $scope.storeSearch = function(query){
		        return $http.get(APP_URL+"/api/v1/stores?search_text="+query+"&auth_user_id="+auth_user_id, {params: {q: query}})
                .then(function(response){ 
				//alert(JSON.stringify(response.data.data.data));
                    return response.data.data.data;
                }) 
            };
		 
		 $scope.selectedStoreChange = function(values, data) {
                //$log.info('Item changed to ' + JSON.stringify(values)); 
                 $scope.store_id =  values.store_id ;   
			    if($scope.store_id != undefined || $scope.store_id != null){
				   $scope.store = $scope.store_id; 
			    }  
            
            }
	        $scope.searchStoreChange = function(text,data){
				 if(data.identifier == 'store_id' ){ 
				   $scope.store = ''; 
				} 
				 
			}
			
	 //==========================function for update user======================================
		   $scope.updateUser = function(){
			    
				
				 var item_photo = $('#item_photo').val(); 
         for(var j=0;j<$scope.editUser[0].fields.length;j++){
               if($scope.editUser[0].fields[j].identifier == 'photo'){
	               $scope.editUser[0].fields[j].value = item_photo; 
                }  
				 if($scope.editUser[0].fields[j].identifier == 'store_id'){
	               $scope.editUser[0].fields[j].value = $scope.store;  
                }  
 
              }
			   
			  console.log(JSON.stringify($scope.editUser)); 
			    var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/form/user/'+$scope.customer_id+'?auth_user_id='+auth_user_id,
            data:  $scope.editUser,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {  
            $scope.data  = data; 
			
			if(data.status_code == 1){
             toastr.success(data.message, 'Success');
            $('#loading').css('display', 'none');    
			 document.getElementById("res").value = JSON.stringify(data); 
			}
			else{
				 toastr.error(data.message, 'Error');
			}
        })
		.error(function (data, status, headers, config) {  	
		
             toastr.error(data.message, 'Error');
		      document.getElementById("res").value = JSON.stringify(data);
        });  
 
		   };		


//==========================function for RESET PASSWORD======================================
           $scope.resetPass = function(){ 
		    
			$scope.pass = $('#password').val();
			$scope.confirm_pass = $('#confirm_password').val();
			if($scope.pass != $scope.confirm_pass){
				toastr.error('Password does not match', 'Error');
			}
			else{
				
				$scope.passwordValue = { "password":$scope.pass };
			 var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/users/update-password/'+$scope.customer_id,
            data:$scope.passwordValue,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {  
        	 console.log(JSON.stringify(data));
            $scope.data  = data; 
             if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
                else { toastr.error(data.message, 'Error'); return false; }
   
        })
		.error(function (data, status, headers, config) {   
             toastr.error(data.message, 'Error');
		      console.log(JSON.stringify(data));
        });  
		   
			}
           };		   
	});



