app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : APP_URL+"/store.html",
        controller : "storeController"
    })
    .when("/manager", {
        templateUrl : APP_URL+"/manager.html",
        controller : "managerController"
    }) ;
});
	
	
	
 


//======================================================VIEW STORES LIST CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('storeController', function ($http,$scope,$window,toastr) { 

        //$('#loading').css('display', 'block'); 
		
		var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/stores?exclude_busy_store=false',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.stores = data;
			 $scope.storesData = data.data;
			 
           // $('#loading').css('display', 'none');  
		  
			document.getElementById("res").value = JSON.stringify(data);
			
			 $scope.storesData1 = { 
	    selected: null,
        data: {"data": []}
    };
 

	for (var i = 0; i <= $scope.storesData.data.length; ++i) {
        $scope.storesData1.data.data.push($scope.storesData.data[i]);   
		
    }

	 
    // Model to JSON for demo purpose
    $scope.$watch('storesData1', function(model) {  
        $scope.modelAsJson = angular.toJson(model, true);
		console.log("iiii"+JSON.stringify(model.data.data)); 
		 
    }, true);

	
	 
        })
		.error(function (data, status, headers, config) { 
			console.log(JSON.stringify(data));
		     document.getElementById("res").value = JSON.stringify(data);
        });       
	
	
	 
	
	
	
	// FUNCTION FOR DELETE STORE  ============================================================== clean done
	$scope.deleteStore = function(storeId) {
		    
			$scope.store_id = storeId;
			
			if (confirm("Are you sure?")) {
		        var request = $http({
                    method: "DELETE",
                    url: APP_URL+'/api/v1/stores/'+$scope.store_id,
                    data:  '',
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
				    $scope.data = data;
			     

			         if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }


                       location.reload();					
			    })
				.error(function (data, status, headers, config) { 
			        document.getElementById("res").value = JSON.stringify(data); 
				    toastr.error('Error Occurs','Error!');
                }); 
		    }
		
        };
		
		
		
		// FUNCTION FOR PAGINATION  ============================================================== clean done
		$scope.pagination = function(api_url){ 
			 if(api_url == null || api_url== ''){
				 return false;
			 }
			 else{
			 $('#loading').css('display', 'block'); 
		
		var request = $http({
            method: "GET",
            url: api_url+'&?exclude_busy_store=false',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
             $scope.stores = data;
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
		} 
		}
		
		
		
	
	
	
	
 	});
	
	
	
	
	
	
//======================================================ADD STORE CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('addStoreController', function ($http,$scope,$window,toastr,$log,$q) {
		
		
		  $scope.locationpickerOptions = {
                        location: {
                            latitude: 30.65118399999999,
                            longitude: 76.81360100000006
                        },
                        inputBinding: {
                            latitudeInput: $('#us3-lat'),
                            longitudeInput: $('#us3-lon'),
                            radiusInput: $('#us3-radius'),
                            locationNameInput: $('#us3-address') 
                        },
						zoom: 15,

                        radius: 0,
						enableReverseGeocode: false,
                        enableAutocomplete: true,
						 styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"color":"#7c93a3"},{"lightness":"-10"}]},{"featureType":"administrative.country","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"administrative.country","elementType":"geometry.stroke","stylers":[{"color":"#a0a4a5"}]},{"featureType":"administrative.province","elementType":"geometry.stroke","stylers":[{"color":"#62838e"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"color":"#dde3e3"}]},{"featureType":"landscape.man_made","elementType":"geometry.stroke","stylers":[{"color":"#3f4a51"},{"weight":"0.30"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"poi.attraction","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.government","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.place_of_worship","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.school","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.sports_complex","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":"-100"},{"visibility":"on"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#bbcacf"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"lightness":"0"},{"color":"#bbcacf"},{"weight":"0.50"}]},{"featureType":"road.highway","elementType":"labels","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"labels.text","stylers":[{"visibility":"on"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.stroke","stylers":[{"color":"#a9b4b8"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"invert_lightness":true},{"saturation":"-7"},{"lightness":"3"},{"gamma":"1.80"},{"weight":"0.01"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#a3c7df"}]}],
						 
						markerIcon: APP_URL+"/admin/assets/images/map-marker.png",
                       markerDraggable: true,
                       markerVisible : true 
					   
					   
                    };
		 
		 
					  // $('#us3-address').val() = $('#address').val();	
		 
		 
		 //========= FUNCTIONS FOR GET VENDOR FOR STORE A AREA====================================================================================== 
		  $scope.vendorSearch = function(query){
		        return $http.get(APP_URL+"/api/v1/users?user_type=3&search="+query, {params: {q: query}})
                .then(function(response){  
                    return response.data.data.data;
                }) 
            };
		  
		    $scope.selectedVendorChange = function(values) {
                $log.info('Item changed to ' + JSON.stringify(values));  
	            $scope.vendor_id =  values.user_id;  
            }
			
			 $scope.searchVendorChange = function(text){  
				   $scope.vendor_id = ''; 
			}
			
			//========= FUNCTIONS FOR GET MANAGER FOR STORE====================================================================================== 
		  $scope.managerSearch = function(query){
		        return $http.get(APP_URL+"/api/v1/users?user_type=4&search="+query, {params: {q: query}})
                .then(function(response){ 
				//alert(JSON.stringify(response.data.data.data));
                    return response.data.data.data;
                }) 
            };
		  
		    $scope.selectedManagerChange = function(values) {
                $log.info('Item changed to ' + JSON.stringify(values));  
	            $scope.manager_id =  values.user_id;  
            }
			
			 $scope.searchManagerChange = function(text){  
				   $scope.manager_id = ''; 
			}
		 
         //$('#loading').css('display', 'block');  
		$scope.addStore = function(){
			  
           $scope.featured = $('#featured').val();	
		   if(!$scope.featured){
			   toastr.error('Choose Featured', 'Error'); return false; 
		   }
			
			$scope.store_title = $('#title').val(); 
			$scope.comission = $('#comission').val(); 
			$scope.manager_id = $('#manager_id').val(); 
			$scope.vendor_id = $('#vendor_id').val(); 	
			$scope.store_tax = $('#store_tax').val(); 		
			$scope.latitude = $('#us3-lat').val(); 
			$scope.longitude = $('#us3-lon').val();  
             $scope.address = $('#address').val();	
             $scope.store_phone = $('#store_phone').val();			
			 $scope.store_photo = $('#file_name').val();
			$scope.storeData = { "store_title" : $scope.store_title,"store_phone" : $scope.store_phone, "commission" : $scope.comission, "manager_id" : $scope.manager_id, "vendor_id" : $scope.vendor_id,"store_tax" : $scope.store_tax, "longitude" : $scope.longitude, "latitude" : $scope.latitude,  "address" : $scope.address , "featured" : $scope.featured, "store_photo" : $scope.store_photo};
		 
		    var request = $http({
                 method: "POST",
                 url: APP_URL+'/api/v1/stores',
                 data:  $scope.storeData,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) { 
                $scope.stores = data;  
               if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); 
			    $scope.id  = data.data.id;
			   $window.location.href = 'store/'+$scope.id;
			  }
            else { toastr.error(data.message, 'Error'); return false; }


				
             // $('#loading').css('display', 'none');  
			    document.getElementById("res").value = JSON.stringify(data);
            })
		    .error(function (data, status, headers, config) {
                 toastr.error(data.message, 'Error');				
		        document.getElementById("res").value = JSON.stringify(data);
            });       
	
		}
 	});
		
 
  
 
 	
//======================================================EDIT STORE CONTROLLER=================================================================================
//============================================================================================================================================================
 
	
	app.controller('editStoreController', function ($http,$scope,$window,toastr,$location,$log, $q) {

		 	var auth_user_id = document.getElementById('auth_user_id').value;
		 
         //$('#loading').css('display', 'block'); 

		 $scope.store_id = window.location.href.substr(window.location.href.lastIndexOf('/') + 1); 
		 
		$http.get(APP_URL+'/api/v1/stores/'+$scope.store_id)
            .success(function(data, status, headers, config) { 
		    $scope.store = data.data[0];  
		    $scope.latitude = $scope.store.latitude; 

	 

		 
             $scope.vendor = $scope.store.vendor_id;
			  $scope.manager = $scope.store.manager_id; 
 $scope.locationpickerOptions();console.log($scope.something);

	    }) 
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
		
		
			
		//========= FUNCTIONS FOR GET META DATA FOR STORE====================================================================================== 
         var request = $http({
                 method: "GET",
                 url: APP_URL+'/api/v1/stores/'+$scope.store_id+'/form/meta',
                 data:  '',
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) { 
                $scope.storeMeta = data;    


             // $('#loading').css('display', 'none');  
			    document.getElementById("res").value = JSON.stringify($scope.storeMeta);
            })
		    .error(function (data, status, headers, config) { 
		        document.getElementById("res").value = JSON.stringify(data);
            });   


			
			
		//========= FUNCTIONS FOR LOCATION FOR STORE====================================================================================== 
		
		 

 
			  $scope.locationpickerOptions = { 
                        location: {
                            latitude:30.64258767675,
                            longitude: 76.81360100000006
                        },
                        inputBinding: {
                            latitudeInput: $('#us3-edit-lat'),
                            longitudeInput: $('#us3-edit-lon'),
                            radiusInput: $('#us3-radius'),
                            locationNameInput: $('#us3-edit-address') 
                        },
						zoom: 15,

                        radius: 0,
                        enableAutocomplete: true,
						 styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"color":"#7c93a3"},{"lightness":"-10"}]},{"featureType":"administrative.country","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"administrative.country","elementType":"geometry.stroke","stylers":[{"color":"#a0a4a5"}]},{"featureType":"administrative.province","elementType":"geometry.stroke","stylers":[{"color":"#62838e"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"color":"#dde3e3"}]},{"featureType":"landscape.man_made","elementType":"geometry.stroke","stylers":[{"color":"#3f4a51"},{"weight":"0.30"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"poi.attraction","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.government","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.place_of_worship","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.school","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.sports_complex","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":"-100"},{"visibility":"on"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#bbcacf"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"lightness":"0"},{"color":"#bbcacf"},{"weight":"0.50"}]},{"featureType":"road.highway","elementType":"labels","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"labels.text","stylers":[{"visibility":"on"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.stroke","stylers":[{"color":"#a9b4b8"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"invert_lightness":true},{"saturation":"-7"},{"lightness":"3"},{"gamma":"1.80"},{"weight":"0.01"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#a3c7df"}]}],
						 
						 
						markerIcon: APP_URL+"/admin/assets/images/map-marker.png",
                       markerDraggable: true,
                       markerVisible : true

                    };
					
					
					
					
					 //========= FUNCTIONS FOR GET VENDOR FOR STORE A AREA====================================================================================== 
		  $scope.vendorSearch = function(query){
		        return $http.get(APP_URL+"/api/v1/users?user_type=3&search="+query, {params: {q: query}})
                .then(function(response){  
                    return response.data.data.data;
                }) 
            };
		  
		    $scope.selectedVendorChange = function(values) { 
                $log.info('Item changed to ' + JSON.stringify(values));  
	            $scope.vendor_id =  values.user_id ; 				
			    if($scope.vendor_id != undefined || $scope.vendor_id != null){
				   $scope.vendor = $scope.vendor_id;
			    }
            }
			
			 $scope.searchVendorChange = function(text){ 
				   $scope.vendor = ''; 
			}
			//========= FUNCTIONS FOR GET MANAGER FOR STORE====================================================================================== 
		  $scope.managerSearch = function(query){
		        return $http.get(APP_URL+"/api/v1/users?user_type=4&search="+query, {params: {q: query}})
                .then(function(response){ 
				//alert(JSON.stringify(response.data.data.data));
                    return response.data.data.data;
                }) 
            };
		  
		    $scope.selectedManagerChange = function(values) {
                $log.info('Item changed to ' + JSON.stringify(values));  
	            $scope.manager_id =  values.user_id; 
                 if($scope.manager_id != undefined || $scope.manager_id != null){
				   $scope.manager = $scope.manager_id;
			    }				
            }
			 $scope.searchManagerChange = function(text){ 
				   $scope.manager = ''; 
			}
			
		 //========= FUNCTIONS FOR GET META DATA FOR STORE====================================================================================== 
		$scope.updateStoreData = function(){  
		           $scope.featured = $('#featured').val();	 
		   if(!$scope.featured){
			   toastr.error('Choose Featured', 'Error'); return false; 
		   }
			$scope.store_title = $('#title').val(); 
			$scope.comission = $('#comission').val(); 
			$scope.manager_id = $('#manager_id').val(); 
			$scope.vendor_id = $('#vendor_id').val();   
			$scope.latitude = $('#us3-edit-lat').val(); 
			$scope.longitude = $('#us3-edit-lon').val();   
			$scope.address = $('#address2').val();   		
			$scope.store_tax = $('#store_tax').val(); 	
			$scope.store_phone = $('#store_phone').val(); 		
			 $scope.store_photo = $('#file_name').val();
			$scope.storeData = [{ "store_title" : $scope.store_title,"store_phone" : $scope.store_phone, "commission" : $scope.comission,"store_tax" : $scope.store_tax, "manager_id" : $scope.manager_id, "vendor_id" : $scope.vendor_id, "longitude" : $scope.longitude, "latitude" : $scope.latitude,  "address" : $scope.address,"featured" : $scope.featured, "store_photo" : $scope.store_photo}];
 
		 console.log("post data ="+JSON.stringify($scope.storeData));
 
		  
		   $scope.updateStoreDataValue = {"basic_data" : $scope.storeData , "meta_data" : $scope.storeMeta }
		  
		 console.log(JSON.stringify($scope.updateStoreDataValue));
		 
	  
		    var request = $http({
                 method: "POST",
                 url: APP_URL+'/api/v1/stores/'+$scope.store_id+'/submit_store_edit_forms?auth_user_id='+auth_user_id,
                 data:  $scope.updateStoreDataValue,
                 headers: { 'Accept':'application/json' }
            }); 
            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {
                $scope.data = data;  
               if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }

console.log(JSON.stringify(data));
             // $('#loading').css('display', 'none');  
			     
            })
		    .error(function (data, status, headers, config) { 
		     
		        console.log(JSON.stringify(data));
            });       
	
		}
		
		 

				//========= FUNCTIONS FOR UPDATE  META DATA FOR STORE====================================================================================== 
		
		   $scope.updateMetaStore = function(){  
		       
		 console.log("Post meta form ="+JSON.stringify($scope.storeMeta));
		
 
		  var request = $http({
                 method: "POST",
                 url: APP_URL+'/api/v1/stores/'+$scope.store_id+'/form/meta',
                 data: $scope.storeMeta,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) { 
                $scope.data = data;  
		

		    if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }

            return false;
				 location.reload();
             // $('#loading').css('display', 'none');  
			    document.getElementById("res").value = JSON.stringify($scope.store);
            })
		    .error(function (data, status, headers, config) {  
			 toastr.error(data.message, 'Error');
		        document.getElementById("res").value = JSON.stringify(data);
            });   
		    }
 	});
		
 
 
 
 
 
 
 
 
 
 
 
 //======================================================VIEW STORE MANAGER LIST CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('managerController', function ($http,$scope,$window,toastr) { 
        //$('#loading').css('display', 'block'); 
		
		var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/users?user_type=4',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            $scope.manager = data;
           // $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
	
	
	// FUNCTION FOR DELETE ITEM  ============================================================== clean done
	  $scope.deleteUser = function(userId) { 
			$scope.user_id = userId; 
			if (confirm("Are you sure?")) {
		        var request = $http({
                    method: "DELETE",
                    url: APP_URL+'/api/v1/users/'+$scope.user_id,
                    data:  '',
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
				    $scope.data = data;
					if(data.status_code == 1){
			        toastr.success(data.message,'Success!');
                       location.reload();		
					}		
                    else{
					   toastr.error(data.message,'Error!');
				    }					
			    })
				.error(function (data, status, headers, config) { 
			        console.log(JSON.stringify(data)); 
				    toastr.error('Error Occurs','Error!');
                }); 
		    }
		
        };
		
		
		// FUNCTION FOR PAGINATION  ============================================================== clean done
		$scope.pagination = function(api_url){ 
			 if(api_url == null || api_url== ''){
				 return false;
			 }
			 else{
			 $('#loading').css('display', 'block'); 
		
		var request = $http({
            method: "GET",
            url: api_url+'&user_type=4',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            $scope.manager = data;
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
		} 
		}
	
 	});
	
	
	