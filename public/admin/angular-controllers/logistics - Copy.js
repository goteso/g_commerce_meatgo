app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : APP_URL+"/tasks.html",
        controller : "taskController"
    })
    .when("/driver", {
        templateUrl : APP_URL+"/driver.html",
        controller : "driverController"
    }) ;
});
	
	
//======================================================VIEW STORES LIST CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('logisticsController', function ($http,$scope,$window,toastr,$interval) { 
	
	
	
	 $scope.Markers = [
            {
                "title": 'Aksa Beach',
                "lat": '19.1759668',
                "lng": '72.79504659999998',
                "description": 'Aksa Beach is a popular beach and a vacation spot in Aksa village at Malad, Mumbai.'
            },
            {
                "title": 'Juhu Beach',
                "lat": '19.0883595',
                "lng": '72.82652380000002',
                "description": 'Juhu Beach is one of favorite tourist attractions situated in Mumbai.'
            },
            {
                "title": 'Girgaum Beach',
                "lat": '18.9542149',
                "lng": '72.81203529999993',
                "description": 'Girgaum Beach commonly known as just Chaupati is one of the most famous public beaches in Mumbai.'
            },
            {
                "title": 'Jijamata Udyan',
                "lat": '18.979006',
                "lng": '72.83388300000001',
                "description": 'Jijamata Udyan is situated near Byculla station is famous as Mumbai (Bombay) Zoo.'
            },
            {
                "title": 'Sanjay Gandhi National Park',
                "lat": '19.2147067',
                "lng": '72.91062020000004',
                "description": 'Sanjay Gandhi National Park is a large protected area in the northern part of Mumbai city.'
            }];
 
            //Setting the Map options.
            $scope.MapOptions = {
                center: new google.maps.LatLng($scope.Markers[0].lat, $scope.Markers[0].lng),
                zoom: 8,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
 
            //Initializing the InfoWindow, Map and LatLngBounds objects.
            $scope.InfoWindow = new google.maps.InfoWindow();
            $scope.Latlngbounds = new google.maps.LatLngBounds();
            $scope.Map = new google.maps.Map(document.getElementById("dvMap"), $scope.MapOptions);
            var poly;
            //Looping through the Array and adding Markers.
            for (var i = 0; i < $scope.Markers.length; i++) {
                var data = $scope.Markers[i];
                var myLatlng = new google.maps.LatLng(data.lat, data.lng);
 
                //Initializing the Marker object.
                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: $scope.Map,
                    title: data.title
                });
 
                //Adding InfoWindow to the Marker.
                (function (marker, data) {
					 poly = new google.maps.Polyline({
         strokeColor: '#000000',
         strokeOpacity: 1.0,
         strokeWeight: 3
       });
       poly.setMap($scope.Map);
	   
                    google.maps.event.addListener(marker, "click", function (e) {
                        $scope.InfoWindow.setContent("<div style = 'width:200px;min-height:40px'>" + data.description + "</div>");
                        $scope.InfoWindow.open($scope.Map, marker);
                    });
                })(marker, data);
 
                //Plotting the Marker on the Map.
                $scope.Latlngbounds.extend(marker.position);
            }
 
 
            
	   
 
            //Adjusting the Map for best display.
            $scope.Map.setCenter($scope.Latlngbounds.getCenter());
            $scope.Map.fitBounds($scope.Latlngbounds);
			
			
        
		$scope.locationpickerOptions = { 
                        location: {
                            latitude: 30.65118399999999,
                            longitude: 76.81360100000006
                        },
                        inputBinding: {
                            latitudeInput: $('#us3-lat'),
                            longitudeInput: $('#us3-lon'),
                            radiusInput: $('#us3-radius'),
                            locationNameInput: $('#us3-address') 
                        },
						zoom: 15,

                        radius: 0,
                        origin:'30.6554,76.3444',
                        enableAutocomplete: true,
						 styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"color":"#7c93a3"},{"lightness":"-10"}]},{"featureType":"road","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"administrative.country","elementType":"geometry.stroke","stylers":[{"color":"#a0a4a5"}]},{"featureType":"administrative.province","elementType":"geometry.stroke","stylers":[{"color":"#62838e"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"color":"#dde3e3"}]},{"featureType":"landscape.man_made","elementType":"geometry.stroke","stylers":[{"color":"#3f4a51"},{"weight":"0.30"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"poi.attraction","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.government","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.place_of_worship","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.school","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.sports_complex","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":"-100"},{"visibility":"on"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#bbcacf"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"lightness":"0"},{"color":"#bbcacf"},{"weight":"0.50"}]},{"featureType":"road.highway","elementType":"labels","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"labels.text","stylers":[{"visibility":"on"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.stroke","stylers":[{"color":"#a9b4b8"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"invert_lightness":true},{"saturation":"-7"},{"lightness":"3"},{"gamma":"1.80"},{"weight":"0.01"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#a3c7df"}]}],
						 
						markerIcon: APP_URL+"/admin/assets/images/map-marker.png",
                       markerDraggable: true,
                       markerVisible : true
                    };
					
					
					
					
			/*		 var markers = [];
              for (var i=0; i<8 ; i++) {
      markers[i] = new google.maps.Marker({
        title: "Hi marker " + i
      })
    }
    $scope.GenerateMapMarkers = function() {
        $scope.date = Date(); // Just to show that we are updating
        
        var numMarkers = Math.floor(Math.random() * 4) + 4;  // betwween 4 & 8 of them
        for (i = 0; i < numMarkers; i++) {
            var lat =   1.280095 + (Math.random()/100);
            var lng = 103.850949 + (Math.random()/100);
            // You need to set markers according to google api instruction
            // you don't need to learn ngMap, but you need to learn google map api v3
            // https://developers.google.com/maps/documentation/javascript/markers
            var latlng = new google.maps.LatLng(lat, lng);
            markers[i].setPosition(latlng);
            markers[i].setMap($scope.map);
        }      
    };  
    
    $interval( $scope.GenerateMapMarkers, 2000);
	*/
	
 	});
	
	
	
	app.controller('taskController', function ($http,$scope,$window,toastr, $log) {
	var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/task?status=0',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            $scope.unassigned = data;
           // $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
	 
	
	
	$scope.assignedTask = function(){
		 	var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/task?status=1,2',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            $scope.assigned = data;
           // $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
	}
	
	$scope.completedTask = function(){
		 	var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/task?status=3',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            $scope.completed = data;
           // $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
	}
	
	
	
	
	 /*=============================FUNCTION FOR POPUP FOR ASSIGN TASK TO DRIVER=============================================================*/		
     	$scope.getDriver = function(data){ 			
		    $scope.task_id = data.task_id;
			$('#driverModal').modal('show');
			
		}
		
		


		//============================= FUNCTIONS FOR GET DRIVERS LIST=================================================================
		  $scope.driverSearch = function(query) {  
            return $http.get(APP_URL + "/api/v1/users?user_type=5&search_text=" + query , {
                params: {
                    q: query
                }
            })
            .then(function(response) {  
                return response.data.data.data;
            })
        };
  
        $scope.selectedDriverChange = function(data) { 
            $log.info('Item changed to ' + JSON.stringify(data)); 
                 $scope.driver_id =   data.user_id;  
				 alert($scope.driver_id);
        } 
		
		



		//============================= FUNCTIONS FOR ASSIGN DRIVER TO A TASK================================================================
		$scope.assignDriver = function(){
alert($scope.task_id);		
		alert($scope.driver_id);
	     	$scope.driver = {"driver_id": $scope.driver_id}; 
		     var request = $http({
				 method: "PUT",
				 url: APP_URL+'/api/v1/task/assign-driver/'+$scope.task_id,
				 data: $scope.driver,
				 headers: { 'Accept':'application/json' }
             });

			/* Check whether the HTTP Request is successful or not. */
			    request.success(function (data) { 
				$scope.statusData =  data ;  
				$('#loading').css('display', 'none');
       
				if( data.status_text == 'Success'  )
				{
                   toastr.success(data.message, 'Success');
                   location.reload();		
				}
				else
				{
					toastr.error(data.message, 'Error');
				}
			 
				 
				document.getElementById('res').value = JSON.stringify(data); 
			})
			.error(function (data, status, header, config) {            
				document.getElementById('res').value = JSON.stringify(data);        
			}); 
		
		 }
		 
	
	});
	
	
	
	
	
	// =====================================================DRIVER CONTROLLER STARTS HERE===================================================================
	//========================================================================================================================================================
	
	
	
	app.controller('driverController', function ($http,$scope,$window,toastr) { 
	
	
	   	var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/users?user_type=5',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            $scope.driver = data;
           // $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
	
	// FUNCTION FOR DELETE ITEM  ============================================================== clean done
	  $scope.deleteUser = function(userId) { 
			$scope.user_id = userId; 
			if (confirm("Are you sure?")) {
		        var request = $http({
                    method: "DELETE",
                    url: APP_URL+'/api/v1/users/'+$scope.user_id,
                    data:  '',
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
				    $scope.data = data;
					if(data.status_code == 1){
			        toastr.success(data.message,'Success!');
                       location.reload();		
					}		
                    else{
					   toastr.error(data.message,'Error!');
				    }					
			    })
				.error(function (data, status, headers, config) { 
			        console.log(JSON.stringify(data)); 
				    toastr.error('Error Occurs','Error!');
                }); 
		    }
		
        };
	
	});
	
	
	
