 
//========================================================== ORDER CONTROLLER==================================================================================
//==============================================================================================================================================================
 
	
	 app.controller('OrderController', function($http,$scope,$window) {
		     
		//========= ONLOAD GET ORDER DETAILS  VIA CALLING API================================================================== 
        var request = $http({ 
             method: "GET",   
             url: APP_URL+'/api/v1/orders?include_count_blocks=true',  
            data: '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
			$scope.orders =  data ; 
            document.getElementById('res').value = JSON.stringify(data); 

 
        })
		.error(function (data, status, header, config) {            
        	document.getElementById('res').value = JSON.stringify(data);        
	    }); 
			
 
 
  
  
		
	});



 