var app = angular.module("mainApp", ['ngMaterial', 'ngAnimate', 'ngMessages',  'toastr'])


app.controller('newsletterController', function($http,$scope,toastr) { 
 
		 //==========================function for update user======================================
		   $scope.submitNewsletter = function(){
			 
			    $scope.email = $('#newsletter-email').val();
			  $scope.newsletter_email = {"email" : $scope.email};
				
			    var request = $http({
            method: "POST",
            url: APP_URL+'/api/v1/newsletters',
            data:  $scope.newsletter_email,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {  
            $scope.data  = data;  
             if(data.status_code == 1) { toastr.success(data.message, 'Success'); $('#newsletter-email').val('');}
            else { toastr.error(data.message, 'Error'); }
             
            $('#loading').css('display', 'none');    
			 console.log(JSON.stringify(data)); 
        })
		.error(function (data, status, headers, config) {  	
		      toastr.error(data.message, 'Error');
		      console.log(JSON.stringify(data));
        });  
 
		   };			   
 
});




app.controller('profileController', function($http,$scope) { 
 
		
		 var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/users/1',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.profileData  = data;
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) {
		     document.getElementById("res").value = JSON.stringify(data);
        });  

});
