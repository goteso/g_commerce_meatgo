  
app.controller('loginController', function($http,$scope,$window) { 


$scope.login = function(){
	
	
$scope.user_email = $('#user_email').val();
$scope.user_password = $('#user_password').val();

$scope.loginData = {"email":$scope.user_email, "password":$scope.user_password};
console.log(JSON.stringify($scope.loginData));
 
  var request = $http({
            method: "POST",
            url: APP_URL+'/api/v1/users/login',
            data: $scope.loginData,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.data  = data;
			$scope.user_id = data.user_data[0].user_id;
			$window.location.href = 'account/'+$scope.user_id;
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) {
		     document.getElementById("res").value = JSON.stringify(data);
        });  
 		   
}
});


 