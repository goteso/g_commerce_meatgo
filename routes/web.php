<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use App\Events\SendLocation;
Route::get('/index', function () {
    return view('index');
});
Route::get('/cache', function () {
    return Cache::get('key');
});


Route::get('/admin_login', function () {
    return view('admin.login.index');
});



Route::get('email-test', function(){
   $details['email'] = 'harvindersingh@goteso.com';
   $details['email_body'] = 'tested body from route';
   $d = dispatch(new App\Jobs\SendEmailTest($details));
  dd($d);
});

/**
// socialite starts
Route::get('facebook', function () {
    return view('facebook');
});
Route::get('auth/facebook', 'Auth\FacebookController@redirectToFacebook');
Route::get('auth/facebook/callback', 'Auth\FacebookController@handleFacebookCallback');
**/


Route::get('logouts', '\App\Http\Controllers\Admin\LoginController@logout');




Route::get('/map', function (Request $request) {
 
    $location = [];
    event(new SendLocation($location));
    return response()->json(['status'=>'success', 'data'=>$location]);
});



 
 
Route::get('/admin/dashboard', function () { return view('admin.dashboard.index'); }); 
 
Route::get('/v1/dashboard_data', function () { return view('admin.dashboard.index'); }); 
 

Route::get('/v1/store-requests', function () {
    return view('admin.requests.store-request');
});


Route::get('/settings', function () {
    return view('admin.settings.index');
});
 

 Route::get('/v1/order-cancel-reasons', function () {  return view('admin.order-cancel-reasons.index'); }); 


 Route::get('/v1/account', function () { return view('admin.settings.account'); }); 
Route::get('/v1/faqs', function () { return view('admin.settings.faqs'); }); 
 Route::get('/v1/business-logo', function () { return view('admin.settings.logo'); }); 
 Route::get('/v1/banners', function () { return view('admin.settings.banner'); });  
Route::get('/v1/update_store', function () { return view('admin.settings.update_store'); }); 

 Route::get('/v1/restaurants', function () { return view('admin.settings.restaurants'); });
Route::get('/v1/timeslots', function () { return view('admin.settings.timeslots'); });
Route::get('/v1/tax', function () { return view('admin.settings.tax'); });

Route::get('/v1/stores/sort', function () { return view('admin.settings.sortStores'); });

Route::get('/v1/locations', function () { return view('admin.settings.locations'); }); 
Route::get('/v1/areas', function () { return view('admin.settings.areas'); });
Route::get('/v1/plugins', function () { return view('admin.settings.plugins'); });
 
Route::get('/v1/categories', function () {  return view('admin.settings.categories'); }); 
Route::get('/v1/tags', function () {   return view('admin.settings.tags'); });








   //image upload for GCommerce================
   Route::match(['get', 'post'], 'image-upload-items', 'ImageController@image_upload_items');
   Route::delete('ajax-remove-image-items/{filename}', 'ImageController@deleteImageItems');

      //image upload for GCommerce================
   Route::match(['get', 'post'], 'image-upload-users', 'ImageController@image_upload_users');
   Route::delete('ajax-remove-image-users/{filename}', 'ImageController@deleteImageUsers');


   //image upload for categories================
   Route::match(['get', 'post'], 'image-upload-categories', 'ImageController@image_upload_categories');
   Route::delete('ajax-remove-image-categories/{filename}', 'ImageController@deleteImageCategories');

    //image upload for tags================
   Route::match(['get', 'post'], 'image-upload-tags', 'ImageController@image_upload_tags');
   Route::delete('ajax-remove-image-tags/{filename}', 'ImageController@deleteImageTags');


  //image upload for stores================
   Route::match(['get', 'post'], 'image-upload-stores', 'ImageController@image_upload_stores');
   Route::delete('ajax-remove-image-stores/{filename}', 'ImageController@deleteImageStores');

 //image upload for coupons================
   Route::match(['get', 'post'], 'image-upload-coupons', 'ImageController@image_upload_coupons');
   Route::delete('ajax-remove-image-coupons/{filename}', 'ImageController@deleteImageCoupons');

 //image upload for logo================
   Route::match(['get', 'post'], 'image-upload-logo', 'ImageController@image_upload_logo');
   Route::delete('ajax-remove-image-logo/{filename}', 'ImageController@deleteImageLogo');

//image upload for banner================
   Route::match(['get', 'post'], 'image-upload-banner', 'ImageController@image_upload_banners');
   Route::delete('ajax-remove-image-banner/{filename}', 'ImageController@deleteImageBanners');


   
// route to show the login form for admin section
Route::group(array('namespace'=>'Admin'), function()
{



//================================== G_Commerce Routes Starts==============================================================
//=========================================================================================================================



 //================================================ item Forms starts Starts ===========================================================================//

  Route::get('/v1/form/item', array('uses' => 'ItemFormsController@get_add_form'));//Route-14
  Route::get('/v1/form/item/{id}', array('uses' => 'ItemFormsController@get_edit_form'));//Route-15
  Route::get('/v1/items', function () { return view('admin.items.items'); });
  Route::get('/v1/item', function () { return view('admin.items.item_add'); });
   Route::get('/v1/item/{id}', function () { return view('admin.items.item_edit'); });
 
   //================================================ Item Forms Ends ===============================================================================//



   Route::get('/v1/dashboard', array('uses' => 'DashboardController@index'));//Route-65
   
   Route::get('/v1/dashboard-store', array('uses' => 'DashboardStoreController@index'));//Route-65
   
   Route::get('/v1/dashboard-vendor', array('uses' => 'DashboardVendorController@index'));//Route-65
   
   

 

 //================================================ Users Forms Starts ===========================================================================//
 
 
Route::get('/v1/logistics', function () { return view('admin.logistics.index'); });

  Route::get('/v1/form/user', array('uses' => 'UserFormsController@get_add_form')); 
  Route::get('/v1/form/user/{id}', array('uses' => 'UserFormsController@get_edit_form')); 
 
 
 
Route::get('/v1/logistics', function () { return view('admin.logistics.index'); });
 
  Route::get('/v1/form/user', array('uses' => 'UserFormsController@get_add_form')); 
  Route::get('/v1/form/user/{id}', array('uses' => 'UserFormsController@get_edit_form')); 

 
//================================================ Users Forms Ends ===========================================================================//



//==============================send notification routes starts ===============================
Route::get('/v1/emails-to-customers', function () { return view('admin.notifications.emails_to_customers'); });
Route::get('/v1/push-to-customers', function () { return view('admin.notifications.push_to_customers'); });
Route::get('/v1/emails-to-stores', function () { return view('admin.notifications.emails_to_stores'); });
//================================ send notification routes ends ==============================


Route::get('/v1/email_main', function () { return view('emails.email_main'); });

Route::get('/notifications', function () { return view('admin.notifications.index'); });



Route::get('/invoice/{id}', function () { return view('admin.orders.invoice'); });




Route::get('/v1/reviews', function () { return view('admin.reviews.reviews'); });
Route::get('/v1/freshdesk', function () {   return view('admin.freshdesk-enquiries.index'); });

 Route::get('/v1/coupons', function () { return view('admin.coupons.coupons'); });
 Route::get('/v1/coupon', function () { return view('admin.coupons.coupon_add'); });
 Route::get('/v1/coupon/{id}', function () { return view('admin.coupons.coupon_edit'); });
 
 
 
 
 
 Route::get('/v1/stores', function () { return view('admin.stores.stores'); });
  Route::get('/v1/store', function () { return view('admin.stores.store_add'); });
 Route::get('/v1/store/{id}', function () { return view('admin.stores.store_edit'); }); 
 
 
     Route::group([ 'middleware' => 'auth' ], function() {
 Route::get('/v1/customers', function () { return view('admin.users.customers'); });
 Route::get('/v1/customer', function () { return view('admin.users.customer_add'); }); 
 Route::get('/v1/customer/{id}', function () { return view('admin.users.customer_edit'); });  
  Route::get('/v1/customer-profile/{id}', function () { return view('admin.users.customer-profile'); });
  });
 
 Route::get('/v1/vendors', function () { return view('admin.users.vendors'); });
 Route::get('/v1/vendor', function () { return view('admin.users.vendor_add'); }); 
 Route::get('/v1/vendor/{id}', function () { return view('admin.users.vendor_edit'); });
  Route::get('/v1/vendor-profile/{id}', function () { return view('admin.users.vendor-profile'); }); 
 
  Route::get('/v1/driver', function () { return view('admin.logistics.driver_add'); }); 
 Route::get('/v1/driver/{id}', function () { return view('admin.logistics.driver_edit'); }); 
  Route::get('/v1/driver-profile/{id}', function () { return view('admin.logistics.driver-profile'); });
 
 Route::get('/v1/manager', function () { return view('admin.stores.manager_add'); }); 
 Route::get('/v1/manager/{id}', function () { return view('admin.stores.manager_edit'); }); 
 
 
 
 Route::get('/v1/order_detail/{id}', function () { return view('admin.orders.order-detail'); });
 Route::get('/v1/orders', function () { return view('admin.orders.orders'); });
  Route::get('/v1/order', function () { return view('admin.orders.order_add'); });


    Route::get('/v1/reports', function () { return view('admin.reports.reports'); });
	 //Route::get('/v1/reports-detail', function () { return view('admin.reports.report-detail'); });
  	 Route::get('/v1/reports-detail/{identifier}', function () { return view('admin.reports.report-detail'); });
  
  

    Route::get('/admin_login', array('as' => 'admin', 'uses' => 'LoginController@index'));
    Route::post('/login_post',  array('as' => 'admin', 'uses' => 'LoginController@login_post'));
  
 
    Route::get('/admin/change-password', array('middleware' => 'App\Http\Middleware\Role', 'uses' => 'DashController@change_password'));
    Route::put('/admin/change-password', array('middleware' => 'App\Http\Middleware\Role','uses' => 'DashController@update_password'));

	
	 
	Route::post('/admin/ajax_users_data', array( 'middleware' => 'auth', 'uses' => 'DashController@ajax_users_data'));
	Route::post('/admin/ajax_likes_data', array( 'middleware' => 'auth', 'uses' => 'DashController@ajax_likes_data'));
	Route::post('/admin/get_countries_data', array( 'middleware' => 'auth', 'uses' => 'DashController@get_countries_data'));
	Route::post('/admin/get_genders_data', array( 'middleware' => 'auth', 'uses' => 'DashController@get_genders_data'));
	
	
	Route::post('/admin/ajax_transaction_data', array( 'middleware' => 'auth', 'uses' => 'DashController@ajax_transaction_data'));
 
	//Users
   Route::resource('user', 'UsersController');
   Route::get('/admin/user', array('middleware' => 'App\Http\Middleware\Role','uses' => 'UsersController@index'));
   Route::get('/admin/user/status/{status}', array('middleware' => 'App\Http\Middleware\Role','uses' => 'UsersController@user_by_status'));
   Route::get('/admin/user/verified/{verified}', array('middleware' => 'App\Http\Middleware\Role','uses' => 'UsersController@user_by_verified'));
   Route::get('/admin/user/verified/{verified}', array('middleware' => 'App\Http\Middleware\Role','uses' => 'UsersController@user_by_verified'));
   
   Route::get('/admin/user/create', array('middleware' => 'App\Http\Middleware\Role','uses' => 'UsersController@create'));
   Route::post('/admin/user', array('middleware' => 'App\Http\Middleware\Role','uses' => 'UsersController@store'));
   Route::get('/admin/user/{id}', array('middleware' => 'App\Http\Middleware\Role','uses' => 'UsersController@show'));
   Route::get('/admin/user/verify/{id}', array('middleware' => 'App\Http\Middleware\Role','uses' => 'UsersController@verify'));
   Route::get('/admin/user/{id}/edit', array('middleware' => 'App\Http\Middleware\Role','uses' => 'UsersController@edit'));
   Route::put('/admin/user/{id}', array('middleware' => 'App\Http\Middleware\Role','uses' => 'UsersController@update'));
   Route::get('/admin/user/{id}/delete', array('uses' => 'UsersController@destroy'));
   Route::get('/admin/user/{id}/block/', array('uses' => 'UsersController@block'));
   Route::get('/admin/user/{id}/unblock/', array('uses' => 'UsersController@unblock'));
   Route::get('/admin/user/{id}/verify/', array('uses' => 'UsersController@verify'));
   Route::get('/admin/user/{id}/unverify/', array('uses' => 'UsersController@unverify'));
   
   Route::get('/admin/user/show/{id}', array('uses' => 'UsersController@show'));
   
   
   
   
   
   

   
   	//Admin Users
   Route::resource('admins', 'AdminController');
   Route::get('/admins/admin/status/{status}', array('middleware' => 'App\Http\Middleware\Role','uses' => 'AdminController@user_by_status'));
   Route::get('/admins/admin/verified/{verified}', array('middleware' => 'App\Http\Middleware\Role','uses' => 'AdminController@user_by_verified'));
   Route::get('/admins/admin/create', array('middleware' => 'App\Http\Middleware\Role','uses' => 'AdminController@create'));
   Route::post('/admins/admin', array('middleware' => 'App\Http\Middleware\Role','uses' => 'AdminController@store'));
   Route::get('/admins/admin/{id}', array('middleware' => 'App\Http\Middleware\Role','uses' => 'AdminController@show'));
   Route::get('/admins/admin/verify/{id}', array('middleware' => 'App\Http\Middleware\Role','uses' => 'AdminController@verify'));
   Route::get('/admins/admin/{id}/edit', array('middleware' => 'App\Http\Middleware\Role','uses' => 'AdminController@edit'));
   Route::put('/admins/admin/{id}', array('middleware' => 'App\Http\Middleware\Role','uses' => 'AdminController@update'));
   Route::get('/admins/admin/{id}/delete', array('uses' => 'AdminController@destroy'));
   Route::get('/admins/admin/{id}/block/', array('uses' => 'AdminController@block'));
   Route::get('/admins/admin/{id}/unblock/', array('uses' => 'AdminController@unblock'));
   
   

   
   
   
   
  //posts 
   Route::get('posts_list', array('middleware' => 'App\Http\Middleware\Role','uses' => 'PostsController@index'))->name('posts_list');
   Route::get('/posts_list/create/', array('middleware' => 'App\Http\Middleware\Role','uses' => 'PostsController@create'));
   Route::get('/posts_list/{id}/edit', array('middleware' => 'App\Http\Middleware\Role','uses' => 'PostsController@edit_post'))->name('/posts_list/edit');
   Route::post('/posts_list/update', array('middleware' => 'App\Http\Middleware\Role','uses' => 'PostsController@update_post'))->name('/posts_list/update');
   Route::get('/posts_list/{id}/delete', array('middleware' => 'App\Http\Middleware\Role','uses' => 'PostsController@destroy'));
   
   Route::get('/posts_list/images/{id}/delete', array('middleware' => 'App\Http\Middleware\Role','uses' => 'PostsController@destroy_post_images'));
   Route::get('/posts_list/comments/{id}/delete', array('middleware' => 'App\Http\Middleware\Role','uses' => 'PostsController@destroy_post_comments'));
   
   
   
   
    Route::resource('pop_up_content', 'TermsConditionsController');
    Route::get('/admin/pop_up_content', array('middleware' => 'App\Http\Middleware\Role','uses' => 'AppPopUpController@index'));
	Route::post('/admin/app_pop_up_update', array('middleware' => 'App\Http\Middleware\Role','uses' => 'AppPopUpController@pop_up_content_update'));

    //purchase points
    Route::resource('terms_conditions', 'TermsConditionsController');
    Route::get('/admin/terms_conditions', array('middleware' => 'App\Http\Middleware\Role','uses' => 'TermsConditionsController@index'));
	Route::post('/admin/terms_conditions_update', array('middleware' => 'App\Http\Middleware\Role','uses' => 'TermsConditionsController@terms_conditions_update'));
	
	
	
	
    //Membership Card Types
    Route::resource('membership_card_types', 'MemberShipCardTypesController');
    Route::get('/admin/membership_card_types', array('middleware' => 'App\Http\Middleware\Role','uses' => 'MemberShipCardTypesController@index'));//show the list of records
	
	Route::get('/admin/membership_card_types/create', array('middleware' => 'App\Http\Middleware\Role','uses' => 'MemberShipCardTypesController@create')); //show add form
    Route::post('/admin/membership_card_types/store', array('middleware' => 'App\Http\Middleware\Role','uses' => 'MemberShipCardTypesController@store'))->name("membership_card_types_store"); // add data to database
    
	Route::get('/admin/membership_card_types/{id}/delete', array('uses' => 'MemberShipCardTypesController@destroy'));//delete
	
	Route::get('/admin/membership_card_types/{id}/edit', array('middleware' => 'App\Http\Middleware\Role','uses' => 'MemberShipCardTypesController@edit'));//show edit form
    Route::post('/admin/membership_card_types/{id}', array('middleware' => 'App\Http\Middleware\Role','uses' => 'MemberShipCardTypesController@update'))->name("membership_card_types_update");;//update to database
	
	
	
	
	Route::post('/admin/terms_conditions_update', array('middleware' => 'App\Http\Middleware\Role','uses' => 'TermsConditionsController@terms_conditions_update'));
	
	//App Data
	    Route::resource('app_data', 'AppDataController');
    Route::get('/admin/app_data', array('middleware' => 'App\Http\Middleware\Role','uses' => 'AppDataController@index'));
	Route::post('/admin/app_data_update', array('middleware' => 'App\Http\Middleware\Role','uses' => 'AppDataController@app_data_update'));
 
	//Auctions
    Route::resource('country', 'CountryController');
    Route::get('/admin/country', array('middleware' => 'App\Http\Middleware\Role','uses' => 'CountryController@index'));
    Route::get('/admin/country/create', array('middleware' => 'App\Http\Middleware\Role','uses' => 'CountryController@create'));
    Route::post('/admin/country', array('middleware' => 'App\Http\Middleware\Role','uses' => 'CountryController@store'));
    Route::get('/admin/country/{id}', array('middleware' => 'App\Http\Middleware\Role','uses' => 'CountryController@show'));
    Route::get('/admin/country/{id}/edit', array('middleware' => 'App\Http\Middleware\Role','uses' => 'CountryController@edit'));
    Route::put('/admin/country/{id}', array('middleware' => 'App\Http\Middleware\Role','uses' => 'CountryController@update'));
    Route::get('/admin/country/{id}/delete', array('uses' => 'CountryController@destroy'));
	
	
	//mail templates
Route::get('/email_otp', function () {
    return view('emails.otp');
});
	
	
	
	// New admin Services 
	
	   Route::get('/admin/post_orders/', array('middleware' => 'App\Http\Middleware\Role','uses' => 'PostsController@post_orders'));
	   
	   Route::get('/admin/trades/', array('middleware' => 'App\Http\Middleware\Role','uses' => 'PostsController@trades'));
	   
	   Route::get('/admin/trades_orders/', array('middleware' => 'App\Http\Middleware\Role','uses' => 'PostsController@trades_orders'));
	   
	   
	   //users orders
        Route::get('/admin/users_orders/{user_id}', array('middleware' => 'App\Http\Middleware\Role','uses' => 'UsersController@users_orders'));
		Route::get('/admin/users_trades/{user_id}', array('middleware' => 'App\Http\Middleware\Role','uses' => 'UsersController@users_trades'));
		Route::get('/admin/users_transactions/{user_id}', array('middleware' => 'App\Http\Middleware\Role','uses' => 'UsersController@users_transactions'));
		Route::get('/admin/users_posts/{user_id}', array('middleware' => 'App\Http\Middleware\Role','uses' => 'UsersController@users_posts'));
		Route::get('/admin/users_trades_posts/{user_id}', array('middleware' => 'App\Http\Middleware\Role','uses' => 'UsersController@users_trades_posts'));
		Route::get('/admin/trades_posts_details/{trades_posts_id}/{user_id}', array('middleware' => 'App\Http\Middleware\Role','uses' => 'UsersController@trades_posts_details'));
	 
 
 
   
	
	
});














//================================== FOOD WEBSITE Routes Starts==============================================================

Route::get('/', function () {
    return view('web-food.index');
});

Route::get('/web-login', function () {
    return view('web-food.login');
});
Route::get('/faq', function () {
    return view('web-food.faq');
});
Route::get('/web-login', function () {
    return view('web-food.login');
});
Route::get('/register', function () {
    return view('web-food.register');
});
Route::get('/my-orders', function () {
    return view('web-food.my-orders');
});
Route::get('/addresses', function () {
    return view('web-food.address');
});
Route::get('/reviews', function () {
    return view('web-food.reviews');
});
Route::get('/favourites', function () {
    return view('web-food.favourites');
});

Route::get('/account/{id}', function () {
    return view('web-food.account-settings');
});

Route::get('/stores', function () {
    return view('web-food.stores');
});
Route::get('/order-items', function () {
    return view('web-food.menu-items');
});
Route::get('/checkout', function () {
    return view('web-food.checkout');
});
Route::get('/order-items1', function () {
    return view('web-food.menu-items1');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
