<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoyaltyPoints extends Model
{
	 protected $fillable = [
        'user_id','type','points','expiry_date','source' ];
       
	  public function user()
    {
    return $this->belongsTo('App\User', 'notification_by_user_id');
    }

protected $casts = [ "points"=>"int" ];
	
 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }
	


public function getDisplayStringAttribute($value) 
{       
       $type = $this->type;
       $user_id = $this->user_id;
       $string = '';

       if($type =='order_spent')
       {
           $string = abs($this->points).' points are spent on order #'.$this->source; 
           $type_title = 'Spent on Order';  
       }

        if($type =='order_earned')
       {
           $string = abs($this->points).' points are credited to your account for order #'.$this->source; 
           $type_title = 'Earn on Order';  
       }

       if($type =='transfer_earned')
       {
           @$customer_name = @\App\User::where('user_id' , $this->source )->first(['first_name'])->first_name;
           $string = abs($this->points).' points are transfer to you by '.@$customer_name; 
           $type_title = 'Transfer Earned';  
       }

       if($type =='transfer_spent')
       {
           @$customer_name = @\App\User::where('user_id' , $this->source )->first(['first_name'])->first_name;
           $string = 'you transferred '.abs($this->points).' points to '.@$customer_name; 
           $type_title = 'Transfer Spent';   
       }





        if($type =='referral_from')
       {
           	@$customer_name = @\App\User::where('user_id' , $this->source )->first(['first_name'])->first_name;
            $string = abs($this->points).' points are credited to your account as '.@$customer_name.' signup with your referral code'; 
            $type_title = 'Referral';  
       }



        if($type =='order_reclaimed')
       {
           	@$customer_name = @\App\User::where('user_id' , $this->source )->first(['first_name'])->first_name;
            $string = abs($this->points).' points are credited to your account as '.@$customer_name.' signup with your referral code'; 
            $type_title = 'Referral';  
       }

 

            if($type =='referral_registered')
       {
         	@$customer_name = @\App\User::where('user_id' , $this->source )->first(['first_name'])->first_name;
           $string = abs($this->points).' points are credited to your account you signup with referral code'; 
            $type_title = 'Referral';  
       }

       $main = array();

       $d['string'] = @$string;
       $d['title'] = @$type_title;

       $main[] = $d;
       return $main;


 }





 
	

			    public function getCreatedAtAttribute($value) {
         $v = \Carbon\Carbon::parse($value)->diffforhumans();
		 
		 
	 
        $v = str_replace([' seconds', ' second'], 'sec', $v);
        $v = str_replace([' minutes', ' minute'], 'min', $v);
        $v = str_replace([' hours', ' hour'], 'h', $v);
        $v = str_replace([' months', ' month'], 'm', $v);
		$v = str_replace([' days', ' month' , ' day'], 'd', $v);
		$v = str_replace([' weeks', ' week'], 'w', $v);
		$v =  str_replace([' ago', ' ago'], '', $v);

        if(preg_match('(years|year)', $v)){
            $v = $v->toFormattedDateString();
        }

        return $v;
		
		
    }


	
}
