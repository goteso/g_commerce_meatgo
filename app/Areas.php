<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Areas extends Model
{
        protected $fillable = [ 'area_id', 'title' , 'latitude' , 'longitude' , 'pincode' , 'parent_id' , 'location_id'];
		protected $table = 'areas';
		
		protected $casts = [ 'area_id'=>'int'  ];

        

        
 public function getCreatedAtFormattedAttribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
    }

 public function getCreatedAtFormatted2Attribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->format('M d, Y');
    }
	

      public function getParentLocationTitleAttribute($value) {
         //return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
        @$location_id = @$this->location_id;
 
        if($location_id != 'null' && $location_id != '' &&  $location_id != '0' &&  $location_id != 0)
        {
            $title = @\App\Locations::where('location_id',$location_id)->first(['title'])->title;
        }
        else
        {
            $title ='';
        }
        return $title;
    }


      public function getParentAreaTitleAttribute($value) {
         //return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
        @$parent_id = @$this->parent_id;
 
        if($parent_id != 'null' && $parent_id != '' &&  $parent_id != '0' &&  $parent_id != 0)
        {
            $title = @\App\Areas::where('area_id',$parent_id)->first(['title'])->title;
        }
        else
        {
            $title ='';
        }
        return $title;
    }





 
 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }
	
}