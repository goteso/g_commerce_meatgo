<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderTransactionDiscount extends Model
{
        protected $fillable = [ 'order_id' , 'user_id', 'coupon_id' , 'coupon_code' ,'discount_amount' , 'type' , 'loyalty_points_id' ];
		protected $table = 'order_transaction_discount';
		
 
	
	 public function getCreatedAtFormattedAttribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
    }
	
	
	
 
 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }
	
	
}