<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
        protected $fillable = [ 'order_id' , 'item_id', 'item_title' , 'order_item_quantity' , 'order_item_unit' , 'item_price' , 'order_item_discount' ];
		protected $table = 'order_item';
		
 
	
	 public function getCreatedAtFormattedAttribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
    }






         public function getSingleQuantityItemPriceAttribute($value) {
         $single_quantity_item_price =  @\App\Items::where('item_id',$this->item_id)->first(['item_price'])->item_price;
         return round($single_quantity_item_price , 2 );
    }


    


	             public function getSingleQuantityItemPriceIncludingVariantsAttribute($value) {
	                 
	                 $single_quantity_item_price =  @\App\Items::where('item_id',$this->item_id)->first(['item_price'])->item_price;

               $order_item_id = $this->order_item_id;
                $order_item_variants = @\App\OrderItemVariant::where('order_item_id',$order_item_id)->get();
             
             $d = 0;
             
             foreach( $order_item_variants as $order_item_variant)
             {
                  $item_variant_price = @\App\ItemVariantValue::where('item_variant_value_id' , $order_item_variant['item_variant_id'])->first(['item_variant_price'])->item_variant_price;
                  $item_variant_price_difference = @\App\ItemVariantValue::where('item_variant_value_id' , $order_item_variant['item_variant_id'])->first(['item_variant_price_difference'])->item_variant_price_difference;
                  
                  if($item_variant_price > 0)
                  {
                    $d = $item_variant_price;
                  }
                  else
                  {
                       $d =  $d + $item_variant_price_difference;
                  }
             }
         $final = $single_quantity_item_price + $d;
         return $final;
    }

	

 
     public function getItemUnitAttribute($value) {
        
    
                $item_meta_type_id = @\App\ItemMetaType::where('identifier','unit')->first(['item_meta_type_id'])->item_meta_type_id;
              $value = @\App\ItemMetaValue::where('item_meta_type_id',$item_meta_type_id)->where('item_id',$this->item_id)->first(['value'])->value;
              if($value == '' || $value == null || $value == ' ')
              {
                return 'item';
              }

              if($value == '' || $value == '')
              {
                return 'item';
              }
              return  $value;
          
 


 
    }
	
	
 
 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }
	
	
}