<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemVariantType extends Model
{
        protected $fillable = [ 'item_variant_type_id' , 'item_variant_type_title' , 'price_status' , 'price_difference_status' ,'photo_status' , 'variant_stock_count_status' , 'minimum_selection_needed' , 'maximum_selection_needed'];
		protected $table = 'item_variant_type';
		
		
		 
protected $casts = [ 'item_variant_type_id'=>'int'   ];




		
 public function getCreatedAtFormattedAttribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
    }

 public function getCreatedAtFormatted2Attribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->format('M d, Y');
    }
	
	
 
 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }
	
}