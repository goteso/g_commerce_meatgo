<?php

namespace App;

 
use Illuminate\Database\Eloquent\Model;
class SettingOrderStatus extends Model
{
 

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title','color','label_colors','identifier','type','customer_notify'
    ];
	protected $table = 'setting_order_status';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
 
/**
     public function getTotalOrdersAmountAttribute($value) {
         return  round(\App\Order::where('order_status',$this->identifier)->sum('total') , 2);
    }
   **/ 
    
    
 
 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }
 
 
	
}
