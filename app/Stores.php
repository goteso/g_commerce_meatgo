<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stores extends Model
{
        protected $fillable = [ 'store_title' , 'store_photo','store_thumb_photo','sort_index', 'store_rating' , 'latitude' , 'longitude' , 'vendor_id' , 'address' ,'manager_id' , 'status','featured','commission','busy_end_time','store_tax','store_phone'];
		protected $table = 'stores';
		
 

 protected $casts = [ 'store_id' => 'int' , 'vendor_id' => 'int' , 'manager_id' => 'float'  ];


	
	 public function getCreatedAtFormattedAttribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
    }



      public function getManagerNameAttribute($value) {
                 $v =@\App\User::where('user_id',$this->manager_id)->first(['first_name'])->first_name;
      if($v == null ||  $v == '' || $v == ' ')
      {
      	$v = '';
      }
      return $v;
    }



         public function getFavouriteAttribute($value) {
         //return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
  
      if(isset($_GET['user_id']) == null ||   $_GET['user_id'] == '' || $_GET['user_id']  == ' ')
      {
      return 0;
      }
      
      $v =  @\App\FavouriteStore::where('store_id',$this->store_id)->where('user_id',$_GET['user_id'])->count();

      if($v > 0)
      {
          return 1;
      }
      else
      {
        return 0;
      }
    }








         public function getVendorNameAttribute($value) {
         //return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
      $v = @\App\User::where('user_id',$this->vendor_id)->first(['first_name'])->first_name;
      if($v == null ||   $v== '' || $v == ' ')
      {
      	$v = '';
      }
      return $v;
    }




         public function getStoreMetaAttribute($value) {
         //return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
      return  @\App\StoreMetaValue::where('store_id',$this->store_id )->get(['store_meta_value_id','store_meta_type_id','value']);
    }


             public function getBusyStatusAttribute($value) {
       
             $now = @\Carbon\Carbon::now();
             $now =\Carbon\Carbon::parse($now)->format('Y-m-d');
             $count = @\App\Store::where('store_id',$this->store_id)->where('busy_end_time','<>','')->where('busy_end_time','<>',null)->where('busy_end_time','>',$now)->count();
             if($count > 0)
             {
                 return 1;
             }
             else { return 0;}
    }

                 public function getBusyStatusTitleAttribute($value) {
  $now = @\Carbon\Carbon::now();
             $now =\Carbon\Carbon::parse($now)->format('Y-m-d');
             $count = @\App\Store::where('store_id',$this->store_id)->where('busy_end_time','<>','')->where('busy_end_time','<>',null)->where('busy_end_time','>',$now)->count();
             if($count > 0)
             {
                 return 'Busy';
             }
             else { return 'Open';}
    }
 



 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }

	
	
}