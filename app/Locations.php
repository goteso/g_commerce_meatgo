<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Locations extends Model
{
        protected $fillable = [ 'location_id', 'parent_id'];
		protected $table = 'locations';
		
				protected $casts = [ 'location_id'=>'int'  , 'parent_id'=>'int'   ];
				
				
 public function getCreatedAtFormattedAttribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
    }

 public function getCreatedAtFormatted2Attribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->format('M d, Y');
    }
	
	
	
  public function getParentLocationTitleAttribute($value) {
         //return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
        @$parent_id = @$this->parent_id;
 
        if($parent_id != 'null' && $parent_id != '' &&  $parent_id != '0' &&  $parent_id != 0)
        {
            $title = @\App\Locations::where('location_id',$parent_id)->first(['title'])->title;
        }
        else
        {
            $title ='';
        }
        return $title;
    }


 

 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }
	
}