<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
        protected $fillable = [ 'order_status' , 'customer_id', 'store_id' , 'total','items_sub_total','delivery_fee' , 'driver_commission' , 'store_commission' , 'discount_type' , 'cancel_reason_admin' , 'cancel_reason_customer' ];
		protected $table = 'orders';
		
 

    
 
protected $casts = [ 'order_id'=>'int' , 'store_id' => 'float', 'items_sub_total' => 'float' , 'driver_commission' => 'float' , 'store_commission' => 'float' , 'delivery_fee' => 'float'  ];



     public function getCustomerDetailsAttribute($value) {
         if(!isset($_GET['type']))
      {
         return  @\App\User::where('user_id',$this->customer_id)->get(['user_id','first_name','last_name','email','phone','created_at']);
       }
       else
       {
        return [];
       }
    }


     public function getStoreDetailsAttribute($value) {
        if(!isset($_GET['type']))
      {
         return  @\App\Stores::where('store_id',$this->store_id)->get(['store_id','store_title','store_photo','store_rating','created_at','address','store_phone']);
       }
       else
       {
        return [];
       }
    }

 

 public function getStatusDetailsAttribute($value) {
      if(!isset($_GET['type']))
      {
           return  @\App\SettingOrderStatus::where('identifier',$this->order_status)->get(['title','color','label_colors','identifier']);
     }
     else { return [];}
    }



public function getOrderAddressArrayAttribute($value) {
          if(isset($_GET['include_address']) && $_GET['include_address'] != null && $_GET['include_address'] != '')
                    { $include_address = $_GET['include_address']; 
                $setting_order_meta_type = @\App\SettingOrderMetaType::where('input_type','address')->get();
                $address_array = array();
          
                foreach($setting_order_meta_type as $mt)
                {
                    $order_meta_value_linked_id = @\App\OrderMetaValue::where( 'order_id',$this->order_id )->where('setting_order_meta_type_id',$mt->setting_order_meta_type_id)->first(['order_meta_value_linked_id'])->order_meta_value_linked_id;
                    $address_data['type'] = @\App\SettingOrderMetaType::where('setting_order_meta_type_id',$mt->setting_order_meta_type_id)->first(['identifier'])->identifier;
                    $address_data['data']= @\App\Address::where('address_id',$order_meta_value_linked_id)->get();
                    $address[] = $address_data;
                    

                }
return $address;
            }
else 
{
    return [];
}
                  
    }



//for reports 
 
    public function getCustomerNameAttribute($value) 
    {
 
          if(isset($_GET['type']) && $_GET['type'] != null && $_GET['type'] != '' && $_GET['type'] == 'report')
          {
                 $customer_name  = @\App\User::where('user_id',$this->customer_id)->first(['first_name'])->first_name." ".@\App\User::where('user_id',$this->customer_id)->first(['last_name'])->last_name;
                 return $customer_name;
          }
          else 
          {
            return '';
          }
                  
    }

        public function getStoreTitleAttribute($value) 
    {
 
          if(isset($_GET['type']) && $_GET['type'] != null && $_GET['type'] != '' && $_GET['type'] == 'report')
          {
                 $att  = @\App\Store::where('store_id',$this->store_id)->first(['store_title'])->store_title;
                 return $att;
          }
          else 
          {
            return '';
          }
                  
    }
  














	
	 public function getCreatedAtFormattedAttribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
    }
	
	
	
 
 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }
	
	
}