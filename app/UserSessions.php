<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSessions extends Model
{
        protected $fillable = [ 'session_id','user_id', 'device_type' , 'device_id' , 'screen_size' , 'device_os' , 'ip_address' , 'session_status' , 'location_name' , 'latitude' , 'longitude' , 'browser' , 'timezone' , 'notification_token'];
		protected $table = 'user_sessions';
		
 
	
	 public function getCreatedAtFormattedAttribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
    }
	
	
	
 
 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }
	
	
}