<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationsSms extends Model
{
  protected $fillable = [ 'notification_type' , 'title' , 'sub_title' , 'user_type' ];
  protected $table = 'notifications_sms';
}
