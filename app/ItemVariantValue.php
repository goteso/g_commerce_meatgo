<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemVariantValue extends Model
{
        protected $fillable = [ 'item_variant_value_id', 'item_variant_type_id', 'item_variant_value_title', 'item_id', 'item_variant_price', 'item_variant_price_difference','item_variant_photo'
		,'item_variant_stock_count'];
		protected $table = 'item_variant_value';
		
		
		protected $casts = [ 'item_variant_value_id'=>'int'  ];


		
 public function getCreatedAtFormattedAttribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
    }

 public function getCreatedAtFormatted2Attribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->format('M d, Y');
    }




 public function getSelectionTypeAttribute($value) {
         return  @\App\ItemVariantType::where('item_variant_type_id',$this->item_variant_type_id )->first(['selection_type'])->selection_type;
    }



    public function getItemTitleAttribute($value) {
         //return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
        @$item_id = @$this->item_id;
 
        if($item_id != 'null' && $item_id != '' &&  $item_id != '0' &&  $item_id != 0)
        {
            $item_title = @\App\Items::where('item_id',$item_id)->first(['item_title'])->item_title;
        }
        else
        {
            $item_title ='';
        }
        return $item_title;
    }





    public function getItemVariantTypeTitleAttribute($value) {
         //return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
        @$item_variant_type_id = @$this->item_variant_type_id;
 
        if($item_variant_type_id != 'null' && $item_variant_type_id != '' &&  $item_variant_type_id != '0' &&  $item_variant_type_id != 0)
        {
            $item_variant_type_title = @\App\ItemVariantType::where('item_variant_type_id',$item_variant_type_id)->first(['item_variant_type_title'])->item_variant_type_title;
        }
        else
        {
            $item_variant_type_title ='';
        }
        return $item_variant_type_title;
    }


	
 
 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }
	
}