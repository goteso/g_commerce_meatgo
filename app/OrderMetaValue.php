<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderMetaValue extends Model
{
        protected $fillable = [ 'order_id' , 'setting_order_meta_type_id', 'setting_order_meta_type_title' , 'order_meta_value_text' , 'order_meta_value_linked_id' ];
		protected $table = 'order_meta_value';
		
 
	
	 public function getCreatedAtFormattedAttribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
    }


         public function getIdentifierAttribute($value) {
         return  @\App\SettingOrderMetaType::where('setting_order_meta_type_id',$this->setting_order_meta_type_id)->first(['identifier'])->identifier;
    }
	
	
	
 
 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }
	
	
}