<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderCancelReasons extends Model
{
        protected $fillable = [ 'id', 'title'];
		protected $table = 'order_cancel_reasons';
		
     public function getCreatedAtFormattedAttribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
    }

 
 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }
	
}