<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Items extends Model
{
        protected $fillable = [ 'item_id', 'item_title', 'item_price', 'item_discount', 'item_discount_expiry_date', 'item_stock_count','item_photo'
		,'item_thumb_photo','item_tags','item_categories','vendor_id','store_id','item_active_status'];
		protected $table = 'items';
		
		
 public function getCreatedAtFormattedAttribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
    }

 public function getCreatedAtFormatted2Attribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->format('M d, Y');
    }
    





 public function getStoreTitleAttribute($value) {
         $store_title =  @\App\Store::where('store_id',$this->store_id)->first(['store_title'])->store_title;
         if($store_title == '' || $store_title == null || $store_title == ' ')
         {
           return '';
         }
         return $store_title;
    }
    
     public function getVendorNameAttribute($value) {
               $vendor_name = @\App\User::where('user_id',$this->vendor_id)->first(['first_name'])->first_name." ".@\App\User::where('user_id',$this->vendor_id)->first(['last_name'])->last_name;
       if($vendor_name == '' || $vendor_name == null || $vendor_name == ' ')
         {
           return '';
         }
         return $vendor_name;
    }
    


    
 
protected $casts = [ 'item_id'=>'int','item_discount' => 'float'  ,'status' => 'int' , 'store_id' => 'float', 'vendor_id' => 'float'  ];




 public function getVariantExistAttribute($value) {
        $variants_count = @\App\ItemVariantValue::where('item_id' , $this->item_id)->count();
        if($variants_count > 0)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }






     public function getItemUnitAttribute($value) {
        
 
              $item_meta_type_id = @\App\ItemMetaType:: where('identifier','unit')->first(['item_meta_type_id'])->item_meta_type_id;
              $value = @\App\ItemMetaValue::where('item_meta_type_id',$item_meta_type_id)->where('item_id',$this->item_id)->first(['value'])->value;
              if($value == '' || $value == null || $value == ' ')
              {
                return 'item';
              }

              if($value == '' || $value == '')
              {
                return 'item';
              }
              return  $value;
  }







     public function getItemTagsFormattedAttribute($value) {


if($this->item_tags != '' && $this->item_tags != null)
{
         
         $item_tags_array = explode(',',$this->item_tags);
         $final_array = array();
         for($t=0;$t<sizeof($item_tags_array);$t++)
         {
            $d['id'] = $item_tags_array[$t]; 
            $d['value'] = @\App\SettingTags::where('tag_id',$item_tags_array[$t])->first(['tag_title'])->tag_title;
            $final_array[] = $d;
         }

         if(sizeof($item_tags_array) < 1) { return [];}

         return $final_array;
     }else{ return [];}
   



             }
         


	
	
	
 
 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }
	
}