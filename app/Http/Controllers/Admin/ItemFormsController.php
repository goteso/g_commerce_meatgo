<?php
namespace App\Http\Controllers\Admin; //admin add
use App;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; // using controller class
use Auth;
use Session;
use DB;
use Validator;
use App\User;
use Carbon\Carbon;
use App\Otp;
use App\UserSocialLinks;
use App\UserLikesDislikes;
use App\UserRatings;
use App\Country;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Traits\one_signal; // <-- you'll need this line...
use App\Traits\bitcoin_price;
use App\Traits\trait_functions;
use Illuminate\Support\Arr;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Hash;
use Mail;
use File;
 
 


class ItemFormsController extends Controller 
{
	
use one_signal; // <-- ...and also this line.
use bitcoin_price; // <-- ...and also this line.
use trait_functions; // <-- ...and also this line.
 
   
   


 // Route-14 ============================================================== To get a form for adding Item to database =========================================> 
 // Route::get('/v1/form/item', array('uses' => 'ItemFormsController@get_add_form'));//Route-14
   public function get_add_form()
   {
 

     //user types filters ==========================================
    $auth_user_id = $this->get_auth_user_id();
    $user_type = $this->get_auth_user_type();
 



      $main_array = array();
	  
	  // 1. Basic Informations
	  $data_array1['title'] = 'Basic Information';
	  $data_array1['type'] = 'basic';
 	 
 	  $fields_array = array();

 	  $fields_keys0["title"] = "Image";
	  $fields_keys0["type"] = 'file';
	  $fields_keys0["identifier"] = 'item_photo';
	  $fields_keys0["value"] = '';
	  $fields_keys0["limit"] = '1';
      $fields_array[] = $fields_keys0;




	  $fields_keys1["title"] = "Item Name";
	  $fields_keys1["type"] = 'text';
	  $fields_keys1["identifier"] = 'item_title';
	  $fields_keys1["value"] = '';
      $fields_array[] = $fields_keys1;
	  
 	  $fields_keys2["title"] = "Item Categories";
	  $fields_keys2["type"] = 'tag';
      $fields_keys2["options"] = $this->get_categories_options();
	  $fields_keys2["identifier"] = 'item_categories';
      $fields_keys2["value"] = [];
	  $fields_array[] = $fields_keys2;






 
	  $fields_keys3["title"] = "Price";
	  $fields_keys3["type"] = 'float';
	  $fields_keys3["identifier"] = 'item_price';
	  $fields_keys3["symbol"] = @\App\Setting::where('key_title','currency_symbol')->first(['key_value'])->key_value;
	  $fields_keys3["value"] = '';
      $fields_array[] = $fields_keys3;

 

$vendor_plugins = @\App\Setting::where('key_title','vendors')->first(['key_value'])->key_value;
if($vendor_plugins == 1)
{
if($user_type == '1' || $user_type == '3' || $user_type == '4')
{
	    $type ='api';
	    $value = '';

	    if($user_type == '3')
		{
		  $value = $auth_user_id;
		  $type = 'hidden'; 
		}

		 if($user_type == '4')
		{
         $value = @\App\Store::where('manager_id',$auth_user_id)->first(['vendor_id'])->vendor_id;
         $type = 'hidden'; 
		}

      $fields_keys5["title"] = "Vendor";
	  $fields_keys5["type"] = $type;
      $fields_keys5["options"] = '';
	  $fields_keys5["identifier"] = 'vendor_id';
      $fields_keys5["value"] = $value;
	  $fields_array[] = $fields_keys5;
}

}





if($user_type == '1' || $user_type == '3' || $user_type == '4')
{
	    $type ='api';
	    $value = '';
	    if($user_type == '4')
		{ 
			$value = @\App\Store::where('manager_id',$auth_user_id)->first(['store_id'])->store_id; 
		    $type = 'hidden'; 
		}


	  $fields_keys6["title"] = "Store";
	  $fields_keys6["type"] =$type;
      $fields_keys6["options"] = '';
	  $fields_keys6["identifier"] = 'store_id';
      $fields_keys6["value"] =$value;
	  $fields_array[] = $fields_keys6;
	}

	  


      
      $data_array1['fields'] = $fields_array;
	  $main_array[] =$data_array1;

	  
	  // 2. Meta Informations ==============
	  $data_array2['title'] = 'Meta Informations';
	  $data_array2['type'] = 'meta';

	  $item_meta_types = @\App\ItemMetaType::get();
      $meta_fields_array = array();
	  foreach($item_meta_types as $meta_type)
	  {
	 	 $meta_data["title"] = $meta_type->item_meta_type_title;
	 	 $meta_data["type"] =  $meta_type->type;
	 	 $meta_data["identifier"] =  $meta_type->identifier;
	 	 $meta_data["value"] = '';
	 	 $meta_data["limit"] = $meta_type->limit;
      	 $meta_fields_array[] = $meta_data;
	  }
 	 
 	  $data_array2['fields'] = $meta_fields_array;
	  $main_array[] =$data_array2;

      $result = $main_array;

      return $result;
       //return view('admin.items.item_add', compact('result'));
  }
  

   //get_item_meta_value($item_id , $item_meta_type_id)





 // Route-15 ============================================================== To get a form for editing Item with all value from database =========================================> 
  //Route::get('/v1/form/item/{id}', array('uses' => 'ItemFormsController@get_edit_form'));//Route-15
   public function get_edit_form($id)
   {


 //user types filters ==========================================
    $auth_user_id = $this->get_auth_user_id();
    $user_type = $this->get_auth_user_type();

    
   	  $main_array = array();
	  
	               //check existance of item with ID in items table
					$exist = $this->item_exist($id);	
                    if($exist == 0 or $exist == '0')
                    {
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Item with this ID does not exist';
                          $data['data']      =   [];
                          return $data;						  
					}

	  $item_details = @\App\Items::where('item_id',@$id)->first();
	  // 1. Basic Informations
	  $data_array1['title'] = 'Basic Information';
	  $data_array1['type'] = 'basic';
 

  $fields_array = array();

	  $fields_keys0["title"] = "Image";
	  $fields_keys0["type"] = 'file';
	  $fields_keys0["identifier"] = 'item_photo';
	  $fields_keys0["value"] = $item_details->item_photo;
	  $fields_keys0["limit"] = '1';
      $fields_array[] = $fields_keys0;


 	 
 	
	  $fields_keys1["title"] = "Item Name";
	  $fields_keys1["type"] = 'text';
	  $fields_keys1["identifier"] = 'item_title';
	  $fields_keys1["value"] = $item_details->item_title;
      $fields_array[] = $fields_keys1;
	  
 	  $fields_keys2["title"] = "Item Categories";
	  $fields_keys2["type"] = 'tag';
	 
	  $fields_keys2["identifier"] = 'item_categories';
	  $fields_keys2["options"] = $this->get_categories_options();

	  $value_array = explode(",",$item_details->item_categories);
	  $categories_values_array = array();
      for($ci=0;$ci<sizeof($value_array);$ci++)
      {
      	$c['label'] = @\App\SettingCategories::where('category_id',$value_array[$ci])->first(['category_title'])->category_title;
      	$c['value'] = $value_array[$ci];
        $categories_values_array[] = $c;
      }
 
      $fields_keys2["value"]  = $categories_values_array;
	  $fields_array[] = $fields_keys2;
 
 
	  $fields_keys3["title"] = "Price";
	  $fields_keys3["type"] = 'float';
	  $fields_keys3["identifier"] = 'item_price';
	  $fields_keys3["symbol"] =  @\App\Setting::where('key_title','currency_symbol')->first(['key_value'])->key_value;
	  $fields_keys3["value"] = $item_details->item_price;;
      $fields_array[] = $fields_keys3;

       








$vendor_plugins = @\App\Setting::where('key_title','vendors')->first(['key_value'])->key_value;
if($vendor_plugins == 1)
{
	  if($user_type == '1' || $user_type == '3' || $user_type == '4')
{
	    $type ='api';
	    $value = '';

	    if($user_type == '3')
		{
		  $value = $auth_user_id;
		  $type = 'hidden'; 
		}

		 if($user_type == '4')
		{
         $value = @\App\Store::where('manager_id',$auth_user_id)->first(['vendor_id'])->vendor_id;
         $type = 'hidden'; 
		}

      $fields_keys5["title"] = "Vendor";
	  $fields_keys5["type"] = $type;
      $fields_keys5["options"] = '';
	  $fields_keys5["identifier"] = 'vendor_id';
	  	
	  $display_value_details = @\App\User::where('user_id' , @$item_details->vendor_id )->get(['first_name']);
      $fields_keys5["display_value"] = @$display_value_details[0]['first_name'];
 
      $fields_keys5["value"] = @$item_details->vendor_id;
	  $fields_array[] = $fields_keys5;

}
}







if($user_type == '1' || $user_type == '3' || $user_type == '4')
{
	    $type ='api';
	    $value = '';
	    if($user_type == '4')
		{ 
			$value = @\App\Store::where('manager_id',$auth_user_id)->first(['store_id'])->store_id; 
		    $type = 'hidden'; 
		}


	  $fields_keys6["title"] = "Store";
	  $fields_keys6["type"] =$type;
      $fields_keys6["options"] = '';
	  $fields_keys6["identifier"] = 'store_id';
      $fields_keys6["value"] = @$item_details->store_id;
        $display_value_details = @\App\Store::where('store_id' , @$item_details->store_id )->get(['store_title']);
      $fields_keys6["display_value"] = @$display_value_details[0]['store_title'];
	  $fields_array[] = $fields_keys6;
}




	  


      $data_array1['fields'] = $fields_array;
	  $main_array[] =$data_array1;

	  // 2. Meta Informations ==========================================================================
	  $data_array2['title'] = 'Meta Informations';
	  $data_array2['type'] = 'meta';

	  $item_meta_types = @\App\ItemMetaType::get();
      $meta_fields_array = array();
	  foreach($item_meta_types as $meta_type)
	  {
	 	 $meta_data["title"] = $meta_type->item_meta_type_title;
	 	 $meta_data["type"] =  $meta_type->type;
	 	 $meta_data["identifier"] =  $meta_type->identifier;
	 	 $meta_data["value"] = $this->get_item_meta_value(@$id , @$meta_type->item_meta_type_id);
	 	 $meta_data["limit"] = $meta_type->limit;
      	 $meta_fields_array[] = $meta_data;


	  }
 	 
 	  $data_array2['fields'] = $meta_fields_array;
 	  $main_array[] =$data_array2;



	  // 3. Items Images ==========================================================================
	  $data_array2['title'] = 'Items Images';
	  $data_array2['type'] = 'items_images';
 	  $data_array2['fields'] = @\App\ItemsImages::where('item_id',$id)->get();
 	  $main_array[] =$data_array2;


	 /**
	  $data_array3['title'] = 'Variants';
	  $data_array3['type'] = 'variants';
	  $tabs = @\App\ItemvariantType::get(['item_variant_type_id','item_variant_type_title']);

 

      foreach($tabs as $tab)
      {
      	 $tab['item_variant_value']= @\App\ItemVariantValue::where('item_id',$id)->where('item_variant_type_id',$tab->item_variant_type_id)->get();
      }
	  //$item_variant_value = @\App\ItemVariantValue::where('item_id',$id)->get();

     // $meta_fields_array = array();
 	 
 	  $data_array3['fields'] = $tabs;
	  $main_array[] =$data_array3;


/**old block
	  
	  $data_array3['title'] = 'Variants';
	  $data_array3['type'] = 'variants';
	  $data_array3['tabs'] = @\App\ItemvariantType::get(['item_variant_type_id','item_variant_type_title']);

	  $item_variant_value = @\App\ItemVariantValue::where('item_id',$id)->get();

      $meta_fields_array = array();
 	 
 	  $data_array3['fields'] = $item_variant_value;
	  $main_array[] =$data_array3;
	  **/









//variant form starts
	  $distinct_variants_types = \App\ItemVariantType::get();
	  $variants_array = array();
	  foreach($distinct_variants_types as $vt)
	  { 
         $cols_array = array();
	  
	  	 $tabs = array();
	     $fields_array_variants = array();
		 $data_array3["title"] =   $vt->item_variant_type_title;
		 
		 $d1['title'] = 'Title';
		 $d1['type'] = 'text';
		 $d1['identifier'] = 'title';
	     $fields_array_variants[] = $d1;
		 
		 $col_array[] = 'item_variant_value_id';
		 $col_array[] = 'item_variant_value_title';
		 
		 

		 if($vt->price_status != 0 && $vt->price_status != '0' )
		 {
		   $col_array[] = 'item_variant_price';
		   $d2['title'] = 'Price';
		   $d2['type'] = 'float';
		   $d2['identifier'] = 'price';
		   $d2['symbol'] = @\App\Setting::where('key_title','currency_symbol')->first(['key_value'])->key_value;
		   $fields_array_variants[] = $d2; 
		}
		  
		  
	 
		 
		  if($vt->price_difference_status != 0 && $vt->price_difference_status != '0' )
		 {
		   $col_array[] = 'item_variant_price_difference';
		   $d4['title'] = 'Price Difference';
		   $d4['type'] = 'float';
		   $d4['identifier'] = 'price_difference';
	       $d4['symbol'] = @\App\Setting::where('key_title','currency_symbol')->first(['key_value'])->key_value;
		   $fields_array_variants[] = $d4;
		}
			 
		  $vd['title'] =   $vt->item_variant_type_title;
			$vd['product_variant_type_id'] =   $vt->item_variant_type_id;
			$vd['fields'] =   $fields_array_variants;
			$vd['values'] =   @\App\ItemVariantValue::where('item_variant_type_id',$vt->item_variant_type_id)->where('item_id',$id)->get($col_array);
			$variants_array[] = $vd;

	  }
	  
       $data_array3['title'] = 'Variants';
	   $data_array3['type'] = 'item_variants';
       $data_array3['fields'] = $variants_array;
	  $main_array[] =$data_array3;












      $result = $main_array;

      return $result;
       //return view('admin.items.item_edit', compact('result'));
   }



















  



///================================ function to check GET variable's and Defaults ====================================================//
public function get_variable_per_page()
{
	 if(isset($_GET['per_page']) && $_GET['per_page'] != null && $_GET['per_page'] != '')
					{ $per_page = $_GET['per_page']; }
					else 
					{ $per_page = 20; }
    return $per_page;
}

public function get_variable_orderby()
{
	 if(isset($_GET['orderby']) && $_GET['orderby'] != null && $_GET['orderby'] != '')
					{ $orderby = $_GET['orderby']; }
					else 
					{ $orderby = 'created_at'; }
    return $orderby;
}

public function get_variable_order()
{
	 if(isset($_GET['order']) && $_GET['order'] != null && $_GET['order'] != '')
					{ $order = $_GET['order']; }
					else 
					{ $order = 'DESC'; }
    return $order;
}


public function get_variable_search()
{
	 if(isset($_GET['search']) && $_GET['search'] != null && $_GET['search'] != '')
					{ $search = $_GET['search']; }
					else 
					{ $search = ''; }
    return $search;
}	
      
   
   public function get_variable_category()
{
	 if(isset($_GET['category']) && $_GET['category'] != null && $_GET['category'] != '')
					{ $category = $_GET['category']; }
					else 
					{ $category = ''; }
    return $category;
}	

   public function get_variable_tag()
{
	 if(isset($_GET['tag']) && $_GET['tag'] != null && $_GET['tag'] != '')
					{ $tag = $_GET['tag']; }
					else 
					{ $tag = ''; }
    return $tag;
}	

  
   public function get_variable_status()
{
	 if(isset($_GET['status']) && $_GET['status'] != null && $_GET['status'] != '')
					{ $status = $_GET['status']; }
					else 
					{ $status = 'active'; }
    return $status;
}


   public function get_variable_include_meta()
{
	 if(isset($_GET['include_meta']) && $_GET['include_meta'] != null && $_GET['include_meta'] != '')
					{ $include_meta = $_GET['include_meta']; }
					else 
					{ $include_meta = 'true'; }
    return $include_meta;
}	
 
 ///================================ function to check GET variable's and Defaults Ends ====================================================//
 
 
 
 
 
  
 
 
 
 
	
	
	public function paginateWithoutKey($items, $perPage = 15, $page = null, $options = [])
    {

        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

        $items = $items instanceof Collection ? $items : Collection::make($items);

        $lap = new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);

        return [
            'current_page' => $lap->currentPage(),
            'data' => $lap ->values(),
            'first_page_url' => $lap ->url(1),
            'from' => $lap->firstItem(),
            'last_page' => $lap->lastPage(),
            'last_page_url' => $lap->url($lap->lastPage()),
            'next_page_url' => $lap->nextPageUrl(),
            'per_page' => $lap->perPage(),
            'prev_page_url' => $lap->previousPageUrl(),
            'to' => $lap->lastItem(),
            'total' => $lap->total(),
        ];
    }
	
	
	
	 public function paginate($items, $perPage = 15, $page = null, $options = [])
{
	$page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
	$items = $items instanceof \Collection ? $items : Collection::make($items);
	return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
}
	 
	 	public function add_trades_feedback(Request $request)
{
	 
        $validator = Validator::make($request->all(), [
           
	 
        ]);
        if ($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';
            $data['message']        =   $validator->errors()->first();
        }
        else
        {
        
		
			$trade_id = $request->trade_id;
		    $user_id = $request->user_id;
			$feedback = $request->feedback;
			$ratings = $request->ratings;
		  
		  
		    $order_seller_id = @\App\Trades::where('id',$request->trade_id)->first(['seller_id'])->seller_id;
			$order_buyer_id = @\App\Trades::where('id',$request->trade_id)->first(['buyer_id'])->buyer_id;
			
			if($user_id == $order_seller_id)
			{
				 
				 \App\Trades::where('id',$request->trade_id)->update([ 'seller_ratings' => $ratings , 'seller_feedback' => $feedback]);
			}
			
		 
			if($user_id == $order_buyer_id)
			{
				 
				 \App\Trades::where('id',$request->trade_id)->update([ 'buyer_ratings' => $ratings , 'buyer_feedback' => $feedback]);
			}
		  
 
		
 
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';
            $data['message']        =   'Submitted successfully';

        }
        return $data;
    }
	
	




    
 
   public function make_thumb($src, $dest, $desired_width) 
   {

    /* read the source image */
    $source_image = imagecreatefromjpeg($src);
    $width = imagesx($source_image);
    $height = imagesy($source_image);
    
    /* find the "desired height" of this thumbnail, relative to the desired width  */
    $desired_height = floor($height * ($desired_width / $width));
    
    /* create a new, "virtual" image */
    $virtual_image = imagecreatetruecolor($desired_width, $desired_height);
    
    /* copy source image at a resized size */
    imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
    
    /* create the physical thumbnail image to its destination */
    imagejpeg($virtual_image, $dest);
    }

    /**
     * @author Dikshant
     * set user language by default
     */
 
 
 


}