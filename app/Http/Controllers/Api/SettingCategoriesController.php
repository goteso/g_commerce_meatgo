<?php
namespace App\Http\Controllers\Api; //admin add
use App;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; // using controller class
use Auth;
use Session;
use DB;
use Validator;
use App\User;
use Carbon\Carbon;
use App\Otp;
use App\UserSocialLinks;
use App\UserLikesDislikes;
use App\UserRatings;
use App\Country;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Traits\one_signal; // <-- you'll need this line...
use App\Traits\bitcoin_price;
use App\Traits\trait_functions;
use Illuminate\Support\Arr;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Hash;
use Mail;
use File;
 
 


class SettingCategoriesController extends Controller 
{
	
use one_signal; // <-- ...and also this line.
use bitcoin_price; // <-- ...and also this line.
use trait_functions; // <-- ...and also this line. 
   
   
   
   
   
 // Route-2.1 ============================================================== Store Item to Items table =========================================> 
   public function store(Request $request)
   {
                $validator = Validator::make($request->all(), [
					//'title' => 'required|unique:posts|max:255',
					'category_title' => 'required',
			      ]);
	   
				if($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();
                    return $data;					
                }
			 
                    $store_id = $this->validate_integer($request->store_id);
					if($store_id == '' || $store_id == null )
					 {
                        $data['status_code']    =   0;
                        $data['status_text']    =   'Failed';             
                        $data['message']        =  'Store Required';
                        return $data;	
					 }
					$category = new \App\SettingCategories;
					$category->category_title = @$request->category_title;
					$category->category_photo = $this->validate_string($request->category_photo);
					$category->parent_id = $this->validate_integer($request->parent_id);
					$category->store_id = $this->validate_integer($request->store_id);
					$category->status = $this->validate_integer_return_one($request->status);
			 
                    $category->save();
					
				    if($category != '')
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Category Added Successfully';
                          $data['data']      =   $category;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Unable to Add Category';
                          $data['data']      =   [];  
					}
				   
				  return $data;
				 
  }
   

   
  
  // Route-2.2 ============================================================== Get Categories List =========================================> 
   public function get_list()
   {
	   
        $per_page = $this->get_variable_per_page(); //ASC or DESC
		$orderby = $this->get_variable_orderby();
		$order = $this->get_variable_order();
		$search = $this->get_variable_search();
		$request_type = $this->get_variable_request_type();
		$parent_id = $this->get_variable_parent_id();
		$store_id = $this->get_variable_store_id();
		$status = $this->get_variable_status(); // ON | OFF
		$parents_only = $this->get_variable_parents_only();
		$type = $this->get_variable_type(); //regular / catering
		$include_items = $this->get_variable_include_items();

		$exclude_empty_categories = $this->get_variable_exclude_empty_categories();

		
 


		$exclude_id = $this->get_variable_exclude_id();

		$model = new \App\SettingCategories;
		$model = $model::where('category_id' ,'<>', '0');  
	   
	    if($parent_id != '' && $parent_id != null)
		{   $model = $model->where('parent_id' , $parent_id);  }	

		    if($status != '' && $status != null)
		{   $model = $model->where('status' , $status);  }	






          if(isset($_GET["auth_user_id"]) && $_GET["auth_user_id"] != '')
            {
            	$auth_user_id = $_GET["auth_user_id"];


            	  $auth_user_type = @\App\User::where('user_id',$auth_user_id)->first(['user_type'])->user_type;
            	
            	if($auth_user_type == '4')
            	{
 
            		$store_id = @\App\Stores::where('manager_id',$auth_user_id)->first(['store_id'])->store_id;

            	}
            }





			if($store_id != '' && $store_id != null)
		{   

			$model = $model->where('store_id' , $store_id);  

         }	


	if($exclude_empty_categories == 'true')
	{
		     $final_array = array();



		     if($type != '' && $type != null)
		     {
				     	if($store_id != '' && $store_id != null)
				     	{


				     		$categories_array_from_items = @\App\Items::where('item_active_status','active')->where('store_id',$store_id)->where('type',$type)->get();


				     	}
				     	else
				     	{
				     		$categories_array_from_items = @\App\Items::where('item_active_status','active')->where('type',$type)->get();
				     	}
		     }
		     else
		     {
				     	  if($store_id != '' && $store_id != null)
				     	{
				     		$categories_array_from_items = @\App\Items::where('item_active_status','active')->where('store_id',$store_id)->get();
				     	}
				     	else
				     	{
				     		$categories_array_from_items = @\App\Items::where('item_active_status','active')->get();
				     	}
		     }
             

               $categories_array_from_items;
             foreach($categories_array_from_items as $cat_in_items)
             {
             	$item_categories = $cat_in_items['item_categories'];
             	if($item_categories != '' && $item_categories != null)
             	{

             		  $item_categories;
                      $item_categories_array = explode(',',$item_categories);
                      $final_array = array_merge($final_array,$item_categories_array);
             	}

             }
 
 
              $category_ids_array = array_values(array_unique($final_array));
 
             $model = $model->whereIn('category_id' , $category_ids_array); 
	}



 

 







			if($exclude_id != '' && $exclude_id != null)
		{   $model = $model->where('category_id' ,'<>', $exclude_id);  }	

	    if($parents_only == 'true' && $parents_only != null)
		{   $model = $model->where('parent_id' , '0');  }	

 
		$model = $model->orderBy($orderby,$order);
        
	    if($search != '' && $search != null)
		{  $model = $model->where(function($q) use ($search) { $q->where( DB::raw("CONCAT(category_id,' ',category_title)"),'like', '%'.$search.'%'); });  }

                if($request_type =='input_field')
		{	 $result = $model->paginate($per_page , ['category_id','category_title','store_id','status']); 			}
	    else { $result = $model->paginate($per_page); }
 
 

        if($include_items == 'true' && $include_items == true)
        {
            foreach($result as $r)
            {

            	    		if($type != '' && $type != null)
								{
									$items = @\App\Items::whereRaw("FIND_IN_SET('".$r['category_id']."',item_categories)")->where('item_active_status','active')->where('type' , $type)->where('store_id' , $store_id)->get(); 
								}
								else
								{
									$items = @\App\Items::whereRaw("FIND_IN_SET('".$r['category_id']."',item_categories)")->where('item_active_status','active')->where('store_id' , $store_id)->get(); 
								}
            	 

		            	 		if(isset($_GET['include_meta']) && $_GET['include_meta'] != '' && $_GET['include_meta'] == 'true')
								{
								   foreach($items as $item)
									{
									  $item['item_meta_value'] = @\App\ItemMetaValue::where('item_id',$item->item_id)->get();
								  }
								}

           $r['items'] = $items;


            }
		}	



 
                   if($request_type =='input_field')
			    	{
                  		foreach($result as $r)
			        	{
			         	 $r->label = $r->category_title; 
			        	  $r->value = $r->category_id; 
			        	}    
					}

 
	              if(sizeof($result) > 0)
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Category List Fetched Successfully';
                          $data['data']      =   $result;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Category Found';
                          $data['data']      =   [];  
					}
				   return $data;
   }  



  // Route-2.3 ============================================================== Update Categories List =========================================> 
   public function update(Request $request , $id)
   {
	   
					$validator = Validator::make($request->all(), [
					//'title' => 'required|unique:posts|max:255',
					'category_title' => 'required',
					]);
	   
					if($validator->errors()->all()) 
					{
						$data['status_code']    =   0;
						$data['status_text']    =   'Failed';             
						$data['message']        =   $validator->errors()->first();
						return $data;					
					}				
				
	               //check existance of category with ID in categories table
					$exist = $this->model_exist($id);	
                    if($exist == 0 or $exist == '0')
                    {
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Categories with this ID does not exist';
                          $data['data']      =   [];
                          return $data;						  
					}


					$store_id = $this->validate_integer($request->store_id);
					if($store_id == '' || $store_id == null )
					 {
                        $data['status_code']    =   0;
                        $data['status_text']    =   'Failed';             
                        $data['message']        =  'Store Required';
                        return $data;	
					 }



				    
					$category_title =$request->category_title;
					$category_photo = $this->validate_string($request->category_photo);
					$parent_id = $this->validate_integer($request->parent_id);
					$store_id = $this->validate_integer($request->store_id);
					$status = $this->validate_integer_return_one($request->status);
				 
	                @\App\SettingCategories::where('category_id', $id)->update(['category_title' => $category_title ,'category_photo' => $category_photo , 'parent_id' => $parent_id , 'store_id' => $store_id  , 'status' => $status ]);
	               
				    $result = @\App\SettingCategories::where('category_id',$id)->get();
			 			
	                if(sizeof($result) > 0)
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Category Updated Successfully';
                          $data['data']      =   $result;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Unable to Update';
                          $data['data']      =   [];  
					}
				   return $data;
   }  


   
 
 
 
 
 
 
 
 

   // Route-2.4 ======================================================================
   public function destroy($id)
   {
   	 
   	         //check existance of item with ID in items table
				 	$exist = $this->model_exist($id);	
                    if($exist == 0 or $exist == '0')
                    {
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Category with this ID does not exist';
                          $data['data']      =   [];
                          return $data;						  
					}

   	 $this->remove_id_from_items($id);
   	 @\App\SettingCategories::where('category_id',$id)->delete();
   	 @\App\SettingCategories::where('parent_id', $id )->update(['parent_id' => '0']);
 

   	 	                  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Category Deleted Successfully';
                          $data['data']      =   [];  
                          return $data;
   }



// Route-2.5=========================================================================
public function update_status(Request $request, $id)
{
	   	  if($request->status == '' || $request->status == null)
   	      {
   	  	  	 			  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Status Required';
                          $data['data']      =   []; 
                          return $data;
   	      }
                    $status = $request->status;
	              	$user =  \App\SettingCategories::where('category_id',$id)->update([ 'status' => $status ]);
				 
	 					  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Status Updated';
                          $data['data']      =   []; 
                          return $data;

}

































public function remove_id_from_items($id)
{
     $items = @\App\Items::get();
     foreach($items as $item)
     {
     	  $new_array = array();
     	 $item_categories = $item->item_categories;
     	 $item_categories_array = explode("," , $item_categories);
     	  
     	 for($i=0;$i<sizeof($item_categories_array);$i++)
     	 {
     	 	if($item_categories_array[$i] != $id)
     	 	{
     	 		if($item_categories_array[$i] != '' && $item_categories_array[$i] != null)
     	 		{
     	 			$new_array[] = $item_categories_array[$i];
     	 		}
     	 		
     	 	}
          }
     	 $new_string = implode("," , $new_array);
     	 App\Items::where('item_id', $item['item_id'])->update(['item_categories' => $new_string]);
     }
}





 
 
 
 
 
   
//==========================================================================misc functions===================================================================//   
//check item existence by id
public function model_exist($id)
{
	$count = @\App\SettingCategories::where('category_id',$id)->count();
	if($count < 1) {
		return 0;
	}
	else{
		return 1;
	}
}	


 
 

///================================ function to check GET variable's and Defaults ====================================================//
public function get_variable_per_page()
{
	 if(isset($_GET['per_page']) && $_GET['per_page'] != null && $_GET['per_page'] != '')
					{ $per_page = $_GET['per_page']; }
					else 
					{ $per_page = 20; }
    return $per_page;
}

public function get_variable_orderby()
{
	 if(isset($_GET['orderby']) && $_GET['orderby'] != null && $_GET['orderby'] != '')
					{ $orderby = $_GET['orderby']; }
					else 
					{ $orderby = 'created_at'; }
    return $orderby;
}

public function get_variable_order()
{
	 if(isset($_GET['order']) && $_GET['order'] != null && $_GET['order'] != '')
					{ $order = $_GET['order']; }
					else 
					{ $order = 'DESC'; }
    return $order;
}


public function get_variable_search()
{
	 if(isset($_GET['search']) && $_GET['search'] != null && $_GET['search'] != '')
					{ $search = $_GET['search']; }
					else 
					{ $search = ''; }
    return $search;
}	
      
   
   public function get_variable_parent_id()
{
	 if(isset($_GET['parent_id']) && $_GET['parent_id'] != null && $_GET['parent_id'] != '')
					{ $parent_id = $_GET['parent_id']; }
					else 
					{ $parent_id = ''; }
    return $parent_id;
}	

   public function get_variable_type()
{
	 if(isset($_GET['type']) && $_GET['type'] != null && $_GET['type'] != '')
					{ $type = $_GET['type']; }
					else 
					{ $type = ''; }
    return $type;
}	



   public function get_variable_parents_only()
{
	 if(isset($_GET['parents_only']) && $_GET['parents_only'] != null && $_GET['parents_only'] != '')
					{ $parents_only = $_GET['parents_only']; }
					else 
					{ $parents_only = ''; }
    return $parents_only;
}	



public function get_variable_include_items()
{
	 if(isset($_GET['include_items']) && $_GET['include_items'] != null && $_GET['include_items'] != '')
					{ $include_items = $_GET['include_items']; }
					else 
					{ $include_items = ''; }
     return $include_items;
}	



 public function get_variable_request_type()
 {
 	   if(isset($_GET['request_type']) && $_GET['request_type'] != null && $_GET['request_type'] != '')
					{ $request_type = $_GET['request_type']; }
					else 
					{ $request_type = ''; }
       return $request_type;
 }



   public function get_variable_store_id()
 {
 	   if(isset($_GET['store_id']) && $_GET['store_id'] != null && $_GET['store_id'] != '')
					{ $store_id = $_GET['store_id']; }
					else 
					{ $store_id = ''; }
       return $store_id;
 }

    public function get_variable_status()
 {
 	   if(isset($_GET['status']) && $_GET['status'] != null && $_GET['status'] != '')
					{ $status = $_GET['status']; }
					else 
					{ $status = ''; }
       return $status;
 }


    public function get_variable_exclude_id()
 {
 	   if(isset($_GET['exclude_id']) && $_GET['exclude_id'] != null && $_GET['exclude_id'] != '')
					{ $exclude_id = $_GET['exclude_id']; }
					else 
					{ $exclude_id = ''; }
       return $exclude_id;
 }

 
      public function get_variable_exclude_empty_categories()
 {
 	   if(isset($_GET['exclude_empty_categories']) && $_GET['exclude_empty_categories'] != null && $_GET['exclude_empty_categories'] != '')
					{ $exclude_empty_categories = $_GET['exclude_empty_categories']; }
					else 
					{ $exclude_empty_categories = 'false'; }
       return $exclude_empty_categories;
 }

 ///================================ function to check GET variable's and Defaults Ends ====================================================//
 
 
 
 
 
  
 
 
 
 
	
	
	public function paginateWithoutKey($items, $perPage = 15, $page = null, $options = [])
    {

        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

        $items = $items instanceof Collection ? $items : Collection::make($items);

        $lap = new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);

        return [
            'current_page' => $lap->currentPage(),
            'data' => $lap ->values(),
            'first_page_url' => $lap ->url(1),
            'from' => $lap->firstItem(),
            'last_page' => $lap->lastPage(),
            'last_page_url' => $lap->url($lap->lastPage()),
            'next_page_url' => $lap->nextPageUrl(),
            'per_page' => $lap->perPage(),
            'prev_page_url' => $lap->previousPageUrl(),
            'to' => $lap->lastItem(),
            'total' => $lap->total(),
        ];
    }
	
	
	
	 public function paginate($items, $perPage = 15, $page = null, $options = [])
{
	$page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
	$items = $items instanceof \Collection ? $items : Collection::make($items);
	return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
}
	 
	 	public function add_trades_feedback(Request $request)
{
	 
        $validator = Validator::make($request->all(), [
           
	 
        ]);
        if ($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';
            $data['message']        =   $validator->errors()->first();
        }
        else
        {
        
		
			$trade_id = $request->trade_id;
		    $user_id = $request->user_id;
			$feedback = $request->feedback;
			$ratings = $request->ratings;
		  
		  
		    $order_seller_id = @\App\Trades::where('id',$request->trade_id)->first(['seller_id'])->seller_id;
			$order_buyer_id = @\App\Trades::where('id',$request->trade_id)->first(['buyer_id'])->buyer_id;
			
			if($user_id == $order_seller_id)
			{
				 
				 \App\Trades::where('id',$request->trade_id)->update([ 'seller_ratings' => $ratings , 'seller_feedback' => $feedback]);
			}
			
		 
			if($user_id == $order_buyer_id)
			{
				 
				 \App\Trades::where('id',$request->trade_id)->update([ 'buyer_ratings' => $ratings , 'buyer_feedback' => $feedback]);
			}
		  
 
		
 
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';
            $data['message']        =   'Submitted successfully';

        }
        return $data;
    }
	
	
	
	
 
 
	
 
	

    
 
   public function make_thumb($src, $dest, $desired_width) 
   {

    /* read the source image */
    $source_image = imagecreatefromjpeg($src);
    $width = imagesx($source_image);
    $height = imagesy($source_image);
    
    /* find the "desired height" of this thumbnail, relative to the desired width  */
    $desired_height = floor($height * ($desired_width / $width));
    
    /* create a new, "virtual" image */
    $virtual_image = imagecreatetruecolor($desired_width, $desired_height);
    
    /* copy source image at a resized size */
    imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
    
    /* create the physical thumbnail image to its destination */
    imagejpeg($virtual_image, $dest);
    }

    /**
     * @author Dikshant
     * set user language by default
     */
 
 
 


}