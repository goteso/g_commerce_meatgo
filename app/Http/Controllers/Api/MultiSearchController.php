<?php
namespace App\Http\Controllers\Api; //admin add
use App;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; // using controller class
use Auth;
use Session;
use DB;
use Validator;
use App\User;
use Carbon\Carbon;
use App\Otp;
use App\UserSocialLinks;
use App\UserLikesDislikes;
use App\UserRatings;
use App\Country;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Traits\one_signal; // <-- you'll need this line...
use App\Traits\bitcoin_price;
use App\Traits\trait_functions;
use Illuminate\Support\Arr;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Hash;
use Mail;
use File;
 
 


class MultiSearchController extends Controller 
{
	
use one_signal; // <-- ...and also this line.
use bitcoin_price; // <-- ...and also this line.
use trait_functions; // <-- ...and also this line. 
   
   
   
   
   
 // Route-18.1 ============================================================== Multi Search Api =========================================> 
   public function search(Request $request)
   {
      $store_id = '';
        $search_text = @$_GET['search_text'];
        
        $main = array();
   		  $model = new \App\Items;
		    $model = $model::where('item_id' ,'<>', '0');  

             if(isset($_GET["auth_user_id"]) && $_GET["auth_user_id"] != '')
            {
               $auth_user_id = $_GET["auth_user_id"];
               $auth_user_type = @\App\User::where('user_id',$auth_user_id)->first(['user_type'])->user_type;
              if($auth_user_type == '4')
              {  $store_id = @\App\Stores::where('manager_id',$auth_user_id)->first(['store_id'])->store_id; }
            }
        
        if($store_id != '' && $store_id != '')
        {
           $model = $model->where('store_id' , $store_id); 
        }

		    if($search_text != '' && $search_text != null)
		    {  $model = $model->where(function($q) use ($search_text) { $q->where( DB::raw("CONCAT(item_id,' ',item_title)"),'like', '%'.$search_text.'%'); });  }
        $items_array= $model->get(['item_id','item_title','item_price','item_photo']);



$users_array = array();
   if($store_id == '' || $store_id == null)
        {
        $model = new \App\User;
		    $model = $model::where('user_id' ,'<>', '0');  
        if($search_text != '' && $search_text != null)
		    {  $model = $model->where(function($q) use ($search_text) { $q->where( DB::raw("CONCAT(email,' ',phone,' ',first_name,' ',last_name,' ',user_id)"),'like', '%'.$search_text.'%'); });  }
        $users_array = $model->get(['user_id','first_name','last_name','email','photo','user_id','user_type']);

}

/**

                $model = new \App\Store;
    $model = $model::where('user_id' ,'<>', '0');  
        if($search_text != '' && $search_text != null)
    {  $model = $model->where(function($q) use ($search_text) { $q->where( DB::raw("CONCAT(email,' ',phone,' ',first_name,' ',last_name,' ',user_id)"),'like', '%'.$search_text.'%'); });  }
        $users_array = $model->get(['user_id','first_name','last_name','email','photo','user_id','user_type']);
 **/



        $model = new \App\Order;
		    $model = $model::where('order_id' ,'<>', 0); 
 
     if($store_id != '' && $store_id != '')
        {
           $model = $model->where('store_id' , $store_id); 
        }
         if($search_text != '' && $search_text != null)
        {
           $model = $model->where('order_id' , $search_text); 
        } 
        $orders_array = $model->get(['order_id','total','order_status']);
 

        $result_json_data['type'] = 'items';
        $result_json_data['data'] = $items_array;
        $main[] = $result_json_data;

        $result_json_data['type'] = 'users';
        $result_json_data['data'] = $users_array;
        $main[] = $result_json_data;

        $result_json_data['type'] = 'orders';
        $result_json_data['data'] = $orders_array;
        $main[] = $result_json_data;
 

 
					
				    if(sizeof($main) > 0)
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Search Result Fetched';
                          $data['data']      =   $main;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Result Found';
                          $data['data']      =   [];  
					}
				   
				  return $data;
				 
  }
   

   
   
 
}