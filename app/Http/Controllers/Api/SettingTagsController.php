<?php
namespace App\Http\Controllers\Api; //admin add
use App;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; // using controller class
use Auth;
use Session;
use DB;
use Validator;
use App\User;
use Carbon\Carbon;
use App\Otp;
use App\UserSocialLinks;
use App\UserLikesDislikes;
use App\UserRatings;
use App\Country;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Traits\one_signal; // <-- you'll need this line...
use App\Traits\bitcoin_price;
use App\Traits\trait_functions;
use Illuminate\Support\Arr;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Hash;
use Mail;
use File;
 
 


class SettingTagsController extends Controller 
{
	
use one_signal; // <-- ...and also this line.
use bitcoin_price; // <-- ...and also this line.
use trait_functions; // <-- ...and also this line. 
   
   
   
   
   
 // Route-3.1 ============================================================== Store Tags =========================================> 
   public function store(Request $request)
   {
                $validator = Validator::make($request->all(), [
					//'title' => 'required|unique:posts|max:255',
					'tag_title' => 'required',
			      ]);
	   
				if($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();
                    return $data;					
                }
			 
					$category = new \App\SettingTags;
					$category->tag_title = @$request->tag_title;
					$category->tag_photo = $this->validate_string($request->tag_photo);
					$category->parent_id = $this->validate_integer($request->parent_id);
					$category->store_id = $this->validate_integer($request->store_id);
                    $category->save();
					
				    if($category != '')
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Tag Added Successfully';
                          $data['data']      =   $category;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Unable to Add Tag';
                          $data['data']      =   [];  
					}
				   
				  return $data;
				 
  }
   

   
  
  // Route-3.2 ============================================================== Get Tags List =========================================> 
   public function get_list()
   {
	    $per_page = $this->get_variable_per_page(); //ASC or DESC
		$orderby = $this->get_variable_orderby();
		$order = $this->get_variable_order();
		$search = $this->get_variable_search();
		$store_id = $this->get_variable_store_id();
		$request_type = $this->get_variable_request_type();
		$parent_id = $this->get_variable_parent_id();
		$parents_only = $this->get_variable_parents_only();
		 
  
		$model = new \App\SettingTags;

		$model = $model::where('tag_id' ,'<>', '0');  

		if($parents_only == 'true' && $parents_only != null)
		{ $model = $model->where('parent_id' , 0);  }	
	   
	    if($parent_id != '' && $parent_id != null)
		{ $model = $model->where('parent_id' , $parent_id);  }	
        
        if($store_id != '' && $store_id != null)
		{ $model = $model->where('store_id' , $store_id);  }	
        
	    if($search != '' && $search != null)
		{ $model = $model->where(function($q) use ($search) { $q->where( DB::raw("CONCAT(tag_id,' ',tag_title)"),'like', '%'.$search.'%'); });  }

        $model = $model->orderBy($orderby,$order);
	   
        if($request_type =='input_field')
		{ $result = $model->paginate($per_page , ['tag_id','tag_title','store_id','status']); 			}
	    else { $result = $model->paginate($per_page); }

			if($request_type =='input_field')
				{
                  	foreach($result as $r)
			        {
			          $r->label = $r->tag_id; 
			          $r->value = $r->tag_title; 
			        }    
				}
        
	   
	              if(sizeof($result) > 0)
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Tags List Fetched Successfully';
                          $data['data']      =   $result;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Tag Found';
                          $data['data']      =   [];  
					}
				   return $data;
   }  



  // Route-3.3 ============================================================== Update Tag =========================================> 
   public function update(Request $request , $id)
   {
	   
					$validator = Validator::make($request->all(), [
					//'title' => 'required|unique:posts|max:255',
					'tag_title' => 'required',
					]);
	   
					if($validator->errors()->all()) 
					{
						$data['status_code']    =   0;
						$data['status_text']    =   'Failed';             
						$data['message']        =   $validator->errors()->first();
						return $data;					
					}				
				
	                //check existance of category with ID in categories table
					$exist = $this->model_exist($id);	
                    if($exist == 0 or $exist == '0')
                    {
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Tag with this ID does not exist';
                          $data['data']      =   [];
                          return $data;						  
					}
				    
					$tag_title =$request->tag_title;
					$tag_photo = $this->validate_string($request->tag_photo);
					$parent_id = $this->validate_integer($request->parent_id);
					$store_id = $this->validate_integer($request->store_id);
				 
	                App\SettingTags::where('tag_id', $id)->update(['tag_title' => $tag_title ,'tag_photo' => $tag_photo , 'parent_id' => $parent_id , 'store_id' => $store_id  ]);
	               
				    $result = @\App\SettingTags::where('tag_id',$id)->get();
			 			
	                if(sizeof($result) > 0)
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Tag Updated Successfully';
                          $data['data']      =   $result;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Unable to Update';
                          $data['data']      =   [];  
					}
				   return $data;
   }  


   
 
 
   // Route-3.4 ============================================================== Items Delete =========================================> 
   public function destroy($id)
   {
   	                //check existance of item with ID in items table
				 	$exist = $this->model_exist($id);	
                    if($exist == 0 or $exist == '0')
                    {
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Tag with this ID does not exist';
                          $data['data']      =   [];
                          return $data;						  
					}
                          $this->remove_id_from_items($id);
 					  	  @\App\SettingTags::where('tag_id',$id)->delete();
  						  @\App\SettingTags::where('parent_id', $id )->update(['parent_id' => '0']);
 
                          $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Tag Deleted Successfully';
                          $data['data']      =   [];  
                          return $data;
   }



// Route-3.5======================================================================================
public function update_status(Request $request, $id)
{
	   	  if($request->status == '' || $request->status == null)
   	      {
   	  	  	 			  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Status Required';
                          $data['data']      =   []; 
                          return $data;
   	      }
                    $status = $request->status;
	              	$user =  \App\SettingTags::where('tag_id',$id)->update([ 'status' => $status ]);
				 
	 					  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Status Updated';
                          $data['data']      =   []; 
                          return $data;

}
































public function remove_id_from_items($id)
{
     $items = @\App\Items::get();
     foreach($items as $item)
     {
     	  $new_array = array();
     	 $item_tags = $item->item_tags;
     	 $item_tags_array = explode("," , $item_tags);
     	  
     	 for($i=0;$i<sizeof($item_tags_array);$i++)
     	 {
     	 	if($item_tags_array[$i] != $id)
     	 	{
     	 		if($item_tags_array[$i] != '' && $item_tags_array[$i] != null)
     	 		{
     	 			$new_array[] = $item_tags_array[$i];
     	 		}
     	 		
     	 	}
          }
     	 $new_string = implode("," , $new_array);
     	 App\Items::where('item_id', $item['item_id'])->update(['item_tags' => $new_string]);
     }
}

 
 
 
 
   
//==========================================================================misc functions===================================================================//   
//check item existence by id
public function model_exist($id)
{
	$count = @\App\SettingTags::where('tag_id',$id)->count();
	if($count < 1) {
		return 0;
	}
	else{
		return 1;
	}
}	


 
 

///================================ function to check GET variable's and Defaults ====================================================//
public function get_variable_per_page()
{
	 if(isset($_GET['per_page']) && $_GET['per_page'] != null && $_GET['per_page'] != '')
					{ $per_page = $_GET['per_page']; }
					else 
					{ $per_page = 20; }
    return $per_page;
}

public function get_variable_orderby()
{
	 if(isset($_GET['orderby']) && $_GET['orderby'] != null && $_GET['orderby'] != '')
					{ $orderby = $_GET['orderby']; }
					else 
					{ $orderby = 'created_at'; }
    return $orderby;
}

public function get_variable_order()
{
	 if(isset($_GET['order']) && $_GET['order'] != null && $_GET['order'] != '')
					{ $order = $_GET['order']; }
					else 
					{ $order = 'DESC'; }
    return $order;
}


public function get_variable_search()
{
	 if(isset($_GET['search']) && $_GET['search'] != null && $_GET['search'] != '')
					{ $search = $_GET['search']; }
					else 
					{ $search = ''; }
    return $search;
}	
      
   
 
 
  
   public function get_variable_parent_id()
{
	 if(isset($_GET['parent_id']) && $_GET['parent_id'] != null && $_GET['parent_id'] != '')
					{ $parent_id = $_GET['parent_id']; }
					else 
					{ $parent_id = ''; }
    return $parent_id;
}	

   public function get_variable_parents_only()
{
	 if( (isset($_GET['parents_only'])) &&  $_GET['parents_only'] == 'true'   )
					{ $parents_only = 'true'; }
					else 
					{ $parents_only = 'false'; }
    return $parents_only;
}	
public function get_variable_request_type()
 {
 	   if(isset($_GET['request_type']) && $_GET['request_type'] != null && $_GET['request_type'] != '')
					{ $request_type = $_GET['request_type']; }
					else 
					{ $request_type = ''; }
       return $request_type;
 }


 public function get_variable_store_id()
 {
 	   if(isset($_GET['store_id']) && $_GET['store_id'] != null && $_GET['store_id'] != '')
					{ $store_id = $_GET['store_id']; }
					else 
					{ $store_id = ''; }
       return $store_id;
 }


 
 ///================================ function to check GET variable's and Defaults Ends ====================================================//
 
 
 
 
 
  
 
 
 
 
	
	
	public function paginateWithoutKey($items, $perPage = 15, $page = null, $options = [])
    {

        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

        $items = $items instanceof Collection ? $items : Collection::make($items);

        $lap = new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);

        return [
            'current_page' => $lap->currentPage(),
            'data' => $lap ->values(),
            'first_page_url' => $lap ->url(1),
            'from' => $lap->firstItem(),
            'last_page' => $lap->lastPage(),
            'last_page_url' => $lap->url($lap->lastPage()),
            'next_page_url' => $lap->nextPageUrl(),
            'per_page' => $lap->perPage(),
            'prev_page_url' => $lap->previousPageUrl(),
            'to' => $lap->lastItem(),
            'total' => $lap->total(),
        ];
    }
	
	
	
	 public function paginate($items, $perPage = 15, $page = null, $options = [])
{
	$page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
	$items = $items instanceof \Collection ? $items : Collection::make($items);
	return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
}
	 
	 	public function add_trades_feedback(Request $request)
{
	 
        $validator = Validator::make($request->all(), [
           
	 
        ]);
        if ($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';
            $data['message']        =   $validator->errors()->first();
        }
        else
        {
        
		
			$trade_id = $request->trade_id;
		    $user_id = $request->user_id;
			$feedback = $request->feedback;
			$ratings = $request->ratings;
		  
		  
		    $order_seller_id = @\App\Trades::where('id',$request->trade_id)->first(['seller_id'])->seller_id;
			$order_buyer_id = @\App\Trades::where('id',$request->trade_id)->first(['buyer_id'])->buyer_id;
			
			if($user_id == $order_seller_id)
			{
				 
				 \App\Trades::where('id',$request->trade_id)->update([ 'seller_ratings' => $ratings , 'seller_feedback' => $feedback]);
			}
			
		 
			if($user_id == $order_buyer_id)
			{
				 
				 \App\Trades::where('id',$request->trade_id)->update([ 'buyer_ratings' => $ratings , 'buyer_feedback' => $feedback]);
			}
		  
 
		
 
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';
            $data['message']        =   'Submitted successfully';

        }
        return $data;
    }
	
	
	
	
 
 
	
 
	

    
 
   public function make_thumb($src, $dest, $desired_width) 
   {

    /* read the source image */
    $source_image = imagecreatefromjpeg($src);
    $width = imagesx($source_image);
    $height = imagesy($source_image);
    
    /* find the "desired height" of this thumbnail, relative to the desired width  */
    $desired_height = floor($height * ($desired_width / $width));
    
    /* create a new, "virtual" image */
    $virtual_image = imagecreatetruecolor($desired_width, $desired_height);
    
    /* copy source image at a resized size */
    imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
    
    /* create the physical thumbnail image to its destination */
    imagejpeg($virtual_image, $dest);
    }

    /**
     * @author Dikshant
     * set user language by default
     */
 
 
 


}