<?php
namespace App\Http\Controllers\Api; //admin add
use App;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; // using controller class
use Auth;
use Session;
use DB;
use Validator;
use App\User;
use Carbon\Carbon;
use App\Otp;
use App\UserSocialLinks;
use App\UserLikesDislikes;
use App\UserRatings;
use App\Country;
use App\Traits\one_signal; // <-- you'll need this line...
use App\Traits\bitcoin_price;
use App\Traits\trait_functions;
use App\Traits\notifications;
use Hash;
use Mail;
use File;

use App\CoinbaseLog;
use Coinbase\Wallet\Client;
use Coinbase\Wallet\CoinBasePayments;
//use Coinbase\CoinBasePayments;

use Coinbase\Wallet\Configuration;
use Coinbase\Wallet\Resource\Account;
use Coinbase\Wallet\Resource\Address;
use Coinbase\Wallet\Enum\CurrencyCode;
use Coinbase\Wallet\Resource\Transaction;
use Coinbase\Wallet\Value\Money;
use Coinbase;
 
 


class UsersApiController extends Controller 
{
    

use one_signal; // <-- ...and also this line.
use bitcoin_price; // <-- ...and also this line.
use trait_functions; // <-- ...and also this line. 
use notifications; // <-- ...and also this line. 
   
   
   
   
   

   public function create_referral_code($first_name)
   {
     $insertable_referral_code = $first_name."".rand(1000, 9999);
     $referral_code_count = @\App\User::where('referral_code' , $insertable_referral_code)->count();
     if($referral_code_count > 0)
      {
            $insertable_referral_code = $first_name."".rand(1000, 9999);
      }

      return $insertable_referral_code;
   }


   //Route-24.1 ========================================================== 
    public function register(Request $request)
    {        
       if($request->login_type == 'email')
            {    
    
              $validator = Validator::make($request->all(),[
                'first_name' => 'required|max:255',
         
                'email' => 'required|max:255|unique:user|email',
                'password'  =>  'required|min:6|max:20',
                
                'login_type'=>'required',
                 ]);
               if ($validator->errors()->all()) 
                {
                $data['status_code']    =   0;
                $data['status_text']    =   'Failed';             
                $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
            
                     $insertable_referral_code = $this->create_referral_code($request->first_name);
                    

                    $input = $request->all();
                    $input['password'] = Hash::make($request->password);        
                    $input['status'] = 1;
                    $input['first_name'] = $this->validate_string($request->first_name); 
                    $input['last_name'] = $this->validate_string($request->last_name);
                    $input['email'] = $request->email;
                    $input['phone'] = $request->phone;
                    $input['user_type'] = '2';
                    $input['status'] = '1';
                    $input['phone'] = $request->phone;

                    $input['photo'] = $this->validate_string($request->photo);
                    $input['otp'] = $this->validate_string($request->otp);
                    $input['referral_code'] = $this->validate_string($insertable_referral_code);
                    $input['remember_token'] =$this->validate_string($request->remember_token);
                    $input['email_token'] =$this->validate_string($request->email_token);
                    $input['longitude'] = $this->validate_string($request->longitude);
                    $input['latitude'] = $this->validate_string($request->latitude);

                 


                    //$input['encrypted_password'] =  $this->encrypt($request->password);
                    $user = @\App\User::create($input);
                    $users_data = array();
                    $users_data[] = $user;
                    //$token =  $user->createToken('MyApp')->accessToken;


                    $referral_code = $this->validate_string($request->referral_code);
                    if($referral_code != '')
                    {
                          $referral_code_count = @\App\User::where('referral_code' , $referral_code )->count();
                          if($referral_code_count < 1 )
                          {
                             
                          }
                          else
                          {
                             $referral_user_id = @\App\User::where('referral_code' , $referral_code )->first(['user_id'])->user_id;
                             $registration_loyalty_points = @\App\Setting::where('key_title' , 'registration_loyalty_points' )->first(['key_value'])->key_value;
                             $referral_loyalty_points = @\App\Setting::where('key_title' , 'referral_loyalty_points' )->first(['key_value'])->key_value;

                             $registered_user_stored_request['user_id'] = $user['user_id'];
                             $registered_user_stored_request['type'] =  'referral_registered';
                             $registered_user_stored_request['source'] =  $referral_user_id;
                             $registered_user_stored_request['points'] = $registration_loyalty_points;

                             $referred_user_stored_request['user_id'] = $referral_user_id;
                             $referred_user_stored_request['type'] = 'referral_from';
                             $referred_user_stored_request['source'] = $user['user_id'];
                             $referred_user_stored_request['points'] =$referral_loyalty_points;
                             
                              app('App\Http\Controllers\Api\LoyaltyPointsController')->store($request , $registered_user_stored_request  );
                             app('App\Http\Controllers\Api\LoyaltyPointsController')->store($request , $referred_user_stored_request  );
                          }
                    }

 
                  
                   //send notification 
                      @$this->notify($request , 'signup',@$user['user_id'] , @$user['user_id']  , @$user['user_id'] );
                  //send notification ends

                    $data['status_code']    =   1;                      
                    $data['status_text']    =   'Success';
                    $data['message']        =   'Users has been created successfully';
                    $data['data']      =   $users_data;
                    //$data['access_token']      =   $token;

                }            
              
             }
              
             
             else if($request->login_type == 'facebook')
             {
                $validator = Validator::make($request->all(),[
                'first_name' => 'required|max:255',
                'login_type'=>'required',
                 ]);
               if ($validator->errors()->all()) 
                {
                $data['status_code']    =   0;
                $data['status_text']    =   'Failed';             
                $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
 
                        $facebook_id = $request->facebook_id;

                        $facebook_id_exist = @\App\User::where('facebook_id',$facebook_id)->count();                        //saving profile image

                        if($facebook_id_exist > 0)
                        {

                             $user_data = @\App\User::where('facebook_id',$facebook_id)->take(1)->get();
                             $user_id = $user_data[0]['user_id'];
                             $user_session_id =  $this->add_user_session($request ,$user_id , $request->notification_token);
                             $user_data[0]['user_session_id'] = $user_session_id;

                             $data['status_code']    =   1;
                             $data['status_text']    =   'Success';             
                             $data['message']        =   'You have logged-in successfully.';
                            $data['user_data']      =   $user_data;    
                            return $data;   
                        }


                       if($request->email == '' || $request->email == null)
                       {
                             $data['status_code']    =   0;
                             $data['status_text']    =   'Success';             
                             $data['message']        =   'Email_Required';
                             $data['user_data']      =   [];    
                             return $data;   
                        }


 
                        @$path = @public_path().'/images/users/';
                        if(!File::exists(@$path))
                        {
                            File::makeDirectory(@$path, $mode = 0777, true, true);
                        }

                        @$fbProfilePicUrl = @$request->facebook_pic_url;
                        @$unique_string = 'profile-image-'.strtotime(date('Y-m-d h:i:s'));
                        @$file = file_get_contents(@$fbProfilePicUrl);

                        @$photo_name = $unique_string.'.png';
                        @$thumb_name = "thumb-".$photo_name;

                        @file_put_contents($path.'/'.$photo_name, $file);
                   
                       
                       //saving profile image ends



            
                    $insertable_referral_code = $this->create_referral_code($request->first_name);
                    

                    $input = $request->all();
                    $input['password'] = Hash::make($request->password);        
                    $input['status'] = 1;
         

                                      $input['first_name'] = $this->validate_string($request->first_name); 
                    $input['last_name'] = $this->validate_string($request->last_name);



                    $input['email'] = $this->validate_string($request->email);
                    $input['phone'] = $request->phone;
                    $input['user_type'] = '2';
                    $input['status'] = '1';
                    $input['phone'] = $request->phone;
                    $input['photo'] = $this->validate_string(@$photo_name);
                    $input['otp'] = $this->validate_string($request->otp);
                    $input['referral_code'] = $this->validate_string($insertable_referral_code);
                    $input['remember_token'] =$this->validate_string($request->remember_token);
                    $input['email_token'] =$this->validate_string($request->email_token);
                    $input['longitude'] = $this->validate_string($request->longitude);
                    $input['latitude'] = $this->validate_string($request->latitude);

                 


                    //$input['encrypted_password'] =  $this->encrypt($request->password);
                    $user = @\App\User::create($input);
                    $users_data = array();
                    $users_data[] = $user;
                    //$token =  $user->createToken('MyApp')->accessToken;


                    $referral_code = $this->validate_string($request->referral_code);
                    if($referral_code != '')
                    {
                          $referral_code_count = @\App\User::where('referral_code' , $referral_code )->count();
                          if($referral_code_count < 1 )
                          {
                             
                          }
                          else
                          {
                             $referral_user_id = @\App\User::where('referral_code' , $referral_code )->first(['user_id'])->user_id;
                             $registration_loyalty_points = @\App\Setting::where('key_title' , 'registration_loyalty_points' )->first(['key_value'])->key_value;
                             $referral_loyalty_points = @\App\Setting::where('key_title' , 'referral_loyalty_points' )->first(['key_value'])->key_value;

                             $registered_user_stored_request['user_id'] = $user['user_id'];
                             $registered_user_stored_request['type'] =  'referral_registered';
                             $registered_user_stored_request['source'] =  $referral_user_id;
                             $registered_user_stored_request['points'] = $registration_loyalty_points;

                             $referred_user_stored_request['user_id'] = $referral_user_id;
                             $referred_user_stored_request['type'] = 'referral_from';
                             $referred_user_stored_request['source'] = $user['user_id'];
                             $referred_user_stored_request['points'] =$referral_loyalty_points;
                             
                              app('App\Http\Controllers\Api\LoyaltyPointsController')->store($request , $registered_user_stored_request  );
                              app('App\Http\Controllers\Api\LoyaltyPointsController')->store($request , $referred_user_stored_request  );
                          }
                    }


                             $user_data = @\App\User::where('facebook_id',$facebook_id)->take(1)->get();
                             $user_id = $user_data[0]['user_id'];
                             $user_session_id =  $this->add_user_session($request ,$user_id , $request->notification_token);
                             $user_data[0]['user_session_id'] = $user_session_id;
 

                       //send notification 
                         @$this->notify($request ,'signup',@$user['user_id']);
                       //send notification ends
                             $data['status_code']    =   1;
                             $data['status_text']    =   'Success';             
                             $data['message']        =   'You have logged-in successfully.';
                            $data['user_data']      =   $user_data;    
                            return $data;   



          

                }            
              
                
             }




        else if($request->login_type == 'google')
             {
                $validator = Validator::make($request->all(),[
                'first_name' => 'required|max:255',
                'login_type'=>'required',
                'email'=>'required',
                 ]);
               if ($validator->errors()->all()) 
                {
                $data['status_code']    =   0;
                $data['status_text']    =   'Failed';             
                $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
 
                        $email = $request->email;

                        $email_exist = @\App\User::where('email',$email)->count();                        //saving profile image

                        if($email_exist > 0)
                        {

                             $user_data = @\App\User::where('email',$email)->take(1)->get();
                             $user_id = $user_data[0]['user_id'];
                             $user_session_id =  $this->add_user_session($request ,$user_id , $request->notification_token);
                             $user_data[0]['user_session_id'] = $user_session_id;

                             $data['status_code']    =   1;
                             $data['status_text']    =   'Success';             
                             $data['message']        =   'You have logged-in successfully.';
                            $data['user_data']      =   $user_data;    
                            return $data;   
                        }


                    if($request->email == '' || $request->email == null)
                       {
                             $data['status_code']    =   0;
                             $data['status_text']    =   'Success';             
                             $data['message']        =   'Email_Required';
                             $data['user_data']      =   [];    
                             return $data;   
                        }



 /**
                        @$path = @public_path().'/images/users/';
                        if(!File::exists(@$path))
                        {
                            File::makeDirectory(@$path, $mode = 0777, true, true);
                        }

                        @$fbProfilePicUrl = @$request->fbProfilePicUrl;
                        @$unique_string = 'profile-image-'.strtotime(date('Y-m-d h:i:s'));
                        @$file = file_get_contents(@$fbProfilePicUrl);

                        @$photo_name = $unique_string.'.png';
                        @$thumb_name = "thumb-".$photo_name;

                        @file_put_contents($path.'/'.$photo_name, $file);
                   
                       
                       **/
                       //saving profile image ends



            
                    $insertable_referral_code = $this->create_referral_code($request->first_name);
                    

                    $input = $request->all();
                    $input['password'] = Hash::make($request->password);        
                    $input['status'] = 1;
 

                                      $input['first_name'] = $this->validate_string($request->first_name); 
                    $input['last_name'] = $this->validate_string($request->last_name);


                    $input['email'] = $this->validate_string($request->email);
                    $input['facebook_id'] = $this->validate_string($request->facebook_id);
                    $input['phone'] = $request->phone;
                    $input['user_type'] = '2';
                    $input['status'] = '1';
                    $input['phone'] = $request->phone;
                    $input['photo'] = $this->validate_string(@$photo_name);
                    $input['otp'] = $this->validate_string($request->otp);
                    $input['referral_code'] = $this->validate_string($insertable_referral_code);
                    $input['remember_token'] =$this->validate_string($request->remember_token);
                    $input['email_token'] =$this->validate_string($request->email_token);
                    $input['longitude'] = $this->validate_string($request->longitude);
                    $input['latitude'] = $this->validate_string($request->latitude);

                 


                    //$input['encrypted_password'] =  $this->encrypt($request->password);
                    $user = @\App\User::create($input);
                    $users_data = array();
                    $users_data[] = $user;
                    //$token =  $user->createToken('MyApp')->accessToken;


                    $referral_code = $this->validate_string($request->referral_code);
                    if($referral_code != '')
                    {
                          $referral_code_count = @\App\User::where('referral_code' , $referral_code )->count();
                          if($referral_code_count < 1 )
                          {
                             
                          }
                          else
                          {
                             $referral_user_id = @\App\User::where('referral_code' , $referral_code )->first(['user_id'])->user_id;
                             $registration_loyalty_points = @\App\Setting::where('key_title' , 'registration_loyalty_points' )->first(['key_value'])->key_value;
                             $referral_loyalty_points = @\App\Setting::where('key_title' , 'referral_loyalty_points' )->first(['key_value'])->key_value;

                             $registered_user_stored_request['user_id'] = $user['user_id'];
                             $registered_user_stored_request['type'] =  'referral_registered';
                             $registered_user_stored_request['source'] =  $referral_user_id;
                             $registered_user_stored_request['points'] = $registration_loyalty_points;

                             $referred_user_stored_request['user_id'] = $referral_user_id;
                             $referred_user_stored_request['type'] = 'referral_from';
                             $referred_user_stored_request['source'] = $user['user_id'];
                             $referred_user_stored_request['points'] =$referral_loyalty_points;
                             
                              app('App\Http\Controllers\Api\LoyaltyPointsController')->store($request , $registered_user_stored_request  );
                              app('App\Http\Controllers\Api\LoyaltyPointsController')->store($request , $referred_user_stored_request  );
                          }
                    }


                             $user_data = @\App\User::where('email',$email)->take(1)->get();
                             $user_id = $user_data[0]['user_id'];
                             $user_session_id =  $this->add_user_session($request ,$user_id , $request->notification_token);
                             $user_data[0]['user_session_id'] = $user_session_id;
 

                      //send notification 
                      @$this->notify('signup',@$user['user_id']);
                      //send notification ends


                             $data['status_code']    =   1;
                             $data['status_text']    =   'Success';             
                             $data['message']        =   'You have logged-in successfully.';
                            $data['user_data']      =   $user_data;    
                            return $data;   



          

                }            
              
                
             }


             else
             {
                $data['status_code']    =   0;
                $data['status_text']    =   'Failed';             
                $data['message']        =   'login type is invalid';
             } 
             return $data;
            
    }

 




 

 

 // Route-24.2 =====================================================
       
    public function login(Request $request)
    {   
 
        $validator = Validator::make($request->all(), [
        'email' => 'required|email',
        'password' => 'required|min:4',
 
        ]);
        //return $validator->errors()->all();
        if($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   $validator->errors()->first();             
        }
        else
        {   
 

    if (auth()->attempt(['email' => $request->input('email'), 'password' => $request->input('password')]))  
 
            {

 
              $user_type = Auth::user()->user_type;


              if($user_type != $request->user_type)
              { 
                $data['status_code']    =   0;
                $data['status_text']    =   'Failed';             
                $data['message']        =   'Invalid User Type';
                $data['user_data']      =   []; 
                return $data;
              }
             
                 $user_id = Auth::id(); 

                 $user_data = @\App\User::where('user_id',$user_id)->get();
                  $user_session_id =  $this->add_user_session($request ,$user_id , $request->notification_token);

                   $user_data[0]['user_session_id'] = $user_session_id;
 
                $data['status_code']    =   1;
                $data['status_text']    =   'Success';             
                $data['message']        =   'You have logged-in successfully.';
                $data['user_data']      =   $user_data;       

  
            
      
             // $this->notification_to_single($user["notification_token"], 'Login by '.$user["name"] , 'Thank You for Login');
      
            }            
            else
            {
                $data['status_code']    =   0;
                $data['status_text']    =   'Failed';             
                $data['message']        =   'Please enter correct email and password.';

            }
            

        }

        return $data;
    }
  
 

   public function add_user_session( $request='' , $user_id , $notification_token )
   {

       @\App\UserSessions::where('notification_token',$notification_token)->delete();
         $flight = new \App\UserSessions;
         $flight->user_id = $user_id;
         $flight->device_type = $this->validate_string($request->device_type );
         $flight->device_id = $this->validate_string($request->device_id );
         $flight->screen_size = $this->validate_string($request->screen_size );
         $flight->device_os = $this->validate_string($request->device_os );
         $flight->ip_address =  \Request::ip(); 
         $flight->session_status = '1';
         $flight->location_name = $this->validate_string($request->location_name );
         $flight->latitude = $this->validate_string($request->latitude );
         $flight->longitude = $this->validate_string($request->longitude );
         $flight->browser = $this->validate_string($request->browser );
         $flight->timezone = $this->validate_string($request->timezone );
         $flight->notification_token = $this->validate_string($request->notification_token );
         $flight->save();
         return $flight->id;
   }
   
 
        
 
//Route-24.3 ======================================================================
public function logout(Request $request)
{
         $validator = Validator::make($request->all(), [
                 
                 ]);
               if ($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();                   
                }
                else
                {
                    $input = $request->all();
                

                    
                        @\App\UserSessions::where('notification_token',@$request->notification_token)->delete();
                        @\App\UserSessions::where('id',@$request->user_session_id)->delete();
                                
                        //Auth::guard('api')->logout();
                        $data['status_code']    =   1;                      
                        $data['status_text']    =   'Success';
                        $data['message']        =   'User logged-out successfully';
                        return $data;


                    } 
                   
}
 

 
//onetimeapp@hotmail.com

    public function send_contact_us_mail(Request $request)
    {
        
        $email = @\App\AppData::where('key_name','admin_email')->first(['value'])->value;
        $username = @\App\User::where('id',$request->user_id)->first(['username'])->username;
                $user_email = @\App\User::where('id',$request->user_id)->first(['email'])->email;
        $query_message = @$request->message;
        $data=array('first_name' => $username , 'query_message'=>$query_message);
        Mail::send('emails.contact_us', $data, function($message) use ($username, $email ,$user_email) {
        $message->to($email, 'OneTime')->replyTo($user_email, $username)->subject('OneTimeApp : Query ('.$username.')');
                $message->from('noreply@onetimeapp.co.uk', 'OneTime');
            });
            
  //    $count = @\App\User::where('facebook_id',$request->facebook_id)->count();
        
            if('1'=='1')
             {
                $data['status_code']    =   1;
                $data['status_text']    =   'Success';             
                $data['message']        =   'Query Submitted Successfully';
             }
             else
             {
                $data['status_code']    =   0;
                $data['status_text']    =   'Failed';             
                $data['message']        =   'Unable to Send Email';
             } 
             return $data;
             
        
    }

     
    
 
     public function check_username_existence($username)
    {
        $status = \App\User::where('username',$username)->count();
        if($status > 0)
        {
            return 1;
        }
        else
       {
        return 0;
       }
    }
    
    
    //Enc and Dec function starts --------------------------------
    
    public function encrypt($string)
    {
        $password="sgdhsfdtysdftyvhgsftdrdr78653e3g43jh7837g3673r33geu3hw89e73eghwdu837te763dg36er56e463je887et67slkdheytdtrg3y346357974825724356";
        $encrypted_string=openssl_encrypt($string,"AES-128-ECB",$password);
        //$decrypted_string=openssl_decrypt('RF22CTE9MsYcP+YTLdbyzA==',"AES-128-ECB",$password);
        return $encrypted_string;
    }
    
        public function decrypt($string)
    {
        $password="sgdhsfdtysdftyvhgsftdrdr78653e3g43jh7837g3673r33geu3hw89e73eghwdu837te763dg36er56e463je887et67slkdheytdtrg3y346357974825724356";
       /// $encrypted_string=openssl_encrypt($string,"AES-128-ECB",$password);
        $decrypted_string=openssl_decrypt($string,"AES-128-ECB",$password);
        return $decrypted_string;
    }
    //Enc and Dec function starts --------------------------------




    
    public function check_facebook_id_existence(Request $request)
    {
         
        $count = @\App\User::where('facebook_id',$request->facebook_id)->count();
        
            if($count > 0)
             {
                $data['status_code']    =   1;
                $data['status_text']    =   'Success';             
                $data['message']        =   'Facebook Id exist';
             }
             else
             {
                $data['status_code']    =   0;
                $data['status_text']    =   'Failed';             
                $data['message']        =   'Facebook Id not exist';
             } 
             return $data;
             
        
    }
    
    
 

 
    
 
 
 
 
    
    
    
             //For update user profile
    public function upload_profile_image(Request $request)
    {   
        $validator = Validator::make($request->all(), [
                'user_id' => 'required',
         
               ]);
           if ($validator->errors()->all()) 
            {
                $data['status_code']    =   0;
                $data['status_text']    =   'Failed';             
                $data['message']        =   $validator->errors()->first();                   
            }
            else
            {   
                $user_data = User::where('id',$request->user_id)->get();
                    if(count($user_data) != 0)
                    {  
                        //for file upload
                        $path = public_path().'/users/'.$request->user_id;
                        if(!File::exists($path)) 
                        {
                        File::makeDirectory($path, $mode = 0777, true, true);
                        }
                        
                       if(isset($request->profile_image) && !empty($request->profile_image))
                        {
                          $unique_string = 'profile-image-'.strtotime(date('Y-m-d h:i:s'));    
                          $file = $request->profile_image;                       
                          $photo_name = $unique_string.$file->getClientOriginalName();
                          $thumb_name = "thumb-". $photo_name;                     
                          $file->move($path,$photo_name);
                         // $this->make_thumb($path.'/'.$photo_name,$path.'/'.$thumb_name,'400');
                       }
                       else
                       {
                        $photo_name = '';   
                       }
                       
                       $data['status_code']  =   1;
                       $data['status_text']    =   'Success';           
                       $data['message']        =   'Image is Uploaded.';
                       $data['data'][]["image_url"] =  $request->user_id.'/'.$photo_name;
                       
                    }
                    else
                    {
                       $data['status_code']  =   0;
                       $data['status_text']    =   'Failed';           
                       $data['message']        =   'User not found';
                    }   
                
           }
        return $data;
    }
    
    
 
 
    
    
    
    
    
    
    
 
    
    
    
    
    
    
    
    
    
 
    
 
    
    
    
    
 
 
    
 
    
    
    
    
    
    
 
    
    
 
    
    
    
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
    
    
    
    
 
 
  
    

 
    
     
    
    
    
    
    
    
    //Route-24.4 =================================

    public function forgotPasswordEmail(Request $request)
    {

         $validator = Validator::make($request->all(), [
            'email' => 'required|max:255|email',
             ]);
        if ($validator->errors()->all()) 
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   $validator->errors()->first();
            $data['email'] = $request->email;           
            return $data;                  
        }
        
        
        $email = $request->email;
        $email_verify = App\User::where('email', $email)->get(["first_name","user_id"]);
        $first_name = $email_verify[0]->first_name;

 
         
        if(count($email_verify) > 0){
             $six_digit_random_number = mt_rand(100000, 999999);


             DB::table('user')->where('email', $email)->update(array('otp' => $six_digit_random_number));
             $data=array('otp' => $six_digit_random_number , 'first_name' => $first_name);

           /**
             Mail::send('emails.otp', $data, function($message) use ($first_name, $email) {
             $message->to($email, 'gCommerce')->subject('gCommerce - Password Reset');
             $message->from('noreply@gCommerce.com', 'gCommerce');
            });**/
         
         
 
        $data['status_code']    =   1;                      
        $data['status_text']    =   'Success';
        $data['message']        =   'Your new otp sent on your email address.';
        $data['user_id']             =   $email_verify[0]->user_id;
        $data['one_time_code']  =         $six_digit_random_number;
        
        } else {
        
        $data['status_code']    =   0;                      
        $data['status_text']    =   'Failed';
        $data['message']        =   'Email address is not registered.';
        }
        
        return $data;
    }
    
    
    
 //Route-24.5 ================================================

    public function verify_otp(Request $request){
    
    $email                      =       $request->email;
    $otp     =       $request->otp;    
    $otp_verify = App\User::where('email', $email)->where('otp', $otp)->get(["user_id","email"]);
    
    
 
    if(count($otp_verify) > 0){
    
        $data['status_code']    =   1;
        $data['status_text']    =   'Success';
        $data['message']        =   'Otp matches successfully';
        $data['user_id']        =   $otp_verify[0]->user_id;
        $data["data"] = $otp_verify;
     
    
        } else {
        $data['status_code']    =   0;
        $data['status_text']    =   'Failed';
        $data['message']        =   'Invalid OTP';
        
        }
        
        return $data;
    
    }
    
//Route-24.6 ====================================

    public function forgotPasswordChange(Request $request){
    
    $user_id    =  $request->user_id;
    $password     =       Hash::make($request->password);     
     App\User::where('user_id', $request->user_id)->update(['password' => $password]);
    
        $data['status_code']    =   1;
        $data['status_text']    =   'Success';
        $data['message']        =   'Password Changes Successfully';
        $data['user_id']        =   $user_id;
        return $data;
   }
    
    
    
    
    


  //Route-24.7 ========================================
     public function newPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
        'user_id' =>  'required|numeric',
        'new_password' => 'required|min:6',
        ]);
        //return $validator->errors()->all();
        if($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';             
            $data['message']        =   $validator->errors()->first();             
        }
  
        else if(isset($request->old_password) && !empty($request->old_password))
        {
            $id                      =       $request->user_id;
            $old_password            =       $request->old_password;  
            $password                =       Hash::make($request->new_password);  
            $old_password_data = DB::table('user')->select('user_id','password')->where('user_id', '=', $id)->get();            
            if(Hash::check($old_password, $old_password_data[0]->password))
            {  
            DB::table('user')->where('user_id', $id)->update(array('password' => $password));    
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';
            $data['message']        =   'Password changed successfully.';
            $data['user_id']        =   $old_password_data[0]->user_id;
        
            } 
            else 
            {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';
            $data['message']        =   'Invalid Old Password';            
            }   
        }
        else
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';
            $data['message']        =   'Invalid Data'; 
        }        
        return $data;
    
    }
    
 
 
  
 
         
    
    
    
 
    
    
    
    
    
    
     
    
    
    
    
 
   public function make_thumb($src, $dest, $desired_width) 
   {

    /* read the source image */
    $source_image = imagecreatefromjpeg($src);
    $width = imagesx($source_image);
    $height = imagesy($source_image);
    
    
     $source_image = imagecreatefrompng($src);
    $width = imagesx($source_image);
    $height = imagesy($source_image);
    
    /* find the "desired height" of this thumbnail, relative to the desired width  */
    $desired_height = floor($height * ($desired_width / $width));
    
    /* create a new, "virtual" image */
    $virtual_image = imagecreatetruecolor($desired_width, $desired_height);
    
    /* copy source image at a resized size */
    imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
    
    /* create the physical thumbnail image to its destination */
    imagejpeg($virtual_image, $dest);
    }

    /**
     * @author Dikshant
     * set user language by default
     */
 
 
 


}