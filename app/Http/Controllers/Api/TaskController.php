<?php
namespace App\Http\Controllers\Api; //admin add
use App;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; // using controller class
use Auth;
use Session;
use DB;
use Validator;
use App\User;
use Carbon\Carbon;
use App\Otp;
use App\UserSocialLinks;
use App\UserLikesDislikes;
use App\UserRatings;
use App\Country;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Traits\one_signal; // <-- you'll need this line...
use App\Traits\bitcoin_price;
use App\Traits\trait_functions;
use App\Traits\notifications;
use Illuminate\Support\Arr;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Hash;
use Mail;
use File;
 
 


class TaskController extends Controller 
{
	
use one_signal; // <-- ...and also this line.
use bitcoin_price; // <-- ...and also this line.
use trait_functions; // <-- ...and also this line. 
use notifications; // <-- ...and also this line. 
   
   
 
 // Route-17.1  ============================================================== Add Task to tasks table =========================================> 
   public function store(Request $request , $create_item_request = '')
   {
               if($create_item_request != '')
                {
                	$request = $create_item_request;
                }


 
             if($request['order_id'] == '')
               {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'Order Id Required';
                    return $data;	
               }

              $order_id = $this->validate_string($request['order_id']); 
              $driver_id = $this->validate_string($request['driver_id']); 
             
              $store_id = $this->validate_string($request['store_id']); 
			        $pickup_contact_title = $this->validate_string($request['pickup_contact_title']);
			        $pickup_contact_phone = $this->validate_string($request['pickup_contact_phone']);
			        $pickup_time = $this->validate_string($request['pickup_time']);

            


              $picked_up_time = $this->validate_string($request['picked_up_time']); 
              $pickup_address = $this->validate_string($request['pickup_address']); 
              $dropoff_contact_title = $this->validate_string($request['dropoff_contact_title']);
              $dropoff_contact_phone = $this->validate_string($request['dropoff_contact_phone']);
              $dropoff_time = $this->validate_string($request['dropoff_time']);

		       
			        $dropped_off_time = @$this->validate_string(@$request['dropped_off_time']);
			        $dropoff_address = $this->validate_string(@$request['dropoff_address']);
			        $status = $this->validate_string(@$request['status']);
			        $task_type = $this->validate_integer(@$request['task_type']);
			  
 
              $task = new App\Task;
					    $task->order_id = $order_id;
			        $task->driver_id = $driver_id;
              $task->store_id = $store_id;
              
			        $task->pickup_contact_title = $pickup_contact_title;
			        $task->pickup_contact_phone = $pickup_contact_phone;
			        $task->pickup_time = $pickup_time;

			        $task->picked_up_time = $picked_up_time;
			        $task->pickup_address = $pickup_address;
			        $task->dropoff_contact_title = $dropoff_contact_title;
			        $task->dropoff_contact_phone = $dropoff_contact_phone;
			        $task->dropoff_time = $dropoff_time;

              $task->dropped_off_time = $dropped_off_time;
              $task->dropoff_address = $dropoff_address;
              $task->status = $status;
              $task->task_type = $task_type;

 			        $task->save();
 

					
				    if($task != '')
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Task Added Successfully';
                          $data['data']      =   $task;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Unable to Add Task';
                          $data['data']      =   [];  
					}
				   
				  return $data;
	 }
   




   
  
  // Route-17.2 ============================================================== Get Users List =========================================> 
   public function get_list()
   {



	   
    $per_page = $this->get_variable_per_page(); //ASC or DESC
		$orderby = $this->get_variable_orderby();
		$order = $this->get_variable_order();
		$search = $this->get_variable_search();
    $driver_id = $this->get_variable_driver_id();
		$order_id = $this->get_variable_order_id();
    $status = $this->get_variable_status();
    $task_type = $this->get_variable_task_type();
		
	  $model = new \App\Task;
	  $model = $model::where('task_id' ,'<>', '0');  


		if($driver_id != '' && $driver_id != null)
		{  $model = $model->where('driver_id' , $driver_id);  }	

    if($order_id != '' && $order_id != null)
    {  $model = $model->where('order_id' , $order_id);  } 

      if($status != '' && $status != null)
    {   
      $status = explode(',',$status);
       $model = $model->whereIn('status',$status);
     } 

      if($task_type != '' && $task_type != null)
    {  $model = $model->where('task_type' , $task_type);  } 

 
    $model = $model->orderBy($orderby,$order);	
    $result = $model->paginate($per_page); 
	    
 
	      if(sizeof($result) > 0)
					{
						              $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Task List Fetched Successfully';
                          $data['data']      =   $result;  
				  }
				else
					{
						              $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Task Found';
                          $data['data']      =   [];  
					}
				  
          return $data;
   }  





  // Route-17.3 ============================================================== Get Items List =========================================> 
   public function update(Request $request , $id , $create_task_request = '')
   {
            if($create_task_request != '')
                {
                  $request = $create_task_request;
                }
 
 
             if($request['order_id'] == '')
               {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'Order Id Required';
                    return $data; 
               }
 
                 //check existance of task with ID in taks table
                    $exist = $this->model_exist($id); 
                    if($exist == 0 or $exist == '0')
                    {
                          $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Task with this ID does not exist';
                          $data['data']      =   [];
                          return $data;             
                    }


              $order_id = $this->validate_string($request['order_id']); 
              $driver_id = $this->validate_string($request['driver_id']); 
              $vendor_id = $this->validate_string($request['vendor_id']); 
              $pickup_contact_title = $this->validate_string($request['pickup_contact_title']);
              $pickup_contact_phone = $this->validate_string($request['pickup_contact_phone']);
              $pickup_time = $this->validate_string($request['pickup_time']);


              $picked_up_time = $this->validate_string($request['picked_up_time']); 
              $pickup_address = $this->validate_string($request['pickup_address']); 
              $dropoff_contact_title = $this->validate_string($request['dropoff_contact_title']);
              $dropoff_contact_phone = $this->validate_string($request['dropoff_contact_phone']);
              $dropoff_time = $this->validate_string($request['dropoff_time']);

           
              $dropped_off_time = @$this->validate_string(@$request['dropped_off_time']);
              $dropoff_address = $this->validate_string(@$request['dropoff_address']);
              $status = $this->validate_string(@$request['status']);
              $task_type = $this->validate_integer(@$request['task_type']);
              $store_id = $this->validate_integer(@$request['store_id']);

 
             
              $user =  \App\Task::where('task_id',$id)->update([
                        'order_id' => $order_id,
                        'driver_id' =>  $driver_id,
                        'vendor_id' =>  $vendor_id,
                        'pickup_contact_title' => $pickup_contact_title,
                        'pickup_contact_phone' => $pickup_contact_phone,

                        'pickup_time' => $pickup_time,
                        'picked_up_time' =>  $picked_up_time,
                        'pickup_address' => $pickup_address,
                        'dropoff_contact_title' => $dropoff_contact_title,

                        'dropoff_contact_phone' => $dropoff_contact_phone,
                        'dropoff_time' =>  $dropoff_time,
                        'dropped_off_time' => $dropped_off_time,
                        'dropoff_address' => $dropoff_address,
                        'status' => $status,
                        'store_id' => $store_id,
                        'task_type' => $task_type ]);
         
          $result = @\App\Task::where('task_id',$id)->get();
            
                  if(sizeof($result) > 0)
          {
              $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Task Updated Successfully';
                          $data['data']      =   $result;  
            }
          else
          {
              $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Unable to Update';
                          $data['data']      =   [];  
          }
           return $data;
   }  
 



  


   
 
  // Route-17.4 ============================================================== Delete a Task =========================================> 
   public function destroy(Request $request , $id)
   {

   }
  


  // Route-17.5 ============================================================== Assign Driver =========================================> 
    public function assign_driver(Request $request , $id)
   {
              if($request['driver_id'] == '')
               {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'Driver Id Required';
                    return $data; 
               }

         $driver_id =$request['driver_id'];
 
         $order_id = @\App\Task::where('task_id',$id)->first(['order_id'])->order_id;

         $driver_commission = @\App\User::where('user_id',$driver_id)->first(['commission'])->commission;

         $driver_commission = $this->validate_integer($driver_commission);
 


 
 if(intval(abs($driver_commission)) < 1   ) { $commission = '0';}

          $re =  \App\Order::where('order_id',$order_id)->update([ 'driver_commission' => $driver_commission ]);
			   
			   
          $user =  \App\Task::where('task_id',$id)->update([
                         'driver_id' =>  $request->driver_id,
                         ]);
         
          $result = @\App\Task::where('task_id',$id)->get();
            
          if(sizeof($result) > 0)
          {

                  @$this->notify($request , 'task_assigned_to_driver',@$id , $request->driver_id);


                          $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Driver Assigned Successfully';
                          $data['data']      =   $result;  
          }
          else
          {
              $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Unable to Assign';
                          $data['data']      =   [];  
          }
           return $data;
   }
 
 

  // Route-17.6 ============================================================== Update Status  =========================================> 
    public function update_status(Request $request , $id)
   {
            if($request['status'] == '')
               {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'Status Required';
                    return $data; 
               }

          $user =  \App\Task::where('task_id',$id)->update([
                         'status' =>  $request->status
                         ]);
 
            $order_id = @\App\Task::where('task_id',$id)->first(['order_id'])->order_id;
          if($request->status == 3 || $request->status == '3')
          {
           
           $create_request_data['order_status'] = 'delivered';
           $create_request_data['order_id'] = $order_id;
           app('App\Http\Controllers\Api\OrderController')->status_update($request , $order_id , $create_request_data);
            $now = @\Carbon\Carbon::now()->format('Y-m-d H:i:s')."";
             $user =  \App\Task::where('task_id',$id)->update([ 'dropped_off_time'=>$now]);
          }


          if($request->status == '4' || $request->status == 4)
          {
             $now = @\Carbon\Carbon::now()->format('Y-m-d H:i:s')."";
             $user =  \App\Task::where('task_id',$id)->update(['driver_id' =>  ''  ]);
             $order_id = @\App\Task::where('task_id',$id)->first(['order_id'])->order_id;
               $this->notify($request , 'order_dropped_off_by_driver',$order_id);
          }


             if($request->status == '2' || $request->status == 2)
          {
              $now = @\Carbon\Carbon::now()->format('Y-m-d H:i:s')."";
             $user =  \App\Task::where('task_id',$id)->update(['picked_up_time'=>$now]);
             $order_id = @\App\Task::where('task_id',$id)->first(['order_id'])->order_id;

             $create_request_data['order_status'] = 'in_transit';
             $create_request_data['order_id'] = $order_id;
             app('App\Http\Controllers\Api\OrderController')->status_update($request , $order_id , $create_request_data);
             $this->notify($request , 'order_picked_up_by_driver',$order_id);
          }
 
          $result = @\App\Task::where('task_id',$id)->get();
            
          if(sizeof($result) > 0)
          {
                         $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Status Updated Successfully';
                          $data['data']      =   $result;  
            }
          else
          {
                          $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Unable to Update';
                          $data['data']      =   [];  
          }
           return $data;

   }
 
 
  



 















   public function store_meta_values($user_id , $user_meta)
  {
	  $user_meta_type = @\App\UserMetaType::get();
	  	  
	  foreach($user_meta_type as $imt)
	  {
		$identifier = $imt->identifier;  
	 	$value = $this->validate_string(@$user_meta[$identifier]);
	    $user_meta_type_id = $this->get_user_meta_type_id($identifier);
	    
		$user_meta_value = new \App\UserMetaValue;
		$user_meta_value->user_meta_type_id = $imt->user_meta_type_id;
		$user_meta_value->user_id = @$user_id;
		$user_meta_value->value = $this->validate_string(@$value);
		$user_meta_value->save();
	  }
	  return 1;
  }
  


     public function update_meta_values($user_id , $user_meta)
  {
	  $user_meta_type = @\App\UserMetaType::get();
	  	  
	  foreach($user_meta_type as $imt)
	  {
		$identifier = $imt->identifier;  
	 	$value = $this->validate_string(@$user_meta[$identifier]);
	    $user_meta_type_id = $this->get_user_meta_type_id($identifier);
		App\UserMetaValue::where('user_id', $user_id)->where('user_meta_type_id', $user_meta_type_id)->update(['value' => $value ]);
	  }
	  return 1;
  }
  
  public function get_user_meta_type_id($identifer)
  {
	  $user_meta_type_id = @\App\UserMetaType::where('identifier',$identifer)->first(['user_meta_type_id'])->user_meta_type_id;
	  return $user_meta_type_id;
  }
  



 
 
   
//==========================================================================misc functions===================================================================//   
//check user existence by id
public function model_exist($id)
{
	$count = @\App\Task::where('task_id',$id)->count();
	if($count < 1) {
		return 0;
	}
	else{
		return 1;
	}
}	


 
 

///================================ function to check GET variable's and Defaults ====================================================//
 
 



public function get_variable_per_page()
{
	 if(isset($_GET['per_page']) && $_GET['per_page'] != null && $_GET['per_page'] != '')
					{ $per_page = $_GET['per_page']; }
					else 
					{ $per_page = 20; }
    return $per_page;
}

public function get_variable_orderby()
{
	 if(isset($_GET['orderby']) && $_GET['orderby'] != null && $_GET['orderby'] != '')
					{ $orderby = $_GET['orderby']; }
					else 
					{ $orderby = 'created_at'; }
    return $orderby;
}

public function get_variable_order()
{
	 if(isset($_GET['order']) && $_GET['order'] != null && $_GET['order'] != '')
					{ $order = $_GET['order']; }
					else 
					{ $order = 'DESC'; }
    return $order;
}

public function get_variable_search()
{
	 if(isset($_GET['search']) && $_GET['search'] != null && $_GET['search'] != '')
					{ $search = $_GET['search']; }
					else 
					{ $search = ''; }
    return $search;
}	

public function get_variable_driver_id()
{
   if(isset($_GET['driver_id']) && $_GET['driver_id'] != null && $_GET['driver_id'] != '')
          { $var = $_GET['driver_id']; }
          else 
          { $var = ''; }
    return $var;
} 

public function get_variable_order_id()
{
   if(isset($_GET['order_id']) && $_GET['order_id'] != null && $_GET['order_id'] != '')
          { $var = $_GET['order_id']; }
          else 
          { $var = ''; }
    return $var;
} 

public function get_variable_status()
{
   if(isset($_GET['status']) && $_GET['status'] != null && $_GET['status'] != '')
          { $var = $_GET['status']; }
          else 
          { $var = ''; }
    return $var;
} 

public function get_variable_task_type()
{
   if(isset($_GET['task_type']) && $_GET['task_type'] != null && $_GET['task_type'] != '')
          { $var = $_GET['task_type']; }
          else 
          { $var = ''; }
    return $var;
} 

 
 ///================================ function to check GET variable's and Defaults Ends ====================================================//
 
 
 
 
 
  
 
 
 
 
	
	
	public function paginateWithoutKey($items, $perPage = 15, $page = null, $options = [])
    {

        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

        $items = $items instanceof Collection ? $items : Collection::make($items);

        $lap = new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);

        return [
            'current_page' => $lap->currentPage(),
            'data' => $lap ->values(),
            'first_page_url' => $lap ->url(1),
            'from' => $lap->firstItem(),
            'last_page' => $lap->lastPage(),
            'last_page_url' => $lap->url($lap->lastPage()),
            'next_page_url' => $lap->nextPageUrl(),
            'per_page' => $lap->perPage(),
            'prev_page_url' => $lap->previousPageUrl(),
            'to' => $lap->lastItem(),
            'total' => $lap->total(),
        ];
    }
	
 
	
 
 
	
 
	

    
 
   public function make_thumb($src, $dest, $desired_width) 
   {

    /* read the source image */
    $source_image = imagecreatefromjpeg($src);
    $width = imagesx($source_image);
    $height = imagesy($source_image);
    
    /* find the "desired height" of this thumbnail, relative to the desired width  */
    $desired_height = floor($height * ($desired_width / $width));
    
    /* create a new, "virtual" image */
    $virtual_image = imagecreatetruecolor($desired_width, $desired_height);
    
    /* copy source image at a resized size */
    imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
    
    /* create the physical thumbnail image to its destination */
    imagejpeg($virtual_image, $dest);
    }

    /**
     * @author Dikshant
     * set user language by default
     */
 
 
 


}