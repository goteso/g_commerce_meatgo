<?php
namespace App\Http\Controllers\Api; //admin add
use App;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; // using controller class
use Auth;
use Session;
use DB;
use Validator;
use App\User;
use Carbon\Carbon;
use App\Otp;
use App\UserSocialLinks;
use App\UserLikesDislikes;
use App\UserRatings;
use App\Country;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Traits\one_signal; // <-- you'll need this line...
use App\Traits\bitcoin_price;
use App\Traits\trait_functions;
use App\Traits\notifications;
use Illuminate\Support\Arr;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Hash;
use Mail;
use File;
 
 


class OrderReviewController extends Controller 
{
	
use one_signal; // <-- ...and also this line.
use bitcoin_price; // <-- ...and also this line.
use trait_functions; // <-- ...and also this line. 
use notifications; // <-- ...and also this line. 
   
   
   
   
   
   
 /**
 Route::post('/v1/favourite-item/', array('uses' => 'FavouriteItemController@store'));//Route-66
  Route::get('/v1/favourite-item/', array('uses' => 'FavouriteItemController@get_list'));//Route-67
  Route::get('/v1/favourite-item/{id}', array('uses' => 'FavouriteItemController@show'));//Route-68
 **/

 
 





   
 // Route-16.1 ============================================================== Add Review =========================================> 
   public function store(Request $request  )
   {
                
 

              if($request['order_id'] == '')
               {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'Order Id Required';
                    return $data; 
               }

               if($request['user_id'] == '')
               {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'User Id Required';
                    return $data;	
               }

                  if($request['rating'] == '')
               {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'Rating Required';
                    return $data; 
               }

                  if($request['review'] == '')
               {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'Review Required';
                    return $data; 
               }



               $store_id = @\App\Order::where('order_id', $request['order_id'] )->first(['store_id'])->store_id;
               

               $user_id = $request['user_id'];
               $review = $request['review'];
               $rating = $request['rating'];
               $order_id = $request['order_id'];


               $count = @\App\OrderReview::where('user_id',$user_id)->where('store_id',$store_id)->where('order_id',$order_id)->count();
               if($count > 0)
               {


                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'You already gave ratings to this order';
                    return $data; 
               }

                
				        $order_review = new \App\OrderReview;
				      	$order_review->user_id = @$user_id;
					      $order_review->store_id =  @$store_id;
                $order_review->order_id = @$order_id;
                $order_review->review =  @$review;
                $order_review->rating =  @$rating;
                $order_review->save();
 


                    $customer_id = @\App\Order::where('order_id',$order_id)->first(['customer_id'])->customer_id;
                      $driver_id = @\App\Task::where('order_id',$order_id)->first(['driver_id'])->driver_id;
 
                    if($user_id == $customer_id)
                    {
                      //send notification 
                         $this->notify($request , 'order_reviewed_by_customer',$order_id);
                      //send notification ends
                    }

                    if($user_id == $driver_id)
                    {
                      //send notification 
                           $this->notify($request , 'order_reviewed_by_driver',$order_id);
                      //send notification ends
                    }


					
				    if($order_review != '')
					{
						              $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Review Submitted';
                          $data['data']      =         $order_review;  
				    }
					else
					{
						              $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Unable to Add';
                          $data['data']           =   [];  
					}
				   return $data;
				 
  }
  

   // Route-16.2 ============================================================== Get Review List =========================================> 
   public function get_list()
   {
       
    $per_page = $this->get_variable_per_page(); //ASC or DESC
    $orderby = $this->get_variable_orderby();
    $order = $this->get_variable_order();

    $store_id = $this->get_variable_store_id();
    $user_id = $this->get_variable_user_id();
    $order_id = $this->get_variable_order_id();



    $model = new \App\OrderReview;
    $model = $model::where('id' ,'<>', '0'); 


    if($order_id != '' && $order_id != '')
    {
            $model = $model->where('order_id' , $order_id);   
    }

    if($user_id != '' && $user_id != '')
    {
          $model = $model->where('user_id' , $user_id);   
    }

    if($store_id != '' && $store_id != '')
    {
         $model = $model->where('store_id' , $store_id);   
    }
 
     $model = $model->orderBy($orderby,$order);  
     $result = $model->paginate($per_page);
 
     if(sizeof($result) > 0)
          {
                          $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Review List Fetched Successfully';
                          $data['data']      =   $result;  
            }
          else
          {
                          $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Reviews Found';
                          $data['data']      =   [];  
          }
           return $data;
   }  

 

 


//Route-16.3
public function destroy($id)
{
                          $deleted = @\App\OrderReview::where('id',$id)->delete();
                          $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Review Deleted Successfully';
                          $data['data']      =   [];  
                          return $data;
}
 



 
   
//==========================================================================misc functions===================================================================//   
//check item existence by id
public function item_exist($id)
{
	$count = @\App\Items::where('item_id',$id)->count();
	if($count < 1) {
		return 0;
	}
	else{
		return 1;
	}
}	

 

//validate all the request variables if they are null or empty , it will return values
public function validate_datetime($var)
{
	 if(!isset($var) or $var == null or $var == '' or $var == ' ')
	 {
	 	 $date = @\Carbon\Carbon::now();
	 	 $date = @\Carbon\Carbon::parse($date);
		 return $this->add_days($date , '7' , 'Y-m-d h:i:s');
	 }
	 else{
		 return $var;
	 }
}


 
  
 
  
  public function get_item_meta_type_id($identifer)
  {
	  $item_meta_type_id = @\App\ItemMetaType::where('identifier',$identifer)->first(['item_meta_type_id'])->item_meta_type_id;
	  return $item_meta_type_id;
  }
  



///================================ function to check GET variable's and Defaults ====================================================//
public function get_variable_per_page()
{
	 if(isset($_GET['per_page']) && $_GET['per_page'] != null && $_GET['per_page'] != '')
					{ $per_page = $_GET['per_page']; }
					else 
					{ $per_page = 20; }
    return $per_page;
}

public function get_variable_orderby()
{
	 if(isset($_GET['orderby']) && $_GET['orderby'] != null && $_GET['orderby'] != '')
					{ $orderby = $_GET['orderby']; }
					else 
					{ $orderby = 'created_at'; }
    return $orderby;
}

public function get_variable_order()
{
	 if(isset($_GET['order']) && $_GET['order'] != null && $_GET['order'] != '')
					{ $order = $_GET['order']; }
					else 
					{ $order = 'DESC'; }
    return $order;
}


public function get_variable_search()
{
	 if(isset($_GET['search']) && $_GET['search'] != null && $_GET['search'] != '')
					{ $search = $_GET['search']; }
					else 
					{ $search = ''; }
    return $search;
}	





public function get_variable_store_id()
{
   if(isset($_GET['store_id']) && $_GET['store_id'] != null && $_GET['store_id'] != '')
          { $store_id = $_GET['store_id']; }
          else 
          { $store_id = ''; }
    return $store_id;
} 
public function get_variable_user_id()
{
   if(isset($_GET['user_id']) && $_GET['user_id'] != null && $_GET['user_id'] != '')
          { $user_id = $_GET['user_id']; }
          else 
          { $user_id = ''; }
    return $user_id;
} 
public function get_variable_order_id()
{
   if(isset($_GET['order_id']) && $_GET['order_id'] != null && $_GET['order_id'] != '')
          { $order_id = $_GET['order_id']; }
          else 
          { $order_id = ''; }
    return $order_id;
} 
 
      
 


 



  
 


 
 
 ///================================ function to check GET variable's and Defaults Ends ====================================================//
 
 
 
 
 
  
 
 
 
 
	
	
	public function paginateWithoutKey($items, $perPage = 15, $page = null, $options = [])
    {

        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

        $items = $items instanceof Collection ? $items : Collection::make($items);

        $lap = new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);

        return [
            'current_page' => $lap->currentPage(),
            'data' => $lap ->values(),
            'first_page_url' => $lap ->url(1),
            'from' => $lap->firstItem(),
            'last_page' => $lap->lastPage(),
            'last_page_url' => $lap->url($lap->lastPage()),
            'next_page_url' => $lap->nextPageUrl(),
            'per_page' => $lap->perPage(),
            'prev_page_url' => $lap->previousPageUrl(),
            'to' => $lap->lastItem(),
            'total' => $lap->total(),
        ];
    }
	
	
	
	 public function paginate($items, $perPage = 15, $page = null, $options = [])
{
	$page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
	$items = $items instanceof \Collection ? $items : Collection::make($items);
	return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
}
	 
	 	public function add_trades_feedback(Request $request)
{
	 
        $validator = Validator::make($request->all(), [
           
	 
        ]);
        if ($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';
            $data['message']        =   $validator->errors()->first();
        }
        else
        {
        
		
			$trade_id = $request->trade_id;
		    $user_id = $request->user_id;
			$feedback = $request->feedback;
			$ratings = $request->ratings;
		  
		  
		    $order_seller_id = @\App\Trades::where('id',$request->trade_id)->first(['seller_id'])->seller_id;
			$order_buyer_id = @\App\Trades::where('id',$request->trade_id)->first(['buyer_id'])->buyer_id;
			
			if($user_id == $order_seller_id)
			{
				 
				 \App\Trades::where('id',$request->trade_id)->update([ 'seller_ratings' => $ratings , 'seller_feedback' => $feedback]);
			}
			
		 
			if($user_id == $order_buyer_id)
			{
				 
				 \App\Trades::where('id',$request->trade_id)->update([ 'buyer_ratings' => $ratings , 'buyer_feedback' => $feedback]);
			}
		  
 
		
 
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';
            $data['message']        =   'Submitted successfully';

        }
        return $data;
    }
	
	
	
	
 
 
	
 
	

    
 
   public function make_thumb($src, $dest, $desired_width) 
   {

    /* read the source image */
    $source_image = imagecreatefromjpeg($src);
    $width = imagesx($source_image);
    $height = imagesy($source_image);
    
    /* find the "desired height" of this thumbnail, relative to the desired width  */
    $desired_height = floor($height * ($desired_width / $width));
    
    /* create a new, "virtual" image */
    $virtual_image = imagecreatetruecolor($desired_width, $desired_height);
    
    /* copy source image at a resized size */
    imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
    
    /* create the physical thumbnail image to its destination */
    imagejpeg($virtual_image, $dest);
    }

    /**
     * @author Dikshant
     * set user language by default
     */
 
 
 


}