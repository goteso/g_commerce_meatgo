<?php
namespace App\Http\Controllers\Api; //admin add
use App;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; // using controller class
use Auth;
use Session;
use DB;
use Validator;
use App\User;
use Carbon\Carbon;
use App\Otp;
use App\UserSocialLinks;
use App\UserLikesDislikes;
use App\UserRatings;
use App\Country;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Traits\one_signal; // <-- you'll need this line...
use App\Traits\bitcoin_price;
use App\Traits\trait_functions;
use App\Traits\notifications;
use Illuminate\Support\Arr;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Hash;
use Mail;
use File;
 
 


class OrderController extends Controller 
{
	

use one_signal; // <-- ...and also this line.
use bitcoin_price; // <-- ...and also this line.
use trait_functions; // <-- ...and also this line. 
use notifications; // <-- ...and also this line. 
   
   
   
 
 

public function apply_setting_tax_to_order($order_total , $store_id )
{
     $setting_tax = @\App\SettingTax::get();
     $total_tax_amount = 0;
    // $store_tax_percentage = @\App\Store::where('store_id',$store_id)->first(['store_tax'])->store_tax;

     $display_array = array(); // for calculate function only
     foreach($setting_tax as $tax)
     {
      // $tax_amount = floatval($tax->percentage) / 100 * $order_total;
      $tax_amount = floatval($tax->percentage ) / 100 * $order_total;
      $tax['order_transaction_tax_amount'] = round($tax_amount , 2 );
      $total_tax_amount = $total_tax_amount + $tax_amount;

      $display_array_data['title'] =  $tax->title;
      $display_array_data['value'] =  round($tax_amount , 2 );
      $display_array[] = $display_array_data;
     }
      $order_total = $order_total + $total_tax_amount;

      $d['setting_tax'] = $setting_tax;
      $d['total_tax_amount'] = round( $total_tax_amount , 2 );
      $d['order_total'] = round($order_total , 2 );
      $d['display_array'] = $display_array;
      return $d;
}




   
 // Route-13.1 ============================================================== Store Item to Items table =========================================> 
   public function store(Request $request )
   {






       $customer_id = @$request->customer_id;
       $store_id = @$request->store_id;



       if($store_id != '' || $store_id != null)
       {
          @\App\Store::where('store_id' , $store_id)->increment('popular_count',1);
       }

 
              $items = @$request->items;
              $order_meta = @$request->order_meta;
              $coupon_code = @$request->coupon_code;


              if($customer_id == '' or $customer_id == null)
               {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'Customer Id Required';
                    return $data; 
               }
            
           
 
              //apply coupon process
              $items_with_variant_price = $this->add_variant_price_to_order_items($items); // also quantity calculation is done here
              $order_total = $this->total_order_amount_from_items($items_with_variant_price); 







              if($request->loyalty_points != '1')
              {
                    $apply_coupon_response = app('App\Http\Controllers\Api\CouponsController')->apply($request);
                 $discount = $apply_coupon_response['discount']; // returned discount in Amount
                 $discount_message = @$apply_coupon_response['message']; // returned discount in Amount
                 $loyalty_point_id = @$apply_coupon_response['loyalty_point_id'];
              }
              else
              {
                 $apply_loyalty_points_response = app('App\Http\Controllers\Api\LoyaltyPointsController')->apply($request , $customer_id);
                 $discount = $apply_loyalty_points_response['discount']; // returned discount in Amount
                  $discount_message = $apply_loyalty_points_response['message']; // returned discount in Amount
                 $applicable_points = $apply_loyalty_points_response['applicable_points'];
              }
 

             $order_total = $order_total -  $discount;
 
 
             // apply taxes
               $total_tax = $this->apply_setting_tax_to_order($order_total , $store_id);
              $setting_tax = @$total_tax['setting_tax']; // needs to insert into  =  order_tax_transaction
              $order_total = @$total_tax['order_total'];
              $items_sub_total = $order_total; //included with coupon and Tax Also , will be inserted to items_sub_total
  

             //Calculate Delivery Fee According to Settings and Distance between store and delivery address
              $order_meta =  $request["order_meta"]['fields'];
              $delivery_fee = 0;
                 $delivery_address = $this->get_order_meta_value_from_identifier($order_meta , 'delivery_address');
                if($delivery_address == '' || $delivery_address == null)
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'Delivery Address Missing';
                
                
                
               }


 



                              //Calculate delivery fee
               if($delivery_address != '' && $delivery_address != null)
               {
		                $delivery_address = json_decode($delivery_address , true);
		                $delivery_fee = $this->get_delivery_fee($delivery_address , $store_id );
		                $status_code = $delivery_fee['status_code'];
		                if($status_code == 0 || $status_code == '0')
		                {
		                  return $delivery_fee;
		                }

		                $delivery_fee = $delivery_fee['delivery_fee'];
		                if($delivery_fee == '' || $delivery_fee == null || $delivery_fee == ' ')
		                {
		                  $delivery_fee = 0;
		                }
		                    $delivery_fee_data['title'] = 'Delivery Fee';
		                    $delivery_fee_data['value'] = $delivery_fee;
		                    $display_array[] = $delivery_fee_data;
		                    $order_total = $order_total + $delivery_fee;
              }
             






 


              //insert data to order table
              $first_order_status = @\App\SettingOrderStatus::first();
              $first_order_status = $first_order_status['identifier'];
              $order = new \App\Order;
           
              $order->order_status =   $first_order_status;
              $order->customer_id = @$request['customer_id'];
              $order->store_id =@$this->validate_integer(@$request['store_id']);
              $order->total =round(@$order_total,2);
              $order->save();


              $order_id =   $order->id;


               if($request->loyalty_points != '1')
              {
                  @\App\LoyaltyPoints::where('id', $loyalty_point_id)->update(['source' => $order_id]);
              }



              //insert order tax transactions
              foreach($setting_tax as $tax)
              {
                  $order_transaction_tax = new \App\OrderTransactionTax;
                  $order_transaction_tax->order_id = @$order_id;
                  $order_transaction_tax->setting_tax_id =   @$tax->setting_tax_id;
                  $order_transaction_tax->setting_tax_title =   @$tax->title;
                  $order_transaction_tax->order_transaction_tax_amount = @$tax->order_transaction_tax_amount;
                  $order_transaction_tax->save();
              }

              //insert order transaction Discount=====================================================
              if($request->loyalty_points == '1' || $request->loyalty_points == 1)
              {

                    
                      $order_loyalty_points_expiry_day_count =  @\App\Setting::where('key_title','order_loyalty_points_expiry_day_count')->first(['key_value'])->key_value;

                      if($order_loyalty_points_expiry_day_count > 0)
                      {
                       $today = @\Carbon\Carbon::now();
                       $expiry_date = $today->addDays($order_loyalty_points_expiry_day_count);
                      }
                      else
                      {
                        $expiry_date = '';
                      }

 
                      $LoyaltyPoints = new \App\LoyaltyPoints;
                      $LoyaltyPoints->user_id = $this->validate_string(@$customer_id);
                      $LoyaltyPoints->points = "-".abs($applicable_points);
                      $LoyaltyPoints->expiry_date = $expiry_date;
                      $LoyaltyPoints->type = 'order_spent';
                      $LoyaltyPoints->source = $order_id;
                      $LoyaltyPoints->save();
              }


                 
                  @$coupon_id = @\App\Coupons::where('coupon_code' , $coupon_code)->first(['coupon_id'])->coupon_id;

                  $order_transaction_coupon = new \App\OrderTransactionDiscount;
                  $order_transaction_coupon->order_id = @$order->id;
                  $order_transaction_coupon->user_id =  $this->validate_string( @$request->customer_id);
                  $order_transaction_coupon->coupon_id = $this->validate_string(@$coupon_id);
                  $order_transaction_coupon->coupon_code =$this->validate_string( @$request->coupon_code);
                  $order_transaction_coupon->discount_amount = $this->validate_integer(@$discount);
                  $order_transaction_coupon->type = $this->validate_integer(@$discount_type);
                  $order_transaction_coupon->loyalty_points_id = $this->validate_integer(@$LoyaltyPoints->id);
                  $order_transaction_coupon->save();
            


 

  //insert products and variants
  if( sizeof($items_with_variant_price) > 0)
    {
      for($t=0;$t<sizeof($items_with_variant_price);$t++)
      {

            $order_item = new \App\OrderItem;
            $order_item->order_id = @$order_id;
            $order_item->item_id = @$items_with_variant_price[$t]['item_id'];
            $order_item->item_photo = @$items_with_variant_price[$t]['item_photo'];
            $order_item->item_thumb_photo = @$items_with_variant_price[$t]['item_thumb_photo'];
            $order_item->item_title =@$items_with_variant_price[$t]['item_title'];
            $order_item->order_item_quantity =@$items_with_variant_price[$t]['quantity'];
          
         
                 $unit_item_meta_type_id = @\App\ItemMetaType::where('identifier','unit')->first(['item_meta_type_id'])->item_meta_type_id;
                 $unit = @\App\ItemMetaValue::where('item_meta_type_id',$unit_item_meta_type_id)->where('item_id',@$items_with_variant_price[$t]['item_id'])->first(['value'])->value;
               
 
                  if($unit == '' || $unit == null)
                  {
                   $order_item->order_item_unit = 'lbs';
                  }
                  else
                  {
                    $order_item->order_item_unit = $unit;
                  }



            $order_item->item_price =@$items_with_variant_price[$t]['item_price'];
            $order_item->order_item_discount = @$items_with_variant_price[$t]['item_discount']."";
            $order_item->save();
         

       
        $order_items_variants = @$items_with_variant_price[$t]['variants'];
        if( sizeof($order_items_variants) > 0)
            {
              
           
              for($y=0;$y<sizeof($order_items_variants);$y++)
                {
                 
                     $item_variant_type_id =  @$order_items_variants[$y]['item_variant_type_id'];
                     $item_variant_value_id = @$order_items_variants[$y]['item_variant_value_id'];
                     
                     $item_variant_value_title =  $order_items_variants[$y]['item_variant_value_title'];
                     $item_variant_type_title = @\App\ItemVariantType::where('item_variant_type_id',$item_variant_type_id)->first(['item_variant_type_title'])->item_variant_type_title;
                    
                 
                    $order_item_variant = new \App\OrderItemVariant;
                    $order_item_variant->order_item_id = @$order_item->id;
                    $order_item_variant->item_variant_id = @$item_variant_value_id;
                    $order_item_variant->item_variant_title = @$item_variant_type_title;
                    $order_item_variant->order_item_variant_value = @$item_variant_value_title;
                    $order_item_variant->save();
                }
            
           
             }
      
      
      
           }


         }









$order_meta = array();
$inner_array = array();
//insert meta data
 $t=array();
$order_meta =  $request["order_meta"]['fields'];
  if(sizeof($order_meta) > 0 )
     { 
                $order_meta_array_size = sizeof($order_meta);
                for($sss=0; $sss < $order_meta_array_size; $sss++)
                {
                    $inner_array = $order_meta[$sss];
 
                    $inner_array_size =  @sizeof($inner_array);
                         for($j=0;$j<$inner_array_size;$j++)
                            {
                                     $identifier = @$inner_array[$j]['identifier'];
                                     $setting_order_meta_type_id = @$inner_array[$j]['setting_order_meta_type_id'];
                                     $input_type = @$inner_array[$j]['input_type'];
                                     $order_meta_model = new \App\OrderMetaValue;

                                     $order_meta_model->order_id = $order_id;
                                     $order_meta_model->setting_order_meta_type_id = @$inner_array[$j]['setting_order_meta_type_id'];
                                     $order_meta_model->setting_order_meta_type_title = @$inner_array[$j]['setting_order_meta_type_title'];


                                     $setting_order_meta_type_identifier = @\App\SettingOrderMetaType::where('setting_order_meta_type_id', @$inner_array[$j]['setting_order_meta_type_id'] )->first(['identifier'])->identifier;

                                     $order_meta_model->order_meta_value_text = $this->validate_string($inner_array[$j]['value']);

                                     if($input_type == 'address')
                                      {

                                       if(  $setting_order_meta_type_identifier  == '' || $inner_array[$j]['value'] == null || $inner_array[$j]['value'] == '{}') 
                                       {
                                                $store_address_details = @\App\Store::where('store_id',$store_id)->get(['address','latitude','longitude']);

                                               @$store_address_details[0]['address'];
                                                 $address_json['address_line1'] = @$store_address_details[0]['address'];
                                               $address_json['latitude'] = @$store_address_details[0]['latitude'];
                                               $address_json['longitude'] = @$store_address_details[0]['longitude'];


                                               $order_meta_model->order_meta_value_text = $this->validate_string(json_encode($address_json));
                                               $order_meta_model->order_meta_value_linked_id =  $this->store_order_address_for_restaurant_pickup($address_json , $setting_order_meta_type_id);
                                       }
                                       else

                                       {
                                         $address_json = json_decode($inner_array[$j]['value']);
                                         $order_meta_model->order_meta_value_linked_id =  $this->store_order_address($address_json , $setting_order_meta_type_id);
                                       }
                                 

 
 
                                        
                                           
                                      }
                                    else 
                                      {
                                         $order_meta_model->order_meta_value_linked_id = '';
                                      }
                                     $order_meta_model->save();


                                
                            }
                }   
      }






//update order tables with commissions
 
         
           $items_sub_total = $this->validate_integer($items_sub_total);
           $store_commission = @\App\Store::where('store_id',$store_id)->first(['commission'])->commission;
 
           $item =  \App\Order::where('order_id',$order_id)->update([
          'delivery_fee' => $this->validate_integer($delivery_fee),
          'items_sub_total' => $this->validate_integer($items_sub_total),
          'store_commission' => $this->validate_integer($store_commission),
       ]);
         
 

     //inserting task to tasks table
 
    // $task_data = $this->get_task_data($order_meta , $order_id , $store_id);
 
    //  app('App\Http\Controllers\Api\TaskController')->store($request ,$task_data);
  //send notification 
                   @$this->notify($request , 'order_placed',@$order_id , '');
                  //send notification ends

 


          if($order_id != '')
          {
                          $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Order Placed Successfully';
                          $data['discount_message']        =   $discount_message;
                          $main=array();
                          $d['order_id'] = $order_id;
                          $main[] = $d;
                          $data['data']      =   $main;  
            }
          else
          {
                          $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Some Error Occurred';
                          $data['data']      =   [];  
          }
           return $data;






 }


public function get_delivery_fee( $delivery_address , $store_id )
{
 

 if( getType($delivery_address) == 'array')
 {

 }
 else
 {
  $delivery_address = json_decode($delivery_address , true);
 }
         

 
         $delivery_latitude = $delivery_address['latitude'];
         $delivery_longitude = $delivery_address['longitude'];

         $store_details =@\App\Store::where('store_id',$store_id)->get(['latitude','longitude']);
         $store_latitude = @$store_details[0]['latitude'];
         $store_longitude = @$store_details[0]['longitude'];

         $google_api_result =file_get_contents("https://maps.googleapis.com/maps/api/distancematrix/json?units=mile&origins=".$store_latitude.",".$store_longitude."&destinations=".$delivery_latitude.",".$delivery_longitude."&key=AIzaSyCo0S5dcqwj11plZQyOn7Sx6VJPHQPVZko");
                    //https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=Washington,DC&destinations=New+York+City,NY&key=YOUR_API_KEY
         $google_api_result = json_decode($google_api_result , true );
         $distance_in_m =  @$google_api_result['rows'][0]['elements'][0]['distance']['value'];

         $distance_in_m = $this->validate_integer($distance_in_m);
         $distance_in_miles = $distance_in_m / 1609.34;
         $distance_in_miles = round($distance_in_miles , 2);
         $distance_in_miles = floatval($distance_in_miles);


 
         if($distance_in_miles > 24.99)
         {
                          $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Delivery beyound 25 miles not available';
                          $data['data']      =   []; 
                          return $data;
         }


       //  return 'Orig_latstore ='.$store_latitude.' ,Orig_longstore='.$store_longitude.' , DestLatdel = '.$delivery_latitude.' Deslongdel ='.$delivery_longitude.", Distance = ".$distance_in_miles;

         $delivery_fee_key_title = 'delivery_fee_1';
         if($distance_in_miles > 0 && $distance_in_miles < 5)
         {
           $delivery_fee_key_title = 'delivery_fee_1';
         }
           if($distance_in_miles > 4.9999999999 && $distance_in_miles < 10)
         {
           $delivery_fee_key_title = 'delivery_fee_2';
         }
           if($distance_in_miles > 9.9999999999 && $distance_in_miles < 15)
         {
           $delivery_fee_key_title = 'delivery_fee_3';
         }
           if($distance_in_miles > 14.9999999999 && $distance_in_miles < 20)
         {
           $delivery_fee_key_title = 'delivery_fee_4';
         }
           if($distance_in_miles > 19.9999999999 && $distance_in_miles < 25)
         {
           $delivery_fee_key_title = 'delivery_fee_5';
         }

         if($distance_in_miles > 24.99999999999)
         {
          $delivery_fee_key_title = 'delivery_fee_5';
         }

           $delivery_fee = @\App\Setting::where('key_title',$delivery_fee_key_title)->first(['key_value'])->key_value;
       

                          $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Delivery Fee Calculated';
                          $data['data']      =   []; 
                          $data['delivery_fee']      =   floatval($delivery_fee); 
                          return $data;
  }





public function store_order_address_for_restaurant_pickup($data , $setting_order_meta_type_id)
{
 
  //{\"address_id\":85,\"address_line1\":\"3 tony villa\",\"address_line2\":\"plot 10\",\"city\":\"thane\",\"state\":\"maharashtra\",\"country\":\"india\",\"pincode\":\"400604\",\"created_at\":\"2018-05-05 13:38:35\",\"$$hashKey\":\"object:525\"}
 
              $address = new App\Address;
              $address->address_type = 'order';
              $address->linked_id = $setting_order_meta_type_id;
              $address->address_title = $this->validate_string(@$data['address_title'] );
              $address->address_phone = $this->validate_string(@$data['address_phone']);
              $address->address_line1 = $this->validate_string(@$data['address_line1'] );
              $address->address_line2 = $this->validate_string(@$data['address_line2'] );
              $address->latitude = $this->validate_string(@$data['latitude'] );
              $address->longitude =$this->validate_string( @$data['longitude'] );
              $address->city = $this->validate_string(@$data['city'] );
              $address->state = $this->validate_string(@$data['state'] );
              $address->pincode = $this->validate_string(@$data['pincode']);
              $address->country = $this->validate_string(@$data['country'] );
              $address->save();
              $address_id = $address->id; 
              return $address_id;
}

//function to get particular identifier's value from meta data
 public function get_order_meta_value_from_identifier(  $order_meta , $identifier)
 {
 
$inner_array = array();
//insert meta data
 $t=array();
 
  if(sizeof($order_meta) > 0 )
     { 
                $order_meta_array_size = sizeof($order_meta);
                for($sss=0; $sss < $order_meta_array_size; $sss++)
                {
                    $inner_array = $order_meta[$sss];
 
                    $inner_array_size =  @sizeof($inner_array);
                         for($j=0;$j<$inner_array_size;$j++)
                            {
                                     $identifier_json = @$inner_array[$j]['identifier'];
                                   
                                    if($identifier_json == $identifier)
                                    {
                                     return  $required_meta_value = @$inner_array[$j]['value'];
                                    }
                            }
                }   
      }
 }


public function get_task_data($order_meta , $order_id , $store_id)
{
	$order_meta_array_size = sizeof($order_meta);
	           for($sss=0; $sss < $order_meta_array_size; $sss++)
                {
                    $inner_array = $order_meta[$sss];
 
                    $inner_array_size =  @sizeof($inner_array);
                         for($j=0;$j<$inner_array_size;$j++)
                            {
                                     $identifier = $inner_array[$j]['identifier'];
                                  
                                     if($identifier == 'delivery_address')
                                     {
                                     	$dropoff_address = $this->validate_string(@$inner_array[$j]['value']);
                                     }

                                      if($identifier == 'delivery_time')
                                     {
                                     	$delivery_time = $this->validate_string(@$inner_array[$j]['value']);
                                     }

                                     if($identifier == 'customer_id')
                                     {
                                     	$customer_id = $this->validate_string(@$inner_array[$j]['value']);
                                     	$customer_details = @\App\User::where('user_id',$customer_id)->get();

                                     	$dropoff_contact_title = @$customer_details[0]["first_name"]." ".@$customer_details[0]["last_name"]; 
                                     	$dropoff_contact_phone = @$customer_details[0]["phone"];  
                                     }
                              	
                            }

                            
                } 

                    $store_details = @\App\Store::where('store_id',$store_id)->get();
                	  $task_data['order_id'] = $order_id;
										$task_data['driver_id'] = '';
										$task_data['vendor_id'] = @$store_details[0]['vendor_id'];
										$task_data['store_id'] = $store_id;
										$task_data['pickup_contact_title'] = @$store_details[0]['store_title'];
										$task_data['pickup_contact_phone'] = @\App\User::where('user_id',@$store_details[0]['manager_id'])->first(['phone'])->phone;
										$task_data['pickup_time'] = @\Carbon\Carbon::now();

										$task_data['picked_up_time'] = '';
										$task_data['pickup_address'] =@$store_details[0]['address'];
										$task_data['dropoff_contact_title'] = @$dropoff_contact_title;
										$task_data['dropoff_contact_phone'] = @$dropoff_contact_phone;

										$task_data['dropoff_time'] = @$delivery_time;
										$task_data['dropped_off_time'] = '';
										$task_data['dropoff_address'] = @$dropoff_address;

										$task_data['status'] = '0';
										$task_data['task_type'] = 'order';

                    return $task_data;
}
  

public function store_order_address($data , $setting_order_meta_type_id)
{
 
 

  //{\"address_id\":85,\"address_line1\":\"3 tony villa\",\"address_line2\":\"plot 10\",\"city\":\"thane\",\"state\":\"maharashtra\",\"country\":\"india\",\"pincode\":\"400604\",\"created_at\":\"2018-05-05 13:38:35\",\"$$hashKey\":\"object:525\"}


              $address = new App\Address;
              $address->address_type = 'order';
              $address->linked_id = $setting_order_meta_type_id;
              $address->address_title = $this->validate_string(@$data->address_title );
              $address->address_phone = $this->validate_string(@$data->address_phone );
              $address->address_line1 = $this->validate_string(@$data->address_line1 );
              $address->address_line2 = $this->validate_string(@$data->address_line2 );
              $address->latitude = $this->validate_string(@$data->latitude );
              $address->longitude =$this->validate_string( @$data->longitude );
              $address->city = $this->validate_string(@$data->city );
              $address->state = $this->validate_string(@$data->state );
              $address->pincode = $this->validate_string(@$data->pincode );
              $address->country = $this->validate_string(@$data->country );
              $address->save();
                    $address_id = $address->id; 

                    return $address_id;
}




     // Route-13.2 ============================================================== Get Orders List =========================================> 
   public function get_list()
   {
    $per_page = $this->get_variable_per_page(); //ASC or DESC
    $orderby = $this->get_variable_orderby();
    $order = $this->get_variable_order();
    $store_id = $this->get_variable_store_id();
    $customer_id = $this->get_variable_customer_id();
    $fields = $this->get_variable_fields();
    $include_count_blocks = $this->get_variable_include_count_blocks();

    //reports starts
    $columns_fields_array = array();
    $data_array = array();
    $table_fields = $this->get_variable_table_fields(); //for reports
    //reports ends here
       
 
    $status = $this->get_variable_status(); //default Pending
    


 

    //user types filters
    $auth_user_id = $this->get_auth_user_id();
     $user_type = $this->get_auth_user_type();
    if($user_type == '4') //store
    {
       $store_id = @\App\Store::where('manager_id',$auth_user_id)->first(['store_id'])->store_id;
       if($store_id == '' || $store_id == null) { $store_id = 'NA';}
    }
    //user type_filter check ends 
 
    $model = new \App\Order;
    $model = $model::where('order_id' ,'<>', '0');  
   
    if($status != '' && $status != null && $status != 'any')
    { 

        $model = $model->where('order_status' , $status); 

     } 

 

   //user type_filter check=============
      if($user_type == '3') //vendor
    {
          $store_id_array = @\App\Store::where('vendor_id',$auth_user_id)->pluck('store_id');
          $model = $model->whereIn('store_id' , $store_id_array);   
    }
    //user type_filter check ends

 

      if($store_id != '' && $store_id != null)
    {  

      $model = $model->where('store_id' , $store_id);  } 

      if($customer_id != '' && $customer_id != null)
    {  $model = $model->where('customer_id' , $customer_id);  } 


    



 if(isset($_GET['type']) && $_GET['type'] == 'report')
    {
//reports starts here ===========
        if($table_fields !='' && $table_fields != null)
        { $table_fields_array = explode(",",$table_fields); }
        else  { $table_fields_array = [];  }
        
        if(isset($_GET["date_from"]) && $_GET["date_from"] != '')
        { $date_from = $_GET["date_from"]; 
        }
        else
        {  $todays_date = @\Carbon\Carbon::now()->format('Y-m-d');
           $past_date = @\Carbon\Carbon::parse($todays_date)->addDays(-90);
           $date_from = $past_date; 
         }

       if(isset($_GET["date_to"]) && $_GET["date_to"] != '')
        {   $date_to = $_GET["date_to"]; }
        else
        {   $todays_date = @\Carbon\Carbon::now()->format('Y-m-d');
            $date_to = $todays_date;  
        }

            $date_to =\Carbon\Carbon::parse($date_to)->format('Y-m-d');
            $model = $model->where('created_at','<',$date_to)->where('created_at','>',$date_from); 
            $result = $model->get();
            return $result;
        }
//reports ends here ===========


    $model = $model->orderBy($orderby,$order);  

    if($fields != '' && $fields != null)
    {
      $fields_array = explode(",",$fields);
       $result = $model->paginate($per_page,$fields_array);
    }
    else
    {
       $result = $model->paginate($per_page);
    }




 

        if($include_count_blocks == 'true')
        {
             $order_status = @\App\SettingOrderStatus::get();
        
             foreach($order_status as $status)
             {
            

                           if($user_type == '4') //store
                            {
                                 $status['count'] = @\App\Order::where('order_status',$status->identifier)->where('store_id',$store_id)->count();
                                 $status['total_orders_amount'] = round(\App\Order::where('order_status',$status->identifier)->where('store_id',$store_id)->sum('total') , 2);  
                            }
                            else
                            {
                                 $status['count'] = @\App\Order::where('order_status',$status->identifier)->count();
                                 $status['total_orders_amount'] = round(\App\Order::where('order_status',$status->identifier)->sum('total') , 2);  
                            }
 
             }
             $count_blocks_array = $order_status;
        }
        else
        {
          $count_blocks_array = array();
        }
 

 
     
          if(sizeof($result) > 0)
          {
                          $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Item List Fetched Successfully';
                          $data['data']           =   $result; 
                          $data['count_data']     =   $count_blocks_array;  
            }
          else
          {
                          $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Orders Found';
                          $data['data']           =   [];  
                          $data['count_data']     =   $count_blocks_array;  
          }
           return $data;
   }  











 // Route-13.3 ============================================================== Get Order Details =========================================> 
 
 
  public function show(   $id , $create_request_data = '')
  {



     $restaurant_pickup_or_not = @\App\OrderMetaValue::where('order_id',$id)->where('order_meta_value_text','restaurant_pickup')->count();
     if($restaurant_pickup_or_not > 0)
     {
             $order_delivery_type = 'restaurant_pickup';
     }
     else
     {
              $order_delivery_type = 'home_delivery';
     }


 


    if($create_request_data != '' && $create_request_data != null)
    {
      $request = $create_request_data;
    }
      $order_id = $id;
 
      //get_order_basic_data
       $order_details = @\App\Order::where('order_id',$order_id)->get();


      //fetching customer_details
      $customer_id = $order_details[0]['customer_id'];
      $customer_details = @\App\User::where('user_id',$customer_id)->get();    //  $order_details = $this->get_order_meta_value( $request , $order_id , 'customer_id');
 
  
      //order meta details
      $order_meta_values = @\App\OrderMetaValue::where('order_id',$order_id)->get(['order_meta_value_id','setting_order_meta_type_id','setting_order_meta_type_title','order_meta_value_text','order_meta_value_linked_id']);
      foreach($order_meta_values as $omv)
      {
        if( $omv->order_meta_value_linked_id != '' )
        {
         $omv->order_meta_value_text = @\App\Address::where('address_id',$omv->order_meta_value_linked_id)->get(['address_title','address_line1','address_line2','address_phone','city','state','country','pincode']);
        }
      }

  
    //order items
    $order_items = @\App\OrderItem::where('order_id',$order_id)->get(['item_id','order_item_id','item_title','order_item_quantity','order_item_unit','item_price','order_item_discount','item_photo','item_thumb_photo']);
    $order_items_sub_total = 0;
    foreach($order_items as $order_item)
    {
      $order_items_sub_total = $order_items_sub_total + floatval($order_item->item_price);
      $order_item->order_item_variant = @\App\OrderItemVariant::where('order_item_id',$order_item->order_item_id)->get();
    }

 
   //order coupon discount
    $total_discount = 0;
    $order_transaction_coupon = @\App\OrderTransactionDiscount::where('order_id',$order_id)->get(['coupon_code','discount_amount','loyalty_points_id','type']);
    foreach($order_transaction_coupon as $transaction_discount)
    {
       $coupon_code = $transaction_discount->coupon_code;
       $total_discount = $total_discount + floatval($transaction_discount->discount_amount);

    }
  
 
   // order transaction tax
    $order_transaction_tax = @\App\OrderTransactionTax::where('order_id',$order_id)->get(['setting_tax_id','setting_tax_title','order_transaction_tax_amount']);
 
 
 
   //addresses starts
   $setting_order_meta_type = @\App\SettingOrderMetaType::where('input_type','address')->get();
   $address_array = array();
          
                foreach($setting_order_meta_type as $mt)
                {
                    $order_meta_value_linked_id = @\App\OrderMetaValue::where( 'order_id',$id )->where('setting_order_meta_type_id',$mt->setting_order_meta_type_id)->first(['order_meta_value_linked_id'])->order_meta_value_linked_id;
                    $address_data['type'] = @\App\SettingOrderMetaType::where('setting_order_meta_type_id',$mt->setting_order_meta_type_id)->first(['identifier'])->identifier;
                    $address_data['title'] = $mt->setting_order_meta_type_title;
                    $address_data['data']= @\App\Address::where('address_id',$order_meta_value_linked_id)->get();

                    if($order_delivery_type == 'home_delivery')
                    {
                      $blocks[]  = $address_data;
                    }


                   
                }
 
         //Order Meta Starts
           $meta_data = array();
           $setting_order_meta_type = @\App\SettingOrderMetaType::where('important','1')->get();
            foreach($setting_order_meta_type as $mt)
                {
                    $meta_data_obj['title'] = $mt->setting_order_meta_type_title; 
  $mt->setting_order_meta_type_id;
                     $order_meta_value_text = @\App\OrderMetaValue::where( 'order_id',$id )->where('setting_order_meta_type_id',$mt->setting_order_meta_type_id)->first(['order_meta_value_text'])->order_meta_value_text;


                  

                  

 

              

                         
                            if($order_meta_value_text == 'restaurant_pickup') {  $order_meta_value_text = 'Store Pickup'; }  
                            if($order_meta_value_text == 'home_delivery') { $order_meta_value_text = 'Home Delivery'; }                  
                            $meta_data_obj['value'] = $order_meta_value_text; 
                     
 $meta_data[] = $meta_data_obj;

                     
                    
                    
                     
                 }

                
         $meta_block['type'] = 'order_meta';
         $meta_block['title'] = 'Additional Details';
         $meta_block['data'] = $meta_data;
 

   $order_details[0]['store_id'];
         //Store Details
          $store_details = @\App\Store::where('store_id',$order_details[0]['store_id'])->get();

         $store_details[0]['meta'] = @\App\StoreMetaValue::where('store_id',$order_details[0]['store_id'])->get(['store_meta_value_id','store_meta_type_id','value']);
         $store_details_block['type'] = 'store_details';
         $store_details_block['title'] = 'Store Details';
         $store_details_block['data'] = $store_details;

 
         //Task Details
         $task_details = @\App\Task::where('order_id',$id)->get();
         $tasks_block['type'] = 'tasks';
         $tasks_block['title'] = 'Tasks';
         $tasks_block['data'] = $task_details;
 
         //customer_buttons==============================
         $customer_buttons = array();
         $order_status = $order_details[0]['order_status'];

     
         $customer_buttons[] = $this->get_customer_buttons($order_status , $id);

         $customer_buttons_block['type'] = 'customer_buttons';
         $customer_buttons_block['title'] = 'Actions';
         $customer_buttons_block['data'] = $customer_buttons;
 
         //admin_buttons==============================
         $admin_buttons = array();
         $order_status = $order_details[0]['order_status'];
         

         $admin_buttons_block['type'] = 'admin_buttons';
         $admin_buttons_block['title'] = 'Actions';
         $admin_buttons_block['data'] = $this->get_admin_buttons($order_status , $id);
 
     //======== starts creating a json for order_details========//

     $block = array();

 
    //Order Basic Json Starts ===================================
     $order_details_block['type'] = 'order_basic_details';
     $order_details_block['title'] = 'Basic Details';
     $order_details_block['data'] = $order_details;
 




      //Cancel Reason ===================================
     $order_cancel_reasons_block['type'] = 'order_cancel_reasons';
     $order_cancel_reasons_block['title'] = 'Cancel Reasons';

     $cancel_reason_array = array();

     if($this->validate_string($order_details[0]['cancel_reason_admin']) != '' && $this->validate_string($order_details[0]['cancel_reason_admin']) != null)
     {
         $d_cancel['label'] = 'Reason by '.env('APP_NAME');
         $d_cancel['value'] = $this->validate_string($order_details[0]['cancel_reason_admin']);
         $cancel_reason_array[] = $d_cancel;
     }


          if( $this->validate_string($order_details[0]['cancel_reason_customer']) != '' && $this->validate_string($order_details[0]['cancel_reason_customer']) != null)
     {
         $d_cancel['label'] = 'Reason by Customer';
         $d_cancel['value'] = $this->validate_string($order_details[0]['cancel_reason_customer']); 
        $cancel_reason_array[] = $d_cancel;
     }
      $order_cancel_reasons_block['data'] = $cancel_reason_array;






     //items Json Starts ===================================
     $items_block['type'] = 'items';
     $items_block['title'] = 'Items';
     $items_block['sub_total'] = strval($order_items_sub_total);
     $items_block['data'] = $order_items;



     ////Customer Json Starts ===================================
     $user_block['type'] = 'customer_details';
     $user_block['title'] = 'Customer Details';
     $user_block['data'] = $customer_details;

 
     ////Payment details Starts ===================================
     $payment_details_block['type'] = 'payment_details';
     $payment_details_block['title'] = 'Payment Details';
     $payment_details_block['order_total'] = strval( round($order_details[0]['total'],2));
     $payment_details_block_data_array= array();


     $payment_details_block_keys['title'] = 'Order Sub Total';
     $payment_details_block_keys['value'] = strval(round($order_items_sub_total,2));
     $payment_details_block_data_array[] = $payment_details_block_keys;


     $payment_details_block_keys['title'] = 'Discount';
     $payment_details_block_keys['value'] = strval(round($total_discount , 2 ));
     $payment_details_block_data_array[] = $payment_details_block_keys;

 
     foreach($order_transaction_tax as $tax)
     {
       $payment_details_block_keys['title'] = $tax->setting_tax_title;
       $payment_details_block_keys['value'] = strval(round($tax->order_transaction_tax_amount , 2 ));
       $payment_details_block_data_array[] = $payment_details_block_keys;
     }
 

     $payment_details_block_keys['title'] = 'Delivery Charges';
     $payment_details_block_keys['value'] =  strval( round($order_details[0]['delivery_fee'],2));
     $payment_details_block_data_array[] = $payment_details_block_keys;
 
     //  $payment_details_block_keys['title'] = 'Order Total';
     // $payment_details_block_keys['value'] = strval( round($order_details[0]['total'],2));
      // $payment_details_block_data_array[] = $payment_details_block_keys;


       $payment_details_block['data'] = $payment_details_block_data_array;


       
    //insert into blocks array as per sorting order needed
       $blocks[] = $order_details_block;
       $blocks[] = $items_block;
       $blocks[] = $user_block;
       $blocks[] = $payment_details_block;
       $blocks[] = $meta_block;
       $blocks[] = $store_details_block;
      
       $blocks[] = $customer_buttons_block;
       $blocks[] = $admin_buttons_block;


              if($order_details[0]['order_status'] == 'cancelled')
       {
        $blocks[] = $order_cancel_reasons_block;
       }
       

          if($order_details[0]['order_status'] != 'cancelled')
       {
         $blocks[] = $tasks_block;
       }



 
       return $blocks;
 
  }










 // Route-13.7 ============================================================== Get Order Details =========================================> 
 
 
    public function get_reorder_data(Request $request , $id)
  {
    $resp = '';
      $order_id = $id;


      //get_order_basic_data
    $order_items = @\App\OrderItem::where('order_id',$order_id)->get();
    $order_details = @\App\Order::where('order_id',$order_id)->get();
 

$items_array = array();


foreach($order_items as $order_item)
{
  $item_exist_count  = @\App\Items::where('item_id' ,$order_item['item_id'])->count();

  if($item_exist_count > 0)
  {
      $item_details_from_items_table  = @\App\Items::where('item_id' ,$order_item['item_id'])->get();


 
      $order_item['item_active_status'] = $item_details_from_items_table[0]['item_active_status']; 
    $order_item['item_discount_expiry_date'] = $item_details_from_items_table[0]['item_discount_expiry_date']; 
    $order_item['vendor_id'] = $item_details_from_items_table[0]['vendor_id']; 
    $order_item['item_discount'] =  $item_details_from_items_table[0]['item_discount']; 
    $order_item['store_id'] =  $item_details_from_items_table[0]['store_id']; 
    $order_item['quantity'] =  intval($order_item['order_item_quantity']); 
    $order_item['item_photo'] =  $item_details_from_items_table[0]['item_photo'];



    $variants_count = @\App\ItemVariantValue::where('item_id' , $order_item['item_id'])->count();
        if($variants_count > 0)
        {
            $order_item['variant_exist'] = 1;
        }
        else
        {
            $order_item['variant_exist']  = 0;
        }
 

        $order_item['item_price'] = strval($item_details_from_items_table[0]['item_price']);
        $order_item['item_id'] =  intval($order_item['item_id']);
        $order_item['item_title'] =  $item_details_from_items_table[0]['item_title'];
        $order_item['item_discount'] =  $item_details_from_items_table[0]['item_discount'];
        $order_item['item_thumb_photos'] = $item_details_from_items_table[0]['item_thumb_photo'];
  



        //varriants starts
        $order_item_variant_array = array();
        $order_item_variant_count = @\App\OrderItemVariant::where('order_item_id' , $order_item['order_item_id'])->count();
        $order_item_variant = @\App\OrderItemVariant::where('order_item_id' , $order_item['order_item_id'])->get();

        if($order_item_variant_count > 0)
        {
            foreach($order_item_variant as $order_variant)
            {
              $order_item_id = $order_variant['order_item_id'];
              $item_variant_value_id = $order_variant['item_variant_id'];
              $item_variant_title = $order_variant['item_variant_title'];
              $order_item_variant_value = $order_variant['order_item_variant_value'];

              $order_item_variant_value_details_count = @\App\ItemVariantValue::where('item_variant_value_id',$item_variant_value_id)->where('item_id',$order_item['item_id'])->count();
              $order_item_variant_value_details = @\App\ItemVariantValue::where('item_variant_value_id',$item_variant_value_id)->where('item_id',$order_item['item_id'])->get();
              if($order_item_variant_value_details_count > 0)
              {
                    foreach($order_item_variant_value_details as $order_item_variant_value)
                    {

 
 
                           $item_variant_type_id = $order_item_variant_value['item_variant_type_id'];//
                           $item_variant_type_title = $order_item_variant_value['item_variant_type_title'];//

                           $item_id = $order_item['item_id'];//
                              $item_title = @$item_details_from_items_table[0]['item_title'];
                           $item_variant_value_id = $order_item_variant_value['item_variant_value_id'];//
                           $item_variant_type_details = @\App\ItemVariantType::where('item_variant_type_id',$item_variant_type_id)->get(['price_status','price_difference_status']);

                           $item_variant_price_difference = $item_variant_type_details[0]['price_difference_status'];
                           $price_status = $item_variant_type_details[0]['price_status'];
                           $item_variant_photo = $item_variant_type_details[0]['item_variant_photo'];

                           $variant_data['item_id'] = $order_item['item_id'];
                     //      $variant_data['item_title'] = $item_title;
                           $variant_data['item_variant_type_id'] = $item_variant_type_id;
                           $variant_data['item_variant_type_title'] = $item_variant_type_title;

                           $variant_data['item_variant_price_difference'] = $order_item_variant_value['item_variant_price_difference'];
                           $variant_data['item_variant_price'] = $order_item_variant_value['item_variant_price'];
                           $variant_data['price_status'] = $price_status;
                           $variant_data['item_variant_photo'] = $item_variant_photo;
                           $variant_data['item_variant_value_id'] = $item_variant_value_id;

                           $variant_data['item_variant_value_title'] = $order_item_variant_value['item_variant_value_title'];

                           
        





                       
                           $order_item_variant_array[] = $variant_data;
 
                    }
              }

            } 
 
      }


 
            $extra_object['customer_id'] = $order_details[0]['customer_id'];
            $extra_object['coupon_code'] = '';
            $extra_object['loyalty_points'] = '0';
            $order_item['variants'] = $order_item_variant_array;


             // $items_with_variant_price = $this->add_variant_price_to_order_items($items); // also quantity calculation is done here
       
   
  }

   $items_array[] = $order_item;
 
 
 
}
   
 
     $items['items'] = $items_array;
     return $apply_coupon_response = app('App\Http\Controllers\Api\OrderController')->order_calculate($request , @$items , @$extra_object);
        
  
}

 






  // Route-62 ============================================================== Get Items List =========================================> 
   public function update(Request $request , $id , $create_item_request = '')
   {
	          if($create_item_request != '')
                {
                	$request = $create_item_request;
                }

              if($request['item_title'] == '')
               {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'Item Title Required';
                    return $data;	
               }

               if($request['item_price'] == '')
               {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'Item Price Required';
                    return $data;	
               }
			  
	                  //check existance of item with ID in items table
					$exist = $this->item_exist($id);	
                    if($exist == 0 or $exist == '0')
                    {
						              $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Item with this ID does not exist';
                          $data['data']      =   [];
                          return $data;						  
					          }

				   $item_active_status = $this->get_variable_item_active_status($request);
	           
	         $item =  \App\Items::where('item_id',$id)->update([
					'item_title' => $request['item_title'],
					'item_price' =>  @$request['item_price'],
					'item_discount' => $this->validate_integer(@$request['item_discount']),
					'item_discount_expiry_date' => $this->validate_datetime(@$request['item_discount_expiry_date']),
					'item_stock_count' => $this->validate_integer(@$request['item_stock_count']),
					'item_stock_count_type' => $this->validate_string(@$request['item_stock_count_type']),
					'item_photo' => $this->validate_string(@$request['item_photo']),
					'item_thumb_photo' => $this->validate_string(@$request['item_thumb_photo']),
          'item_tags' => $this->validate_string(@$request['item_tags']),
					'item_categories' => $this->validate_string($request['item_categories']),
					'vendor_id' => $this->validate_integer(@$request['vendor_id']),
					'store_id' => $this->validate_integer(@$request['store_id']),
					'item_active_status' => $item_active_status

				]);
				 


					
					//update meta value
					$item_meta = $request['meta'];
					$this->update_meta_values($id , $item_meta);
					
	               
				    $result = @\App\Items::where('item_id',$id)->get();
			 			
	                if(sizeof($result) > 0)
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Item Updated Successfully';
                          $data['data']      =   $result;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Unable to Update';
                          $data['data']      =   [];  
					}
				   return $data;
   }  


   

 



// Route-13.5 ============================================================== Store Item to Items table =========================================> 
   public function order_calculate(Request $request , $items = '' , $extra_object = '' )
   {
 
              
/**
  $twilio = new \Aloha\Twilio\Twilio('ACfb093e03fd084a1a79d594a3d1c711dd', 'c2d9a7928c3f1f82c217e484d27a6f53', '+1 773-251-8701');
 

return $twilio->message('+1 773-251-8701', 'Pink Elephants and Happy Rainbows');
  **/  
              if($items != '' && $items != null)
              {
         
                 $coupon_code = @$extra_object['coupon_code'];
                 $loyalty_points = @$extra_object['loyalty_points'];
                 $items = @$items['items'];+
                 $customer_id = @$extra_object['customer_id'];
                 $store_id = @$extra_object['store_id'];
                 $delivery_address = @$extra_object['delivery_address'];
              
              }
              else
              {
                  $items = @$request->items;
                  $coupon_code = @$request->coupon_code;
                  $customer_id = @$request->customer_id;
                  $store_id = @$request->store_id;
                  $delivery_address = @$request->delivery_address;
              }
 
               $delivery_address = $this->validate_string($delivery_address);
              

               //apply coupon process
               $items_with_variant_price = $this->add_variant_price_to_order_items($items); // also quantity calculation is done here
               $order_total = $this->total_order_amount_from_items($items_with_variant_price); 
               $order_sub_total = $order_total; //order sub total = items price * quantity ==================================================important




              //check store minimum delivery
              $delivery_type = $this->validate_string($request->delivery_type);
              if($delivery_type == 'home_delivery' )
              {
                    //check distance between store and delivery address
                    if($store_id != '' && $store_id != null)
                    {
                            $store_location_details = @\App\Stores::where('store_id',$store_id)->get(['latitude','longitude']);

                            $delivery_address_for_delivery_range_check = json_decode($delivery_address , true);
                            $delivery_latitude = $delivery_address_for_delivery_range_check['latitude'];
                            $delivery_longitude = $delivery_address_for_delivery_range_check['longitude'];
                            $store_latitude = $store_location_details[0]['latitude'];
                            $store_longitude = $store_location_details[0]['longitude'];
                           if($delivery_latitude != '' && $delivery_latitude != null && $delivery_longitude != '' && $delivery_longitude != null && $store_latitude != '' && $store_latitude != null && $store_longitude != '' && $store_longitude != null  )
                            {
                                   $minimum_delivery_range = @\App\Setting::where('key_title','minimum_delivery_range')->first(['key_value'])->key_value;
                                   $distance_between_store_delivery_address = $this->distance($delivery_latitude, $delivery_longitude,$store_latitude , $store_longitude , 'M' );

                                   if($distance_between_store_delivery_address > $minimum_delivery_range )
                                   {
                                                  $data['status_code']    =   0;
                                                  $data['status_text']    =   'Failed';             
                                                  $data['message']        =   'Order delivery is only available within '.$minimum_delivery_range.' miles';
                                                  $data['data']           =   [];
                                                  return $data; 
                                   }
                            }
                    }
                   ////check distance between store and delivery address
   

              	 		$minimum_delivery_check = $this->check_store_minimum_delivery($delivery_type , $order_sub_total , $store_id );
              	 		if($minimum_delivery_check['status_text'] == 'Failed')
              	 		{
                         return $minimum_delivery_check;
              	 		}
              }
              //check store minimum delivery ends

             
              if($request->loyalty_points != '1')
              {
                  $apply_coupon_response = app('App\Http\Controllers\Api\CouponsController')->apply($request);
                  $apply_coupon_response_array = array();
                  $apply_coupon_response_array[] = $apply_coupon_response;
                  $discount = @$apply_coupon_response['discount']; // returned discount in Amount
                  $discount_response = $apply_coupon_response;
                    
              }
              else
              {
                  $apply_loyalty_points_response = app('App\Http\Controllers\Api\LoyaltyPointsController')->apply($request , $customer_id);
                  $discount = $apply_loyalty_points_response['discount']; // returned discount in Amount
                  $applicable_points = $apply_loyalty_points_response['applicable_points'];
                  $discount_response = $apply_loyalty_points_response;
              }

      
              $order_total = $order_total -  $discount;
              $order_total_after_coupon = $order_total;//order total after applying coupon discount = items price * quantity - discount ==========================important
              

 
             // apply taxes
              $total_tax = $this->apply_setting_tax_to_order($order_total , $store_id);
              $setting_tax = $total_tax['setting_tax']; //tax transactions object ==========================important
              $total_tax_amount = $total_tax['total_tax_amount']; //total tax amount applied on previous total ==========================important
              $order_total = $total_tax['order_total']; 
              $order_total_after_tax = $order_total; //order total after tax on previous order total ===============================================important

              $tax_display_array = $total_tax['display_array'];  //array with all the taxes applied on order total ===============================================important


              //insert data to Json
              /**
              $json_array = array();
              $calculate_data['order_sub_total'] = round( $order_sub_total , 2 );
              $calculate_data['discount'] = round($discount , 2 );
              $calculate_data['order_total_after_coupon'] = round($order_total_after_coupon , 2 );
              $calculate_data['setting_tax'] = $setting_tax;
              $calculate_data['total_tax_amount'] = round($total_tax_amount , 2 );
              $calculate_data['order_total_after_tax'] = round($order_total_after_tax , 2 );
              $calculate_data['total'] = round($order_total_after_tax , 2 );
              $json_array[] = $calculate_data;
              **/
 
             //create display json
              $display_array = array();

              $calculate_data['title'] = 'Sub Total';
              $calculate_data['value'] = round( $order_sub_total , 2 );
              $display_array[] = $calculate_data;


              $applied_discount = round($discount , 2 );
              if($applied_discount > 0)
              {
              $calculate_data['title'] = 'Discount';
              $calculate_data['value'] = round($discount , 2 );
              $display_array[] = $calculate_data;
              }

              
              for($r=0; $r<sizeof($tax_display_array); $r++)
              {
                   $display_array[] = $tax_display_array[$r]; // its shows all the taxes applied on order total
              }

             // $calculate_data['title'] = 'Total';
             //$calculate_data['value'] = $order_total;
             // $display_array[] = $calculate_data;

 


              //Calculate delivery fee
               if($delivery_address != '' && $delivery_address != null)
               {
                    $delivery_address = json_decode($delivery_address , true);
           

 

                 $delivery_fee = $this->get_delivery_fee($delivery_address , $store_id );
                $status_code = $delivery_fee['status_code'];
                if($status_code == 0 || $status_code == '0')
                {
                  return $delivery_fee;
                }

                $delivery_fee = $delivery_fee['delivery_fee'];
                if($delivery_fee == '' || $delivery_fee == null || $delivery_fee == ' ')
                {
                  $delivery_fee = 0;
                }
         




                    $delivery_fee_data['title'] = 'Delivery Fee';
                    $delivery_fee_data['value'] = $delivery_fee;
                    $display_array[] = $delivery_fee_data;
                    $order_total = $order_total + $delivery_fee;
              }
             
              //Calculate delivery fee ends
              $data_d_array = array();
              $data_d['data'] = $display_array;

              $data_d['discount_response'] = $discount_response;

              $data_d['total'] = $order_total;
              $data_d['items'] = $items;
              $data_d_array[] = $data_d;
  
              $max_loyalty_points_to_use_order_place = @\App\Setting::where('key_title','max_loyalty_points_to_use_order_place')->first(['key_value'])->key_value;
              $loyalty_points_single_currency = @\App\Setting::where('key_title','loyalty_points_single_currency')->first(['key_value'])->key_value;
              $users_loyalty_points = @\App\LoyaltyPoints::where('user_id' , $customer_id )->sum('points');
              $applicable_users_loyalty_points = intval($max_loyalty_points_to_use_order_place *  $users_loyalty_points / 100);
              $applicable_users_loyalty_points_in_cash = $applicable_users_loyalty_points / $loyalty_points_single_currency;
              $currency_symbol = @\App\Setting::where('key_title','currency_symbol')->first(['key_value'])->key_value;


          if(sizeof($data_d_array) > 0)
          {
                          $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Order Calculation Fetched';
                          $data['data']           =   $data_d_array; 

                          if(intval($applicable_users_loyalty_points) < 1 )
                          {
                                    $data["points_title"]=  'you do not have enough loyalty points available';
                                    $data["use_button_enabled"]='0';
                                  
                          }  
                          else
                          {
                                    $data["points_title"]=  $applicable_users_loyalty_points.' points can be used to get discount worth '.$currency_symbol."".$applicable_users_loyalty_points_in_cash;

                                    if($applicable_users_loyalty_points > 0)
                                    {
                                      $data["use_button_enabled"]='1';
                                    }
                                    else
                                    {
                                      $data["use_button_enabled"]='0';
                                      $data["points_title"]=  'you do not have enough loyalty points available';
                                    }
                                    
                          }
                         
                          
           }
          else
          {
                          $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Some Error Occurred';
                          $data['data']      =   [];  
          }
           return $data;
 }










function distance($lat1, $lon1,$lat2 , $lon2 , $unit ) {

 
 

 
          
                $theta = $lon1 - $lon2;
          $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
          $dist = acos($dist);
          $dist = rad2deg($dist);
          $miles = $dist * 60 * 1.1515;
          $unit = strtoupper($unit);

          if ($unit == "K") {
            return ($miles * 1.609344);
          } else if ($unit == "N") {
              return ($miles * 0.8684);
            } else {

                        return $miles;
             
              }

 

  return $miles;


}







public function check_store_minimum_delivery($delivery_type , $order_sub_total , $store_id )
{

 
	if($delivery_type != '' && $delivery_type != null )
		              {


		              	if($delivery_type == 'home_delivery')
		              	{
                              $store_meta_type_id_minimum_delivery = @\App\StoreMetaType::where('identifier','minimum_delivery')->first(['store_meta_type_id'])->store_meta_type_id;
                             if($store_meta_type_id_minimum_delivery != '' && $store_meta_type_id_minimum_delivery != null)
                            {
                             	$store_minimum_delivery = @\App\StoreMetaValue::where('store_id',$store_id)->where('store_meta_type_id',$store_meta_type_id_minimum_delivery)->first(['value'])->value;

                            	if($store_minimum_delivery != '' && $store_minimum_delivery != null)
                            	{
                            		if($store_minimum_delivery > $order_sub_total)
                            		{
                                  $currency_symbol = @\App\Setting::where('key_title','currency_symbol')->first(['key_value'])->key_value;
                            			$data['status_code']    =   0;
				                          $data['status_text']    =   'Failed';             
				                          $data['message']        =   'Minimum Item Subtotal must be greater than '.$currency_symbol.''.$store_minimum_delivery.' for this store';
				                          return $data;
				                        }
                            	}
                            }
		              	}
		              }
		                              $data['status_code']    =   1;
				                          $data['status_text']    =   'Success';             
				                          $data['message']        =   'Success';
				                          return $data;
}



















  // Route-43 ============================================================== Items Delete =========================================> 

   public function destroy($id)
   {
   	 
   	         //check existance of item with ID in items table
					$exist = $this->item_exist($id);	
                    if($exist == 0 or $exist == '0')
                    {
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Item with this ID does not exist';
                          $data['data']      =   [];
                          return $data;						  
					}

   	 $this->remove_id_from_coupons($id);
   	 @\App\Items::where('item_id',$id)->delete();
   	 @\App\ItemVariantValue::where('item_id',$id)->delete();
   	 @\App\ItemMetaValue::where('item_id',$id)->delete();

   	 	                  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Item Deleted Successfully';
                          $data['data']      =   [];  
                          return $data;
   }






public function get_task_data_from_order( $order_id )
{

  $store_id = @\App\Order::where('order_id',$order_id)->first(['store_id'])->store_id;
   $customer_id = @\App\Order::where('order_id',$order_id)->first(['customer_id'])->customer_id;
           $customer_details = @\App\User::where('user_id',$customer_id)->get();

                                      $dropoff_contact_title = @$customer_details[0]["first_name"]." ".@$customer_details[0]["last_name"]; 
                                      $dropoff_contact_phone = @$customer_details[0]["phone"];  
   $order_meta_value = @\App\OrderMetaValue::where('order_id',$order_id)->get();
 
  $order_meta_array_size = sizeof($order_meta_value);
            foreach($order_meta_value as $meta)
            {
                 
                                      $identifier = $meta['identifier'];
                                  
                                     if($identifier == 'delivery_address')
                                     {
                                      $dropoff_address = $this->validate_string(@$meta['order_meta_value_text']);
                                     }

                                      if($identifier == 'delivery_time')
                                     {
                                      $delivery_time = $this->validate_string(@$meta['order_meta_value_text']);
                                     }

                                  
                                
            }

                            
                

                    $store_details = @\App\Store::where('store_id',$store_id)->get();
                    $task_data['order_id'] = $order_id;
                    $task_data['driver_id'] = '';
                    $task_data['vendor_id'] = @$store_details[0]['vendor_id'];
                    $task_data['store_id'] = $store_id;
                    $task_data['pickup_contact_title'] = @$store_details[0]['store_title'];
                    $task_data['pickup_contact_phone'] = @\App\User::where('user_id',@$store_details[0]['manager_id'])->first(['phone'])->phone;
                    $task_data['pickup_time'] = @\Carbon\Carbon::now();

                    $task_data['picked_up_time'] = '';
                    $task_data['pickup_address'] =@$store_details[0]['address'];
                    $task_data['dropoff_contact_title'] = @$dropoff_contact_title;
                    $task_data['dropoff_contact_phone'] = @$dropoff_contact_phone;

                    $task_data['dropoff_time'] = @$delivery_time;
                    $task_data['dropped_off_time'] = '';
                    $task_data['dropoff_address'] = @$dropoff_address;

                    $task_data['status'] = '0';
                    $task_data['task_type'] = 'order';

                    return $task_data;
}



///Route-13.6 ====================== Order Status Update

  public function status_update(Request $request , $id , $create_request_data = '')
   {

                $cancel_reason_admin = @$request->cancel_reason_admin;
            if($cancel_reason_admin != '' &&  $cancel_reason_admin != null )
            {
                      @\App\Order::where('order_id', $id)->update(['cancel_reason_admin' => $cancel_reason_admin  ]);
            }


            $cancel_reason_customer = @$request->cancel_reason_customer;
            if($cancel_reason_customer != '' &&  $cancel_reason_customer != null )
            {
                      @\App\Order::where('order_id', $id)->update(['cancel_reason_customer' => $cancel_reason_customer  ]);
            }



 
            if($create_request_data != '' && $create_request_data != null)
            {
              $request = $create_request_data;
              $id = $create_request_data['order_id'];
            }
 







             $order_status = $request['order_status'];
             if($order_status == '')
               {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'Status Required';
                    return $data; 
               }


                             //reclaim loyalty points
                  if($order_status == 'cancelled')
                  {
                        $this->reclaim_loyalty_points($id);
                  }

                  //give loyalty points to user_id

                  @\App\Order::where('order_id', $id)->update(['order_status' => $order_status  ]);

                  //give loyalty points to user_id


                 if($order_status == 'order_accepted' || $order_status == 'order_received')
                    {   @\App\Task::where('order_id',$id)->delete();  }
 
                 if($order_status == 'order_accepted')
                    {
                   
                    @\App\Task::where('order_id',$id)->delete(); 

                      $delivery_type = $this->get_order_meta_value( $request , $id , 'delivery_type');

                      if($delivery_type != 'restaurant_pickup')
                      {
                        $task_data = $this->get_task_data_from_order($id);
                        app('App\Http\Controllers\Api\TaskController')->store($request ,$task_data);
                      }
                      
                    }
 
                  if($order_status == 'delivered')
                  {
                    $items_sub_total = @\App\Order::where('order_id' , $id)->first(['items_sub_total'])->items_sub_total;
                    $customer_id = @\App\Order::where('order_id' , $id)->first(['customer_id'])->customer_id;
                    $order_placed_loyalty_points_percentage = @\App\Setting::where('key_title' , 'order_placed_loyalty_points_percentage')->first(['key_value'])->key_value;
                    $order_loyalty_points_expiry_day_count = @\App\Setting::where('key_title' , 'order_loyalty_points_expiry_day_count')->first(['key_value'])->key_value;
                    $percentage_value = $order_placed_loyalty_points_percentage/100 * $items_sub_total;

                    
                     if($order_loyalty_points_expiry_day_count > 0)
                      {
                         $today = @\Carbon\Carbon::now();
                         $expiry_date = $today->addDays($order_loyalty_points_expiry_day_count);
                      }
                      else
                      {
                        $expiry_date = '';
                      }

                    $LoyaltyPoints = new \App\LoyaltyPoints;
                    $LoyaltyPoints->user_id = $this->validate_string(@$customer_id);
                    $LoyaltyPoints->points = $this->validate_integer($percentage_value);
                    $LoyaltyPoints->expiry_date = $this->validate_string($expiry_date);
                    $LoyaltyPoints->type = 'order_earned';
                    $LoyaltyPoints->source = $id;
                    $LoyaltyPoints->save();
                  }

                  //send notification 
                    $this->notify($request , 'order_status_updated',$id);
                  //send notification ends


                          $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Order Status Updated Successfully';
                          $data['data']      =   [];  
                          return $data;


                 
         
   }





public function reclaim_loyalty_points($order_id)
{
  $order_details = @\App\Order::where('order_id',$order_id)->get(['customer_id']);
  $customer_id = $order_details[0]['customer_id'];

  $count = @\App\LoyaltyPoints::where('type','order_spent')->where('user_id',$customer_id)->where('source', $order_id )->count();

  if($count > 0 )
  {
     $loyalty_points = \App\LoyaltyPoints::where('type','order_spent')->where('user_id',$customer_id)->where('source', $order_id )->get();

     foreach($loyalty_points as $points)
     {

                   $expiry_date = $points['expiry_date'];
                   $rec_points = abs($points['points']);
                   
                   $reclaim_count = \App\LoyaltyPoints::where('type','order_reclaimed')->where('user_id',$customer_id)->where('source', $order_id )->count();

                   if($reclaim_count <  1)
                   {
                      $LoyaltyPoints = new \App\LoyaltyPoints;
                      $LoyaltyPoints->user_id = $this->validate_string(@$customer_id);
                      $LoyaltyPoints->points = $rec_points;
                      $LoyaltyPoints->expiry_date = $expiry_date;
                      $LoyaltyPoints->type = 'order_reclaimed';
                      $LoyaltyPoints->source = $order_id;
                      $LoyaltyPoints->save();
                    }

     }

       $this->notify(  '' , 'order_reclaimed' ,$order_id , $customer_id );

  }
  return 1;

}

















public function remove_id_from_coupons($id)
{
     $coupons = @\App\Coupons::get();
     foreach($coupons as $coupon)
     {
     	  $new_included_items_id = array();
     	 $items_included = $coupon->items_included;
     	 $items_included_array = explode("," , $items_included);
     	  
     	 for($i=0;$i<sizeof($items_included_array);$i++)
     	 {
     	 	if($items_included_array[$i] != $id)
     	 	{
     	 		if($items_included_array[$i] != '' && $items_included_array[$i] != null)
     	 		{
     	 			$new_included_items_id[] = $items_included_array[$i];
     	 		}
     	 		
     	 	}
          }
     	 $new_string = implode("," , $new_included_items_id);
     	 App\Coupons::where('coupon_id', $coupon['coupon_id'])->update(['items_included' => $new_string]);
     }
}


public function get_order_address_array($order_id)
{

}





  public function get_customer_buttons($order_status , $order_id = '')
  {
         if($order_status == 'order_received')
         {
          $button['title'] = 'Cancel Order';
          $button['enabled'] = '1';
          $button['action'] = 'cancelled';

         }

         if($order_status == 'order_accepted')
         {
          $button['title'] = 'Cancel Order';
          $button['enabled'] = '0';
          $button['action'] = 'cancelled';

         }


         if($order_status == 'in_preparation')
         {
          $button['title'] = 'Cancel Order';
          $button['enabled'] = '0';
          $button['action'] = 'cancelled';

         }

 
          if($order_status == 'ready_for_delivery')
         {
          $button['title'] = 'Track Order';
          $button['enabled'] = '1';
          $button['action'] = 'track';
         }


          if($order_status == 'in_transit')
         {
          $button['title'] = 'Track Order';
          $button['enabled'] = '1';
          $button['action'] = 'track';
         }



        if($order_status == 'cancelled')
         {
          $button['title'] = 'Cancelled';
          $button['enabled'] = '0';
          $button['action'] = '';
         }


 
         if($order_status == 'delivered')
         {
 
          $order_review = @\App\OrderReview::where('order_id',$order_id)->first(['review'])->review;
 
          if($order_review == '' || $order_review == null || $order_review == ' ')
            { $enabled = '1';} else { $enabled = '0';} 
          $button['title'] = 'Leave FeedBack';
          $button['enabled'] = $enabled;
          $button['action'] = 'feedback';
         }

          if($order_status == 'cancelled')
         {
           
          $button['title'] = 'Cancel';
          $button['enabled'] = '0';
          $button['action'] = '';
         }



               $button['status_details'] = @\App\SettingOrderStatus::where('identifier',$order_status)->get();
         


         return $button;
  }

    public function get_admin_buttons($order_status , $order_id = '')
  {

        $auth_user_id = $this->get_auth_user_id();
        $user_type = $this->get_auth_user_type();


         $buttons_array = array();
         if($order_status == 'order_received')
         {
          $button['title'] = 'Accept Order';
          $button['enabled'] = '1';
          $button['action'] = 'order_accepted';

         }
          if($order_status == 'order_accepted')
         {
          $button['title'] = 'Mark In Preparation';
          $button['enabled'] = '1';
          $button['action'] = 'in_preparation';
        
         }
          if($order_status == 'in_preparation')
         {
          $button['title'] = 'Mark Ready';
          $button['enabled'] = '1';
          $button['action'] = 'ready_for_delivery';
         }
          if($order_status == 'ready_for_delivery')
         {
          $button['title'] = 'Mark In Transit';
          $button['enabled'] = '0';
          $button['action'] = 'in_transit';
         }

          if($order_status == 'in_transit')
         {
          $button['title'] = 'Mark Delivered';
          $button['enabled'] = '1';
          $button['action'] = 'delivered';
         }

               if($order_status == 'delivered')
         {
          $button['title'] = 'Delivered';
          $button['enabled'] = '0';
          $button['action'] = '';
         }


                          if($order_status == 'cancelled')
         {
          $button['title'] = 'Cancelled';
          $button['enabled'] = '0';
          $button['action'] = '';
         }
 

      if($order_status == 'order_received' || $order_status == 'order_accepted')
         {
          $button2_enabled ='1';
         }
         else
         {
           $button2_enabled ='0';
         }
           $button2['title'] = 'Mark Cancelled';
           $button2['enabled'] = $button2_enabled;
           $button2['action'] = 'cancelled';
         




          $button['status_details'] = @\App\SettingOrderStatus::where('identifier',$order_status)->get();
          $button2['status_details'] = @\App\SettingOrderStatus::where('identifier','cancelled')->get();
        $buttons_array[] = $button;
        $buttons_array[] = $button2;



         return $buttons_array;
  }



public function driver_location_by_order($id)
{
	$order_id = $id;

	$tasks_count = @\App\Task::where('order_id' , $order_id )->count();
	if($tasks_count < 1)
	{
		                  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Error occurred';
                          $data['data']      =   [];  
                          return $data;
	}

 
	$tasks = @\App\Task::where('order_id' , $order_id )->get();


	foreach($tasks as $task)
	{
		if($task['driver_id'] == '' || $task['driver_id'] == null )
		{


			              $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Driver Assigned';
                          $data['data']      =   [];  
                          return $data;

		}


	$driver_id = $task['driver_id'] ;


	$tasks_count = @\App\User::where('user_id' , $driver_id )->count();
	if($tasks_count < 1)
	{
		                  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Driver Not Found';
                          $data['data']      =   [];  
                          return $data;
	}

 
          $locations = @\App\User::where('user_id' , $driver_id)->get(['latitude','longitude','user_id','thumb_photo','first_name','last_name']);

		$d = array();
		$ds['user_details'] = $locations;

		if($locations[0]['longitude'] == '' || $locations[0]['longitude'] == null )
		{

		                  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Locations Found';
                          $data['data']      =   [];  
                          return $data;
		}

 


        $d[] = $ds;
		$data['status_code']    =   1;
        $data['status_text']    =   'Success';             
        $data['message']        =   'Locations Fetched';
        $data['data']      =  $ds;  
        return $data;

	}

}
   
   
   
   
   
//==========================================================================misc functions===================================================================//   
//check item existence by id
public function item_exist($id)
{
	$count = @\App\Items::where('item_id',$id)->count();
	if($count < 1) {
		return 0;
	}
	else{
		return 1;
	}
}	

 

//validate all the request variables if they are null or empty , it will return values
public function validate_datetime($var)
{
	 if(!isset($var) or $var == null or $var == '' or $var == ' ')
	 {
	 	 $date = @\Carbon\Carbon::now();
	 	 $date = @\Carbon\Carbon::parse($date);
		 return $this->add_days($date , '7' , 'Y-m-d h:i:s');
	 }
	 else{
		 return $var;
	 }
}


 
   public function store_meta_values($item_id , $item_meta)
  {
	  $item_meta_type = @\App\ItemMetaType::get();
	  	  
	  foreach($item_meta_type as $imt)
	  {
		$identifier = $imt->identifier;  
	 	$value = $this->validate_string($item_meta[$identifier]);
	    $item_meta_type_id = $this->get_item_meta_type_id($identifier);
	    
		$item_meta_value = new \App\ItemMetaValue;
		$item_meta_value->item_meta_type_id = @$item_meta_type_id;
		$item_meta_value->item_id = @$item_id;
		$item_meta_value->value = $this->validate_string(@$value);
		$item_meta_value->save();
	  }
	  return 1;
  }
  

  
     public function update_meta_values($item_id , $item_meta)
  {
	  $item_meta_type = @\App\ItemMetaType::get();
	  	  
	  foreach($item_meta_type as $imt)
	  {
		$identifier = $imt->identifier;  
	 	$value = $this->validate_string($item_meta[$identifier]);
	    $item_meta_type_id = $this->get_item_meta_type_id($identifier);
		App\ItemMetaValue::where('item_id', $item_id)->where('item_meta_type_id', $item_meta_type_id)->update(['value' => $value ]);
	  }
	  return 1;
  }


  
  public function get_item_meta_type_id($identifer)
  {
	  $item_meta_type_id = @\App\ItemMetaType::where('identifier',$identifer)->first(['item_meta_type_id'])->item_meta_type_id;
	  return $item_meta_type_id;
  }
  



///================================ function to check GET variable's and Defaults ====================================================//
public function get_variable_per_page()
{
	 if(isset($_GET['per_page']) && $_GET['per_page'] != null && $_GET['per_page'] != '')
					{ $per_page = $_GET['per_page']; }
					else 
					{ $per_page = 20; }
    return $per_page;
}

public function get_variable_orderby()
{
	 if(isset($_GET['orderby']) && $_GET['orderby'] != null && $_GET['orderby'] != '')
					{ $orderby = $_GET['orderby']; }
					else 
					{ $orderby = 'created_at'; }
    return $orderby;
}

public function get_variable_order()
{
	 if(isset($_GET['order']) && $_GET['order'] != null && $_GET['order'] != '')
					{ $order = $_GET['order']; }
					else 
					{ $order = 'DESC'; }
    return $order;
}


public function get_variable_search()
{
	 if(isset($_GET['search']) && $_GET['search'] != null && $_GET['search'] != '')
					{ $search = $_GET['search']; }
					else 
					{ $search = ''; }
    return $search;
}	
      
   
 
   public function get_variable_table_fields()
{
   if(isset($_GET['table_fields']) && $_GET['table_fields'] != null && $_GET['table_fields'] != '')
          { $table_fields = $_GET['table_fields']; }
          else 
          { $table_fields = ''; }
    return $table_fields;
}



public function get_variable_include_count_blocks()
{
     if(isset($_GET['include_count_blocks']) && $_GET['include_count_blocks'] != null && $_GET['include_count_blocks'] != '')
          { $include_count_blocks = $_GET['include_count_blocks']; }
          else 
          { $include_count_blocks = 'false'; }
    return $include_count_blocks;
}
  


   public function get_variable_status()
{
	 if(isset($_GET['status']) && $_GET['status'] != null && $_GET['status'] != '')
					{ $status = $_GET['status']; }
					else 
					{ $status = 'any'; }
    return $status;
}

   public function get_variable_customer_id()
{
   if(isset($_GET['customer_id']) && $_GET['customer_id'] != null && $_GET['customer_id'] != '')
          { $customer_id = $_GET['customer_id']; }
          else 
          { $customer_id = ''; }
    return $customer_id;
}
   public function get_variable_store_id()
{
   if(isset($_GET['store_id']) && $_GET['store_id'] != null && $_GET['store_id'] != '')
          { $store_id = $_GET['store_id']; }
          else 
          { $store_id = ''; }
    return $store_id;
}

   public function get_variable_include_address()
{
   if(isset($_GET['include_address']) && $_GET['include_address'] != null && $_GET['include_address'] != '')
          { $include_address = $_GET['include_address']; }
          else 
          { $include_address = ''; }
    return $include_address;
}


   public function get_variable_fields()
{
   if(isset($_GET['fields']) && $_GET['fields'] != null && $_GET['fields'] != '')
          { $fields = $_GET['fields']; }
          else 
          { $fields = ''; }
    return $fields;
}





 
 ///================================ function to check GET variable's and Defaults Ends ====================================================//
 
 
 
 
 
  
 
 
 
 
	
	
	public function paginateWithoutKey($items, $perPage = 15, $page = null, $options = [])
    {

        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

        $items = $items instanceof Collection ? $items : Collection::make($items);

        $lap = new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);

        return [
            'current_page' => $lap->currentPage(),
            'data' => $lap ->values(),
            'first_page_url' => $lap ->url(1),
            'from' => $lap->firstItem(),
            'last_page' => $lap->lastPage(),
            'last_page_url' => $lap->url($lap->lastPage()),
            'next_page_url' => $lap->nextPageUrl(),
            'per_page' => $lap->perPage(),
            'prev_page_url' => $lap->previousPageUrl(),
            'to' => $lap->lastItem(),
            'total' => $lap->total(),
        ];
    }
	
	
	
	 public function paginate($items, $perPage = 15, $page = null, $options = [])
{
	$page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
	$items = $items instanceof \Collection ? $items : Collection::make($items);
	return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
}
	 
	 	public function add_trades_feedback(Request $request)
{
	 
        $validator = Validator::make($request->all(), [
           
	 
        ]);
        if ($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';
            $data['message']        =   $validator->errors()->first();
        }
        else
        {
        
		
			$trade_id = $request->trade_id;
		    $user_id = $request->user_id;
			$feedback = $request->feedback;
			$ratings = $request->ratings;
		  
		  
		    $order_seller_id = @\App\Trades::where('id',$request->trade_id)->first(['seller_id'])->seller_id;
			$order_buyer_id = @\App\Trades::where('id',$request->trade_id)->first(['buyer_id'])->buyer_id;
			
			if($user_id == $order_seller_id)
			{
				 
				 \App\Trades::where('id',$request->trade_id)->update([ 'seller_ratings' => $ratings , 'seller_feedback' => $feedback]);
			}
			
		 
			if($user_id == $order_buyer_id)
			{
				 
				 \App\Trades::where('id',$request->trade_id)->update([ 'buyer_ratings' => $ratings , 'buyer_feedback' => $feedback]);
			}
		  
 
		
 
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';
            $data['message']        =   'Submitted successfully';

        }
        return $data;
    }
	
	
	
	
 
 
	
 
	

    
 
   public function make_thumb($src, $dest, $desired_width) 
   {

    /* read the source image */
    $source_image = imagecreatefromjpeg($src);
    $width = imagesx($source_image);
    $height = imagesy($source_image);
    
    /* find the "desired height" of this thumbnail, relative to the desired width  */
    $desired_height = floor($height * ($desired_width / $width));
    
    /* create a new, "virtual" image */
    $virtual_image = imagecreatetruecolor($desired_width, $desired_height);
    
    /* copy source image at a resized size */
    imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
    
    /* create the physical thumbnail image to its destination */
    imagejpeg($virtual_image, $dest);
    }

    /**
     * @author Dikshant
     * set user language by default
     */
 
 
 


}