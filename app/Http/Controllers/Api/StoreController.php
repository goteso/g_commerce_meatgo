<?php
namespace App\Http\Controllers\Api; //admin add
use App;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; // using controller class
use Auth;
use Session;
use DB;
use Validator;
use App\User;
use Carbon\Carbon;
use App\Otp;
use App\UserSocialLinks;
use App\UserLikesDislikes;
use App\UserRatings;
use App\Country;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Traits\one_signal; // <-- you'll need this line...
use App\Traits\bitcoin_price;
use App\Traits\trait_functions;
use App\Traits\notifications;

use Illuminate\Support\Arr;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Hash;
use Mail;
use File;
 
 


class StoreController extends Controller 
{
	
use one_signal; // <-- ...and also this line.
use bitcoin_price; // <-- ...and also this line.
use trait_functions; // <-- ...and also this line. 
use notifications; // <-- ...and also this line. 
   
   
   
   
   
 // Route-12.1 ============================================================== Store User to User table =========================================> 
   public function store(Request $request)
   { 
                $validator = Validator::make($request->all(), [
					//'title' => 'required|unique:posts|max:255',
					'store_title' => 'required',
					//'store_photo' => 'required',
					'latitude' => 'required',
					'longitude' => 'required',
				 
			 
			      ]);


                              $vendor_plugins = @\App\Setting::where('key_title','vendors')->first(['key_value'])->key_value;
                              if($vendor_plugins == 1)
                              {
									if($request->vendor_id == '' || $request->vendor_id == null)
									                {
									                    $data['status_code']    =   0;
									                    $data['status_text']    =   'Failed';             
									                    $data['message']        =   'vendor required';
									                    return $data;					
									                }
                              }
	   
				if($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();
                    return $data;					
                }
			 
			        $store_title = $this->validate_string($request->store_title); 
			        $store_photo = $this->validate_string($request->store_photo);
			        $store_rating = $this->validate_integer($request->store_rating);
			        $latitude = $this->validate_string($request->latitude);
			        $store_phone = $this->validate_string($request->store_phone);
			        $longitude = $this->validate_string($request->longitude); 
			        $address = $this->validate_string($request->address);     
			        $vendor_id = $this->validate_integer($request->vendor_id);
			        $manager_id = $this->validate_integer($request->manager_id);
			        $commission = $this->validate_integer($request->commission);
			        $featured = $this->validate_integer($request->featured);
			        $busy_end_time = $this->validate_integer($request->busy_end_time);
			        $sort_index = $this->validate_integer($request->sort_index);
			        $store_tax = $this->validate_integer($request->store_tax);
		 
		

						if($busy_end_time == 0 || $busy_end_time == '0')
						{
							$now = @\Carbon\Carbon::now();
							$now = $now->subDays(1);
							$busy_end_time = $now->format('Y-m-d H:i:s')."";
						}
 
					$store = new App\Store;
					$store->store_title = $store_title;
			        $store->store_photo = $store_photo;

			       if($store_photo != ''&& $store_photo != null)
			       {
                        $store->store_thumb_photo = 'thumb-'.$store_photo;
			       }


			        $store->store_rating = $store_rating;
			        $store->latitude = $latitude;
			        $store->longitude = $longitude;
			        $store->vendor_id = $vendor_id;
			        $store->manager_id = $manager_id;
			        $store->commission = $commission;
			        $store->address = $address;
			        $store->store_phone = $store_phone;
			        $store->featured = $featured;
			        $store->busy_end_time = $busy_end_time;
			        $store->sort_index = $sort_index;
			        $store->store_tax = $store_tax;
			        $store->save();

 
//					$store_meta = $request->meta;
//				    $this->store_meta_values($store->id , $store_meta);
					
				    if($store != '')
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Store Added Successfully';
                          $data['data']      =   $store;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Unable to Add Store';
                          $data['data']      =   [];  
					}
				   
				  return $data;
	 }
   

   
  
  // Route-12.2 ============================================================== Get Users List =========================================> 
   public function get_list(Request $request)
   {	

     ///auth filter starts
        $auth_user_id = $this->get_auth_user_id();
        $auth_user_type = $this->get_auth_user_type();





   
	   
        $per_page = $this->get_variable_per_page(); //ASC or DESC
		$orderby = $this->get_variable_orderby();
		$order = $this->get_variable_order();
		$search = $this->get_variable_search();
		$request_type = $this->get_variable_request_type();
		$vendor_id = $this->get_variable_vendor_id();
		$include_meta = $this->get_variable_include_meta();
		$featured = $this->get_variable_featured();
	 
		$exclude_busy_stores = $this->get_variable_exclude_busy_stores();

		$meta_filters = $this->get_variable_meta_filters();

 


	  
	    $model = new \App\Store;
	    $model = $model::where('store_id' ,'<>', '0');  




	         $lat = @$_GET['latitude'];
             $long = @$_GET['longitude'];
             if($lat !='' && $long != '' && $lat != null && $long != null )
                 {
                      $store_ids_range = $this->distance($lat, $long, 'M');
                     $model = $model->whereIn('store_id' , $store_ids_range);  
                 }
             





        if($auth_user_type == '3')
        {  $vendor_id = $auth_user_id;  }

		if($vendor_id != '' && $vendor_id != null)
		{  $model = $model->where('vendor_id' , $vendor_id);  }	




		if($featured != '' && $featured != null)
		{  $model = $model->where('featured' , $featured);  }	


 



//spplying filter starts =========================
if($meta_filters != '')
{ 


      $meta_filters_store_ids = $this->apply_store_meta_filter($meta_filters);
 
    if(sizeof($meta_filters_store_ids) > 0 )
    {
    	 //$model =    $model->orWhereRaw('FIND_IN_SET( "'.$meta_filters_store_ids[$i].'", coupon_title)');
    	$model = $model->WhereIn("store_id",$meta_filters_store_ids);
    }
 }

//spplying filter ends=========================
 



if($orderby == 'distance')
{
	  $gr_circle_radius = 6371;
      $max_distance = 500;
      $lat = @$_GET['latitude'];
      $long = @$_GET['longitude'];


                 if($lat =='' || $long == '' || $lat == null || $long == null )
                 {
                 	$data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =  'Latitude and Longitude are required for distance sort';
                    return $data;	
                 }
 
      $model = $model->select("*" , "stores.store_id"
		,DB::raw("6371 * acos(cos(radians(" . $lat . ")) 
		* cos(radians(stores.latitude)) 
		* cos(radians(stores.longitude) - radians(" . $long . ")) 
		+ sin(radians(" .$lat. ")) 
		* sin(radians(stores.latitude))) AS distance  "));

}


 







 
if($search != '' && $search != null)
{  $model = $model->where(function($q) use ($search) { $q->where( DB::raw("CONCAT(store_title,' ',store_id,' ',address)"),'like', '%'.$search.'%'); });  }



if($exclude_busy_stores == 'true')
{

             $busy_store_ids = $this->get_busy_store_ids();
             $now = @\Carbon\Carbon::now();
	         $now =\Carbon\Carbon::parse($now)->format('Y-m-d');
             $model = $model->whereNotIn('store_id',$busy_store_ids);
 }



        $model = $model->orderBy($orderby,$order);	

        if($request_type =='input_field')
		{	 $result = $model->paginate($per_page , ['store_id','store_title']); 			}
	    else { $result = $model->paginate($per_page); }


	 

	    if($include_meta == 'true')
		{
			foreach($result as $r)
			{
				$store_meta_value_final = array();
				$store_meta_value = @\App\StoreMetaValue::where('store_id' , $r->store_id)->get(); 
				foreach($store_meta_value as $mv)
				{
				 
						$meta_exist_count = @\App\StoreMetaType::where( 'store_meta_type_id' , $mv->store_meta_type_id )->count(); 
						if($meta_exist_count > 0)
						{
							$store_meta_value_final[] = @\App\StoreMetaValue::where('store_id' , $mv->store_id)->where( 'store_meta_type_id' , $mv->store_meta_type_id )->first(); 
							
						}

				}

				 
				 $r->store_meta = $store_meta_value_final;
				
			}
		}


				if($request_type =='input_field')
				{
                  	foreach($result as $r)
			        {
			          $r->label = $r->store_title; 
			          $r->value = $r->store_id; 
			        }    
				}


	   
	              if(sizeof($result) > 0)
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Stores List Fetched Successfully';
                          $data['data']      =   $result;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Store Found';
                          $data['data']      =   [];  
					}
				   return $data;
   }  




function distance($lat1, $lon1, $unit ) {

$store_in_range = array();
	$stores = @\App\Stores::get(['store_id','latitude','longitude']);
	$max_distance = 20;
	foreach($stores as $store)
	{

		$lat2 = $store['latitude'];
		$lon2 = $store['longitude'];
						$max_distance = 300;
				        $theta = $lon1 - $lon2;
				  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
				  $dist = acos($dist);
				  $dist = rad2deg($dist);
				  $miles = $dist * 60 * 1.1515;
				  $unit = strtoupper($unit);

				  if ($unit == "K") {
				    return ($miles * 1.609344);
				  } else if ($unit == "N") {
				      return ($miles * 0.8684);
				    } else {

                            if($lat2='' || $lat2 == null || $lat1 == '' || $lat1 == null)
                            {
                            	$miles = 9999999999999999999;
                            }
 
                           if($miles < 300)
                           {
                           	  $store_in_range[] = $store['store_id'];

                           }
				     
				      }

	}

	return $store_in_range;


}






public function apply_store_meta_filter($meta_filters)
{


	$meta_store_ids = array();
	$meta_identifier_filters_array = array();
 	$meta_filters_array = explode("|",$meta_filters);
	if(sizeof($meta_filters_array) > 0 )
	{
		for($t=0; $t < sizeof($meta_filters_array); $t++)
		{
		   $arr = explode(":",$meta_filters_array[$t]);

		  $d['key'] = $arr[0];
		  $d['value'] = explode(",",$arr[1]);
          $meta_identifier_filters_array[] =  $d;
		}
	}
 
     foreach($meta_identifier_filters_array as $filter)
     {


       	$store_meta_type_id = @\App\StoreMetaType::where( 'identifier' , $filter['key'] )->first(['store_meta_type_id'])->store_meta_type_id;
        $store_ids_meta_values = @\App\StoreMetaValue::where('store_meta_type_id',$store_meta_type_id)->whereIn('value',$filter['value'])->pluck('store_id'); 
       
        $plucked_store_ids_array =$store_ids_meta_values->toArray();
        
        if(sizeof($plucked_store_ids_array) > 0)
        {
        	for($pl=0;$pl<sizeof($plucked_store_ids_array);$pl++)
        	{
        	  $meta_store_ids[] = $plucked_store_ids_array[$pl];	
        	}
        }

     }

     return $meta_store_ids;
}


public function get_busy_store_ids()
{
	         $now = @\Carbon\Carbon::now();
	         $now =\Carbon\Carbon::parse($now)->format('Y-m-d');
 

             $busy_store_ids = @\App\Store::where('busy_end_time','<>','')->where('busy_end_time','<>',null)->where('busy_end_time','>',$now)->pluck('store_id');
             return $busy_store_ids;
}
  
  // Route-12.3 ============================================================== Get Users List =========================================> 
   public function show($id)
   {
	                //check existance of item with ID in items table
					$exist = $this->model_exist($id);	
                    if($exist == 0 or $exist == '0')
                    {
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Store with this ID does not exist';
                          $data['data']      =   [];
                          return $data;						  
					}						
					
					$details = @\App\Store::where('store_id',$id)->get();
					
					$meta_include_needed = $this->get_variable_include_meta();
 
 
					if($meta_include_needed == 'true'  )
					{
						$details[0]['meta'] = @\App\StoreMetaValue::where('store_id',$id)->get();
					}
					
			 			
				    if($details != '')
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Store Fetched Successfully';
                          $data['data']      =   $details;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Unable to Fetch Store';
                          $data['data']      =   [];  
					}
				   return $data;
				 
  }   
  




public function submit_store_edit_forms(Request $request , $id='' , $type='')
{

 
           $store_edit_approval_status = @\App\Setting::where('key_title','store_edit_approval_status')->first(['key_value'])->key_value;

           
           if($store_edit_approval_status == 1 || $store_edit_approval_status == '1')
           {

                    //insert update request

			        $update_log_request = new App\LogUpdateRequest;

			        $update_log_request->user_id = $_GET['auth_user_id'];
			        $update_log_request->store_id = $id;
			        $update_log_request->request_data =   $request->getContent();
			        $update_log_request->approved_status = '0';
			        $update_log_request->save();


                	 //send notification to store manager
      					 @$this->notify($request , 'store_update_request_placed',@$id);
     				//send notification ends

           	 
           	        $data['status_code']    =   1;
                    $data['status_text']    =   'Success';             
                    $data['message']        =   'Direct Updations is currently blocked , Update request is sent to Admin for Verification, We will get back to you as soon as possible';
                    return $data;

           }
      


	$meta_data = $request['meta_data'];
	$basic_data = $request['basic_data'][0];
	 
	  $this->update($request , $id , $basic_data);


	return $this->submit_store_meta_form($request , $id , $meta_data);
}
   
  // Route-12.4 ============================================================== Get Items List =========================================> 
   public function update(Request $request , $id , $create_store_request = '' , $type ='')
   {

   	if(!isset($_GET['auth_user_id']) || $_GET['auth_user_id'] == '')
   	{
   		            $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'Auth UserID is Required';
                    return $data;
   	}


   
	          if($create_store_request != '')
                {
                	$request = $create_store_request;
                }
 	 
               if($request['store_title'] == '')
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'Store Title is Required';
                    return $data;	
                }
  
               if($request['latitude'] == '')
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'Latitude is Required';
                    return $data;	
                }

               if($request['longitude'] == '')
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'Longitude is Required';
                    return $data;	
                }

         


			  
	               //check existance of user with ID in user table
					$exist = $this->model_exist($id);	
                    if($exist == 0 or $exist == '0')
                    {
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Store with this ID does not exist';
                          $data['data']      =   [];
                          return $data;						  
					}



		            $store_title = $this->validate_string(@$request['store_title']); 
		            $store_rating = $this->validate_integer(@$request['store_rating']);
			        $latitude = $this->validate_string(@$request['latitude']);
			        $longitude = $this->validate_string(@$request['longitude']); 
			        $address = $this->validate_string(@$request['address']);      
			        $vendor_id = $this->validate_integer(@$request['vendor_id']);
			        $manager_id = $this->validate_integer(@$request['manager_id']);
			        $commission = $this->validate_integer(@$request['commission']);
			        $store_photo = $this->validate_string(@$request['store_photo']);
			         $sort_index = $this->validate_integer(@$request['sort_index']);
			         $store_tax = $this->validate_integer(@$request['store_tax']);
			         $store_phone = $this->validate_integer(@$request['store_phone']);


			        



			        if($store_photo != ''&& $store_photo != null)
			       {
                        $store_thumb_photo = 'thumb-'.$store_photo;
			       }
			       else
			       {
			        
			       	 $store_thumb_photo = $this->validate_string(@$request['store_photo']);
			       }


			        $featured = $this->validate_integer(@$request['featured']);

 

 
	           
	              	$user =  \App\Store::where('store_id',$id)->update([
                        'store_title' => $store_title,
						'store_rating' =>  $store_rating,
						'latitude' => $latitude,
						'longitude' => $longitude,
						'address' => $address,
						'vendor_id' => $vendor_id,
						'manager_id' => $manager_id,
						'commission' => $commission,
						'featured' => $featured,
						'store_photo' => $store_photo,
						'store_tax' => $store_tax,
						'store_phone' => $store_phone,
						'store_thumb_photo' => $store_thumb_photo
				 
		              ]);
				 


					
					//update meta value
					$store_meta = @$request['meta'];

					if($store_meta != '')
					{
						$this->update_meta_values($id , $store_meta);
					}
					
					
	               
				    $result = @\App\Store::where('store_id',$id)->get();
			 			
	                if(sizeof($result) > 0)
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Store Updated Successfully';
                          $data['data']      =   $result;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Unable to Update';
                          $data['data']      =   [];  
					}
				   return $data;
   }  
  


   
 
  // Route-12.5 ============================================================== Delete a Address =========================================> 
   public function destroy( $id)
   {
 
   	 
				  //check existance of item with ID in items table
				 $exist = @\App\Order::where('store_id',$id)->count();
				 if($exist > 0)
				 {
				 		                  $data['status_code']    =   0;
				                          $data['status_text']    =   'Failed';             
				                          $data['message']        =   'Store cannot be deleted as Order are present on this Store';
				                          $data['data']      =   [];
				                          return $data;			
				 }
				 

									     $user =  @\App\Coupons::where('store_id',$id)->update([ 'store_id' => $this->validate_integer($a) ]);
									     $user =  @\App\Store::where('store_id',$id)->delete();
									     $user =  @\App\Items::where('store_id',$id)->delete();
									     $user =  @\App\StoreMetaValue::where('store_id',$id)->delete();
									     $user =  @\App\Task::where('store_id',$id)->update([ 'store_id' => $this->validate_integer($a) ]);
									     $user =  @\App\FavouriteStore::where('store_id',$id)->delete();
									 
				                          $data['status_code']    =   1;
				                          $data['status_text']    =   'Success';             
				                          $data['message']        =   'Store Deleted Successfully';
				                          $data['data']      =   [];  
				                          return $data;
				    

   }



//Route-12.10 =====================================================
public function approve_log_update_request(Request $request , $log_update_request_id )
{

  $exist = @\App\LogUpdateRequest::where('id',$log_update_request_id)->count();

  if($exist < 1)
  {
  	                      $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Log Update Request Id Not Found';
                          $data['data']      =   []; 
                          return $data;
  }
    $request_details = @\App\LogUpdateRequest::where('id',$log_update_request_id)->get();

    $store_id = $request_details[0]['store_id'];
    $request_data = $request_details[0]['request_data'];
    $user_id = $request_details[0]['user_id'];
 
   
    $request_data = json_decode($request_data);
    $request_data = get_object_vars($request_data);

    $basic_data = $request_data['basic_data'];
    $meta_data = $request_data['meta_data'];
 
  	 @\App\LogUpdateRequest::where('id',$log_update_request_id )->update([ 'approved_status' => 1 ]);
     
     $basic_data = get_object_vars($basic_data[0]);
    

 
      //send notification to store manager
       @$this->notify($request , 'store_update_request_approved_by_admin',@$store_id , $user_id);
     //send notification ends
   
        $this->update($request , $store_id , $basic_data , 'approve' );
    return  $this->submit_store_meta_form($request , $store_id , $meta_data );
     		     
}


//Route-12.11 ============================================================================
public function reject_log_update_request(Request $request , $log_update_request_id )
{

  $exist = @\App\LogUpdateRequest::where('id',$log_update_request_id)->count();

  if($exist < 1)
  {
  	                      $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Log Update Request Id Not Found';
                          $data['data']      =   []; 
                          return $data;
  }
    $request_details = @\App\LogUpdateRequest::where('id',$log_update_request_id)->get();

    $store_id = $request_details[0]['store_id'];
    $request_data = $request_details[0]['request_data'];
    $user_id = $request_details[0]['user_id'];

    $request_data = json_decode($request_data);
    $request_data = get_object_vars($request_data);

 //send push to user_id that your update request cannot be processes

    //send notification to store manager
       @$this->notify($request , 'store_update_request_rejected_by_admin',@$store_id , $user_id);
     //send notification ends
@\App\LogUpdateRequest::where('id',$log_update_request_id )->update([ 'approved_status' => 2 ]);

  	// @\App\LogUpdateRequest::where('id',$log_update_request_id )->update([ 'approved_status' => 0 ]);


  	                       $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Requests Rejected Successfully';
                          $data['data']      =   [];  
                          return $data;
     		     
}



//Route-12.9 =============================================
public function log_update_request_list()
{
	    $per_page = $this->get_variable_per_page(); //ASC or DESC
		$orderby = $this->get_variable_orderby();
		$order = $this->get_variable_order();

 		$store_id = $this->get_variable_store_id();
		$user_id = $this->get_variable_user_id();
		$approved_status = $this->get_variable_approved_status();

		 
	    $model = new \App\LogUpdateRequest;
	    $model = $model::where('id' ,'<>', '0');  


		if($store_id != '' && $store_id != null)
		{  $model = $model->where('store_id' , $store_id);  }	


		if($approved_status != '' && $approved_status != null)
		{  $model = $model->where('approved_status' , $approved_status);  }	


		if($user_id != '' && $user_id != null)
		{  $model = $model->where('user_id' , $user_id);  }	

 
 
        $model = $model->orderBy($orderby,$order);	

        $result = $model->paginate($per_page); 

 
	              if(sizeof($result) > 0)
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Requests List Fetched Successfully';
                          $data['data']      =   $result;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Requests Found';
                          $data['data']      =   [];  
					}
				   return $data;
}






     // Route-12.6 ============================================================== Delete a Address =========================================> 
   public function update_featured(Request $request , $id)
   {
   	  if($request->featured == '' || $request->featured == null)
   	  {
   	  	  	 			  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Featured Required';
                          $data['data']      =   []; 
                          return $data;
   	  }

                    $featured = $request->featured;

	              	$user =  \App\Store::where('store_id',$id)->update([
                        'featured' => $this->validate_integer($featured),
					 ]);
				 
	 					  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Featured Status Updated';
                          $data['data']      =   []; 
                          return $data;

   }
 
 
  



 /// Route-12.7 ======================== Get Store Meta Form========================================
   public function get_store_meta_form($id ='')
   {
    
 
   	   	  $main_array = array();
	  
	               //check existance of item with ID in items table
					$exist = $this->model_exist($id);	
                    if($exist == 0 or $exist == '0')
                    {
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'User with this ID does not exist';
                          $data['data']      =   [];
                          return $data;						  
					}
 
	  	  // 2. Meta Informations ==============
	  $data_array2['title'] = 'Additional Informations';
	  $data_array2['type'] = 'meta';

	  $store_meta_types = @\App\StoreMetaType::get();
      $meta_fields_array = array();
	  foreach($store_meta_types as $meta_type)
	  {

	  	 $meta_type->user_meta_type_id;
	 	 $meta_data["title"] = $meta_type->title;
	 	 $meta_data["type"] =  $meta_type->type;
	 	 $meta_data["identifier"] =  $meta_type->identifier;
	 	 $meta_data["value"] = $this->get_store_meta_value(@$id , @$meta_type->store_meta_type_id);
	 	 $meta_data["limit"] = $meta_type->count_limit;
	 	 $meta_data["field_options"] = $this->validate_string(json_decode($meta_type->field_options));
      	 $meta_fields_array[] = $meta_data;
	  }

      $data_array2['fields'] = $meta_fields_array;
	  $main_array[] =$data_array2;
      $result = $main_array;

      return $result;
 
   }


//Route-12.8 =====================
 public function submit_store_meta_form(Request $request , $id ='' , $create_request = '')
 {
     

 if($create_request != '' && $create_request != null)
 {
 	$create_request = json_encode($create_request);
 	$request =json_decode($create_request, true);
 }

  $fields = @$request[0]["fields"];

 
  if(sizeof($fields) < 1)
  {	      
  	                      $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Meta Fields Required';
                          $data['data']      =   [];
                          return $data;
  }

  for($i=0;$i<sizeof($fields);$i++)
  {
  	$store_meta[$fields[$i]['identifier']] = $this->validate_string($fields[$i]['value']);
  }

  
 
  //check existance of item with ID in items table
 $exist = $this->model_exist($id);	
                    if($exist == 0 or $exist == '0')
                    {
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Store with this ID does not exist';
                          $data['data']      =   [];
                          return $data;						  
					}	
   $user_meta_type = @\App\StoreMetaType::get();
	  	  
	  foreach($user_meta_type as $imt)
	  {
		$identifier = $imt->identifier;  
	 	$value = $this->validate_string(@$store_meta[$identifier]);
	    $store_meta_type_id = $this->get_store_meta_type_id($identifier);


 
         $exist = @\App\StoreMetaValue::where('store_id', $id)->where('store_meta_type_id', $store_meta_type_id)->count();
       if($exist > 0)
       {
           @\App\StoreMetaValue::where('store_id', $id)->where('store_meta_type_id', $store_meta_type_id)->update(['value' => $value ]);
       }
       else
       {
               $store_meta_value_model = new App\StoreMetaValue;
               $store_meta_value_model->store_meta_type_id = $store_meta_type_id;
               $store_meta_value_model->store_id = $id;
               $store_meta_value_model->save();
       }

		
	  }
	  	        $data['status_code']    =   1;
                $data['status_text']    =   'Success';             
                $data['message']        =   'Updated Successfully';
                $data['data']      =   []; 
                return $data;
   }


  











   public function store_meta_values($store_id , $store_meta)
  {
	  $store_meta_type = @\App\StoreMetaType::get();
	  	  
	  foreach($store_meta_type as $imt)
	  {
		$identifier = $imt->identifier;  


	 	$value = $this->validate_string(@$store_meta[$identifier]);
	    $store_meta_type_id = $this->get_store_meta_type_id($identifier);
	    
		$store_meta_value = new \App\StoreMetaValue;
		$store_meta_value->store_meta_type_id = $imt->store_meta_type_id;
		$store_meta_value->store_id = @$store_id;
		$store_meta_value->value = $this->validate_string(@$value);
		$store_meta_value->save();
	  }
	  return 1;
  }
  


     public function update_meta_values($store_id , $store_meta)
  {
	  $store_meta_type = @\App\StoreMetaType::get();
	  	  
	  foreach($store_meta_type as $imt)
	  {
		$identifier = $imt->identifier;  
	 	$value = $this->validate_string(@$store_meta[$identifier]);
	    $store_meta_type_id = $this->get_store_meta_type_id($identifier);


	    $count_meta_exist = App\StoreMetaValue::where('store_id', $store_id)->where('store_meta_type_id', $store_meta_type_id)->count();

	    if($count_meta_exist > 0 )
	    {
             App\StoreMetaValue::where('store_id', $store_id)->where('store_meta_type_id', $store_meta_type_id)->update(['value' => $value ]);
         }
	    else
	    {
	    	  $flight = new \App\StoreMetaValue;
              $flight->store_id = @$store_id;
              $flight->store_meta_type_id = @$store_meta_type_id;
              $flight->value = @$value;
              $flight->save();
	    }
 
	  }
	  return 1;
  }
  
  public function get_store_meta_type_id($identifer)
  {
	  $store_meta_type_id = @\App\StoreMetaType::where('identifier',$identifer)->first(['store_meta_type_id'])->store_meta_type_id;
	  return $store_meta_type_id;
  }
  



 
 
   
//==========================================================================misc functions===================================================================//   
//check user existence by id
public function model_exist($id)
{
	$count = @\App\Store::where('store_id',$id)->count();
	if($count < 1) {
		return 0;
	}
	else{
		return 1;
	}
}	


 
 

///================================ function to check GET variable's and Defaults ====================================================//
public function get_variable_per_page()
{
	 if(isset($_GET['per_page']) && $_GET['per_page'] != null && $_GET['per_page'] != '')
					{ $per_page = $_GET['per_page']; }
					else 
					{ $per_page = 20; }
    return $per_page;
}

public function get_variable_orderby()
{
	 if(isset($_GET['orderby']) && $_GET['orderby'] != null && $_GET['orderby'] != '')
					{ $orderby = $_GET['orderby']; }
					else 
					{ $orderby = 'created_at'; }
    return $orderby;
}

public function get_variable_order()
{
	 if(isset($_GET['order']) && $_GET['order'] != null && $_GET['order'] != '')
					{ $order = $_GET['order']; }
					else 
					{ $order = 'DESC'; }
    return $order;
}

public function get_variable_search()
{
	 if(isset($_GET['search']) && $_GET['search'] != null && $_GET['search'] != '')
					{ $search = $_GET['search']; }
					else 
					{ $search = ''; }
    return $search;
}	


public function get_variable_vendor_id()
{
	 if(isset($_GET['vendor_id']) && $_GET['vendor_id'] != null && $_GET['vendor_id'] != '')
					{ $vendor_id = $_GET['vendor_id']; }
					else 
					{ $vendor_id = ''; }
    return $vendor_id;
}	
 
      
   
   public function get_variable_linked_id()
{
	 if(isset($_GET['linked_id']) && $_GET['linked_id'] != null && $_GET['linked_id'] != '')
					{ $linked_id = $_GET['linked_id']; }
					else 
					{ $linked_id = ''; }
    return $linked_id;
}	
 

 public function get_variable_meta_filters()
 {
 		 if(isset($_GET['meta_filters']) && $_GET['meta_filters'] != null && $_GET['meta_filters'] != '')
					{ $meta_filters = $_GET['meta_filters']; }
					else 
					{ $meta_filters = ''; }
    return $meta_filters;
 }
  
 
   public function get_variable_include_meta()
{
	 if(isset($_GET['include_meta']) && $_GET['include_meta'] != null && $_GET['include_meta'] != '')
					{ $include_meta = $_GET['include_meta']; }
					else 
					{ $include_meta = 'true'; }
    return $include_meta;
}	


 public function get_variable_request_type()
 {
 	   if(isset($_GET['request_type']) && $_GET['request_type'] != null && $_GET['request_type'] != '')
					{ $request_type = $_GET['request_type']; }
					else 
					{ $request_type = ''; }
       return $request_type;
 }



public function get_variable_featured()
{
	if(isset($_GET['featured']) && $_GET['featured'] != null && $_GET['featured'] != '')
					{ $featured = $_GET['featured']; }
					else 
					{ $featured = ''; }
       return $featured;
}


  public function get_variable_store_id()
 {
 	   if(isset($_GET['store_id']) && $_GET['store_id'] != null && $_GET['store_id'] != '')
					{ $store_id = $_GET['store_id']; }
					else 
					{ $store_id = ''; }
       return $store_id;
 }



  public function get_variable_exclude_busy_stores()
 {
 	   if(isset($_GET['exclude_busy_stores']) && $_GET['exclude_busy_stores'] != null && $_GET['exclude_busy_stores'] != '')
					{ $exclude_busy_stores = $_GET['exclude_busy_stores']; }
					else 
					{ $exclude_busy_stores = 'true'; }
       return $exclude_busy_stores;
 }

  public function get_variable_user_id()
 {
 	   if(isset($_GET['user_id']) && $_GET['user_id'] != null && $_GET['user_id'] != '')
					{ $user_id = $_GET['user_id']; }
					else 
					{ $user_id = ''; }
       return $user_id;
 }

  public function get_variable_approved_status()
 {
 	   if(isset($_GET['approved_status']) && $_GET['approved_status'] != null && $_GET['approved_status'] != '')
					{ $approved_status = $_GET['approved_status']; }
					else 
					{ $approved_status = '0'; }
       return $approved_status;
 }




 

 
 ///================================ function to check GET variable's and Defaults Ends ====================================================//
 
 
 
 
 
  
 
 
 
 
	
	
	public function paginateWithoutKey($items, $perPage = 15, $page = null, $options = [])
    {

        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

        $items = $items instanceof Collection ? $items : Collection::make($items);

        $lap = new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);

        return [
            'current_page' => $lap->currentPage(),
            'data' => $lap ->values(),
            'first_page_url' => $lap ->url(1),
            'from' => $lap->firstItem(),
            'last_page' => $lap->lastPage(),
            'last_page_url' => $lap->url($lap->lastPage()),
            'next_page_url' => $lap->nextPageUrl(),
            'per_page' => $lap->perPage(),
            'prev_page_url' => $lap->previousPageUrl(),
            'to' => $lap->lastItem(),
            'total' => $lap->total(),
        ];
    }
	
	
	
	 public function paginate($items, $perPage = 15, $page = null, $options = [])
{
	$page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
	$items = $items instanceof \Collection ? $items : Collection::make($items);
	return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
}
	 
	 	public function add_trades_feedback(Request $request)
{
	 
        $validator = Validator::make($request->all(), [
           
	 
        ]);
        if ($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';
            $data['message']        =   $validator->errors()->first();
        }
        else
        {
        
		
			$trade_id = $request->trade_id;
		    $user_id = $request->user_id;
			$feedback = $request->feedback;
			$ratings = $request->ratings;
		  
		  
		    $order_seller_id = @\App\Trades::where('id',$request->trade_id)->first(['seller_id'])->seller_id;
			$order_buyer_id = @\App\Trades::where('id',$request->trade_id)->first(['buyer_id'])->buyer_id;
			
			if($user_id == $order_seller_id)
			{
				 
				 \App\Trades::where('id',$request->trade_id)->update([ 'seller_ratings' => $ratings , 'seller_feedback' => $feedback]);
			}
			
		 
			if($user_id == $order_buyer_id)
			{
				 
				 \App\Trades::where('id',$request->trade_id)->update([ 'buyer_ratings' => $ratings , 'buyer_feedback' => $feedback]);
			}
		  
 
		
 
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';
            $data['message']        =   'Submitted successfully';

        }
        return $data;
    }
	
	
	
	
 
 
	
 
	

    
 
   public function make_thumb($src, $dest, $desired_width) 
   {

    /* read the source image */
    $source_image = imagecreatefromjpeg($src);
    $width = imagesx($source_image);
    $height = imagesy($source_image);
    
    /* find the "desired height" of this thumbnail, relative to the desired width  */
    $desired_height = floor($height * ($desired_width / $width));
    
    /* create a new, "virtual" image */
    $virtual_image = imagecreatetruecolor($desired_width, $desired_height);
    
    /* copy source image at a resized size */
    imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
    
    /* create the physical thumbnail image to its destination */
    imagejpeg($virtual_image, $dest);
    }

    /**
     * @author Dikshant
     * set user language by default
     */
 
 
 


}