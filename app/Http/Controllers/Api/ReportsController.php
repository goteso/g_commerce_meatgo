<?php
namespace App\Http\Controllers\Api; //admin add
use App;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; // using controller class
use Auth;
use Session;
use DB;
use Validator;
use App\User;
use Carbon\Carbon;
use App\Otp;
use App\UserSocialLinks;
use App\UserLikesDislikes;
use App\UserRatings;
use App\Country;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Traits\one_signal; // <-- you'll need this line...
use App\Traits\bitcoin_price;
use App\Traits\trait_functions;
use Illuminate\Support\Arr;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Hash;
use Mail;
use File;
 
 


class ReportsController extends Controller 
{
	

use one_signal; // <-- ...and also this line.
use bitcoin_price; // <-- ...and also this line.
use trait_functions; // <-- ...and also this line. 
   
   
   
 /**
    Route::get('/v1/reports/orders', array('uses' => 'ReportsController@reports_orders'));//Route-
    Route::get('/v1/reports/cancelled_orders', array('uses' => 'ReportsController@reports_cancelled_orders'));//Route-
    Route::get('/v1/reports/customers', array('uses' => 'ReportsController@reports_customers'));//Route-
    Route::get('/v1/reports/customers_most_sales', array('uses' => 'ReportsController@reports_customers_most_sales'));//Route-
    Route::get('/v1/reports/tasks', array('uses' => 'ReportsController@reports_tasks'));//Route-
 **/

 
 
 // Route-23.1 ============================================================== Get Reports List =========================================> 
   public function get_list(Request $request  )
   {
 
  
         $reports = @\App\Reports::get();
         if(sizeof($reports) > 0)
					{
						              $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Reports List Fetched';
                          $data['data']      =         $reports;  
				    }
					else
					{
						              $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Reports Found';
                          $data['data']           =   [];  
					}
				   return $data;
				 
  }
  








   //Route-23.2 && Route-23.3=========================================================================
   public function reports_orders(Request $request  )
   {



        $columns_fields_array = array();
        $data_array = array();
      
        $table_fields = 'order_id,order_status,total,customer_name,store_title,discount_type,total,created_at_formatted,delivery_fee,driver_commission,items_sub_total,store_commission';
        $orders = app('App\Http\Controllers\Api\OrderController')->get_list();
 

       if($table_fields !='' && $table_fields != null)
        { $table_fields_array = explode(",",$table_fields); }
        else  { $table_fields_array = [];  }
   
 
        $data_array = array();
        foreach($orders as $order_d)
        {
        	$data_obj_array = array();
        	for($i=0;$i<sizeof($table_fields_array);$i++)
        	{

        		$data_obj_array[] = $order_d[$table_fields_array[$i]];
        	}
        	$data_array[] = $data_obj_array;
        }
        

        // create actual json
        $main = array();
        $main_obj['header'] = 'Orders Report';
        $main_obj['columns'] = $table_fields_array;
        $main_obj['data'] = $data_array;
        $main_obj['footer'] = 'Footer Text Goes Here';
        $main[] = $main_obj;
 
        if(sizeof($main) > 0)
          {
                          $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Order Reports List Fetched';
                          $data['data']      =         $main;  
            }
          else
          {
                          $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Reports Found';
                          $data['data']           =   [];  
          }
           return $data;
         
  }
  

 



//Route-23.4 =============================================================
   public function reports_customers(Request $request  )
   {

    $per_page = $this->get_variable_per_page(); //ASC or DESC
		$orderby = $this->get_variable_orderby();
		$order = $this->get_variable_order();
		$search = $this->get_variable_search();
		$request_type = $this->get_variable_request_type();
		$user_type = $this->get_variable_user_type();
 
		$table_fields = 'user_id,first_name,last_name,email,phone,facebook_id,status,referral_code,longitude,latitude,loyalty_points,created_at_formatted';
	  
	    if($table_fields !='' && $table_fields != null)
        { $table_fields_array = explode(",",$table_fields); }
        else  { $table_fields_array = [];  }



 
	    $model = new \App\User;
	    $model = $model::where('user_id' ,'<>', '0');  


		if($user_type != '' && $user_type != null)
		{  $model = $model->where('user_type' , $user_type);  }	


  ///auth filter starts
    $auth_user_id = $this->get_auth_user_id();
    $user_type = $this->get_auth_user_type();
 
     if($user_type == '3') //vendor
    {   $model = $model->where('vendor_id' , $auth_user_id );   
    }
  ///auth filter starts Ends

    if($search != '' && $search != null)
		{  $model = $model->where(function($q) use ($search) { $q->where( DB::raw("CONCAT(email,' ',phone,' ',first_name,' ',last_name,' ',user_id)"),'like', '%'.$search.'%'); });  }

        $model = $model->orderBy($orderby,$order);	

 	 
        $date_from= $_GET['date_from'];
        $date_to = $_GET['date_to'];
        $date_from =\Carbon\Carbon::parse($date_from)->format('Y-m-d');
        $date_to =\Carbon\Carbon::parse($date_to)->format('Y-m-d');
        $user_data = \App\User::where('created_at','<',$date_to)->where('created_at','>',$date_from)->get();
 
                $data_array = array();
        foreach($user_data as $order_d)
        {
        	$data_obj_array = array();
        	for($i=0;$i<sizeof($table_fields_array);$i++)
        	{

        		$data_obj_array[] = $order_d[$table_fields_array[$i]];
        	}
        	$data_array[] = $data_obj_array;
        }
        

        // create actual json
        $main = array();
        $main_obj['header'] = 'Users  Report';
        $main_obj['columns'] = $table_fields_array;
        $main_obj['data'] = $data_array;
        $main_obj['footer'] = 'Footer Text Goes Here';
        $main[] = $main_obj;


	    	   
	              if(sizeof($main) > 0)
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Users List Fetched Successfully';
                          $data['data']      =   $main;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No User Found';
                          $data['data']      =   [];  
					}
				   return $data;
         
  }

  



//Route-23.5 ================================================
   public function reports_tasks(Request $request  )
   {
    $per_page = $this->get_variable_per_page(); //ASC or DESC
		$orderby = $this->get_variable_orderby();
		$order = $this->get_variable_order();
		$search = $this->get_variable_search();
		$request_type = $this->get_variable_request_type();
		$user_type = $this->get_variable_user_type();
 
		$table_fields = 'task_id,driver_id,order_id,pickup_contact_title,pickup_contact_phone,pickup_time,picked_up_time,store_title,driver_name,dropoff_contact_title,dropoff_contact_phone,dropoff_time,status,task_type,created_at_formatted';
	  
	  if($table_fields !='' && $table_fields != null)
    { $table_fields_array = explode(",",$table_fields); }
    else  { $table_fields_array = [];  }

	  $model = new \App\Task;
	  $model = $model::where('id' ,'<>', '0');  

		if($user_type != '' && $user_type != null)
		{ $model = $model->where('user_type' , $user_type); }	

    ///auth filter starts
    $auth_user_id = $this->get_auth_user_id();
    $user_type = $this->get_auth_user_type();
 
    if($user_type == '3') //vendor
    {
      $model = $model->where('vendor_id' , $auth_user_id );   
    }
    ///auth filter starts Ends

    if($search != '' && $search != null)
		{ $model = $model->where(function($q) use ($search) { $q->where( DB::raw("CONCAT(task_id,' ',order_id,' ',driver_id,' ',store_id)"),'like', '%'.$search.'%'); });  }

        $model = $model->orderBy($orderby,$order);	
 
        $date_from= $_GET['date_from'];
        $date_to = $_GET['date_to'];
        $date_from =\Carbon\Carbon::parse($date_from)->format('Y-m-d');
        $date_to =\Carbon\Carbon::parse($date_to)->format('Y-m-d');
        $user_data = \App\Task::where('created_at','<',$date_to)->where('created_at','>',$date_from)->get();

 
        $data_array = array();
        foreach($user_data as $order_d)
        {
        	$data_obj_array = array();
        	for($i=0;$i<sizeof($table_fields_array);$i++)
        	{

        		$data_obj_array[] = $order_d[$table_fields_array[$i]];
        	}
        	$data_array[] = $data_obj_array;
        }
     
        // create actual json
        $main = array();
        $main_obj['header'] = 'Task  Report';
        $main_obj['columns'] = $table_fields_array;
        $main_obj['data'] = $data_array;
        $main_obj['footer'] = 'Footer Text Goes Here';
        $main[] = $main_obj;

	      if(sizeof($main) > 0)
					{
					 	              $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Task Report Fetched Successfully';
                          $data['data']      =   $main;  
				  }
					else
					{
						              $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Task Found';
                          $data['data']      =   [];  
					}
				   return $data;
         
  }




//Route-23.6 ===========================================================================================================
     public function reports_items(Request $request  )
   {

    $per_page = $this->get_variable_per_page(); //ASC or DESC
		$orderby = $this->get_variable_orderby();
		$order = $this->get_variable_order();
		$search = $this->get_variable_search();
		$category = $this->get_variable_category();
		$tag = $this->get_variable_tag();
		$status = $this->get_variable_status();
    $store_id = $this->get_variable_store_id();
    $table_fields = 'item_id,item_title,item_price,item_discount,item_discount_expiry_date,store_title,vendor_name,item_active_status,item_stock_count';
	  
	  if($table_fields !='' && $table_fields != null)
    { $table_fields_array = explode(",",$table_fields); }
    else  { $table_fields_array = [];  }
        
		
		$model = new \App\Items;
		$model = $model::where('item_id' ,'<>', '0');  
	 

    //user types filters ==========================================
    $auth_user_id = $this->get_auth_user_id();
    $user_type = $this->get_auth_user_type();
    if($user_type == '4') //store manager_id
    {
       $store_id = @\App\Store::where('manager_id',$auth_user_id)->first(['store_id'])->store_id;
       if($store_id == '' || $store_id == null) { $store_id = 'NA';}
    }
     if($user_type == '3') //vendor
    {   $model = $model->where('vendor_id' , $auth_user_id );   
    }
    //user type_filter check ends =================================

 
		if($status != '' && $status != null)
		{  $model = $model->where('item_active_status' , $status);  }	


    if($store_id != '' && $store_id != null)
    {  $model = $model->where('store_id' , $store_id);  } 
	   
	  if($search != '' && $search != null)
		{  $model = $model->where(function($q) use ($search) { $q->where( DB::raw("CONCAT(item_id,' ',item_title)"),'like', '%'.$search.'%'); });  }
      
	  if($category != '' && $category != null)
		{   $model = $model->whereRaw("FIND_IN_SET('".$category."',items.item_categories)");  }	
	
		if($tag != '' && $tag != null)
		{   $model = $model->whereRaw("FIND_IN_SET('".$tag."',items.item_tags)");  }

 
 
 
    $date_from= $_GET['date_from'];
    $date_to = $_GET['date_to'];
    $date_from =\Carbon\Carbon::parse($date_from)->format('Y-m-d');
    $date_to =\Carbon\Carbon::parse($date_to)->format('Y-m-d');
    $model = $model->where('created_at','<',$date_to)->where('created_at','>',$date_from);
    $model = $model->orderBy($orderby,$order);	
    $result = $model->get();



 	 
    $data_array = array();
    foreach($result as $order_d)
    {
     	$data_obj_array = array();
      	for($i=0;$i<sizeof($table_fields_array);$i++)
        	{

        		$data_obj_array[] = $order_d[$table_fields_array[$i]];
        	}
     	$data_array[] = $data_obj_array;
    }
        
        // create actual json
        $main = array();
        $main_obj['header'] = 'Items Report';
        $main_obj['columns'] = $table_fields_array;
        $main_obj['data'] = $data_array;
        $main_obj['footer'] = 'Footer Text Goes Here';
        $main[] = $main_obj;
	    	   
	        if(sizeof($main) > 0)
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Task Report Fetched Successfully';
                          $data['data']      =   $main;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Task Found';
                          $data['data']      =   [];  
					}
				   return $data;
         
  }
















//Route-23.7 ===========================================================================================================
     public function reports_coupons(Request $request  )
   {

    $per_page = $this->get_variable_per_page(); //ASC or DESC
    $orderby = $this->get_variable_orderby();
    $order = $this->get_variable_order();
    $search = $this->get_variable_search();
    $status = $this->get_variable_status();
    $store_id = $this->get_variable_store_id();
      
       $table_fields = 'coupon_id,coupon_title,coupon_desc,coupon_code,discount,valid_from,expiry,type,max_discount,limit_total,limit_user,minimum_order_amount,maximum_order_amount,active_status,vendor_name,store_title';
    
    if($table_fields !='' && $table_fields != null)
    { $table_fields_array = explode(",",$table_fields); }
    else  { $table_fields_array = [];  }
    $include_consumed = $this->get_variable_include_consumed();
    $include_expired  = $this->get_variable_include_expired();
  
    
    $model = new \App\Coupons;
    $model = $model::where('coupon_id' ,'<>', '0');  


    //user types filters ==========================================
    $auth_user_id = $this->get_auth_user_id();
    $user_type = $this->get_auth_user_type();
    if($user_type == '4') //store manager_id
    {
       $store_id = @\App\Store::where('manager_id',$auth_user_id)->first(['store_id'])->store_id;
       if($store_id == '' || $store_id == null) { $store_id = 'NA';}
    }
     if($user_type == '3') //vendor
    {   $model = $model->where('vendor_id' , $auth_user_id );   
    }
    //user type_filter check ends =================================



    

      if($store_id != '' && $store_id != null)
    {   $model = $model->where('store_id' , $store_id);  } 
 
    //user type_filter check ends

        if($status != 'any' && $status != '' && $status != null)
        {
          $model->where('active_status' , $status); 
        }
 
        if(   $include_expired == 'true')
        {

            $model->whereDate('expiry', '<', date("Y-m-d"));
        }
              else
        {

            $model->whereDate('expiry', '>', date("Y-m-d"));
        }
 

      if($search != '' && $search != null)
    {  $model = $model->where(function($q) use ($search) { $q->where( DB::raw("CONCAT(coupon_id,' ',coupon_code,' ',coupon_desc,' ', coupon_title)"),'like', '%'.$search.'%'); });  }
        

    $model = $model->orderBy($orderby,$order);
         
    $date_from= $_GET['date_from'];
    $date_to = $_GET['date_to'];
    $date_from =\Carbon\Carbon::parse($date_from)->format('Y-m-d');
    $date_to =\Carbon\Carbon::parse($date_to)->format('Y-m-d');
    $model = $model->where('created_at','<',$date_to)->where('created_at','>',$date_from);
    $model = $model->orderBy($orderby,$order);  
      $result = $model->get();


 
   
    $data_array = array();
    foreach($result as $order_d)
    {
      $data_obj_array = array();
        for($i=0;$i<sizeof($table_fields_array);$i++)
          {

            $data_obj_array[] = $order_d[$table_fields_array[$i]];
          }
      $data_array[] = $data_obj_array;
    }
        
        // create actual json
        $main = array();
        $main_obj['header'] = 'Coupons Report';
        $main_obj['columns'] = $table_fields_array;
        $main_obj['data'] = $data_array;
        $main_obj['footer'] = 'Footer Text Goes Here';
        $main[] = $main_obj;
           
          if(sizeof($main) > 0)
          {
              $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Coupons Report Fetched Successfully';
                          $data['data']      =   $main;  
            }
          else
          {
              $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Task Found';
                          $data['data']      =   [];  
          }
           return $data;
         
  }












//Route-23.8 ===========================================================================================================
     public function reports_reviews(Request $request  )
   {

    $per_page = $this->get_variable_per_page(); //ASC or DESC
    $orderby = $this->get_variable_orderby();
    $order = $this->get_variable_order();
    $search = $this->get_variable_search();
    $category = $this->get_variable_category();
    $tag = $this->get_variable_tag();
    $status = $this->get_variable_status();
    $store_id = $this->get_variable_store_id();
    $table_fields = $this->get_variable_table_fields();
    
    if($table_fields !='' && $table_fields != null)
    { $table_fields_array = explode(",",$table_fields); }
    else  { $table_fields_array = [];  }
        
    
    $model = new \App\Items;
    $model = $model::where('item_id' ,'<>', '0');  
   

    //user types filters ==========================================
    $auth_user_id = $this->get_auth_user_id();
    $user_type = $this->get_auth_user_type();
    if($user_type == '4') //store manager_id
    {
       $store_id = @\App\Store::where('manager_id',$auth_user_id)->first(['store_id'])->store_id;
       if($store_id == '' || $store_id == null) { $store_id = 'NA';}
    }
     if($user_type == '3') //vendor
    {   $model = $model->where('vendor_id' , $auth_user_id );   
    }
    //user type_filter check ends =================================

 
    if($status != '' && $status != null)
    {  $model = $model->where('item_active_status' , $status);  } 


    if($store_id != '' && $store_id != null)
    {  $model = $model->where('store_id' , $store_id);  } 
     
    if($search != '' && $search != null)
    {  $model = $model->where(function($q) use ($search) { $q->where( DB::raw("CONCAT(item_id,' ',item_title)"),'like', '%'.$search.'%'); });  }
      
    if($category != '' && $category != null)
    {   $model = $model->whereRaw("FIND_IN_SET('".$category."',items.item_categories)");  } 
  
    if($tag != '' && $tag != null)
    {   $model = $model->whereRaw("FIND_IN_SET('".$tag."',items.item_tags)");  }

 
 
 
    $date_from= $_GET['date_from'];
    $date_to = $_GET['date_to'];
    $date_from =\Carbon\Carbon::parse($date_from)->format('Y-m-d');
    $date_to =\Carbon\Carbon::parse($date_to)->format('Y-m-d');
    $model = $model->where('created_at','<',$date_to)->where('created_at','>',$date_from);
    $model = $model->orderBy($orderby,$order);  
    $result = $model->get();

   

   
    $data_array = array();
    foreach($result as $order_d)
    {
      $data_obj_array = array();
        for($i=0;$i<sizeof($table_fields_array);$i++)
          {

            $data_obj_array[] = $order_d[$table_fields_array[$i]];
          }
      $data_array[] = $data_obj_array;
    }
        
        // create actual json
        $main = array();
        $main_obj['header'] = 'Items Report';
        $main_obj['columns'] = $table_fields_array;
        $main_obj['data'] = $data_array;
        $main_obj['footer'] = 'Footer Text Goes Here';
        $main[] = $main_obj;
           
          if(sizeof($main) > 0)
          {
              $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Task Report Fetched Successfully';
                          $data['data']      =   $main;  
            }
          else
          {
              $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Task Found';
                          $data['data']      =   [];  
          }
           return $data;
         
  }
















//Route-23.9 ===========================================================================================================
     public function reports_loyalty_points(Request $request  )
   {

    $per_page = $this->get_variable_per_page(); //ASC or DESC
    $orderby = $this->get_variable_orderby();
    $order = $this->get_variable_order();
    $search = $this->get_variable_search();
    $category = $this->get_variable_category();
    $tag = $this->get_variable_tag();
    $status = $this->get_variable_status();
    $store_id = $this->get_variable_store_id();
    $table_fields = $this->get_variable_table_fields();
    
    if($table_fields !='' && $table_fields != null)
    { $table_fields_array = explode(",",$table_fields); }
    else  { $table_fields_array = [];  }
        
    
    $model = new \App\Items;
    $model = $model::where('item_id' ,'<>', '0');  
   

    //user types filters ==========================================
    $auth_user_id = $this->get_auth_user_id();
    $user_type = $this->get_auth_user_type();
    if($user_type == '4') //store manager_id
    {
       $store_id = @\App\Store::where('manager_id',$auth_user_id)->first(['store_id'])->store_id;
       if($store_id == '' || $store_id == null) { $store_id = 'NA';}
    }
     if($user_type == '3') //vendor
    {   $model = $model->where('vendor_id' , $auth_user_id );   
    }
    //user type_filter check ends =================================

 
    if($status != '' && $status != null)
    {  $model = $model->where('item_active_status' , $status);  } 


    if($store_id != '' && $store_id != null)
    {  $model = $model->where('store_id' , $store_id);  } 
     
    if($search != '' && $search != null)
    {  $model = $model->where(function($q) use ($search) { $q->where( DB::raw("CONCAT(item_id,' ',item_title)"),'like', '%'.$search.'%'); });  }
      
    if($category != '' && $category != null)
    {   $model = $model->whereRaw("FIND_IN_SET('".$category."',items.item_categories)");  } 
  
    if($tag != '' && $tag != null)
    {   $model = $model->whereRaw("FIND_IN_SET('".$tag."',items.item_tags)");  }

 
 
 
    $date_from= $_GET['date_from'];
    $date_to = $_GET['date_to'];
    $date_from =\Carbon\Carbon::parse($date_from)->format('Y-m-d');
    $date_to =\Carbon\Carbon::parse($date_to)->format('Y-m-d');
    $model = $model->where('created_at','<',$date_to)->where('created_at','>',$date_from);
    $model = $model->orderBy($orderby,$order);  
    $result = $model->get();

   

   
    $data_array = array();
    foreach($result as $order_d)
    {
      $data_obj_array = array();
        for($i=0;$i<sizeof($table_fields_array);$i++)
          {

            $data_obj_array[] = $order_d[$table_fields_array[$i]];
          }
      $data_array[] = $data_obj_array;
    }
        
        // create actual json
        $main = array();
        $main_obj['header'] = 'Items Report';
        $main_obj['columns'] = $table_fields_array;
        $main_obj['data'] = $data_array;
        $main_obj['footer'] = 'Footer Text Goes Here';
        $main[] = $main_obj;
           
          if(sizeof($main) > 0)
          {
              $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Task Report Fetched Successfully';
                          $data['data']      =   $main;  
            }
          else
          {
              $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Task Found';
                          $data['data']      =   [];  
          }
           return $data;
         
  }






















 
   
//==========================================================================misc functions===================================================================//   

public function get_variable_user_type()
{
	 if(isset($_GET['user_type']) && $_GET['user_type'] != null && $_GET['user_type'] != '')
					{ $user_type = $_GET['user_type']; }
					else 
					{ $user_type = ''; }
    return $user_type;
}	
 public function get_variable_request_type()
 {
 	   if(isset($_GET['request_type']) && $_GET['request_type'] != null && $_GET['request_type'] != '')
					{ $request_type = $_GET['request_type']; }
					else 
					{ $request_type = ''; }
       return $request_type;
 }
 
public function get_variable_search()
{
   if(isset($_GET['search']) && $_GET['search'] != null && $_GET['search'] != '')
          { $search = $_GET['search']; }
          else 
          { $search = ''; }
    return $search;
} 

public function get_variable_driver_id()
{
   if(isset($_GET['driver_id']) && $_GET['driver_id'] != null && $_GET['driver_id'] != '')
          { $var = $_GET['driver_id']; }
          else 
          { $var = ''; }
    return $var;
} 

    

   public function get_variable_include_expired()
{
   if(isset($_GET['include_expired']) && $_GET['include_expired'] != null && $_GET['include_expired'] != '')
          { $include_expired = $_GET['include_expired']; }
          else 
          { $include_expired = 'false'; }
    return $include_expired;
}

   public function get_variable_include_consumed()
{
   if(isset($_GET['include_consumed']) && $_GET['include_consumed'] != null && $_GET['include_consumed'] != '')
          { $include_consumed = $_GET['include_consumed']; }
          else 
          { $include_consumed = 'false'; }
    return $include_consumed;
}





   




   public function get_variable_expiry($request)
{
   if(  $request['expiry'] != null && $request['expiry'] != '')
          { $expiry = $request['expiry']; }
          else 
          { $expiry = \Carbon\Carbon::now()->addYears(19); }
    return $expiry;
}



   public function get_variable_valid_from($request)
{
   if(  $request['valid_from'] != null && $request['valid_from'] != '')
          { $valid_from = $request['valid_from']; }
          else 
          { $valid_from = \Carbon\Carbon::now(); }
    return $valid_from;
}

   public function get_variable_category()
{
	 if(isset($_GET['category']) && $_GET['category'] != null && $_GET['category'] != '')
					{ $category = $_GET['category']; }
					else 
					{ $category = ''; }
    return $category;
}	

   public function get_variable_tag()
{
	 if(isset($_GET['tag']) && $_GET['tag'] != null && $_GET['tag'] != '')
					{ $tag = $_GET['tag']; }
					else 
					{ $tag = ''; }
    return $tag;
}	

   public function get_variable_table_fields()
{
   if(isset($_GET['table_fields']) && $_GET['table_fields'] != null && $_GET['table_fields'] != '')
          { $table_fields = $_GET['table_fields']; }
          else 
          { $table_fields = ''; }
    return $table_fields;
}

public function get_variable_status()
{
   if(isset($_GET['status']) && $_GET['status'] != null && $_GET['status'] != '')
          { $var = $_GET['status']; }
          else 
          { $var = ''; }
    return $var;
} 

public function get_variable_task_type()
{
   if(isset($_GET['task_type']) && $_GET['task_type'] != null && $_GET['task_type'] != '')
          { $var = $_GET['task_type']; }
          else 
          { $var = ''; }
    return $var;
} 


//check item existence by id
public function item_exist($id)
{
	$count = @\App\Items::where('item_id',$id)->count();
	if($count < 1) {
		return 0;
	}
	else{
		return 1;
	}
}	

 public function get_variable_orderby()
{
	 if(isset($_GET['orderby']) && $_GET['orderby'] != null && $_GET['orderby'] != '')
					{ $orderby = $_GET['orderby']; }
					else 
					{ $orderby = 'created_at'; }
    return $orderby;
}

//validate all the request variables if they are null or empty , it will return values
public function validate_datetime($var)
{
	 if(!isset($var) or $var == null or $var == '' or $var == ' ')
	 {
	 	 $date = @\Carbon\Carbon::now();
	 	 $date = @\Carbon\Carbon::parse($date);
		 return $this->add_days($date , '7' , 'Y-m-d h:i:s');
	 }
	 else{
		 return $var;
	 }
}


 
  
 
  
  public function get_item_meta_type_id($identifer)
  {
	  $item_meta_type_id = @\App\ItemMetaType::where('identifier',$identifer)->first(['item_meta_type_id'])->item_meta_type_id;
	  return $item_meta_type_id;
  }
  



///================================ function to check GET variable's and Defaults ====================================================//
public function get_variable_per_page()
{
	 if(isset($_GET['per_page']) && $_GET['per_page'] != null && $_GET['per_page'] != '')
					{ $per_page = $_GET['per_page']; }
					else 
					{ $per_page = 20; }
    return $per_page;
}

 

public function get_variable_order()
{
	 if(isset($_GET['order']) && $_GET['order'] != null && $_GET['order'] != '')
					{ $order = $_GET['order']; }
					else 
					{ $order = 'DESC'; }
    return $order;
}
 




public function get_variable_store_id()
{
   if(isset($_GET['store_id']) && $_GET['store_id'] != null && $_GET['store_id'] != '')
          { $store_id = $_GET['store_id']; }
          else 
          { $store_id = ''; }
    return $store_id;
} 
public function get_variable_user_id()
{
   if(isset($_GET['user_id']) && $_GET['user_id'] != null && $_GET['user_id'] != '')
          { $user_id = $_GET['user_id']; }
          else 
          { $user_id = ''; }
    return $user_id;
} 
public function get_variable_order_id()
{
   if(isset($_GET['order_id']) && $_GET['order_id'] != null && $_GET['order_id'] != '')
          { $order_id = $_GET['order_id']; }
          else 
          { $order_id = ''; }
    return $order_id;
} 
 
      
  

   public function get_variable_fields()
{
   if(isset($_GET['table_fields']) && $_GET['table_fields'] != null && $_GET['table_fields'] != '')
          { $table_fields = $_GET['table_fields']; }
          else 
          { $table_fields = ''; }
    return $table_fields;
}



 



  
 


 
 
 ///================================ function to check GET variable's and Defaults Ends ====================================================//
 
 

    
 
   public function make_thumb($src, $dest, $desired_width) 
   {

    /* read the source image */
    $source_image = imagecreatefromjpeg($src);
    $width = imagesx($source_image);
    $height = imagesy($source_image);
    
    /* find the "desired height" of this thumbnail, relative to the desired width  */
    $desired_height = floor($height * ($desired_width / $width));
    
    /* create a new, "virtual" image */
    $virtual_image = imagecreatetruecolor($desired_width, $desired_height);
    
    /* copy source image at a resized size */
    imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
    
    /* create the physical thumbnail image to its destination */
    imagejpeg($virtual_image, $dest);
    }

    /**
     * @author Dikshant
     * set user language by default
     */
 
 
 


}