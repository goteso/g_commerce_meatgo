<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FavouriteStore extends Model
{
        protected $fillable = [ 'id' , 'user_id', 'store_id' ];
		protected $table = 'favourite_store';
		
 
 
protected $casts = [ 'id' => 'int' , 'user_id' => 'int' , 'store_id' => 'float'  ];

	
        
 public function getCreatedAtFormattedAttribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
    }

 public function getCreatedAtFormatted2Attribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->format('M d, Y');
    }



     public function getStoreDetailsAttribute($value) {
         return  @\App\Stores::where('store_id' , $this->store_id)->get(['store_id','store_title','store_photo','store_thumb_photo','store_rating','address','manager_id','vendor_id']);
    }



 
	
 
 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }
	
	
}





